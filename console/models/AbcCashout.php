<?php

namespace console\models;

use Yii;

/**
 * This is the model class for table "abc_cashout".
 *
 * @property integer $id
 * @property string $key_cashout
 * @property double $cout
 * @property double $percent_marchand
 * @property integer $status
 */
class AbcCashout extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'abc_cashout';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['key_cashout', 'cout', 'percent_marchand', 'status'], 'required'],
            [['cout', 'percent_marchand'], 'number'],
            [['status'], 'integer'],
            [['key_cashout'], 'string', 'max' => 35],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'key_cashout' => 'Key Cashout',
            'cout' => 'Cout',
            'percent_marchand' => 'Percent Marchand',
            'status' => 'Status',
        ];
    }
}
