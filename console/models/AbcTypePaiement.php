<?php

namespace console\models;

use Yii;

/**
 * This is the model class for table "abc_type_paiement".
 *
 * @property integer $id
 * @property string $key_type
 * @property string $ref
 * @property string $libelle
 * @property integer $status
 *
 * @property AbcRetrait[] $abcRetraits
 */
class AbcTypePaiement extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'abc_type_paiement';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['key_type', 'ref', 'libelle', 'status'], 'required'],
            [['status'], 'integer'],
            [['key_type'], 'string', 'max' => 35],
            [['ref'], 'string', 'max' => 5],
            [['libelle'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'key_type' => 'Key Type',
            'ref' => 'Ref',
            'libelle' => 'Libelle',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAbcRetraits()
    {
        return $this->hasMany(AbcRetrait::className(), ['type_envoiepaiement' => 'id']);
    }
}
