<?php

namespace console\models;

use Yii;

/**
 * This is the model class for table "menu_ussd".
 *
 * @property integer $id_menu_ussd
 * @property integer $position_menu_ussd
 * @property integer $sub_menu
 * @property string $user_access
 * @property string $denomination
 * @property string $description
 * @property integer $status
 * @property string $date_create
 */
class MenuUssd extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'menu_ussd';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['position_menu_ussd', 'user_access', 'denomination', 'description', 'date_create'], 'required'],
            [['sub_menu'], 'integer'],
            [['description'], 'string'],
            [['date_create'], 'safe'],
            [['position_menu_ussd'], 'string', 'max' => 3],
            [['user_access'], 'string', 'max' => 255],
            [['denomination'], 'string', 'max' => 50],
            [['status'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_menu_ussd' => 'Id Menu Ussd',
            'position_menu_ussd' => 'Position Menu Ussd',
            'sub_menu' => 'Sub Menu',
            'user_access' => 'User Access',
            'denomination' => 'Denomination',
            'description' => 'Description',
            'status' => 'Status',
            'date_create' => 'Date Create',
        ];
    }
}
