<?php

namespace console\models;

use Yii;

/**
 * This is the model class for table "secteur_activite".
 *
 * @property integer $idsecteur_activite
 * @property string $key_secteur_activite
 * @property string $denomination_secteur
 * @property integer $position_menu_ussd
 * @property integer $etat
 * @property string $date_create
 * @property integer $created_by
 * @property string $date_update
 * @property integer $updated_by
 */
class SecteurActivite extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'secteur_activite';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['key_secteur_activite', 'position_menu_ussd', 'etat', 'created_by'], 'required'],
            [['position_menu_ussd', 'etat', 'created_by', 'updated_by'], 'integer'],
            [['date_create', 'date_update'], 'safe'],
            [['key_secteur_activite', 'denomination_secteur'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idsecteur_activite' => 'Idsecteur Activite',
            'key_secteur_activite' => 'Key Secteur Activite',
            'denomination_secteur' => 'Denomination Secteur',
            'position_menu_ussd' => 'Position Menu Ussd',
            'etat' => 'Etat',
            'date_create' => 'Date Create',
            'created_by' => 'Created By',
            'date_update' => 'Date Update',
            'updated_by' => 'Updated By',
        ];
    }
}
