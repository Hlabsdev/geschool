<?php

namespace console\models;

use Yii;

/**
 * This is the model class for table "abc_historique".
 *
 * @property integer $id
 * @property string $key_historique
 * @property string $mois_operation
 * @property integer $montant
 * @property integer $montant_operation
 * @property integer $montant_restant
 * @property integer $id_souscription
 * @property string $date_create
 * @property string $date_update
 */
class AbcHistorique extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'abc_historique';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['key_historique', 'mois_operation', 'montant', 'montant_operation', 'montant_restant', 'id_souscription', 'date_create'], 'required'],
            [['montant', 'montant_operation', 'montant_restant', 'id_souscription'], 'integer'],
            [['date_create', 'date_update'], 'safe'],
            [['key_historique'], 'string', 'max' => 35],
            [['mois_operation'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'key_historique' => 'Key Historique',
            'mois_operation' => 'Mois Operation',
            'montant' => 'Montant',
            'montant_operation' => 'Montant Operation',
            'montant_restant' => 'Montant Restant',
            'id_souscription' => 'Id Souscription',
            'date_create' => 'Date Create',
            'date_update' => 'Date Update',
        ];
    }
}
