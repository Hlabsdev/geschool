<?php

namespace console\models;

use Yii;

/**
 * This is the model class for table "mproduits".
 *
 * @property integer $idProduit
 * @property string $key_produit
 * @property integer $id_acteur
 * @property string $denomination
 * @property string $unite
 * @property integer $prix_vente
 * @property integer poids_unitaire
 * @property string $qte
 * @property integer $position_menu_ussd
 * @property integer $etat
 * @property string $date_create
 * @property integer $created_by
 * @property string $date_update
 * @property integer $updated_by
 *
 * @property ActeurUser $idActeur
 */
class Mproduits extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mproduits';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['key_produit', 'id_acteur', 'unite', 'position_menu_ussd', 'etat', 'created_by'], 'required'],
            [['id_acteur', 'prix_vente', 'position_menu_ussd', 'etat', 'created_by', 'updated_by'], 'integer'],
            [['date_create', 'date_update'], 'safe'],
            [['key_produit', 'denomination'], 'string', 'max' => 255],
            [['unite'], 'string', 'max' => 50],
            [['qte'], 'string', 'max' => 10],
            [['id_acteur'], 'exist', 'skipOnError' => true, 'targetClass' => ActeurUser::className(), 'targetAttribute' => ['id_acteur' => 'id_acteur_user']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idProduit' => 'Id Produit',
            'key_produit' => 'Key Produit',
            'id_acteur' => 'Id Acteur',
            'denomination' => 'Denomination',
            'unite' => 'Unite',
            'prix_vente' => 'Prix Vente',
            'qte' => 'Qte',
            'position_menu_ussd' => 'Position Menu Ussd',
            'etat' => 'Etat',
            'date_create' => 'Date Create',
            'created_by' => 'Created By',
            'date_update' => 'Date Update',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdActeur()
    {
        return $this->hasOne(ActeurUser::className(), ['id_acteur_user' => 'id_acteur']);
    }
}
