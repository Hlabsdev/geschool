<?php

namespace console\models;

use Yii;

/**
 * This is the model class for table "acteur_user".
 *
 * @property integer $id_acteur_user
 * @property string $acteur_key
 * @property string $code_reference
 * @property integer $id_user
 * @property integer $idtransaction
 * @property integer $type_acteur
 * @property string $denomination
 * @property string $address_acteur
 * @property string $description_activite
 * @property string $secteur_activite
 * @property string $lat_acteur
 * @property string $long_acteur
 * @property string $other_number
 * @property string $photo_acteur
 * @property integer $etat
 * @property string $date_create
 * @property string $date_update
 * @property integer $updated_by
 * @property integer $etat_conformite
 * @property string $raison_reject
 * @property double $pourcentage
 * @property double $distance
 *
 * @property User $idUser
 * @property CommandeMarchand[] $commandeMarchands
 * @property Mproduits[] $mproduits
 */
class ActeurUser extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'acteur_user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['acteur_key', 'id_user', 'type_acteur', 'denomination', 'address_acteur', 'lat_acteur', 'long_acteur', 'etat'], 'required'],
            [['id_user', 'idtransaction', 'type_acteur', 'etat', 'updated_by'], 'integer'],
            [['date_create', 'date_update'], 'safe'],
            [['raison_reject'], 'string'],
            [['distance'], 'number'],
            [['acteur_key', 'other_number', 'photo_acteur'], 'string', 'max' => 50],
            [['code_reference'], 'string', 'max' => 15],
            [['denomination', 'address_acteur', 'lat_acteur', 'long_acteur'], 'string', 'max' => 255],
            [['description_activite'], 'string', 'max' => 200],
            [['secteur_activite'], 'string', 'max' => 250],
            [['id_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_user' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_acteur_user' => 'Id Acteur User',
            'acteur_key' => 'Acteur Key',
            'code_reference' => 'Code Reference',
            'id_user' => 'Id User',
            'idtransaction' => 'Idtransaction',
            'type_acteur' => 'Type Acteur',
            'denomination' => 'Denomination',
            'address_acteur' => 'Address Acteur',
            'description_activite' => 'Description Activite',
            'secteur_activite' => 'Secteur Activite',
            'lat_acteur' => 'Lat Acteur',
            'long_acteur' => 'Long Acteur',
            'other_number' => 'Other Number',
            'photo_acteur' => 'Photo Acteur',
            'etat' => 'Etat',
            'date_create' => 'Date Create',
            'date_update' => 'Date Update',
            'updated_by' => 'Updated By',
            'raison_reject' => 'Raison Reject',
            'distance' => 'Distance',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdUser()
    {
        return $this->hasOne(User::className(), ['id' => 'id_user']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCommandeMarchands()
    {
        return $this->hasMany(CommandeMarchand::className(), ['id_marchand' => 'id_acteur_user']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMproduits()
    {
        return $this->hasMany(Mproduits::className(), ['id_acteur' => 'id_acteur_user']);
    }
}
