<?php

namespace console\controllers;
use yii\httpclient\Client;
use Yii;
use console\models\AbcRetraitmobile;

class Common
{
	
	//const base_url="https://testbed.datawise.site";
	//const base_url="http://localhost/manage_clin/go_agri";
	
	const base_url="http://abusiness.store";
	 
	public static function save_cashout($find_retrait) {
		
		$retraitmobile=new AbcRetraitmobile ;
		$retraitmobile->key_retraitmobile=Yii::$app->security->generateRandomString(32);
		$retraitmobile->numero_paiement=(string)$find_retrait->numero_beneficiare;
		$retraitmobile->montant=(int)$find_retrait->montant_retrait;
		$retraitmobile->id_retrait=(int)$find_retrait->id;
		$retraitmobile->idclient=(int)$find_retrait->idclient;
		$retraitmobile->etat=0;
		$retraitmobile->date_create=date("Y-m-d H:i:s");
		$retraitmobile->date_update=date("Y-m-d H:i:s");
		$retraitmobile->date_paiement=date("Y-m-d H:i:s");
		$retraitmobile->ref_transaction="ABC".$find_retrait->id.rand(10000,99999);
		$retraitmobile->save();
	}
	
	public static function get_cgu() {
		
		$all_information =Yii::t('app', 'cgu_entete');
		$all_information.=Yii::t('app', 'cgu_content1');
		$all_information.=Yii::t('app', 'cgu_content2');
		$all_information.=Yii::t('app', 'cgu_content3');
		$all_information.=Yii::t('app', 'cgu_bas');
		
		return $all_information; 
	}
	
	public static function hit_sms($message,$to,$sender) {
   
		set_time_limit(0);
		$text = urlencode($message);
		
        $url =Common::base_url."/api/web/web_v1/ussds/send_sms?receiver=".$to."&content=".$text."&sender=".$sender;
		
		$curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_COOKIESESSION, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $return = curl_exec($curl);        
        curl_close($curl);
		return $return ;
		
	}
	
	
}


