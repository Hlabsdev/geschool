<?php

namespace console\controllers;
use yii\httpclient\Client;
use Yii;

class Cgu_content_en
{
	const cgu_entete0="<b>General condition of use</b>";
	
	const cgu_entete="<b>PERSONAL DATA</b>
	By using our services, you want to share with us and other users personal information such as your geographic coordinates, your contact. The data used in the implementation of this mission";
	
	const cgu_content1="<b>TO THE CUSTOMER</b>
	You agree to pay all costs associated with any purchase transaction plus transaction fees applicable by mobile operators. For any missed delivery, you agree to pay the delivery charges for a new passage.";
	
	const cgu_content2="<b>FOR THE DELIVERY</b>
	He is required to collect orders from merchants and deliver them to the location indicated by the customer while maintaining the integrity of the product. Any product whose condition has been degraded will be at its expense.
	The delivery person has an obligation to go to the end of the delivery once he takes charge of an order. Any order overdue, delivered too late or under conditions of non-compliance with customers can range from temporary deactivation to total exclusion from the system. We reserve the right to rate, accept, certify or ban any inefficient delivery person temporarily or permanently from the entire system.";
	
	const cgu_content3="<b>FOR THE MERCHANT</b>
	The merchant agrees to the payment of a commission on the amount of the sale defined at the validation of his account. This commission is subject to modification with 24 hours notice.
	The funds are paid directly to the merchant's electronic wallet or by any other authentic means.
	Funds equivalent to the price including VAT of the product with any deducted commission are only paid if the customer confirms and validates receipt of his order in good condition.
	If the product is refused for non-compliance, a dispute is opened to find a solution with the support of our services within 7 days of the opening of the dispute. Any possible costs related to this dispute are the responsibility of the merchant who agrees to bear them on the amount of his sales through the platform.
	The merchant must respect the ethical rules to avoid litigation and to have a zero litigation counter at all times.
	However, any merchant who has totaled more than 3 disputes over a month is automatically discredited and banned from the system without notice.";
	
	const cgu_bas="<b>ABUSINESS COMMITMENT</b>
	ABUSINESS undertakes to provide all the identifiers necessary for access to the platform as well as assistance and support if necessary by email, telephone or any other means authentic. He agrees to pay the supplier 24 hours at the latest after receipt of the approved order approved by the customer. He undertakes to settle any dispute arising from an order by playing the arbitrator by way of conciliation or failing to refer the protagonists to a competent court in the event of non-compromise. He undertakes to correct any system failure quickly and within the regulatory deadlines, including system and functional updates. It can in no case be held guilty of any failure, or unavailability of the system that could lead to the unavailability of the system to a customer who considers having a loss. In any case, no compensation to customers by ABusiness is provided.

<b>LITIGATION</b>
In the event of a dispute, the parties undertake to settle their dispute by arbitration or failing that by the competent jurisdiction of the court of Lomé.";
	
	
	
	
}


