<?php

namespace console\controllers;
use yii\httpclient\Client;
use Yii;

class Cgu_content_fr
{
	const cgu_entete0="<b>Conditions générales d’utilisation au 11/09/2020 </b>";
	
	const cgu_entete="<b>DONNÉES PERSONNELLES</b>
En utilisant nos services, vous acceptez de partager avec nous et les autres utilisateurs des informations personnelles telles que vos coordonnées géographiques, votre contact. Les données utilisées dans la mise en oeuvre de la présente mission";
	
	const cgu_content1="<b>POUR LE CLIENT</b>
Le client accepte payer la totalité des frais liés à toute opération d’achat majorée des frais de transactions applicables ou des forfaits d'utilisation notifiés 
dans le récapitulatif de votre commande avant paiement. Toute commande validée suppose l'acceptation de tout frais.
Pour toute livraison manquée, vous acceptez de payer les frais de livraison pour un nouveau passage du livreur ou de passer retirer votre produit
 au siège de ABusiness. Tout produit non retiré dans les 5 jours ne saurait faire objet de reclammation si son etat se dégrade ou est perdu. 
 ABusiness n'a pas vocation à stocker les produits en magasin
 En cas de qualité douteuse d'un produit le client est tenu de retourner le produit par le même circuit dans un delai de 24h maximum.
 ";
	
	const cgu_content2="<b>POUR LE LIVREUR</b>
Le livreur est tenu de récupérer les commandes auprès des marchands et de les livrer à l’endroit indiqué par le client tout en conservant l’intégrité du produit. 
Tout produit dont l’état aurait été dégradé serait à sa charge.
Le livreur a obligation d’aller jusqu’au bout de la livraison une fois qu’il prend en charge une commande. 
Toute commande en souffrance de livraison, livrée trop en retard ou dans des conditions de non respect des clients peut aller de la désactivation temporaire 
jusqu'à l’exclusion totale du système. Nous nous réservons le droit de noter, d’accepter , de certifier ou de bannir tout livreur non efficace temporairement 
ou définitivement de tout le système.";
	
	const cgu_content3="<b>POUR LE MARCHAND</b>
Le marchand n'a aucune commission à payer sur les ventes réalisées, à la création et à la validation de son compte.Toutefois il est limité à 5 produits à publier sur notre plateforme.
Si il désire lever la limite du nombre de produits, il devra payer 12000FCFA / AN pour chaque 25 nouveaux produits supplémentaires.
Les fonds sont versés directement sur le portefeuille électronique du marchand ou par tout autre moyen distant faisant foi après confirmation du livreur de la disponibilité et de l'etat de bonne qualité du produit confirmée sur place avant retrait.

Les fonds équivalents au prix TTC du produit sans commission déduite ne sont versés  que si le livreur confirme et valide la réception du produit en bon état à la boutique.
En cas de refus du produit par le client pour non conformité un litige est ouvert pour trouver une solution avec l’appui de nos services dans les 7 jours suivant l'ouverture du litige. Tout frais éventuel lié à ce litige est à la charge du marchand qui accepte de les supporter sur le montant de ses ventes au travers de la plateforme.
Le marchand doit respecter les règles éthiques permettant d'éviter les litiges et d'avoir un compteur zéro litige en tout temps.
En utilisant nos services il accepte le retour produit dans les 72h et le remplacement des produits si la qualité du produit est compromise et mise en cause par le client.
Toutefois tout marchand ayant totalisé plus de 3 litiges sur un mois est automatiquement discrédité et banni du système sans préavis.";
	
	const cgu_bas="<b>ENGAGEMENT DE ABUSINESS</b>
ABUSINESS s’engage à fournir tous les identifiants nécessaires à l’ accès à la plateforme ainsi que l’assistance et le support en cas de besoin par mail, téléphone ou tout autre moyen faisant foi. Il s’engage à payer le fournisseur 24h au plus tard après la réception de la commande approuvée conforme par le client. Il s’engage à régler tout litige émanant d’une commande en jouant à l’arbitre par voie de conciliation ou à défaut de référer les protagonistes à une juridiction compétente en cas de non compromission. Il s'engage à corriger toute panne système avec célérité et dans les délais réglementaires y compris les mises à jour système. 
Il ne peut en aucun cas être tenu coupable d'une quelconque panne, ou indisponibilité système pouvant entraîner l'indisponibilité du système à un client qui estimerait avoir une perte. Dans tous les cas aucune indemnisation aux clients, aux marchands par ABusiness  n'est prévue dans tous les cas.Il ne peut être tenu reponsable d'un dommage qu'un client estimerait avoir pour faute de pannes ou d'indisponibilité système.

<b>RÈGLEMENT DES LITIGES</b>
En cas de différend, les parties s’engagent à régler leur conflit par voie d’arbitrage ou à défaut par la juridiction compétente du tribunal de Lomé.";
	
	
	
	
}


