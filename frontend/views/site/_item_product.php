<?php
$output  = json_encode($model);
$output_1 = str_replace("'", "***", $output);
$final_output = str_replace('"', "'", $output_1);
?>
<div class="" style="border: 1px solid #17A138; border-radius: 0 0 15px 15px; margin-bottom: 15px;">
    <div class="product-item">
        <div class="pi-pic">
            <img style="cursor:pointer" src="<?= $model->photo_produit ?>" height="300" width="1000" alt="<?= $model->denomination ?>" onclick="data(<?php echo $final_output ?>)">
            <div class="icon">
                <i class="icon_heart_alt"></i>
            </div>
            <ul class="whatsapp">
                <li class="w-icon active"><a href="https://wa.me/22891047373?text=<?= $model->referenceProduit ?>" target="_blank"><i class="fab fa-whatsapp" aria-hidden="true"></i></a></li>
                <li class="quick-view"><a href="https://wa.me/22891047373?text=<?= $model->referenceProduit ?>" target="_blank"> <?= Yii::t('app', 'l_buy_whatsapp'); ?></a></li>
            </ul>
            <ul class="telegram">
                <li class="w-icon active"><a href="https://t.me/Abtogo_bot?start=<?= $model->referenceProduit ?>" target="_blank"><i class="fab fa-telegram" aria-hidden="true"></i></a></li>
                <li class="quick-view"><a href="https://t.me/Abtogo_bot?start=<?= $model->referenceProduit ?>" target="_blank"> <?= Yii::t('app', 'l_buy_telegram'); ?></a></li>
            </ul>
        </div>
        <div class="pi-text">
            <div class="catagory-name" style="font-size: 15px; display:inline-flexbox"> <?= Yii::t('app', 'l_product_ref'); ?> : <?php echo "<span style='color:black;'>" . $model->poids_unitaire . "</span>" ?></div>
            <h5><?= $model->denomination ?></h5>
            <div class="product-price">
                <?php echo $model->qte . '(e) ' . $model->unite . ' à ' . number_format($model->prix_vente, '0', '.', ' ') . '  F CFA' ?>
            </div>
            <div class="row">
                <div class="row" style="margin: auto;">
                    <a style="margin-right: 2px;margin-left: 5px;" data-toggle="modal" onclick="data(<?php echo $final_output ?>)" href="#" class="btn btn-success btn-sm"><i class="fas fa-eye"></i> <?= Yii::t('app', 'l_details_btn'); ?>
                        <a style="margin-right: 5px;" href="tel:+22892929285" class="btn btn-outline-success btn-sm"><i class="fas fa-phone"></i> (228) 92929285</a>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"><?= Yii::t('app', 'l_details'); ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table style="width:100%" class="table table-bordered">
                    <tr>
                        <td colspan="2" id="product_picture"></td>
                    </tr>
                    <tr>
                        <th><?= Yii::t('app', 'l_product_ref'); ?></th>
                        <td id="poids_unitaire"></td>
                    </tr>
                    <tr>
                        <th><?= Yii::t('app', 'l_product_name'); ?></th>
                        <td id="denomination"></td>
                    </tr>
                    <tr>
                        <th><?= Yii::t('app', 'l_product_desc'); ?></th>
                        <td id="description_prod"></td>
                    </tr>
                    <tr>
                        <th><?= Yii::t('app', 'l_product_price'); ?></th>
                        <td id="prix_vente"></td>
                    </tr>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" data-dismiss="modal"><?= Yii::t('app', 'l_close'); ?></button>
            </div>
        </div>
    </div>
</div>

<script>
    function data(er) {
        var data = JSON.stringify(er);
        var model = data.replaceAll('***', "\'");
        var final_data = JSON.parse(model);
        // console.log(final_data);
        $('#product_picture').html('<img src="' + final_data.photo_produit + '" alt="' + final_data.denomination + '">');
        $('#poids_unitaire').html(final_data.poids_unitaire);
        $('#denomination').html(final_data.denomination);
        $('#description_prod').html(final_data.descriptionProduit);
        $('#prix_vente').html(final_data.qte + '(e) ' + final_data.unite + ' à ' + new Intl.NumberFormat().format(final_data.prix_vente) + ' F CFA');
        $('#exampleModal').modal('show');
    }
</script>