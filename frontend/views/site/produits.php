<?php

use yii\widgets\ListView;
use yii\widgets\LinkPager;

$this->title = Yii::t('app', 'nos_produits');
?>

<link rel="stylesheet" href="fashi/css/site.css">
<link rel="stylesheet" href="fashi/css/bootstrap.min.css">
<link rel="stylesheet" href="fashi/css/font-awesome.min.css">
<link rel="stylesheet" href="fashi/css/themify-icons.css">
<link rel="stylesheet" href="fashi/css/owl.carousel.min.css">
<link rel="stylesheet" href="fashi/css/nice-select.css">
<link rel="stylesheet" href="fashi/css/jquery-ui.min.css">
<link rel="stylesheet" href="fashi/css/slicknav.min.css">
<link rel="stylesheet" href="fashi/css/style.css">
<link rel="stylesheet" href="fashi/css/style_ab.css">
<style>
    .pagination {
        margin-right: auto;
    }

    .pagination li {
        padding: 3px 9px;
        border-radius: 4px;
        background-color: rgba(22, 158, 55, 0.3);
        margin: 2px;
    }

    .pagination li a {
        color: #fff;
    }

    .pagination li.active {
        background-color: #169E37;
    }

    .pagination li:hover {
        background-color: #169E37;
    }

    #my-listview-id {
        padding: 0;
        margin: 0;
        list-style: none;
        /* border: 1px solid silver; */
        -ms-box-orient: horizontal;
        display: -webkit-box;
        display: -moz-box;
        display: -ms-flexbox;
        display: -moz-flex;
        display: -webkit-flex;
        display: flex;

        -webkit-flex-wrap: wrap;
        flex-wrap: wrap;
    }

    /* .item {
        border: 1px solid #17A138;
        border-radius: 15px;
        margin-bottom: 15px;
        margin-right: 0px;
        padding-right: 0; 
    } */
</style>

<div style="height:220px;background-color: #49BE43;">
    <div class="container">
        <div class="row">
            <div class="col-lg-3">
                <div style="margin-top: 108px;">
                    <h3 style="color:#fff;font-weight:500"><?= Yii::t('app', 'nos_produits') ?></h3>
                    <div style="margin-top:-15px;">
                        <a href="https://wa.me/22892929285" target="_blank" class="btn btn-secondary"><i class="fab fa-whatsapp" aria-hidden="true"></i> <?= Yii::t('app', 'contact_sc') ?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Product Shop Section Begin -->
<section class="product-shop spad">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 order-1 order-lg-2">
                <div class="product-show-option">
                    <div class="row">
                        <div class="col-lg-12 col-md-7">
                            <div class="select-option" style="float: right;">
                                <form action="" method="POST">
                                    <?php
                                    $session = Yii::$app->session;
                                    ?>
                                    <input style="border: 2px solid #49BE43; height:40px; width:247px; " type="text" name="search_product_param" placeholder="<?= Yii::t('app', 'search_product') ?>" <?php echo "value=" . $session->get('product_name') ?>>
                                    <input type="submit" value="<?= Yii::t('app', 'search') ?>" class="btn btn-success">
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="product-list">
                    <div class="row">
                        <?php if (isset($none_product) && ($none_product != "")) { ?>

                            <h3><?= $none_product ?></h3>

                        <?php } else if (isset($all_products)) {

                            echo ListView::widget([
                                'id' => 'my-listview-id',
                                'dataProvider' => $all_products,
                                'itemOptions' => ['class' => 'col-lg-4 col-sm-6 item'],
                                'itemView' => '_item_product',
                                'layout' => '{items}',
                                // 'options' => [
                                //     'tag' => 'div',
                                //     'class' => 'list-wrapper',
                                //     'id' => 'list-wrapper',
                                // ],
                            ]);
                        } else { ?>
                            <h3><?= Yii::t('app', 'errora'); ?></h3>
                        <?php } ?>
                    </div>
                    <div class="row">
                        <div style="margin-right: auto; margin-left: auto;">
                            <?php
                            echo LinkPager::widget(['pagination' => $all_products->getPagination()]);
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Product Shop Section End -->

<script src="fashi/js/jquery-3.3.1.min.js"></script>
<script src="fashi/js/bootstrap.min.js"></script>
<script src="fashi/js/jquery-ui.min.js"></script>
<script src="fashi/js/jquery.countdown.min.js"></script>
<script src="fashi/js/jquery.nice-select.min.js"></script>
<script src="fashi/js/jquery.zoom.min.js"></script>
<script src="fashi/js/jquery.dd.min.js"></script>
<script src="fashi/js/jquery.zoom.min.js"></script>
<script src="fashi/js/jquery.slicknav.js"></script>
<script src="fashi/js/owl.carousel.min.js"></script>
<script src="fashi/js/main.js"></script>