<!-- <link href='https://fonts.googleapis.com/css?family=Raleway:400,200' rel='stylesheet' type='text/css'>  -->
  
<!-- <link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet"> -->
<nav class="social">
    <ul>
        <li>
            <a href="#services" class="scrollto"> <?= Yii::t('app', 'l_chatbot_access'); ?> 
                <img src="abusiness/img/whatsapp.png" alt="">
                <img src="abusiness/img/telegram.png" alt="">
            </a>
        </li>
    </ul>
</nav>

<style>
    nav ul li a {
        color: #fff;
        text-decoration: none;
    }
    .me {
        width: 400px;
        margin: 90px auto;
    }
    .me p,
    .me h1 {
        text-transform: uppercase;
        letter-spacing: 3px;
        text-align: center;
    }
    .me p {
        font-weight: 200;
    }
    .me span {
        font-weight: bold;
    }
    .social img{
        height: 30px;
        margin-left: 5px;
    }
    .social {
        position: fixed;
        /* top: 150px; */
        z-index: 10000;
    }

    @media (max-width: 768px) {
        .social {
            top: 280px;
        }
    }
    .social ul {
        padding: 0px;
        -webkit-transform: translate(-315px, 0);
        -moz-transform: translate(-315px, 0);
        -ms-transform: translate(-315px, 0);
        -o-transform: translate(-315px, 0);
        transform: translate(-315px, 0);
    }
    .social ul li {
        display: block;
        margin: 5px;
        background: rgba(0, 0, 0, 0.36);
        width: 390px;
        text-align: right;
        padding: 10px;
        -webkit-border-radius: 0 30px 30px 0;
        -moz-border-radius: 0 30px 30px 0;
        border-radius: 0 30px 30px 0;
        -webkit-transition: all 1s;
        -moz-transition: all 1s;
        -ms-transition: all 1s;
        -o-transition: all 1s;
        transition: all 1s;
    }
    .social ul li:hover {
        -webkit-transform: translate(270px, 0);
        -moz-transform: translate(270px, 0);
        -ms-transform: translate(270px, 0);
        -o-transform: translate(270px, 0);
        transform: translate(270px, 0);
        background: rgba(255, 255, 255, 0.4);
    }
    .social ul li:hover a {
        color: #000;
    }
    .social ul li:hover i {
        color: #fff;
        background: red;
        -webkit-transform: rotate(360deg);
        -moz-transform: rotate(360deg);
        -ms-transform: rotate(360deg);
        -o-transform: rotate(360deg);
        transform: rotate(360deg);
        -webkit-transition: all 1s;
        -moz-transition: all 1s;
        -ms-transition: all 1s;
        -o-transition: all 1s;
        transition: all 1s;
    }
    .social ul li i {
        margin-left: 10px;
        color: #000;
        background: #fff;
        padding: 10px;
        -webkit-border-radius: 50%;
        -moz-border-radius: 50%;
        border-radius: 50%;
        width: 20px;
        height: 20px;
        font-size: 20px;
        background: #ffffff;
        -webkit-transform: rotate(0deg);
        -moz-transform: rotate(0deg);
        -ms-transform: rotate(0deg);
        -o-transform: rotate(0deg);
        transform: rotate(0deg);
    }

</style>