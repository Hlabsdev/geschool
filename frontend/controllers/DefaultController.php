<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use api\modules\website\v1\controllers\Common;

use frontend\models\CommandeMarchand;
use frontend\models\CommandePayment;

class DefaultController extends Controller
{	


	public function actionPay_invoice()
    {
        $this->layout = "main_pay";
		if(isset($_GET['key']) && trim($_GET['key'])!=""){
			$commande_key=trim($_GET['key']);
			
			//verifier si cette commande est en attente de paiement
			$find_command = CommandeMarchand::find()->where(['commande_key'=>$commande_key,'etat'=>0])->one();
			if($find_command!==null){				
				return $this->render('pay',['find_command'=>$find_command]);
			}else{
				 return $this->redirect(['abusiness/']);
			}
		}else{
			 return $this->redirect(['abusiness/']);
		}
    }

	public function actionPay_commande()
    {
       
		$all_post=Yii::$app->request->get();	
		if(isset($all_post["access"]) && Common::api_ppasse==$all_post["access"]){
			
			if(isset($all_post["key"])){		
				
				if(trim($all_post["key"])!=""){	
				
					$commande_key=trim($all_post["key"]);
					//recuperer le token de la commande
					$find_command = CommandeMarchand::find()->where(['commande_key'=>$commande_key,'etat'=>0])->one();
					if($find_command!==null){
						$id_commande_marchand=$find_command->id_commande_marchand;
						$info_user=$find_command->idUser;
						$id_user=$find_command->id_user;
						$montant_paiement=$find_command->prix_produits+$find_command->prix_livraison;
						$charge_paiement=$find_command->prix_service;
						
						
						$continue_process=true;
						//verifier si ce paiement a ete deja enregistrer
						$new_paiement = CommandePayment::find()->where(['montant_paiement'=>$montant_paiement,'id_commande_marchand'=>$id_commande_marchand,'id_user'=>$id_user,'etat'=>0])->one();
						
						if($new_paiement==null){
							
							  //enregistrer le paiement dans la base local
							  $new_paiement=new CommandePayment;
							  $new_paiement->paiement_key=Yii::$app->security->generateRandomString(32);
							  $new_paiement->num_paiement=$id_user.time();
							  $new_paiement->id_commande_marchand=$id_commande_marchand;
							  $new_paiement->id_user=$id_user;
							  $new_paiement->type_paiement=1;
							  $new_paiement->montant_paiement=$montant_paiement;
							  $new_paiement->charge_paiement=$charge_paiement;
							  $new_paiement->etat=0;
							  $new_paiement->date_create=date("Y-m-d H:i:s");
							  $new_paiement->date_update=date("Y-m-d H:i:s");
							  $new_paiement->save();
							
						  
						}else{
							$new_paiement->num_paiement=$id_user.time();
							$new_paiement->date_update=date("Y-m-d H:i:s");		
							$new_paiement->save();							
						}
						
						if($continue_process){
							
							$info_send["token"]=Common::paygate_token;
							$info_send["amount"]=$montant_paiement+$charge_paiement;
							//$info_send["amount"]=50;
							$info_send["url"]="https://abusiness.store";
							$info_send["identifier"]=$new_paiement->num_paiement;
							
							
							$query_string = http_build_query( $info_send );

							return $this->redirect(Common::paygate_url . '?' . $query_string);
							
							
							
						}
					
					}
				
				}
				
			}		
		}
		
		
       
    }


	public function beforeAction($action)
    {
       if(isset(Yii::$app->session['langue_AB'])){
			Yii::$app->language=Yii::$app->session['langue_AB'];
		}
		
		$this->enableCsrfValidation = false;
		return parent::beforeAction($action);
    }
	
   
	
}
