<?php
$tab_url = array(
    'abusiness' => 'site/index',
    'contact' => 'site/contact',
    'CGU' => 'site/cgu',
    'live' => 'site/live',

    'foire_adjafi' => 'shop/exposant/categorie',
    'stands' => 'shop/exposant/stand',
    // 'produit' => 'shop/exposant/produits',
    'produits' => 'site/produits',
    'PRODUITS' => 'site/produits',

    'ab_credit' => 'abcredit/abcredit',
    'map_recup' => 'abcredit/map_recup',
);
