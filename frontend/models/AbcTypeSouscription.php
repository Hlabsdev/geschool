<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "abc_type_souscription".
 *
 * @property int $id
 * @property string $key_type
 * @property string $libelle
 * @property string $date_create
 * @property string $date_update
 * @property int $create_by
 * @property int $update_by
 */
class AbcTypeSouscription extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'abc_type_souscription';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['key_type', 'libelle', 'date_create', 'date_update', 'create_by', 'update_by'], 'required'],
            [['date_create', 'date_update'], 'safe'],
            [['create_by', 'update_by'], 'integer'],
            [['key_type'], 'string', 'max' => 35],
            [['libelle'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'key_type' => 'Key Type',
            'libelle' => 'Libelle',
            'date_create' => 'Date Create',
            'date_update' => 'Date Update',
            'create_by' => 'Create By',
            'update_by' => 'Update By',
        ];
    }
}