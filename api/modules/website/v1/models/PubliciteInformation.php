<?php

namespace api\modules\website\v1\models;

use Yii;

/**
 * This is the model class for table "publicite_information".
 *
 * @property integer $id_publicite
 * @property string $publicite_key
 * @property string $date_publicite
 * @property integer $type_publicite
 * @property string $content_publicite
 * @property string $content_destinataire
 * @property string $photo_publicite
 * @property integer $etat_publicite
 * @property integer $created_by
 * @property string $denomination_publicite
 * @property string $fichier_publicite
 *
 * @property PubliciteContact[] $publiciteContacts
 */
class PubliciteInformation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'publicite_information';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['publicite_key', 'date_publicite', 'content_publicite', 'etat_publicite', 'created_by'], 'required'],
            [['date_publicite'], 'safe'],
            [['type_publicite', 'etat_publicite', 'created_by'], 'integer'],
            [['content_publicite', 'content_destinataire'], 'string'],
            [['publicite_key', 'photo_publicite', 'fichier_publicite'], 'string', 'max' => 50],
            [['denomination_publicite'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_publicite' => 'Id Publicite',
            'publicite_key' => 'Publicite Key',
            'date_publicite' => 'Date Publicite',
            'type_publicite' => 'Type Publicite',
            'content_publicite' => 'Content Publicite',
            'content_destinataire' => 'Content Destinataire',
            'photo_publicite' => 'Photo Publicite',
            'etat_publicite' => 'Etat Publicite',
            'created_by' => 'Created By',
            'denomination_publicite' => 'Denomination Publicite',
            'fichier_publicite' => 'Fichier Publicite',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPubliciteContacts()
    {
        return $this->hasMany(PubliciteContact::className(), ['id_publicite' => 'id_publicite']);
    }
}
