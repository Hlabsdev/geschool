<?php

namespace api\modules\website\v1\models;

use Yii;

/**
 * This is the model class for table "historique_whatsapp".
 *
 * @property integer $id_historique_whatsapp
 * @property string $name_param
 * @property string $liens_chat
 * @property string $instance_chat
 * @property string $token_chat
 * @property integer $total_message_chat
 * @property string $date_operation
 */
class HistoriqueWhatsapp extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'historique_whatsapp';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name_param', 'liens_chat', 'instance_chat', 'token_chat', 'total_message_chat', 'date_operation'], 'required'],
            [['total_message_chat'], 'integer'],
            [['date_operation'], 'safe'],
            [['name_param'], 'string', 'max' => 50],
            [['liens_chat', 'instance_chat', 'token_chat'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_historique_whatsapp' => 'Id Historique Whatsapp',
            'name_param' => 'Name Param',
            'liens_chat' => 'Liens Chat',
            'instance_chat' => 'Instance Chat',
            'token_chat' => 'Token Chat',
            'total_message_chat' => 'Total Message Chat',
            'date_operation' => 'Date Operation',
        ];
    }
}
