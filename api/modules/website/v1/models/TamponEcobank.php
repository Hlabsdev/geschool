<?php

namespace api\modules\website\v1\models;

use Yii;

/**
 * This is the model class for table "tampon_ecobank".
 *
 * @property integer $idtampon_ecobank
 * @property string $date_create
 * @property integer $etat_tampon
 * @property string $user_ip
 * @property string $user_header
 * @property string $user_response
 */
class TamponEcobank extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tampon_ecobank';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date_create', 'etat_tampon', 'user_ip'], 'required'],
            [['date_create'], 'safe'],
            [['etat_tampon'], 'integer'],
            [['user_header', 'user_response'], 'string'],
            [['user_ip'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idtampon_ecobank' => 'Idtampon Ecobank',
            'date_create' => 'Date Create',
            'etat_tampon' => 'Etat Tampon',
            'user_ip' => 'User Ip',
            'user_header' => 'User Header',
            'user_response' => 'User Response',
        ];
    }
}
