<?php

namespace api\modules\website\v1\models;

use Yii;

/**
 * This is the model class for table "mproduits".
 *
 * @property integer $idProduit
 * @property string $key_produit
 * @property integer $id_acteur
 * @property string $denomination
 * @property string $unite
 * @property double $prix_vente
 * @property string $qte
 * @property string $poids_unitaire
 * @property string $photo_produit
 * @property integer $position_menu_ussd
 * @property integer $etat
 * @property string $date_create
 * @property string $descriptionProduit
 * @property integer $created_by
 * @property string $date_update
 * @property integer $updated_by
 * @property integer $id_adjafi
 * @property integer $visible_vitrine
 *
 * @property ActeurUser $idActeur
 */
class Mproduits extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mproduits';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['key_produit', 'id_acteur', 'unite', 'poids_unitaire', 'photo_produit', 'position_menu_ussd', 'etat', 'created_by'], 'required'],
            [['id_acteur', 'position_menu_ussd', 'etat', 'created_by', 'updated_by', 'id_adjafi', 'visible_vitrine'], 'integer'],
            [['prix_vente'], 'number'],
            [['date_create', 'date_update'], 'safe'],
            [['key_produit', 'denomination'], 'string', 'max' => 255],
            [['unite', 'poids_unitaire', 'photo_produit'], 'string', 'max' => 50],
            [['qte'], 'string', 'max' => 10],
            [['id_acteur'], 'exist', 'skipOnError' => true, 'targetClass' => ActeurUser::className(), 'targetAttribute' => ['id_acteur' => 'id_acteur_user']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idProduit' => 'Id Produit',
            'key_produit' => 'Key Produit',
            'id_acteur' => 'Id Acteur',
            'denomination' => 'Denomination',
            'unite' => 'Unite',
            'prix_vente' => 'Prix Vente',
            'qte' => 'Qte',
            'poids_unitaire' => 'Poids Unitaire',
            'photo_produit' => 'Photo Produit',
            'position_menu_ussd' => 'Position Menu Ussd',
            'etat' => 'Etat',
            'date_create' => 'Date Create',
            'created_by' => 'Created By',
            'date_update' => 'Date Update',
            'updated_by' => 'Updated By',
            'id_adjafi' => 'Id Adjafi',
            'visible_vitrine' => 'Visible Vitrine',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdActeur()
    {
        return $this->hasOne(ActeurUser::className(), ['id_acteur_user' => 'id_acteur']);
    }
}
