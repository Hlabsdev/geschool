<?php

namespace api\modules\website\v1\models;

use Yii;

/**
 * This is the model class for table "abc_souscription".
 *
 * @property integer $id_souscription
 * @property string $key_souscription
 * @property integer $type_souscription
 * @property string $ref_souscription
 * @property integer $id_user
 * @property string $customer
 * @property integer $solde_disponible
 * @property integer $banque
 * @property string $numero_compte
 * @property integer $status_souscription
 * @property string $date_create
 * @property string $code_banque
 * @property string $code_guichet
 * @property string $cle_rib
 * @property string $code_bic
 * @property string $iban
 * @property string $adresse_banque
 * @property string $no_carte
 * @property integer $etat_reset
 *
 * @property AbcRetraitmobile[] $abcRetraitmobiles
 * @property User $idUser
 * @property AbcVente[] $abcVentes
 */
class AbcSouscription extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'abc_souscription';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['key_souscription', 'type_souscription', 'id_user', 'customer', 'banque', 'numero_compte', 'status_souscription', 'date_create'], 'required'],
            [['type_souscription', 'id_user', 'solde_disponible', 'banque', 'status_souscription', 'etat_reset'], 'integer'],
            [['date_create'], 'safe'],
            [['key_souscription'], 'string', 'max' => 35],
            [['ref_souscription'], 'string', 'max' => 10],
            [['customer', 'numero_compte', 'code_banque', 'code_guichet', 'cle_rib', 'code_bic', 'iban', 'adresse_banque', 'no_carte'], 'string', 'max' => 255],
            [['id_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_user' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_souscription' => 'Id Souscription',
            'key_souscription' => 'Key Souscription',
            'type_souscription' => 'Type Souscription',
            'ref_souscription' => 'Ref Souscription',
            'id_user' => 'Id User',
            'customer' => 'Customer',
            'solde_disponible' => 'Solde Disponible',
            'banque' => 'Banque',
            'numero_compte' => 'Numero Compte',
            'status_souscription' => 'Status Souscription',
            'date_create' => 'Date Create',
            'code_banque' => 'Code Banque',
            'code_guichet' => 'Code Guichet',
            'cle_rib' => 'Cle Rib',
            'code_bic' => 'Code Bic',
            'iban' => 'Iban',
            'adresse_banque' => 'Adresse Banque',
            'no_carte' => 'No Carte',
            'etat_reset' => 'Etat Reset',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAbcRetraitmobiles()
    {
        return $this->hasMany(AbcRetraitmobile::className(), ['idclient' => 'id_souscription']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdUser()
    {
        return $this->hasOne(User::className(), ['id' => 'id_user']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAbcVentes()
    {
        return $this->hasMany(AbcVente::className(), ['idclient' => 'id_souscription']);
    }
}
