<?php

namespace api\modules\website\v1\models;

use Yii;

/**
 * This is the model class for table "commande_marchand".
 *
 * @property integer $id_commande_marchand
 * @property string $commande_key
 * @property string $num_commande
 * @property integer $id_user
 * @property integer $id_marchand
 * @property integer $id_livreur
 * @property integer $idtransaction
 * @property string $produit_commande
 * @property string $produit_livraison
 * @property string $qte_livraison
 * @property string $lieu_livraison
 * @property double $lat_livraison
 * @property double $long_livraison
 * @property integer $prix_produits
 * @property integer $prix_livraison
 * @property string $poids_livraison
 * @property string $distance_livraison
 * @property integer $prix_commande
 * @property string $info_supplementaire
 * @property integer $prix_marchand
 * @property integer $prix_livreur
 * @property string $all_livreurId
 * @property integer $etat
 * @property integer $etat_confirmation
 * @property string $date_create
 * @property string $date_update
 * @property string $date_paiement
 * @property integer $idcommande_paiement
 * @property integer $updated_by
 * @property string $raison_reject
 *
 * @property User $idUser
 * @property ActeurUser $idMarchand
 * @property Ussdtransation $idtransaction0
 * @property CommandePayment[] $commandePayments
 */
class CommandeMarchand extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'commande_marchand';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['commande_key', 'id_user', 'id_marchand', 'idtransaction', 'produit_commande', 'produit_livraison', 'qte_livraison', 'lieu_livraison', 'prix_produits', 'prix_livraison', 'poids_livraison', 'prix_commande', 'etat', 'etat_confirmation'], 'required'],
            [['id_user', 'id_marchand', 'id_livreur', 'idtransaction', 'prix_produits', 'prix_livraison', 'prix_commande', 'prix_marchand', 'prix_livreur', 'etat', 'etat_confirmation', 'idcommande_paiement', 'updated_by'], 'integer'],
            [['lat_livraison', 'long_livraison'], 'number'],
            [['info_supplementaire', 'all_livreurId', 'raison_reject'], 'string'],
            [['date_create', 'date_update', 'date_paiement'], 'safe'],
            [['commande_key', 'num_commande', 'poids_livraison', 'distance_livraison'], 'string', 'max' => 50],
            [['produit_commande', 'produit_livraison', 'qte_livraison', 'lieu_livraison'], 'string', 'max' => 255],
            [['id_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_user' => 'id']],
            [['id_marchand'], 'exist', 'skipOnError' => true, 'targetClass' => ActeurUser::className(), 'targetAttribute' => ['id_marchand' => 'id_acteur_user']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_commande_marchand' => 'Id Commande Marchand',
            'commande_key' => 'Commande Key',
            'num_commande' => 'Num Commande',
            'id_user' => 'Id User',
            'id_marchand' => 'Id Marchand',
            'id_livreur' => 'Id Livreur',
            'idtransaction' => 'Idtransaction',
            'produit_commande' => 'Produit Commande',
            'produit_livraison' => 'Produit Livraison',
            'qte_livraison' => 'Qte Livraison',
            'lieu_livraison' => 'Lieu Livraison',
            'lat_livraison' => 'Lat Livraison',
            'long_livraison' => 'Long Livraison',
            'prix_produits' => 'Prix Produits',
            'prix_livraison' => 'Prix Livraison',
            'poids_livraison' => 'Poids Livraison',
            'distance_livraison' => 'Distance Livraison',
            'prix_commande' => 'Prix Commande',
            'info_supplementaire' => 'Info Supplementaire',
            'prix_marchand' => 'Prix Marchand',
            'prix_livreur' => 'Prix Livreur',
            'all_livreurId' => 'All Livreur ID',
            'etat' => 'Etat',
            'etat_confirmation' => 'Etat Confirmation',
            'date_create' => 'Date Create',
            'date_update' => 'Date Update',
            'date_paiement' => 'Date Paiement',
            'idcommande_paiement' => 'Idcommande Paiement',
            'updated_by' => 'Updated By',
            'raison_reject' => 'Raison Reject',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdUser()
    {
        return $this->hasOne(User::className(), ['id' => 'id_user']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdMarchand()
    {
        return $this->hasOne(ActeurUser::className(), ['id_acteur_user' => 'id_marchand']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdtransaction0()
    {
        return $this->hasOne(Ussdtransation::className(), ['id' => 'idtransaction']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCommandePayments()
    {
        return $this->hasMany(CommandePayment::className(), ['id_commande_marchand' => 'id_commande_marchand']);
    }
}
