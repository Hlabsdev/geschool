<?php

namespace api\modules\website\v1\models;

use Yii;

/**
 * This is the model class for table "publicite_partenaire".
 *
 * @property integer $id_publicite
 * @property string $publicite_key
 * @property string $date_publicite
 * @property integer $type_support
 * @property string $content_publicite
 * @property string $support_publicite
 * @property integer $etat_publicite
 * @property integer $created_by
 * @property string $denomination_partenaire
 * @property string $niveau_affichage
 * @property integer $nbre_jours_diffusion
 * @property integer $nbre_total_diffusion
 * @property integer $nbre_diffusion_journalier
 * @property integer $total_send
 */
class PublicitePartenaire extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'publicite_partenaire';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['publicite_key', 'date_publicite', 'etat_publicite', 'created_by', 'niveau_affichage'], 'required'],
            [['date_publicite'], 'safe'],
            [['type_support', 'etat_publicite', 'created_by', 'nbre_jours_diffusion', 'nbre_total_diffusion', 'nbre_diffusion_journalier', 'total_send'], 'integer'],
            [['content_publicite'], 'string'],
            [['publicite_key', 'support_publicite', 'niveau_affichage'], 'string', 'max' => 50],
            [['denomination_partenaire'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_publicite' => 'Id Publicite',
            'publicite_key' => 'Publicite Key',
            'date_publicite' => 'Date Publicite',
            'type_support' => 'Type Support',
            'content_publicite' => 'Content Publicite',
            'support_publicite' => 'Support Publicite',
            'etat_publicite' => 'Etat Publicite',
            'created_by' => 'Created By',
            'denomination_partenaire' => 'Denomination Partenaire',
            'niveau_affichage' => 'Niveau Affichage',
            'nbre_jours_diffusion' => 'Nbre Jours Diffusion',
            'nbre_total_diffusion' => 'Nbre Total Diffusion',
            'nbre_diffusion_journalier' => 'Nbre Diffusion Journalier',
            'total_send' => 'Total Send',
        ];
    }
}
