<?php

namespace api\modules\website\v1\models;

use Yii;

/**
 * This is the model class for table "historique_whatsapp_message".
 *
 * @property integer $idhistorique_whatsapp_message
 * @property string $sender
 * @property string $body
 * @property integer $datesend
 * @property integer $type
 */
class HistoriqueWhatsappMessage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'historique_whatsapp_message';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sender', 'body', 'datesend'], 'required'],
            [['body'], 'string'],
            [['datesend','type'], 'integer'],
            [['sender'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idhistorique_whatsapp_message' => 'Idhistorique Whatsapp Message',
            'sender' => 'Sender',
            'body' => 'Body',
            'datesend' => 'Datesend',
        ];
    }
}
