<?php

namespace api\modules\website\v1\models;

use Yii;

/**
 * This is the model class for table "commande_payment".
 *
 * @property integer $idcommande_paiement
 * @property string $paiement_key
 * @property string $num_paiement
 * @property integer $id_commande_marchand
 * @property integer $id_user
 * @property integer $type_paiement
 * @property integer $montant_paiement
 * @property integer $charge_paiement
 * @property string $response_gateway
 * @property string $identifiant_paiement
 * @property string $photo_qrcode
 * @property string $date_create
 * @property string $date_update
 * @property string $date_sendgateway
 * @property string $date_paiement
 * @property integer $etat
 * @property string $response_paiement
 *
 * @property CommandeMarchand $idCommandeMarchand
 * @property User $idUser
 */
class CommandePayment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'commande_payment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['paiement_key', 'num_paiement', 'id_commande_marchand', 'id_user', 'montant_paiement', 'charge_paiement', 'date_update', 'etat'], 'required'],
            [['id_commande_marchand', 'id_user', 'type_paiement', 'montant_paiement', 'charge_paiement', 'etat'], 'integer'],
            [['response_gateway', 'photo_qrcode', 'response_paiement'], 'string'],
            [['date_create', 'date_update', 'date_sendgateway', 'date_paiement'], 'safe'],
            [['paiement_key', 'num_paiement'], 'string', 'max' => 50],
            [['identifiant_paiement'], 'string', 'max' => 255],
            [['id_commande_marchand'], 'exist', 'skipOnError' => true, 'targetClass' => CommandeMarchand::className(), 'targetAttribute' => ['id_commande_marchand' => 'id_commande_marchand']],
            [['id_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_user' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idcommande_paiement' => 'Idcommande Paiement',
            'paiement_key' => 'Paiement Key',
            'num_paiement' => 'Num Paiement',
            'id_commande_marchand' => 'Id Commande Marchand',
            'id_user' => 'Id User',
            'type_paiement' => 'Type Paiement',
            'montant_paiement' => 'Montant Paiement',
            'charge_paiement' => 'Charge Paiement',
            'response_gateway' => 'Response Gateway',
            'identifiant_paiement' => 'Identifiant Paiement',
            'photo_qrcode' => 'Photo Qrcode',
            'date_create' => 'Date Create',
            'date_update' => 'Date Update',
            'date_sendgateway' => 'Date Sendgateway',
            'date_paiement' => 'Date Paiement',
            'etat' => 'Etat',
            'response_paiement' => 'Response Paiement',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdCommandeMarchand()
    {
        return $this->hasOne(CommandeMarchand::className(), ['id_commande_marchand' => 'id_commande_marchand']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdUser()
    {
        return $this->hasOne(User::className(), ['id' => 'id_user']);
    }
}
