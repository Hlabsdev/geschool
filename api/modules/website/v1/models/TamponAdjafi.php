<?php

namespace api\modules\website\v1\models;

use Yii;

/**
 * This is the model class for table "tampon_adjafi".
 *
 * @property integer $idtampon_adjafi
 * @property string $field_name
 * @property string $last_update
 * @property integer $etat
 */
class TamponAdjafi extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tampon_adjafi';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['field_name', 'last_update', 'etat'], 'required'],
            [['etat'], 'integer'],
            [['field_name', 'last_update'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idtampon_adjafi' => 'Idtampon Adjafi',
            'field_name' => 'Field Name',
            'last_update' => 'Last Update',
            'etat' => 'Etat',
        ];
    }
}
