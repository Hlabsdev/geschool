<?php

namespace api\modules\website\v1\models;

use Yii;

/**
 * This is the model class for table "publicite_contact".
 *
 * @property integer $idpublicite_contact
 * @property integer $id_publicite
 * @property string $numero
 * @property integer $exist
 * @property integer $flash
 * @property integer $send
 * @property string $date_create
 * @property string $date_send
 *
 * @property PubliciteInformation $idPublicite
 */
class PubliciteContact extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'publicite_contact';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_publicite', 'numero', 'exist', 'flash', 'send', 'date_create'], 'required'],
            [['id_publicite', 'exist', 'flash', 'send'], 'integer'],
            [['date_create', 'date_send'], 'safe'],
            [['numero'], 'string', 'max' => 20],
            [['id_publicite'], 'exist', 'skipOnError' => true, 'targetClass' => PubliciteInformation::className(), 'targetAttribute' => ['id_publicite' => 'id_publicite']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idpublicite_contact' => 'Idpublicite Contact',
            'id_publicite' => 'Id Publicite',
            'numero' => 'Numero',
            'exist' => 'Exist',
            'flash' => 'Flash',
            'send' => 'Send',
            'date_create' => 'Date Create',
            'date_send' => 'Date Send',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdPublicite()
    {
        return $this->hasOne(PubliciteInformation::className(), ['id_publicite' => 'id_publicite']);
    }
}
