<?php

namespace api\modules\website\v1\models;

use Yii;

/**
 * This is the model class for table "historique_pub_message".
 *
 * @property integer $id_historique
 * @property integer $id_user
 * @property integer $id_partenaire
 * @property string $date_send
 * @property string $date_create
 */
class HistoriquePubMessage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'historique_pub_message';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_user', 'id_partenaire', 'date_send', 'date_create'], 'required'],
            [['id_user', 'id_partenaire'], 'integer'],
            [['date_send', 'date_create'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_historique' => 'Id Historique',
            'id_user' => 'Id User',
            'id_partenaire' => 'Id Partenaire',
            'date_send' => 'Date Send',
            'date_create' => 'Date Create',
        ];
    }
}
