<?php

namespace api\modules\website\v1\models;

use Yii;

/**
 * This is the model class for table "collecteur".
 *
 * @property integer $idCollecteur
 * @property string $collecteur_key
 * @property string $nom_collecteur
 * @property string $username_collecteur
 * @property string $phone_collecteur
 * @property string $registration_id
 * @property string $info_supplementaire
 * @property integer $etat
 * @property string $date_create
 * @property integer $created_by
 * @property string $date_update
 * @property integer $updated_by
 */
class Collecteur extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'collecteur';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['collecteur_key', 'nom_collecteur', 'username_collecteur', 'phone_collecteur', 'etat', 'created_by'], 'required'],
            [['info_supplementaire'], 'string'],
            [['etat', 'created_by', 'updated_by'], 'integer'],
            [['date_create', 'date_update'], 'safe'],
            [['collecteur_key', 'nom_collecteur', 'username_collecteur', 'phone_collecteur', 'registration_id'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idCollecteur' => 'Id Collecteur',
            'collecteur_key' => 'Collecteur Key',
            'nom_collecteur' => 'Nom Collecteur',
            'username_collecteur' => 'Username Collecteur',
            'phone_collecteur' => 'Phone Collecteur',
            'registration_id' => 'Registration ID',
            'info_supplementaire' => 'Info Supplementaire',
            'etat' => 'Etat',
            'date_create' => 'Date Create',
            'created_by' => 'Created By',
            'date_update' => 'Date Update',
            'updated_by' => 'Updated By',
        ];
    }
}
