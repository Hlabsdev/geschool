<?php
namespace api\modules\website\v1\controllers;
use Yii;
use yii\rest\ActiveController;
use yii\web\Response;
use yii\web\XmlResponseFormatter;
use console\models\Ussdtransation;
use api\modules\website\v1\models\Produits;
use api\modules\website\v1\models\Package;
use api\modules\website\v1\models\PackageInfo;
use api\modules\website\v1\controllers\Common;

use yii\helpers\Json;


class PackageController extends ActiveController
{
public $modelClass = 'api\modules\website\v1\models\HistoriqueWhatsappMessage'; 


public function actionGetpackage_info() {
  
  set_time_limit(0);
	date_default_timezone_set('UTC');
	if (Yii::$app->request->post()) {
		$all_post=Yii::$app->request->post();
	}else{
		$information=file_get_contents('php://input');	
		$all_post=json_decode($information, TRUE);
	}
	
	if(isset($all_post["access_token"]) && Common::all_token==trim($all_post["access_token"])){			
			//recuperer la liste des packages
			$all_package = Package::find()->select(['idPackage','denomination','prix_package','CONCAT(\''.Common::photobase_url.'\',\'\',photo_package) AS photo_package'])->where(['etat'=>1])->orderBy('position_menu_ussd ASC')->all();
			if(sizeof($all_package)>0){
				
				//recuperer la liste des produits
				$tab_produits = Produits::find()->select(['denomination'])->where(['etat'=>1,'type_produit'=>2])->orderBy('position_menu_ussd ASC')->all();
				$all_produits = Produits::find()->select(['idProduit','denomination','unite','qte_unitaire'])->where(['etat'=>1,'type_produit'=>2])->orderBy('position_menu_ussd ASC')->all();
				
				$all_detail=array();
				if(sizeof($all_package)>0){
					foreach($all_package as $info_package){
							$tab_package=array();
							$tab_package['nom_package']=$info_package->denomination;
							$tab_package['prix_package']=Common::show_nbre($info_package->prix_package)." FCFA";
							$tab_package['photo_package']=$info_package->photo_package;
							
							$tab_produit=array();
							if(sizeof($all_produits)>0){
								foreach($all_produits as $info_produits){
									
									$test_info = PackageInfo::find()->select(['qte'])->where(['idProduit'=>$info_produits->idProduit,'idPackage'=>$info_package->idPackage,'etat'=>1])->one();
									$recup["qte"]="";
									if($test_info!=null){
										
										$plus="";
										if($info_produits->qte_unitaire!==null && $info_produits->qte_unitaire!==""){
											$total_poids=Common::show_nbre($test_info->qte*$info_produits->qte_unitaire/1000);
											$plus=" / ".$total_poids."Kg";
										}			
										$recup["qte"]=$test_info->qte." ".$info_produits->unite.$plus;
									}else{
										$plus=" / 0Kg";
										$recup["qte"]="0 ".$info_produits->unite.$plus;
									}
									$tab_produit[]=$recup;
									 
								}
								
							}
							$tab_package['other_price']=$tab_produit;
							/*
								$test_info = PackageInfo::find()->select(['qte'])->where(['idProduit'=>$info_produits->idProduit,'idPackage'=>$info_package->idPackage,'etat'=>1])->one();
								$recup="";
								if($test_info!=null){
									$recup=$test_info->qte;								
								}
								$tab[$info_package->denomination]=$recup;
								*/
							$all_detail[]=$tab_package;
					}	
				}	
				/*
				if(sizeof($all_produits)>0){
					foreach($all_produits as $info_produits){
						$tab_produits[]=$info_produits->denomination;
						$tab=array();
						$tab['name_produit']=$info_produits->denomination;
						$tab['unite_produit']=$info_produits->unite;
						foreach($all_package as $info_package){
							$test_info = PackageInfo::find()->select(['qte'])->where(['idProduit'=>$info_produits->idProduit,'idPackage'=>$info_package->idPackage,'etat'=>1])->one();
							$recup="";
							if($test_info!=null){
								$recup=$test_info->qte;								
							}
							$tab[$info_package->denomination]=$recup;
						}						
						$all_detail[]=$tab;						
					}
					
				}
				*/
				$request_response["status"]="000";
				$request_response["tab_produits"]=$tab_produits;
				$request_response["detail_package"]=$all_detail;
				
			}else{
				$request_response["status"]="002";
				$request_response["message"]="Désolé, votre liste est vide";
				
			}
								
		
			
						
			
	}else{	
		$request_response["status"]="001";
		$request_response["message"]="Erreur d'authentification de l'API";
	}
		
	echo Json::encode($request_response) ;
	exit();
	
}



}