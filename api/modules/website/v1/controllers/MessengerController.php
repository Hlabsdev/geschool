<?php
namespace api\modules\website\v1\controllers;
use Yii;
use yii\rest\ActiveController;
use yii\web\Response;
use yii\web\XmlResponseFormatter;
use console\models\Ussdtransation;

use api\modules\website\v1\models\HistoriqueWhatsapp;
use api\modules\website\v1\models\MessageHistorique;
use api\modules\website\v1\models\HistoriqueWhatsappMessage;
use api\modules\website\v1\controllers\Common;
use backend\controllers\Utils;

class MessengerController extends ActiveController
{
public $modelClass = 'api\modules\website\v1\models\HistoriqueWhatsappMessage';  


public function actionExeconsole($cmd_debut) {    
	$cmd = 'cd ../.. && /usr/bin/php yii consoleappinit/process '.$cmd_debut;	
	return $result = shell_exec($cmd);	
}

public function send_cmessage($format_url,$chat_id,$content,$btn_content) {

	
	$content=trim($content);
	$texte=str_replace("<b>","*",$content);
	$texte=str_replace("</b>","*",$texte);
	
	
	$another_btn=array();
	$new_jsonData="";
	
	$nbre_btn=3;
	$max_size=2000;
	$full_content=""; 
	if(sizeof($btn_content)>1){
		
		$buttons=array();
		$content=str_replace("<b>","*",$content);
		$content=str_replace("</b>","*",$content);
		
		$i=0;
		$u=0;
		foreach($btn_content as $info_btn){
			if($i<$nbre_btn){
				$buttons[$u][]=["type"=>"postback","title"=>$info_btn->text,"payload"=>$info_btn->value];
			}
			$i++;	
			if($i==$nbre_btn){
				$i=0;
				$u++;
			}	
			$full_content="\n - ".$info_btn->text." -".$info_btn->value ;
		}	
		$u=0;
		
		foreach($buttons as $info_btn ){
			
			$title="La suite ...";
			if($u==0)$title=$content;		
			$tab_field=array();
			$tab_field["recipient"]=["id"=>"".$chat_id.""];	
			$tab_field["message"]=["attachment"=>["type"=>"template","payload"=>["template_type"=>"button","text"=>$title,"buttons"=>$info_btn]]];	
			$jsonData=json_encode($tab_field);
			
			$ch = curl_init($format_url);
			curl_setopt($ch, CURLOPT_POST,1);
			curl_setopt($ch, CURLOPT_POSTFIELDS,$jsonData);
			curl_setopt($ch, CURLOPT_HTTPHEADER,array('Content-Type: application/json'));
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,false);
			$result = curl_exec($ch);
			curl_close($ch);
		
			$u++;
		}
		
		
	}else{
		
		$have_next=true;
		$full_text=array();
		while($have_next){
			
			if(trim(strlen($texte))>$max_size){
				$text_send = substr($texte, 0, strrpos(substr($texte, 0, $max_size), ' '));
				
				$texte=substr($texte, strlen($text_send));
			}else{
				$text_send = $texte ;
				$have_next=false;
			}
			$full_text[]=$text_send;
			
		}
		
		
		foreach($full_text as $info_text ){
			$tab_field=array();
			$tab_field["recipient"]=["id"=>"".$chat_id.""];	
			$tab_field["message"]=["text"=>$info_text];	
			$jsonData=json_encode($tab_field);
			
			$ch = curl_init($format_url);
			curl_setopt($ch, CURLOPT_POST,1);
			curl_setopt($ch, CURLOPT_POSTFIELDS,$jsonData);
			curl_setopt($ch, CURLOPT_HTTPHEADER,array('Content-Type: application/json'));
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,false);
			$result = curl_exec($ch);
			curl_close($ch);
			
		}
		
	}
	
	//Utils::send_information("22891393958",$content.$full_content,"WHATSAPP");	
}

public function send_slider($format_url,$chat_id,$message) {

	$sliders=array();
	$nbre_btn=10; 
	$i=0;
	$u=0;
	
	$content="";
	if(isset($message->type) && $message->type=="boutique"){
		
		foreach($message->information as $recup){	
				
			$btn_info=[["type"=>"postback","title"=>"Visiter","payload"=>$recup->code]];
			$sliders[$u][]=["title"=>$recup->denomination,"image_url"=>Common::photobase_url.$recup->photo,"subtitle"=>$recup->description."\n\n".$recup->adresse,"buttons"=>$btn_info];
			
			$i++;	
			if($i==$nbre_btn){
				$i=0;
				$u++;
			}	
		}	
		
	}else if(isset($message->type) && $message->type=="produit"){
		
		$content="<b>Bienvenue chez ".$message->information[0]->marchand."</b>";
		$content.="\n\nVeuillez taper le numéro des produits à commander en les séparant par des <b>virgules</b> .Pour ajouter plusieurs quantités faire Numéro * quantité (Exemple : <b>1*2</b> , <b>2*3</b> )";
		foreach($message->information as $recup){							
			$sliders[$u][]=["title"=>$recup->code.": ".$recup->denomination,"image_url"=>Common::photobase_url.$recup->photo,"subtitle"=>$recup->qte_vente.' à '.$recup->prix_vente];			
			$i++;	
			if($i==$nbre_btn){
				$i=0;
				$u++;
			}	
		}	
		
	}else if(isset($message->type) && $message->type=="my_produit"){
		
		//$content="<b>Bienvenue chez ".$message->information[0]->marchand."</b>";
		foreach($message->information as $recup){		
			$btn_info=[["type"=>"postback","title"=>"Selectionner","payload"=>$recup->code]];
			$sliders[$u][]=["title"=>$recup->denomination,"image_url"=>Common::photobase_url.$recup->photo,"subtitle"=>$recup->qte_vente.' à '.$recup->prix_vente,"buttons"=>$btn_info];			
			$i++;	
			if($i==$nbre_btn){
				$i=0;
				$u++;
			}	
		}	
		
	}
	
	if($content!=""){
		$this->send_cmessage($format_url,$chat_id,$content,array()); 
	}
	
	$u=0;
	foreach($sliders as $info_slider ){
				
		$tab_field=array();
		$tab_field["recipient"]=["id"=>"".$chat_id.""];	
		$tab_field["message"]=["attachment"=>["type"=>"template","payload"=>["template_type"=>"generic","elements"=>$info_slider]]];				

		$jsonData=json_encode($tab_field);
		
		$ch = curl_init($format_url);
		curl_setopt($ch, CURLOPT_POST,1);
		curl_setopt($ch, CURLOPT_POSTFIELDS,$jsonData);
		curl_setopt($ch, CURLOPT_HTTPHEADER,array('Content-Type: application/json'));
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,false);
		$result = curl_exec($ch);
		curl_close($ch);
	
		$u++;
	}		
		
}

public function send_cfmessage($format_url,$chat_id,$url_object,$field_object,$content,$btn_content) {

	
	$content=trim($content);
	$title="Bienvenue sur AB!";		
	$content=str_replace("<b>","",$content);
	$content=str_replace("</b>","",$content);
	
	$explode_text=explode(",",$content);
	if(sizeof($explode_text)>=2){
		$title=$explode_text[0];
		$texte=str_replace($title.",","",$content);
	}else{
		$texte=$content;			
	}
		
	
	$nbre_btn=3;
	 
	if(sizeof($btn_content)>0){
		
		$buttons=array();
		
		
		$i=0;
		$u=0;
		foreach($btn_content as $info_btn){
			if($i<$nbre_btn){
				$buttons[$u][]=["type"=>"postback","title"=>$info_btn->text,"payload"=>$info_btn->value];
			}
			$i++;	
			if($i==$nbre_btn){
				$i=0;
				$u++;
			}	
		}	
		$u=0;
		
		
		
		foreach($buttons as $info_btn ){
			
			
			if($u==0){
				
				$elements[0]['title']=$title;
				$elements[0]['image_url']=$url_object;
				$elements[0]['subtitle']=$texte;
				$elements[0]['buttons']=$info_btn;
				$tab_field=array();
				$tab_field["recipient"]=["id"=>"".$chat_id.""];	
				$tab_field["message"]=["attachment"=>["type"=>"template","payload"=>["template_type"=>"generic","elements"=>$elements]]];	
				$jsonData=json_encode($tab_field);
				
			}else{
				
				$title="La suite ...";
				if($u==0)$title=$content;		
				$tab_field=array();
				$tab_field["recipient"]=["id"=>"".$chat_id.""];	
				$tab_field["message"]=["attachment"=>["type"=>"template","payload"=>["template_type"=>"button","text"=>$title,"buttons"=>$info_btn]]];	
				$jsonData=json_encode($tab_field);
			}
			
			$ch = curl_init($format_url);
			curl_setopt($ch, CURLOPT_POST,1);
			curl_setopt($ch, CURLOPT_POSTFIELDS,$jsonData);
			curl_setopt($ch, CURLOPT_HTTPHEADER,array('Content-Type: application/json'));
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,false);
			$result = curl_exec($ch);
			curl_close($ch);
			
		
			$u++;
		}
		
		
	}else{
		
		
		
		
	
			
		$elements[0]['title']=$title;
		$elements[0]['image_url']=$url_object;
		$elements[0]['subtitle']=$texte;
		
		$tab_field=array();
		$tab_field["recipient"]=["id"=>"".$chat_id.""];	
		$tab_field["message"]=["attachment"=>["type"=>"template","payload"=>["template_type"=>"generic","elements"=>$elements]]];	
		$jsonData=json_encode($tab_field);
		
		
		$ch = curl_init($format_url);
		curl_setopt($ch, CURLOPT_POST,1);
		curl_setopt($ch, CURLOPT_POSTFIELDS,$jsonData);
		curl_setopt($ch, CURLOPT_HTTPHEADER,array('Content-Type: application/json'));
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,false);
		$result = curl_exec($ch);
		curl_close($ch);
		
		
		
	}
	
	
}

public function actionFlash_abusiness() {
			
		
		if (Yii::$app->request->post()) {
			$decoded=Yii::$app->request->post();
		}else{
				
			$information=file_get_contents('php://input');	
			$decoded = json_decode($information,true);			
		}
		
		/*
		$new_cmessage=new HistoriqueWhatsappMessage ;
		$new_cmessage->sender="MESSENGER";
		$new_cmessage->datesend=time();
		$new_cmessage->body=json_encode($decoded);
		$new_cmessage->type=99;
		$new_cmessage->save();
		*/
		
		$auth_key=$_GET['token'];
		$btn_content=array();
		
		$test_rs=HistoriqueWhatsapp::find()->where(['name_param'=>'ab_messenger'])->one();
		if($test_rs!=null){
			$star_content=array("ab","start");
									
			$traitement_key=explode("_",$auth_key);
			if(sizeof($traitement_key)==2 && $traitement_key[0]==Common::default_dev){				
				
				if(isset($decoded["entry"]) && isset($decoded["entry"][0]["id"]) && $decoded["entry"][0]["id"]==$test_rs->instance_chat){					
				//if(isset($decoded["entry"]) && isset($decoded["entry"][0]["id"])){					

							
						$recup_message=$decoded['entry'][0]['messaging'][0];		
						//verifier si ce message a ete deja recu et traite 						
					    
						$content="";						
						if(isset($recup_message['message']['quick_reply']['payload'])){
							
							$content="bot_phone/".trim($recup_message['message']['quick_reply']['payload']);
							
						}else if(isset($recup_message['message']['text'])){
							
							$content=trim($recup_message['message']['text']);
							
						}else if(isset($recup_message['postback']['payload'])){
							
							$content=$recup_message['postback']['payload'];
							
						}else if(isset($recup_message['message']['attachments'][0]["payload"]["coordinates"])){
							
							$lat=$recup_message['message']['attachments'][0]["payload"]["coordinates"]["lat"];
							$long=$recup_message['message']['attachments'][0]["payload"]["coordinates"]["long"];
							$content=$lat.";".$long;
						}else if(isset($recup_message['message']['attachments'][0]["payload"]["url"])){
							
							$url_photo=$recup_message['message']['attachments'][0]["payload"]["url"];
							$content=str_replace("\"","",$url_photo);
							
						}
						
						
						if($content!=""){
							
													
							$username="";
							if(isset($recup_message['sender']['id'])){
								$username=$recup_message['sender']['id'];
							}
							$chatId=$username;
						
						
												
							$test_rs->date_operation=date("Y-m-d H:i:s");
							$test_rs->total_message_chat++;
							$test_rs->save();
							
							if($chatId != $test_rs->instance_chat){
																
								$tps_send=(int)trim($recup_message['timestamp']/1000);
								$type=20;
								
								$test_contentwhatsapp=HistoriqueWhatsappMessage::find()->where(['sender'=>$chatId,'datesend'=>$tps_send,'body'=>json_encode($recup_message),'type'=>$type])->one();
								
								if($test_contentwhatsapp==null){
								
									$new_cmessage=new HistoriqueWhatsappMessage ;
									$new_cmessage->sender=(string)$chatId;
									$new_cmessage->datesend=$tps_send;
									$new_cmessage->body=json_encode($recup_message);
									$new_cmessage->type=$type;
									if($new_cmessage->save()){
										
										
											
											$convert_content=strtolower(str_replace(" ","",$content));
											$convert_content=str_replace("*","",$convert_content);
											$convert_content=str_replace("/","",$convert_content);
											
											
											$content=str_replace("\n\r", '. ', $content);
											$content=str_replace("\n", '. ', $content);
											$content=str_replace("\r", '. ', $content);
											
											$direct=false;
											$test_agent=explode("ABCOLLECTOR",$content);
											if(sizeof($test_agent)==3) $direct=true;
											
									
											$message="";
											$type_response="0";
											$sessionid=$tps_send."_".$chatId;
											if(in_array($convert_content,$star_content) || $direct==true){
											
										
												//fermer toutes les sessions ouvertes
												Ussdtransation::updateAll(['etat_transaction' => 3], 'username= "'.$chatId.'" and reference="'.Common::messenger_ref.'" and etat_transaction!=1');
												
												//demarrer un nouveau dialogue
												$sessionid=$tps_send."_".$chatId;
												$nom="";
												$prenom="";
												$response="1";
												if($direct==true)$response=(int)$test_agent[1];
												$command='-t="'.$chatId.'" -r="'.$response.'" -n="'.$nom.'" -f="'.$prenom.'" -s="0" -i="'.$sessionid.'" -p="'.Common::messenger_ref.'"';
												
												$response=$this->actionExeconsole($command);		
												$recup=json_decode($response);									
												if(isset($recup->type))$type_response=$recup->type;
												if(isset($recup->response))$message=$recup->response;
												if(isset($recup->btncontent))$btn_content=$recup->btncontent;
												
												
												
											}else{
												
												$run=false;
												$test_direct=explode("ABSELLER",$content);
												if(sizeof($test_direct)==3) $run=true;
													
								
												//verifier si cet user a une session ouverte
												$find_transaction = Ussdtransation::find()->where(['username'=>$chatId,'reference'=>Common::messenger_ref,'etat_transaction'=>[0,2]])->orderby(['id'=>SORT_DESC])->one();
												if($find_transaction!=null || $run==true){
													
													if($run==true){
														Ussdtransation::updateAll(['etat_transaction' => 3], 'username= "'.$chatId.'" and reference="'.Common::messenger_ref.'" and etat_transaction!=1');
														$sessionid=$tps_send."_".$chatId;
														if($find_transaction!=null){
															$sessionid=$find_transaction->idtransaction;
														}
														
														$command='-t="'.$chatId.'" -r="'.$content.'" -i="'.$sessionid.'" -s="0" -p="'.Common::messenger_ref.'"';
														
														$response=$this->actionExeconsole($command);	

														$recup=json_decode($response);									
														if(isset($recup->type))$type_response=$recup->type;
														if(isset($recup->response))$message=$recup->response;
														if(isset($recup->btncontent))$btn_content=$recup->btncontent;
														
												
													}else if($tps_send-$find_transaction->last_update < (600*3)){
														
														$find_transaction->last_update=(int)$tps_send;
														$find_transaction->relance=0;
														$find_transaction->save();
														
														$sessionid=$find_transaction->idtransaction;
														$command='-t="'.$chatId.'" -r="'.$content.'" -i="'.$sessionid.'" -s="1" -p="'.Common::messenger_ref.'"';
														
														$response=$this->actionExeconsole($command);	

														$recup=json_decode($response);									
														if(isset($recup->type))$type_response=$recup->type;
														if(isset($recup->response))$message=$recup->response;
														if(isset($recup->btncontent))$btn_content=$recup->btncontent;
														
													}else{
														
														$message="Votre dernière session a expiré.\n\nPour demarrer une discussion tapez: <b>AB</b> ";
														
													}
													
												}else{								
													$message="Bienvenue sur <b>ABusiness</b>, votre chatBot e-commerce de produits de première nécessité\n\nPour démarrer une discussion tapez: <b>AB</b>";
												}
												
											}
											
											
											if($message!=""){
												
												$url =  $test_rs->liens_chat."?access_token=".$test_rs->token_chat;
												$access_user=array();
												if(in_array($chatId,$access_user)){												
													$message="Service Temporairement indisponible\n\nVeuillez réessayer plus tard\nMerci";										
													//$url =  $test_rs->liens_chat.$test_rs->instance_chat.":".$test_rs->token_chat."/sendmessage";		
													$this->send_cmessage($url,$chatId,$message,$btn_content);
												}
												else{
													
													if(is_array($message) || is_object($message)){														
														
														$this->send_slider($url,$chatId,$message);
														
													}
													else{
														
														$message=str_replace("Erreur:","<b>Erreur:</b> \n",$message);
														$agent_message="";
														$contact_message="";
														$contact_fmessage="";
														
														$recup_message=explode("#####",$message);
														if(sizeof($recup_message)==3){
															
															$message=$recup_message[0]."".$recup_message[2];
															$agent_message=$recup_message[1];
														}
														
														$recup_cmessage=explode("CONT_ACT",$message);
														if(sizeof($recup_cmessage)==3){
															
															$message=$recup_cmessage[0]."".$recup_cmessage[2];
															$contact_message=$recup_cmessage[1];
														}
														
														$recup_fmessage=explode("LIVRA_ISON",$message);
														if(sizeof($recup_fmessage)==3){
															
															$message=$recup_fmessage[0]."".$recup_fmessage[2];
															$contact_fmessage=$recup_fmessage[1];
														}
														
																												
														//verifier si cest un message avec image ou si cest un simple message
														$recup_message0=explode("WA_IMAGE",$message);
														$recup_message1=explode("WA_VIDEO",$message);
														if(sizeof($recup_message0)==3){
															
															$new_name=str_replace("location.jpeg","messelocation.jpeg",$recup_message0[1]);
															$message=$recup_message0[0]."".$recup_message0[2];
															$file_url=Common::photobase_url.$new_name; 
															
															$this->send_cfmessage($url,$chatId,$file_url,'photo',$message,$btn_content);
															
														}else if(sizeof($recup_message1)==3){
															
															//$urlvideo =  $test_rs->liens_chat.$test_rs->instance_chat.":".$test_rs->token_chat."/sendVideo";		
															$message=$recup_message1[0]."".$recup_message1[2];
															$file_url=Common::photobase_url.$recup_message1[1]; 
															
															$this->send_cfmessage($url,$chatId,$file_url,'video',$message,$btn_content);
															
														}else{
															
															$recup_usermessage=explode("GOOD_DIAGNOSTIC",$message);
																
															$share_message="";
															if(sizeof($recup_usermessage)==3){													
																$message=$recup_usermessage[0]." ".$recup_usermessage[2];
																$share_message=$recup_usermessage[1];
															}	
															$this->send_cmessage($url,$chatId,$message,$btn_content);
															if(trim($share_message)!=""){	
																$this->send_cmessage($url,$chatId,$share_message,$btn_content);
															}
														
														}
														
																												
														//Un utilisateur a passer une demande de location de drone
														if($agent_message!=""){													
															$sender_agent=array("256767615","22899595353","22891749741");
															foreach($sender_agent as $chatId){
																$this->send_cmessage($url,$chatId,$agent_message,array());
															}
														}
														
														//Un utilisateur a passer une commande
														if($contact_fmessage!=""){
															
															$sender_agent=array("256767615");
															foreach($sender_agent as $chatId){
																$this->send_cmessage($url,$chatId,$contact_fmessage,array());
															}
														}
														
														//Un utilisateur veut parler a notre operateur
														if($contact_message!=""){												
															$this->send_cmessage($url,"256767615",$contact_message,array());
														}
														
													}
												}
											}
											
										
									}
							
								}
								
							}
						}
						
				}
			}
			
		}
		exit();
}

public function actionFlash_abusinessxx(){
	
	
	$access_token = "EAAmOSMacDZCYBAN6YZBTB69yCWknbXCZAHZCa1SxJZA69Rbx0Ai7OFniXr0QHb4ZAwUn2ACao6B3FULZCtGRVY0j3PguuPZARiOQi0EZC38xYIeSKfOh5rvcybXfrcbMHf08RPjYZCNMTVFFVMwKaxR9f8558WrCQZCUPVHWM3rdOe3wgZDZD";
	$verify_token = "EAAmOSMacDZCYBAA";
	$hub_verify_token = "";

	if(isset($_REQUEST['hub_mode']) && $_REQUEST['hub_mode'] == 'subscribe'){
		$challenge = $_REQUEST['hub_challenge'];
		$hub_verify_token = $_REQUEST['hub_verify_token'];
		if($hub_verify_token === $verify_token){
			header('HTTP/1.1 200 OK');
			echo $challenge;
			die;
		}
	}
	exit;
	
	
}


}