<?php
namespace api\modules\website\v1\controllers;
use Yii;
use yii\rest\ActiveController;
use yii\web\Response;
use yii\web\XmlResponseFormatter;
use console\models\Ussdtransation;

use api\modules\website\v1\models\HistoriqueWhatsapp;
use api\modules\website\v1\models\MessageHistorique;
use api\modules\website\v1\models\HistoriqueWhatsappMessage;
use api\modules\website\v1\controllers\Common;

class TelegramController extends ActiveController
{
public $modelClass = 'api\modules\website\v1\models\HistoriqueWhatsappMessage';  
 
	
public function actionExecweb($cmd) {
	  
		//exit();
	    $get_url=yii::$app->basePath;   
        $base_url=explode(DIRECTORY_SEPARATOR."api",$get_url)[0];  
        chdir($base_url);
		
        $command = 'yii consoleappinit/process '.$cmd;
		
		$handler = popen($command, 'r');
		
        $output = '';
        while (!feof($handler)) {
            $output .= fgets($handler);
        }
		$output = trim($output);
        $status = pclose($handler);
        return $output;	
		
	
}

public function actionExeconsole($cmd_debut) {    
	$cmd = 'cd ../.. && /usr/bin/php yii consoleappinit/process '.$cmd_debut;	
	return $result = shell_exec($cmd);	
}



public function send_cmessage($format_url,$chat_id,$content) {

	
	$ch = curl_init();
	$curlConfig = array(
	  CURLOPT_URL            => $format_url,
	  CURLOPT_POST           => true,
	  CURLOPT_RETURNTRANSFER => true,
	  CURLOPT_CONNECTTIMEOUT => 5,
	  CURLOPT_POSTFIELDS     => array(
		'chat_id' => $chat_id,
		'text' => $content,		
		'parse_mode' => 'HTML',
	  ),
	);
	curl_setopt_array($ch, $curlConfig);
	$result = curl_exec($ch);
	curl_close($ch);	
}

public function send_cfmessage($format_url,$chat_id,$url_object,$field_object,$content) {
	
	$ch = curl_init();
	$curlConfig = array(
	  CURLOPT_URL            => $format_url,
	  CURLOPT_POST           => true,
	  CURLOPT_RETURNTRANSFER => true,
	  CURLOPT_CONNECTTIMEOUT => 5,
	  CURLOPT_POSTFIELDS     => array(
		'chat_id' => $chat_id,
		$field_object => $url_object,
		'caption' => $content,
		'parse_mode' => 'HTML',
	  ),
	);
	curl_setopt_array($ch, $curlConfig);
	$result = curl_exec($ch);
	curl_close($ch);	
}





public function actionFlash_abusiness() {
		
		if (Yii::$app->request->post()) {
			$decoded=Yii::$app->request->post();
		}else{
			
			$decoded=Yii::$app->request->get();
			if(sizeof($decoded)==1){	
				$information=file_get_contents('php://input');	
				$decoded = json_decode($information,true);	
			}
		}
		
		$auth_key=$_GET['token'];
		
		$ok="0";
		$test_rs=HistoriqueWhatsapp::find()->where(['name_param'=>'ab_telegram'])->one();
		if($test_rs!=null){
			$ok="1";		
			
			
			$star_content=array("ab","start");
			
			if(isset($decoded["message"]) && isset($decoded["update_id"])){
				$ok="2";				
				$traitement_key=explode("_",$auth_key);
				if(sizeof($traitement_key)==2 && $traitement_key[0]==Common::default_dev && $traitement_key[1]==$test_rs->instance_chat){	
						
						/*
						$new_cmessage=new HistoriqueWhatsappMessage ;
						$new_cmessage->sender="TELEGRAM";
						$new_cmessage->datesend=time();
						$new_cmessage->body=json_encode($decoded);
						$new_cmessage->type=100;
						$new_cmessage->save();
						*/
							
					
						$test_rs->date_operation=date("Y-m-d H:i:s");
						$test_rs->total_message_chat++;
						$test_rs->save();	
													
						
		
						//verifier si ce message a ete deja recu et traite 						
					    $recup_message=$decoded['message'];						
						$chatId=$recup_message['chat']['id'];
						$username="";
						if(isset($recup_message['chat']['username'])){
							$username=$recup_message['chat']['username'];
						}
						
						$content="";
						
						if(isset($recup_message['text'])){
							$content=trim($recup_message['text']);
						}else if(isset($recup_message['location'])){
							$lat=$recup_message['location']['latitude'];
							$long=$recup_message['location']['longitude'];
							$content=$lat.";".$long;
						}else if(isset($recup_message['contact'])){
							$content="bot_phone/".$recup_message['contact']['phone_number'];
						}else if(isset($recup_message['photo'])){
							
							//get file path
							$content="Photo";
							$file_id=$recup_message['photo'][0]['file_id'];
							$full_token=$test_rs->instance_chat.":".$test_rs->token_chat;
							$url_api="https://api.telegram.org/bot".$full_token."/getFile?file_id=".$file_id;
							
							$curl = curl_init(); 
							curl_setopt( $curl, CURLOPT_URL, $url_api ); 
							curl_setopt( $curl, CURLOPT_RETURNTRANSFER, 1 ); 
							curl_setopt( $curl, CURLOPT_TIMEOUT, 20 );
							$json_return = curl_exec( $curl ); 
							curl_close( $curl ); 
							
							$encode_response=json_decode($json_return);
							if(isset($encode_response->result)){								
								$content="https://api.telegram.org/file/bot".$full_token."/".$encode_response->result->file_path;								
							}
							
							//$url =  $test_rs->liens_chat.$test_rs->instance_chat.":".$test_rs->token_chat."/sendmessage";		
							//$this->send_cmessage($url,$chatId,$content);								
						}
						
						
						
						$tps_send=trim($recup_message['date']);
						$type=10;
						
						$test_contentwhatsapp=HistoriqueWhatsappMessage::find()->where(['sender'=>$chatId,'datesend'=>$tps_send,'body'=>$content,'type'=>$type])->one();
						
						if($test_contentwhatsapp==null){
						
							$new_cmessage=new HistoriqueWhatsappMessage ;
							$new_cmessage->sender=(string)$chatId;
							$new_cmessage->datesend=$tps_send;
							$new_cmessage->body=json_encode($recup_message);
							$new_cmessage->type=$type;
							if($new_cmessage->save()){
								
								$convert_content=strtolower(str_replace(" ","",$content));
								$convert_content=str_replace("*","",$convert_content);
								$convert_content=str_replace("/","",$convert_content);
								
								
								$content=str_replace("\n\r", '. ', $content);
								$content=str_replace("\n", '. ', $content);
								$content=str_replace("\r", '. ', $content);
								
								$direct=false;
								$test_agent=explode("ABCOLLECTOR",$content);
								if(sizeof($test_agent)==3) $direct=true;
								
						
								$message="";
								if(in_array($convert_content,$star_content) || $direct==true){
									
									//fermer toutes les sessions ouvertes
									Ussdtransation::updateAll(['etat_transaction' => 3], 'username= "'.$chatId.'" and reference="'.Common::telegram_ref.'" and etat_transaction!=1');
									
									//demarrer un nouveau dialogue
									$sessionid=$tps_send."_".$chatId."___".$username;
									$nom="";
									$prenom="";
									$response="1";
									if($direct==true)$response=(int)$test_agent[1];
									$command='-t="'.$chatId.'" -r="'.$response.'" -n="'.$nom.'" -f="'.$prenom.'" -s="0" -i="'.$sessionid.'" -p="'.Common::telegram_ref.'"';
									$message= $this->actionExeconsole($command);
									
								}else{
									
									$run=false;
									$test_direct=explode("ABSELLER",$content);
									if(sizeof($test_direct)==3) $run=true;
										
					
									//verifier si cet user a une session ouverte
									//$find_transaction = Ussdtransation::findOne(['username'=>$chatId,'reference'=>Common::telegram_ref,'etat_transaction'=>0]);
									$find_transaction = Ussdtransation::find()->where(['username'=>$chatId,'reference'=>Common::telegram_ref,'etat_transaction'=>[0,2]])->orderby(['id'=>SORT_DESC])->one();
									if($find_transaction!=null || $run==true){
										
										if($run==true){
											Ussdtransation::updateAll(['etat_transaction' => 3], 'username= "'.$chatId.'" and reference="'.Common::telegram_ref.'" and etat_transaction!=1');
											$sessionid=$tps_send."_".$chatId."___".$username;											
											if($find_transaction!=null){
												$sessionid=$find_transaction->idtransaction;
											}
											
											$command='-t="'.$chatId.'" -r="'.$content.'" -i="'.$sessionid.'" -s="0" -p="'.Common::telegram_ref.'"';
											$message= $this->actionExeconsole($command);
										}else if($tps_send-$find_transaction->last_update < (600*3)){
											
											$find_transaction->last_update=(int)$tps_send;
											$find_transaction->relance=0;
											$find_transaction->save();
											
											$sessionid=$find_transaction->idtransaction;
											$command='-t="'.$chatId.'" -r="'.$content.'" -i="'.$sessionid.'" -s="1" -p="'.Common::telegram_ref.'"';
											$message= $this->actionExeconsole($command);
										}else{
											
											$message="Votre dernière session a expiré.\n\nPour demarrer une discussion tapez: <b>AB</b> ";
											
										}
									}else{								
										$message="Bienvenue sur <b>ABusiness</b>, votre chatBot e-commerce de produits de première nécessité\n\nPour démarrer une discussion tapez: <b>AB</b>";
									}
								}
							
								if($message!=""){
					
									//$access_user=array("1093179373","1247956582","502481272","502481272","984044996","256767615","915071677","271662654","652716538","867067850","1337002720","642057127","946538546","1297953353");
									$access_user=array();
									if(in_array($chatId,$access_user)){												
										$message="Service Temporairement indisponible\n\nVeuillez réessayer plus tard\nMerci";										
										$url =  $test_rs->liens_chat.$test_rs->instance_chat.":".$test_rs->token_chat."/sendmessage";		
										$this->send_cmessage($url,$chatId,$message);
									}else{
										
										$message=str_replace("Erreur:","<b>Erreur:</b> \n",$message);
										$agent_message="";
										$contact_message="";
										$contact_fmessage="";
										
										$recup_message=explode("#####",$message);
										if(sizeof($recup_message)==3){
											
											$message=$recup_message[0]."".$recup_message[2];
											$agent_message=$recup_message[1];
										}
										
										$recup_cmessage=explode("CONT_ACT",$message);
										if(sizeof($recup_cmessage)==3){
											
											$message=$recup_cmessage[0]."".$recup_cmessage[2];
											$contact_message=$recup_cmessage[1];
										}
										
										$recup_fmessage=explode("LIVRA_ISON",$message);
										if(sizeof($recup_fmessage)==3){
											
											$message=$recup_fmessage[0]."".$recup_fmessage[2];
											$contact_fmessage=$recup_fmessage[1];
										}
										
										$url =  $test_rs->liens_chat.$test_rs->instance_chat.":".$test_rs->token_chat."/sendmessage";		
										
										
										//verifier si cest un message avec image ou si cest un simple message
										$recup_message0=explode("WA_IMAGE",$message);
										$recup_message1=explode("WA_VIDEO",$message);
										if(sizeof($recup_message0)==3){
											
											$new_name=str_replace("location.jpeg","telelocation.jpeg",$recup_message0[1]);
											$urlphoto =  $test_rs->liens_chat.$test_rs->instance_chat.":".$test_rs->token_chat."/sendPhoto";		
											$message=$recup_message0[0]."".$recup_message0[2];
											$file_url=Common::photobase_url.$new_name; 
											
											$this->send_cfmessage($urlphoto,$chatId,$file_url,'photo',$message);
										}else if(sizeof($recup_message1)==3){
											
											$urlvideo =  $test_rs->liens_chat.$test_rs->instance_chat.":".$test_rs->token_chat."/sendVideo";		
											$message=$recup_message1[0]."".$recup_message1[2];
											$file_url=Common::photobase_url.$recup_message1[1]; 
											
											$this->send_cfmessage($urlvideo,$chatId,$file_url,'video',$message);
										}else{
											$recup_usermessage=explode("GOOD_DIAGNOSTIC",$message);
												
											$share_message="";
											if(sizeof($recup_usermessage)==3){													
												$message=$recup_usermessage[0]." ".$recup_usermessage[2];
												$share_message=$recup_usermessage[1];
											}	
											$this->send_cmessage($url,$chatId,$message);
											if(trim($share_message)!=""){	
												$this->send_cmessage($url,$chatId,$share_message);
											}
										
										}
										
										//Un utilisateur a passer une demande de location de drone
										if($agent_message!=""){
											
											$sender_agent=array("256767615","22899595353","22891749741");
											foreach($sender_agent as $chatId){
												$this->send_cmessage($url,$chatId,$agent_message);
											}
										}
										
										//Un utilisateur a passer une commande
										if($contact_fmessage!=""){
											
											$sender_agent=array("256767615");
											foreach($sender_agent as $chatId){
												$this->send_cmessage($url,$chatId,$contact_fmessage);
											}
										}
										
										//Un utilisateur veut parler a notre operateur
										if($contact_message!=""){												
											$this->send_cmessage($url,"256767615",$contact_message);
										}
									}
								}
								
							}
					
						}
					
				}
			}
			
		}
		
		exit();
}

public function actionSet_webhook(){
	
	$response = "";
	$test_whatsapp=HistoriqueWhatsapp::find()->where(['name_param'=>'ab_telegram'])->one();
	if($test_whatsapp!=null){
		$web_hook_link = "https://abusiness.store/api/web/web_v1/telegrams/flash_abusiness?token=".Common::default_dev."_";

		$api_link = $test_whatsapp->liens_chat ;
		$token = $test_whatsapp->instance_chat.":".$test_whatsapp->token_chat;
		$url_api = $api_link.$token."/setWebhook?url=".$web_hook_link.$test_whatsapp->instance_chat;
		
		$ch = curl_init($url_api);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$result = curl_exec($ch);
		curl_close($ch);

		print_r($result);


	}
	exit;
}


}