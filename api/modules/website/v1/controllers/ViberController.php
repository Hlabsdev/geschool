<?php
namespace api\modules\website\v1\controllers;
use Yii;
use yii\rest\ActiveController;
use yii\web\Response;
use yii\web\XmlResponseFormatter;
use console\models\Ussdtransation;

use api\modules\website\v1\models\HistoriqueWhatsapp;
use api\modules\website\v1\models\MessageHistorique;
use api\modules\website\v1\models\HistoriqueWhatsappMessage;
use api\modules\website\v1\controllers\Common;
use backend\controllers\Utils;

class ViberController extends ActiveController
{
public $modelClass = 'api\modules\website\v1\models\HistoriqueWhatsappMessage';  


public function actionExeconsole($cmd_debut) {    
	$cmd = 'cd ../.. && /usr/bin/php yii consoleappinit/process '.$cmd_debut;	
	return $result = shell_exec($cmd);	
}

/*
https://chatapi.viber.com/pa/set_webhook
{"auth_token":"4c110eda55a7d363-69121ba9271d3b21-ffb2ed8ddf70b5ab","url":"https://abusiness.store/api/web/web_v1/vibers/flash_abusiness"}
*/

public function send_cmessage($format_url,$access_token,$chat_id,$content,$btn_content) {

	
	$content=trim($content);
	$texte=str_replace("<b>","",$content);
	$texte=str_replace("</b>","",$texte);	
	
	
	$datas=array();
	if(sizeof($btn_content)>0){
		$all_btn=array();
		
		foreach($btn_content as $info_btn){
			
			$btn["Text"]=$info_btn->text;
			$btn["TextSize"]="large";
			$btn["ActionType"]="reply";
			$btn["ActionBody"]=$info_btn->value;
			$all_btn[]=$btn;
		}
		
		$datas['auth_token'] = $access_token;
		$datas['min_api_version'] = 7;
		$datas['receiver'] = $chat_id;
		$datas['type'] = "text";
		$datas['text'] = $texte;
		$datas['keyboard'] = array("Type"=>"keyboard","DefaultHeight"=>true,"Buttons"=>$all_btn);
		
	}else{
		
		
		$datas['auth_token'] = $access_token;
		$datas['receiver'] = $chat_id;
		$datas['type'] = "text";
		$datas['text'] = $texte;
	}
	
	$jsonData_btn=json_encode($datas);
	
	
	
   
	$ch = curl_init();
	$curlConfig = array(
		CURLOPT_URL => $format_url,
		CURLOPT_POST => true,
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_CONNECTTIMEOUT => 5,
		CURLOPT_POSTFIELDS => $jsonData_btn
	);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
	curl_setopt_array($ch, $curlConfig);
	echo $result = curl_exec($ch);
	
	
	
}

public function send_slider($format_url,$access_token,$chat_id,$message) {

	$sliders=array();
	$nbre_btn=10; 
	$i=0;
	$u=0;
	
	$content="";
	if(isset($message->type) && $message->type=="boutique"){
		
		foreach($message->information as $recup){	
				
			$btn_info=[["type"=>"postback","title"=>"Visiter","payload"=>$recup->code]];
			$sliders[$u][]=["title"=>$recup->denomination,"image_url"=>Common::photobase_url.$recup->photo,"subtitle"=>$recup->description."\n\n".$recup->adresse,"buttons"=>$btn_info];
			
			$i++;	
			if($i==$nbre_btn){
				$i=0;
				$u++;
			}	
		}	
		
	}else if(isset($message->type) && $message->type=="produit"){
		
		$content="<b>Bienvenue chez ".$message->information[0]->marchand."</b>";
		$content.="\n\nVeuillez taper le numéro des produits à commander en les séparant par des <b>virgules</b> .Pour ajouter plusieurs quantités faire Numéro * quantité (Exemple : <b>1*2</b> , <b>2*3</b> )";
		foreach($message->information as $recup){							
			$sliders[$u][]=["title"=>$recup->code.": ".$recup->denomination,"image_url"=>Common::photobase_url.$recup->photo,"subtitle"=>$recup->qte_vente.' à '.$recup->prix_vente];			
			$i++;	
			if($i==$nbre_btn){
				$i=0;
				$u++;
			}	
		}	
		
	}else if(isset($message->type) && $message->type=="my_produit"){
		
		//$content="<b>Bienvenue chez ".$message->information[0]->marchand."</b>";
		foreach($message->information as $recup){		
			$btn_info=[["type"=>"postback","title"=>"Selectionner","payload"=>$recup->code]];
			$sliders[$u][]=["title"=>$recup->denomination,"image_url"=>Common::photobase_url.$recup->photo,"subtitle"=>$recup->qte_vente.' à '.$recup->prix_vente,"buttons"=>$btn_info];			
			$i++;	
			if($i==$nbre_btn){
				$i=0;
				$u++;
			}	
		}	
		
	}
	
	if($content!=""){
		$this->send_cmessage($format_url,$access_token,$chat_id,$content,array()); 
	}
	
	$u=0;
	foreach($sliders as $info_slider ){
				
		$tab_field=array();
		$tab_field["recipient"]=["id"=>"".$chat_id.""];	
		$tab_field["message"]=["attachment"=>["type"=>"template","payload"=>["template_type"=>"generic","elements"=>$info_slider]]];				

		$jsonData=json_encode($tab_field);
		
		$ch = curl_init($format_url);
		curl_setopt($ch, CURLOPT_POST,1);
		curl_setopt($ch, CURLOPT_POSTFIELDS,$jsonData);
		curl_setopt($ch, CURLOPT_HTTPHEADER,array('Content-Type: application/json'));
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,false);
		$result = curl_exec($ch);
		curl_close($ch);
	
		$u++;
	}		
		
}

public function send_cfmessage($format_url,$access_token,$chat_id,$url_object,$field_object,$content,$btn_content) {

	$content=trim($content);
	$texte=str_replace("<b>","",$content);
	$texte=str_replace("</b>","",$texte);	
	
	
	$datas=array();
	if(sizeof($btn_content)>0){
		$all_btn=array();
		
		foreach($btn_content as $info_btn){
			
			$btn["Text"]=$info_btn->text;
			$btn["TextSize"]="large";
			$btn["ActionType"]="reply";
			$btn["ActionBody"]=$info_btn->value;
			$all_btn[]=$btn;
		}
		
		$datas['auth_token'] = $access_token;
		$datas['receiver'] = $chat_id;
		$datas['type'] = "picture";
		$datas['text'] = $texte;
		$datas['media'] = $url_object;
		$datas['keyboard'] = array("Type"=>"keyboard","DefaultHeight"=>true,"Buttons"=>$all_btn);
		
	}else{
		
		
		$datas['auth_token'] = $access_token;
		$datas['receiver'] = $chat_id;
		$datas['type'] = "picture";
		$datas['text'] = $texte;
		$datas['media'] = $url_object;
		
	}
	
	$jsonData_btn=json_encode($datas);
	$ch = curl_init();
	$curlConfig = array(
		CURLOPT_URL => $format_url,
		CURLOPT_POST => true,
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_CONNECTTIMEOUT => 5,
		CURLOPT_POSTFIELDS => $jsonData_btn
	);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
	curl_setopt_array($ch, $curlConfig);
	echo $result = curl_exec($ch);
	
	
	
	
	
	
	
}

public function actionFlash_abusiness() {
			
		
		if (Yii::$app->request->post()) {
			$decoded=Yii::$app->request->post();
		}else{
				
			$information=file_get_contents('php://input');	
			$decoded = json_decode($information,true);			
		}
		/*
		$new_cmessage=new HistoriqueWhatsappMessage ;
		$new_cmessage->sender="VIBER";
		$new_cmessage->datesend=time();
		$new_cmessage->body=json_encode($decoded);
		$new_cmessage->type=40;
		$new_cmessage->save();
		*/
		$btn_content=array();
		
		$test_rs=HistoriqueWhatsapp::find()->where(['name_param'=>'ab_viber'])->one();
		if($test_rs!=null){
			$star_content=array("ab","start");
									
			
				if( isset($decoded["event"]) && trim($decoded["event"])=="message" && isset($decoded["sender"]["id"]) ){
					
				
							
						$recup_message=$decoded['message'];		
						//verifier si ce message a ete deja recu et traite 						
					    
						$content="";						
						/*if(isset($recup_message['message']['quick_reply']['payload'])){
							
							$content="bot_phone/".trim($recup_message['message']['quick_reply']['payload']);
							
						}else */
						if(isset($recup_message['type']) && isset($recup_message['text']) && trim($recup_message['type'])=="text"){
							
							$content=trim($recup_message['text']);
							
						}
						/*else if(isset($recup_message['postback']['payload'])){
							
							$content=$recup_message['postback']['payload'];
							
						}else if(isset($recup_message['message']['attachments'][0]["payload"]["coordinates"])){
							
							$lat=$recup_message['message']['attachments'][0]["payload"]["coordinates"]["lat"];
							$long=$recup_message['message']['attachments'][0]["payload"]["coordinates"]["long"];
							$content=$lat.";".$long;
						}else if(isset($recup_message['message']['attachments'][0]["payload"]["url"])){
							
							$url_photo=$recup_message['message']['attachments'][0]["payload"]["url"];
							$content=str_replace("\"","",$url_photo);
							
						}*/
						
						
						if($content!=""){
							
								
							$username=$decoded["sender"]["id"];
							$chatId=$username;
						
						
												
							$test_rs->date_operation=date("Y-m-d H:i:s");
							$test_rs->total_message_chat++;
							$test_rs->save();
							
							if($chatId != $test_rs->instance_chat){
								
								
								$tps_send=(int)trim($decoded['timestamp']/1000);
								$type=30;
								
								$test_contentwhatsapp=HistoriqueWhatsappMessage::find()->where(['sender'=>$chatId,'datesend'=>$tps_send,'body'=>json_encode($decoded),'type'=>$type])->one();
								
								if($test_contentwhatsapp==null){
								
									$new_cmessage=new HistoriqueWhatsappMessage ;
									$new_cmessage->sender=(string)$chatId;
									$new_cmessage->datesend=$tps_send;
									$new_cmessage->body=json_encode($decoded);
									$new_cmessage->type=$type;
									if($new_cmessage->save()){
										
										
											
											$convert_content=strtolower(str_replace(" ","",$content));
											$convert_content=str_replace("*","",$convert_content);
											$convert_content=str_replace("/","",$convert_content);
											
											
											$content=str_replace("\n\r", '. ', $content);
											$content=str_replace("\n", '. ', $content);
											$content=str_replace("\r", '. ', $content);
											
											$direct=false;
											$test_agent=explode("ABCOLLECTOR",$content);
											if(sizeof($test_agent)==3) $direct=true;
											
									
											$message="";
											$type_response="0";
											$sessionid=$tps_send."_".$chatId;
											
											if(in_array($convert_content,$star_content) || $direct==true){
											
										
												//fermer toutes les sessions ouvertes
												Ussdtransation::updateAll(['etat_transaction' => 3], 'username= "'.$chatId.'" and reference="'.Common::viber_ref.'" and etat_transaction!=1');
												
												//demarrer un nouveau dialogue
												$sessionid=$tps_send."_".$chatId;
												$nom="";
												$prenom="";
												$response="1";
												if($direct==true)$response=(int)$test_agent[1];
												$command='-t="'.$chatId.'" -r="'.$response.'" -n="'.$nom.'" -f="'.$prenom.'" -s="0" -i="'.$sessionid.'" -p="'.Common::viber_ref.'"';
												
												$response=$this->actionExeconsole($command);		
												$recup=json_decode($response);									
												if(isset($recup->type))$type_response=$recup->type;
												if(isset($recup->response))$message=$recup->response;
												if(isset($recup->btncontent))$btn_content=$recup->btncontent;
												
												
												
											}else{
												
												$run=false;
												$test_direct=explode("ABSELLER",$content);
												if(sizeof($test_direct)==3) $run=true;
													
								
												//verifier si cet user a une session ouverte
												$find_transaction = Ussdtransation::find()->where(['username'=>$chatId,'reference'=>Common::viber_ref,'etat_transaction'=>[0,2]])->orderby(['id'=>SORT_DESC])->one();
												if($find_transaction!=null || $run==true){
													
													if($run==true){
														Ussdtransation::updateAll(['etat_transaction' => 3], 'username= "'.$chatId.'" and reference="'.Common::viber_ref.'" and etat_transaction!=1');
														$sessionid=$tps_send."_".$chatId;
														if($find_transaction!=null){
															$sessionid=$find_transaction->idtransaction;
														}
														
														$command='-t="'.$chatId.'" -r="'.$content.'" -i="'.$sessionid.'" -s="0" -p="'.Common::viber_ref.'"';
														
														$response=$this->actionExeconsole($command);	

														$recup=json_decode($response);									
														if(isset($recup->type))$type_response=$recup->type;
														if(isset($recup->response))$message=$recup->response;
														if(isset($recup->btncontent))$btn_content=$recup->btncontent;
														
												
													}else if($tps_send-$find_transaction->last_update < (600*3)){
														
														$find_transaction->last_update=(int)$tps_send;
														$find_transaction->relance=0;
														$find_transaction->save();
														
														$sessionid=$find_transaction->idtransaction;
														$command='-t="'.$chatId.'" -r="'.$content.'" -i="'.$sessionid.'" -s="1" -p="'.Common::viber_ref.'"';
														
														$response=$this->actionExeconsole($command);	

														$recup=json_decode($response);									
														if(isset($recup->type))$type_response=$recup->type;
														if(isset($recup->response))$message=$recup->response;
														if(isset($recup->btncontent))$btn_content=$recup->btncontent;
														
													}else{
														
														$message="Votre dernière session a expiré.\n\nPour demarrer une discussion tapez: <b>AB</b> ";
														
													}
													
												}else{								
													$message="Bienvenue sur <b>ABusiness</b>, votre chatBot e-commerce de produits de première nécessité\n\nPour démarrer une discussion tapez: <b>AB</b>";
												}
												
											}
											
											
											if($message!=""){
												
												$url =  $test_rs->liens_chat;
												$access_token = $test_rs->token_chat;
												
												$access_user=array();
												if(in_array($chatId,$access_user)){												
													$message="Service Temporairement indisponible\n\nVeuillez réessayer plus tard\nMerci";										
													$this->send_cmessage($url,$access_token,$chatId,$message,$btn_content);
												}
												else{
													
													if(is_array($message) || is_object($message)){														
														
														$this->send_slider($url,$access_token,$chatId,$message);
														
													}
													else{
														
														$message=str_replace("Erreur:","<b>Erreur:</b> \n",$message);
														$agent_message="";
														$contact_message="";
														$contact_fmessage="";
														
														$recup_message=explode("#####",$message);
														if(sizeof($recup_message)==3){
															
															$message=$recup_message[0]."".$recup_message[2];
															$agent_message=$recup_message[1];
														}
														
														$recup_cmessage=explode("CONT_ACT",$message);
														if(sizeof($recup_cmessage)==3){
															
															$message=$recup_cmessage[0]."".$recup_cmessage[2];
															$contact_message=$recup_cmessage[1];
														}
														
														$recup_fmessage=explode("LIVRA_ISON",$message);
														if(sizeof($recup_fmessage)==3){
															
															$message=$recup_fmessage[0]."".$recup_fmessage[2];
															$contact_fmessage=$recup_fmessage[1];
														}
														
																												
														//verifier si cest un message avec image ou si cest un simple message
														$recup_message0=explode("WA_IMAGE",$message);
														$recup_message1=explode("WA_VIDEO",$message);
														if(sizeof($recup_message0)==3){
															
															$new_name=str_replace("location.jpeg","messelocation.jpeg",$recup_message0[1]);
															$message=$recup_message0[0]."".$recup_message0[2];
															$file_url=Common::photobase_url.$new_name; 
															
															$this->send_cfmessage($url,$access_token,$chatId,$file_url,'photo',$message,$btn_content);
															
														}else if(sizeof($recup_message1)==3){
															
															//$urlvideo =  $test_rs->liens_chat.$test_rs->instance_chat.":".$test_rs->token_chat."/sendVideo";		
															$message=$recup_message1[0]."".$recup_message1[2];
															$file_url=Common::photobase_url.$recup_message1[1]; 
															
															$this->send_cfmessage($url,$access_token,$chatId,$file_url,'video',$message,$btn_content);
															
														}else{
															
															$recup_usermessage=explode("GOOD_DIAGNOSTIC",$message);
																
															$share_message="";
															if(sizeof($recup_usermessage)==3){													
																$message=$recup_usermessage[0]." ".$recup_usermessage[2];
																$share_message=$recup_usermessage[1];
															}	
															$this->send_cmessage($url,$access_token,$chatId,$message,$btn_content);
															if(trim($share_message)!=""){	
																$this->send_cmessage($url,$access_token,$chatId,$share_message,$btn_content);
															}
														
														}
														
																												
														//Un utilisateur a passer une demande de location de drone
														if($agent_message!=""){													
															$sender_agent=array("MmrS22G7n6MPplVeCpBN8Q==");
															foreach($sender_agent as $chatId){
																$this->send_cmessage($url,$access_token,$chatId,$agent_message,array());
															}
														}
														
														//Un utilisateur a passer une commande
														if($contact_fmessage!=""){
															
															$sender_agent=array("MmrS22G7n6MPplVeCpBN8Q==");
															foreach($sender_agent as $chatId){
																$this->send_cmessage($url,$access_token,$chatId,$contact_fmessage,array());
															}
														}
														
														//Un utilisateur veut parler a notre operateur
														if($contact_message!=""){												
															$this->send_cmessage($url,$access_token,"MmrS22G7n6MPplVeCpBN8Q==",$contact_message,array());
														}
														
													}
												}
											}
											
										
									}
							
								}
								
							}
						}
						
				
			   }
			
		}
		exit();
}


}