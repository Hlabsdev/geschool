<?php
namespace api\modules\website\v1\controllers;
header("Access-Control-Allow-Origin: *");
use Yii;
use yii\rest\ActiveController;
use yii\web\Response;
use yii\web\XmlResponseFormatter;
use console\models\Ussdtransation;

use api\modules\website\v1\models\HistoriqueWhatsapp;
use api\modules\website\v1\models\MessageHistorique;
use api\modules\website\v1\models\HistoriqueWhatsappMessage;
use api\modules\website\v1\controllers\Common;

class WebsiteController extends ActiveController
{
public $modelClass = 'api\modules\website\v1\models\HistoriqueWhatsappMessage';  

//ALTER TABLE `user` ADD `validation_code` VARCHAR(255) NULL AFTER `prenoms`; 
//ALTER TABLE `user` ADD `web_key` VARCHAR(255) NULL AFTER `canal_key`; 
	
public function actionExeconsolev($cmd) {
	  
		
    $get_url=yii::$app->basePath;   
	$base_url=explode(DIRECTORY_SEPARATOR."api",$get_url)[0];  
	chdir($base_url);
	
	$command = 'yii consoleappinit/process '.$cmd;
	
	$handler = popen($command, 'r');
	
	$output = '';
	while (!feof($handler)) {
		$output .= fgets($handler);
	}
	$output = trim($output);
	$status = pclose($handler);
	return $output;
	
		
	
}

public function actionExeconsole($cmd_debut) {    
	$cmd = 'cd ../.. && /usr/bin/php yii consoleappinit/process '.$cmd_debut;	
	return $result = shell_exec($cmd);	
}

public function send_cmessage($phone,$content) {

	$test_whatsapp=HistoriqueWhatsapp::find()->where(['name_param'=>'info_api'])->one();
	$format_url =$test_whatsapp->liens_chat."/".$test_whatsapp->instance_chat."/sendMessage?token=".$test_whatsapp->token_chat;
	
	$ch = curl_init();
	$curlConfig = array(
	  CURLOPT_URL            => $format_url,
	  CURLOPT_POST           => true,
	  CURLOPT_RETURNTRANSFER => true,
	  CURLOPT_CONNECTTIMEOUT => 5,
	  CURLOPT_POSTFIELDS     => array(
		'chatId' => $phone."@c.us",
		'phone' => $phone,
		'body' => $content,
	  ),
	);
	curl_setopt_array($ch, $curlConfig);
	$result = curl_exec($ch);
	curl_close($ch);	
}

public function actionFlash_abusiness() {
		
		if (Yii::$app->request->post()) {
			$decoded=Yii::$app->request->post();
		}else{
			
			$decoded=Yii::$app->request->get();
			if(sizeof($decoded)==1){	
				$information=file_get_contents('php://input');	
				$decoded = json_decode($information,true);	
			}
		}
		
		$auth_key=$_GET['token'];
		$chatId="web".rand(100000,999999);
		
		
		$test_rs=HistoriqueWhatsapp::find()->where(['name_param'=>'ab_webchat'])->one();
		$message="";
		$type_response="0";
		$btn_content="";
		if($test_rs!=null){
			
			
			$star_content=array("ab","start");
			
			if(isset($decoded["token_id"]) && isset($decoded["message"])){
								
				$traitement_key=explode("_",$auth_key);
				if(sizeof($traitement_key)==2 && $traitement_key[0]==Common::default_dev && $traitement_key[1]==$test_rs->instance_chat){	
						
					
						$test_rs->date_operation=date("Y-m-d H:i:s");
						$test_rs->total_message_chat++;
						$test_rs->save();	
							
						
						//verifier si ce message a ete deja recu et traite 						
					    $content=$decoded['message'];
						$chatId=$decoded['token_id'];
						$username="";
						
						$content=str_replace("\/","/",$content);
						if(strpos($content,"abusiness/acteurs/") !== false){
							$recup_explode=explode("abusiness/acteurs/",$content);
							if(sizeof($recup_explode)>=2){
								//$content="http://localhost/manage_clin/go_agri/abusiness/acteurs/".str_replace("'>","",$recup_explode[1]);
								$content="https://abusiness.store/abusiness/acteurs/".str_replace("'>","",$recup_explode[1]);
							}
						}
						
						
						
						$tps_send=time();
						$type=20;
						
						$test_contentwhatsapp=HistoriqueWhatsappMessage::find()->where(['sender'=>$chatId,'datesend'=>$tps_send,'body'=>$content,'type'=>$type])->one();
						
						if($test_contentwhatsapp==null){
							
							$message= "Désolé, une erreur est survenue, veuillez réessayer plus tard";
						
							$new_cmessage=new HistoriqueWhatsappMessage ;
							$new_cmessage->sender=(string)$chatId;
							$new_cmessage->datesend=$tps_send;
							$new_cmessage->body=json_encode($decoded);
							$new_cmessage->type=$type;
							if($new_cmessage->save()){
								
								$convert_content=strtolower(str_replace(" ","",$content));
								$convert_content=str_replace("*","",$convert_content);
								$convert_content=str_replace("/","",$convert_content);
								
								
								$content=str_replace("\n\r", '. ', $content);
								$content=str_replace("\n", '. ', $content);
								$content=str_replace("\r", '. ', $content);
								
								$direct=false;
								$test_agent=explode("ABCOLLECTOR",$content);
								if(sizeof($test_agent)==3) $direct=true;
								
						
								
								if(in_array($convert_content,$star_content) || $direct==true){
									
								
									//fermer toutes les sessions ouvertes
									Ussdtransation::updateAll(['etat_transaction' => 3], 'username= "'.$chatId.'" and reference="'.Common::website_ref.'" and etat_transaction!=1');
									
									//demarrer un nouveau dialogue
									$sessionid=$tps_send."_".$chatId;
									$nom="";
									$prenom="";
									$response="1";
									if($direct==true)$response=(int)$test_agent[1];
									$command='-t="'.$chatId.'" -r="'.$response.'" -n="'.$nom.'" -f="'.$prenom.'" -s="0" -i="'.$sessionid.'" -p="'.Common::website_ref.'"';
									
									$response=$this->actionExeconsole($command);		
									$recup=json_decode($response);									
									if(isset($recup->type))$type_response=$recup->type;
									if(isset($recup->response))$message=$recup->response;
									if(isset($recup->btncontent))$btn_content=$recup->btncontent;
									
									
									
								}else{
									
									$run=false;
									$test_direct=explode("ABSELLER",$content);
									if(sizeof($test_direct)==3) $run=true;
										
					
									//verifier si cet user a une session ouverte
									//$find_transaction = Ussdtransation::findOne(['username'=>$chatId,'reference'=>Common::website_ref,'etat_transaction'=>0]);
									$find_transaction = Ussdtransation::find()->where(['username'=>$chatId,'reference'=>Common::website_ref,'etat_transaction'=>[0,2]])->orderby(['id'=>SORT_DESC])->one();
									if($find_transaction!=null || $run==true){
										
										if($run==true){
											Ussdtransation::updateAll(['etat_transaction' => 3], 'username= "'.$chatId.'" and reference="'.Common::website_ref.'" and etat_transaction!=1');
											$sessionid=$tps_send."_".$chatId;
											if($find_transaction!=null){
												$sessionid=$find_transaction->idtransaction;
											}
											
											$command='-t="'.$chatId.'" -r="'.$content.'" -i="'.$sessionid.'" -s="0" -p="'.Common::website_ref.'"';
											
											$response=$this->actionExeconsole($command);		
											$recup=json_decode($response);									
											if(isset($recup->type))$type_response=$recup->type;
											if(isset($recup->response))$message=$recup->response;
											if(isset($recup->btncontent))$btn_content=$recup->btncontent;
									
										}else if($tps_send-$find_transaction->last_update < (600*3)){
											
											$find_transaction->last_update=(int)$tps_send;
											$find_transaction->relance=0;
											$find_transaction->save();
											
											$sessionid=$find_transaction->idtransaction;
											$command='-t="'.$chatId.'" -r="'.$content.'" -i="'.$sessionid.'" -s="1" -p="'.Common::website_ref.'"';
											
											$response=$this->actionExeconsole($command);	

											
											$recup=json_decode($response);									
											if(isset($recup->type))$type_response=$recup->type;
											if(isset($recup->response))$message=$recup->response;
											if(isset($recup->btncontent))$btn_content=$recup->btncontent;
											
										}else{
											
											$message="Votre dernière session a expiré.\n\nPour demarrer une discussion tapez: <b>AB</b> ";
											
										}
									}else{								
										$message="Bienvenue sur <b>ABusiness</b>, votre chatBot e-commerce de produits de première nécessité\n\nPour démarrer une discussion tapez: <b>AB</b>";
									}
									
								}
							
								if(is_array($message) || is_object($message)){
									$id=rand(10000,99999);
									$new_id="carouselExampleIndicators".$id;
									
									if(isset($message->type) && $message->type=="boutique"){
									
											$ol_info='';
											$content_info='';
											$active='active';
											$position=0;
											foreach($message->information as $recup){
													if($position>0){
														$active="";
													}
													$ol_info.='<li data-target="#'.$new_id.'" data-slide-to="'.$position.'" ></li>';
													$content_info.='<div class="carousel-item '.$active.'">
																	<img class="d-block w-100" style="width:200px!important;height:200px"  src="'.Common::photobase_url.$recup->photo.'" alt="'.$recup->denomination.'">
																	<div class="carousel-caption d-none d-md-block" style="position:relative!important;right:0px!important;bottom:30px!important;left:0px!important;top:30px!important">
																	  <h5 style="color:#169E37;">'.$recup->denomination.'</h5> 	
																	  <p style="color:black;font-size:12px!important;text-align:left">'.$recup->description.'<br/><br/>'.$recup->adresse.'</p>
																	  <button style="padding: 7px 12px;border: 1px solid rgb(16, 182, 52);display: inline-block;margin-bottom: 10px;background: #fff;color: #169e37;cursor: pointer;border-radius: 20px;font-size: 0.9rem;" onclick="direct_send_message(\''.$recup->code.'\')">Visiter</button>
																	</div>
																</div>';
																											
												$position++;
											}
											$message='<div id="'.$new_id.'" class="carousel slide" data-ride="carousel" style="width:100%;height:470px" >';
											$message.='		<ol class="carousel-indicators">'.$ol_info.'</ol>';
											$message.='		<div class="carousel-inner" >'.$content_info.'</div>';									
											$message.='		<a class="carousel-control-prev" href="#'.$new_id.'" role="button" data-slide="prev" >
																<span class="carousel-control-prev-icon" aria-hidden="true" style="background-color:#169E37"></span>
																<span class="sr-only">Previous</span>
															</a>
															<a class="carousel-control-next" href="#'.$new_id.'" role="button" data-slide="next">
																<span class="carousel-control-next-icon" aria-hidden="true" style="background-color:#169E37"></span>
																<span class="sr-only">Next</span>
															</a>
													</div>';
									}else if(isset($message->type) && $message->type=="my_produit"){
									
											$ol_info='';
											$content_info='';
											$active='active';
											$position=0;
											foreach($message->information as $recup){
													if($position>0){
														$active="";
													}
													$ol_info.='<li data-target="#'.$new_id.'" data-slide-to="'.$position.'" ></li>';
													$content_info.='<div class="carousel-item '.$active.'">
																	<img class="d-block w-100" style="width:200px!important;height:200px"  src="'.Common::photobase_url.$recup->photo.'" alt="'.$recup->denomination.'">
																	<div class="carousel-caption d-none d-md-block" style="position:relative!important;right:0px!important;bottom:30px!important;left:0px!important;top:30px!important">
																	  <h5 style="color:#169E37;">'.$recup->denomination.'</h5> 	
																	  <p style="color:black;font-size:12px!important;text-align:left">'.$recup->qte_vente.' à '.$recup->prix_vente.'</p>
																	  <button style="padding: 7px 12px;border: 1px solid rgb(16, 182, 52);display: inline-block;margin-bottom: 10px;background: #fff;color: #169e37;cursor: pointer;border-radius: 20px;font-size: 0.9rem;" onclick="direct_send_message(\''.$recup->code.'\')">Selectionnez</button>
																	</div>
																</div>';
																											
												$position++;
											}
											$message='<div id="'.$new_id.'" class="carousel slide" data-ride="carousel" style="width:100%;height:400px" >';
											$message.='		<ol class="carousel-indicators">'.$ol_info.'</ol>';
											$message.='		<div class="carousel-inner" >'.$content_info.'</div>';									
											$message.='		<a class="carousel-control-prev" href="#'.$new_id.'" role="button" data-slide="prev" >
																<span class="carousel-control-prev-icon" aria-hidden="true" style="background-color:#169E37"></span>
																<span class="sr-only">Previous</span>
															</a>
															<a class="carousel-control-next" href="#'.$new_id.'" role="button" data-slide="next">
																<span class="carousel-control-next-icon" aria-hidden="true" style="background-color:#169E37"></span>
																<span class="sr-only">Next</span>
															</a>
													</div>';
									}else if(isset($message->type) && $message->type=="produit"){
												
										
											$ol_info='';
											$id_input=$id;
											$content_info='';
											$active='active';
											$position=0;
											foreach($message->information as $recup){
													if($position>0){
														$active="";
													}
													$ol_info.='<li data-target="#'.$new_id.'" data-slide-to="'.$position.'" ></li>';
													$content_info.='<div class="carousel-item '.$active.'">
																	<img class="d-block w-100" style="width:200px!important;height:200px"  src="'.Common::photobase_url.$recup->photo.'" alt="'.$recup->denomination.'">
																	<div class="carousel-caption d-none d-md-block" style="height:200px;position:relative!important;right:0px!important;bottom:30px!important;left:0px!important;top:30px!important">
																	  <h5 style="color:#169E37;">'.$recup->denomination.'</h5> 	
																	  <p style="color:black;font-size:12px!important;text-align:left">'.$recup->qte_vente.' à '.$recup->prix_vente.'</p>
																	  <div style="width:100%">
																		  <div style="width:50%;float:left">
																			  <button id="btn_'.$new_id.'_'.$recup->code.'" class="produit_add" onclick="select_produit(\''.$new_id.'_'.$recup->code.'\')">Ajouter</button>
																		  </div>
																		  <div style="width:50%;float:right">
																				<input style="width:70%!important" onkeyup="test_value(\'qte_'.$new_id.'_'.$recup->code.'\')" class="form-control" type="number" value="1" id="qte_'.$new_id.'_'.$recup->code.'" name="qte_'.$new_id.'_'.$recup->code.'">
																		  </div>
																	  </div>
																	</div>
																</div>';
																											
												$position++;
												$id_input++;
											}
											$message='<div id="'.$new_id.'" class="carousel slide" data-ride="carousel" style="width:100%;height:470px" >';
											$message.='		<ol class="carousel-indicators">'.$ol_info.'</ol>';
											$message.='		<div class="carousel-inner" >'.$content_info.'</div>';									
											$message.='		<a class="carousel-control-prev" href="#'.$new_id.'" role="button" data-slide="prev" >
																<span class="carousel-control-prev-icon" aria-hidden="true" style="background-color:#169E37"></span>
																<span class="sr-only">Previous</span>
															</a>
															<a class="carousel-control-next" href="#'.$new_id.'" role="button" data-slide="next">
																<span class="carousel-control-next-icon" aria-hidden="true" style="background-color:#169E37"></span>
																<span class="sr-only">Next</span>
															</a>
													</div><input type="hidden" value="" id="all_'.$new_id.'" name="all_'.$new_id.'">';
											$message.='<button class="produit_add" onclick="direct_buy(\''.$new_id.'\')">Acheter</button>';
										
									}else{
										$message="Service Temporairement indisponible\n\nVeuillez réessayer plus tard\nMerci";
									}
								
								}else if($message!=""){
					
									$access_user=array();
									if(1 !== 1){												
										$message="Service Temporairement indisponible\n\nVeuillez réessayer plus tard\nMerci";
									}
									else{
											
											$message=str_replace("Erreur:","<b>Erreur:</b> <br/>",$message);
											$message=str_replace("\n","<br/>",$message);
											
											
											$agent_message="";
											$contact_message="";
											$contact_fmessage="";
											
											$recup_message=explode("#####",$message);
											if(sizeof($recup_message)==3){
												
												$message=$recup_message[0]."".$recup_message[2];
												$agent_message=$recup_message[1];
												
												$agent_message=str_replace("<br/>","\n",$agent_message);
												$agent_message=str_replace("<b>","*",$agent_message);
												$agent_message=str_replace("</b>","*",$agent_message);
											}
											
											$recup_cmessage=explode("CONT_ACT",$message);
											if(sizeof($recup_cmessage)==3){
												
												$message=$recup_cmessage[0]."".$recup_cmessage[2];
												$contact_message=$recup_cmessage[1];
												
												$contact_message=str_replace("<br/>","\n",$contact_message);
												$contact_message=str_replace("<b>","*",$contact_message);
												$contact_message=str_replace("</b>","*",$contact_message);
											}
											
											$recup_fmessage=explode("LIVRA_ISON",$message);
											if(sizeof($recup_fmessage)==3){
												
												$message=$recup_fmessage[0]."".$recup_fmessage[2];
												$contact_fmessage=$recup_fmessage[1];
												
												$contact_fmessage=str_replace("<br/>","\n",$contact_fmessage);
												$contact_fmessage=str_replace("<b>","*",$contact_fmessage);
												$contact_fmessage=str_replace("</b>","*",$contact_fmessage);
											}
											
											
											
											//verifier si cest un message avec image ou si cest un simple message
											$recup_message=explode("WA_IMAGE",$message);
											$recup_message1=explode("WA_VIDEO",$message);
											if(sizeof($recup_message)==3){
												
												if(strpos($recup_message[1],"location.jpeg") !== false){
													$type_response=2;
												}
												$new_name=str_replace("location.jpeg","telelocation.jpeg",$recup_message[1]);	
												
												$recup=$recup_message[0]."".$recup_message[2];
												$file_url=Common::photobase_url.$new_name; 
												
												$message="<img src='".$file_url."' style='width:200px;height:200px'><br/><br/>".$recup;
												
											}else if(sizeof($recup_message1)==3){
												
												
												$new_name=$recup_message1[1];	
												
												$recup=$recup_message1[0]."".$recup_message1[2];
												$file_url=Common::photobase_url.$new_name; 
												
												
												$extension=explode(".",$new_name)[1];
															
												if($extension=="ogg"){
													$message ="<video width='200px' height='200px' controls><source src='".$file_url."' type='video/ogg'>-----</video>";
												}else{
													$message ="<video width='200px' height='200px' controls><source src='".$file_url."' type='video/mp4'>-----</video>";
													
												}
															
												$message.="<br/><br/>".$recup;
												
											}else{
												$recup_usermessage=explode("GOOD_DIAGNOSTIC",$message);
													
												$share_message="";
												if(sizeof($recup_usermessage)==3){													
													$message=$recup_usermessage[0]." ".$recup_usermessage[2];
													$share_message="<br/><br/><br/>".$recup_usermessage[1];
												}
												
												$message.=$share_message;
											
											}
											
											//Un utilisateur a passer une demande de location de drone
											if($agent_message!=""){
												
												$sender_agent=array("22892929285","22899595353","22891749741");
												foreach($sender_agent as $phone){
													$this->send_cmessage($phone,$agent_message);
												}
											}
											
											//Un utilisateur a passer une commande
											if($contact_fmessage!=""){												
												$this->send_cmessage("22892929285",$contact_fmessage);
											}
											
											//Un utilisateur veut parler a notre operateur
											if($contact_message!=""){												
												$this->send_cmessage("22892929285",$contact_message);
											}
								
											
											
											if(strpos($message,"Envoyez une photo") !== false || strpos($message,"Envoyez la nouvelle photo") !== false){
													$type_response=3;
											}
									}
								}
								
							}
					
						}else{
							$message= "yesoooo";
						}
					
						
				}else{
					$message= "Vous n'êtes pas autorisé à acceder à la page";
				}
			}else{
				$message= "Vous n'êtes pas autorisé à acceder à la page";
			}
			
		}else{
			
			$message= "Service Temporairement indisponible\n\nVeuillez réessayer plus tard\nMerci";
		}
		
		echo json_encode(["type_response"=>$type_response,"response"=>$message,"btn_content"=>$btn_content]);
		
		exit();
}


}