<?php

namespace api\modules\geschool\v1\controllers;
use api\modules\geschool\v1\models\Anneescolaire;
use Yii;
use api\modules\geschool\v1\models\Jourferie;
use yii\db\Query;
use yii\rest\ActiveController;
use api\modules\geschool\v1\models\User;
use api\modules\geschool\v1\models\SchoolCenter;

/**
 * Geschool Controller API
 *
 */

class JourferieController extends ActiveController
{
    public $modelClass = 'api\modules\geschool\v1\models\Jourferie';

    #000 -> Tout est bien
    #001 -> Message client
    #002 -> Message dev


    //Cette méthode nous permet de récupérer les jours fériés d'un centre
    public function actionJourferies()
    {
        $token = Utils::getApiKey();
        if (Yii::$app->request->post()) {
            if (isset($_POST['token']) && $_POST['token'] == $token) {
                if (isset($_POST['user_auth_key']) && $_POST['user_auth_key'] != '') {
                    $user_auth_key = $_POST['user_auth_key'];
                    $user_exist = User::find()->where(['auth_key' => $user_auth_key, 'status' => 10])->one();
                    if ($user_exist != '') {

                        if (isset($_POST['id_center']) && $_POST['id_center'] != '') {
                            $id_center = $_POST['id_center'];

                            $center = SchoolCenter::find()->where(['id_center' => $id_center, 'etat' => [1, 2, 4]])->one();
                            if ($center != '') {
                                $cont = false;
                                $user_id_center = str_replace('-', '', $user_exist->id_center);
                                $test_center = explode(",", $user_id_center);

                                if ($user_exist->id_center == "1") {
                                    $cont = true;
                                } else if (in_array($id_center, $test_center)) {
                                    $cont = true;
                                }

                                if ($cont == true) {

                                    $manage_jourferie = Utils::have_accessapi("manage_jourferie", $user_exist->idP, $user_exist->id, $id_center);
                                    $test = explode('_', $manage_jourferie);
                                    if ($test[1] == '1') { // INDICE : 0 pour CREATE; 1 pour READ; 2 pour UPDATE; 3 pour DELETE;

                                        //On vérifie l'année scolaire
                                        if(isset($_POST['annee_scolaire_key']) && $_POST['annee_scolaire_key'] != ''){
                                            $keyAnneescolaire = $_POST['annee_scolaire_key'];
                                            $anneeScolaire = Anneescolaire::find()->where(['idcentre'=>$center->id_center,'keyanneescolaire'=>$keyAnneescolaire,'status' => [1, 2]])->one();

                                            if($anneeScolaire != '' or $anneeScolaire != null){
                                                $data = Jourferie::find()->where(['idannee'=>$anneeScolaire->id,'status' => [1, 2]])->orderBy(['datejourferie' => SORT_DESC])->all();

                                                if (sizeof($data) > 0) {
                                                    return [
                                                        'status' => '000',
                                                        'message' => 'succes_get_jour_ferie',
                                                        'data' => $data
                                                    ];
                                                } else {
                                                    return [
                                                        'status' => '001',
                                                        'message' => 'empty_jour_ferie',
                                                    ];
                                                }
                                            }else{
                                                return [
                                                    'status' => '001',
                                                    'message' => 'annee_not_found',
                                                ];
                                            }
                                        }else{
                                            //Par défaut on prend l'année scolaire en cours du centre
                                            $anneeEncours = Anneescolaire::find()->where(['idcentre'=>$center->id_center,'status' =>1])->one();
                                            if($anneeEncours != '' or $anneeEncours != null){
                                                $data = Jourferie::find()->where(['idannee'=>$anneeEncours->id,'status' => [1, 2]])->orderBy(['datejourferie' => SORT_DESC])->all();

                                                if (sizeof($data) > 0) {
                                                    return [
                                                        'status' => '000',
                                                        'message' => 'succes_get_jour_ferie',
                                                        'data' => $data
                                                    ];
                                                } else {
                                                    return [
                                                        'status' => '001',
                                                        'message' => 'empty_jour_ferie',
                                                    ];
                                                }

                                            }else{
                                                return [
                                                    'status' => '001',
                                                    'message' => 'annee_encour_invalid',
                                                ];
                                            }
                                        }

                                    } else {
                                        return [
                                            'status' => '002',
                                            'message' => 'Accès non autorisé.',
                                        ];
                                    }
                                } else {
                                    return [
                                        'status' => '002',
                                        'message' => 'Cet utilisateur n\'a pas accès à ce centre.',
                                    ];
                                }
                            } else {
                                return [
                                    'status' => '002',
                                    'message' => 'id_center est incorrect.',
                                ];
                            }
                        } else {
                            return [
                                'status' => '002',
                                'message' => 'id_center ne doit pas être vide.',
                            ];
                        }
                    } else {
                        return [
                            'status' => '002',
                            'message' => 'Utilisateur non trouvé.',
                        ];
                    }
                } else {
                    return [
                        'status' => '002',
                        'message' => 'user_auth_key ne doit pas être vide.',
                    ];
                }
            } else {
                return [
                    'status' => '002',
                    'message' => 'Token incorrect',
                ];
            }
        } else {
            return [
                'status' => '002',
                'message' => 'Aucune donnée reçue en POST',
            ];
        }
        exit;
    }

    public function actionAddjourferie()
    {
        $token = Utils::getApiKey();
        if (Yii::$app->request->post()) {

            if (isset($_POST['token']) && $_POST['token'] == $token) {
                if (isset($_POST['user_auth_key']) && $_POST['user_auth_key'] != '') {
                    $user_auth_key = $_POST['user_auth_key'];
                    $user_exist = User::find()->where(['auth_key' => $user_auth_key, 'status' => 10])->one();
                    if ($user_exist != '') {

                        if (isset($_POST['id_center']) && $_POST['id_center'] != '') {
                            $id_center = $_POST['id_center'];

                            $center = SchoolCenter::find()->where(['id_center' => $id_center, 'etat' => [1, 2, 4]])->one();
                            if ($center != '') {
                                $cont = false;
                                $user_id_center = str_replace('-', '', $user_exist->id_center);
                                $test_center = explode(",", $user_id_center);

                                if ($user_exist->id_center == "1") {
                                    $cont = true;
                                } else if (in_array($id_center, $test_center)) {
                                    $cont = true;
                                }

                                if ($cont == true) {

                                    $manage_jourferie = Utils::have_accessapi("manage_jourferie", $user_exist->idP, $user_exist->id, $id_center);
                                    $test = explode('_', $manage_jourferie);

                                    //On vérifie s'il y a une année scolaire en cours

                                    $anneeEncours = Anneescolaire::find()->where(['idcentre'=>$center->id_center,'status' =>1])->one();

                                    if($anneeEncours != '' or $anneeEncours != null){
                                        if (isset($_POST['evenement']) && $_POST['evenement'] != '') {
                                            $evenement = $_POST['evenement'];
                                            if(isset($_POST['datejourferie']) && $_POST['datejourferie'] != ''){
                                                $dateJourferie = $_POST['datejourferie'];
                                                //Vérifier la date
                                                if(Utils::isValidDate($dateJourferie)){
                                                    if(strtotime($dateJourferie) >strtotime($anneeEncours->datedebutannee) && strtotime($dateJourferie) <strtotime($anneeEncours->datefinannee)){
                                                        if (isset($_POST['operation']) && $_POST['operation'] != '') {
                                                            $operation = $_POST['operation'];
                                                            if ($operation == 1) { // AJOUT
                                                                if ($test[0] == '1') { // INDICE : 0 pour CREATE; 1 pour READ; 2 pour UPDATE; 3 pour DELETE;

                                                                    $exist = Jourferie::find()->where([
                                                                        'evenement' => $evenement,
                                                                        'datejourferie'=>$dateJourferie,
                                                                        'idannee'=>$anneeEncours->id,
                                                                        'status' => [1, 2]
                                                                    ])->one();
                                                                    if ($exist == '' or $exist == null) {
                                                                        $new_record = new Jourferie();
                                                                        $new_record->evenement = $evenement;
                                                                        $new_record->datejourferie = $dateJourferie;
                                                                        $new_record->keyjourferie = Yii::$app->security->generateRandomString(32);
                                                                        $new_record->idannee = $anneeEncours->id;
                                                                        $new_record->status = 1;
                                                                        $new_record->created_at = time();
                                                                        $new_record->updated_at = '';
                                                                        $new_record->create_by = $user_exist->id;
                                                                        $new_record->updated_by = '';
                                                                        //print_r($new_record);exit;
                                                                        if ($new_record->save()) {

                                                                            $data = Jourferie::find()->where(['idannee'=>$anneeEncours->id,'status' => [1, 2]])->orderBy(['datejourferie' => SORT_DESC])->all();

                                                                            return [
                                                                                'status' => '000',
                                                                                'message' => 'succes_creation_jour_ferie',
                                                                                'data' => $data,
                                                                            ];
                                                                        } else {
                                                                            return [
                                                                                'status' => '002',
                                                                                'message' => 'update_error',
                                                                            ];
                                                                        }
                                                                    } else {
                                                                        return [
                                                                            'status' => '003',
                                                                            'message' => 'jour_ferie_used',
                                                                        ];
                                                                    }
                                                                } else {
                                                                    return [
                                                                        'status' => '002',
                                                                        'message' => 'Accès non autorisé.',
                                                                    ];
                                                                }
                                                            } else if ($operation == 2) { // MISE A JOUR
                                                                if ($test[2] == '1') { // INDICE : 0 pour CREATE; 1 pour READ; 2 pour UPDATE; 3 pour DELETE;

                                                                    if (isset($_POST['jour_ferie_key']) && $_POST['jour_ferie_key'] != '') {
                                                                        $jour_ferie_key = $_POST['jour_ferie_key'];
                                                                        $old_record = Jourferie::find()->where(['keyjourferie' => $jour_ferie_key, 'status' => 1])->one();
                                                                        if ($old_record != '') {
                                                                            //Pour vérifier s'il existe une autre occurence avec les mêmes informations
                                                                            $exist = Jourferie::find()->where([
                                                                                'evenement' => $evenement,
                                                                                'datejourferie'=>$dateJourferie,
                                                                                'idannee'=>$anneeEncours->id,
                                                                                'status' => [1, 2]
                                                                            ])->one();

                                                                            if ($exist == '' or $exist == null) {
                                                                                $continue = true;
                                                                            } else {
                                                                                ($exist->keyjourferie != $jour_ferie_key) ? $continue = false : $continue = true;
                                                                            }

                                                                            if ($continue == true) {
                                                                                $old_record->evenement = $evenement;
                                                                                $old_record->datejourferie = $dateJourferie;
                                                                                $old_record->updated_at = time();
                                                                                $old_record->updated_by = $user_exist->id;
                                                                                if ($old_record->save()) {

                                                                                    $data = Jourferie::find()->where(['idannee'=>$anneeEncours->id,'status' => [1, 2]])->orderBy(['datejourferie' => SORT_DESC])->all();

                                                                                    return [
                                                                                        'status' => '000',
                                                                                        'message' => 'succes_update_jour_ferie',
                                                                                        'data' => $data,
                                                                                    ];
                                                                                } else {
                                                                                    return [
                                                                                        'status' => '002',
                                                                                        'message' => 'update_error',
                                                                                    ];
                                                                                }
                                                                            } else {
                                                                                return [
                                                                                    'status' => '003',
                                                                                    'message' => 'jour_ferie_used',
                                                                                ];
                                                                            }
                                                                        } else {
                                                                            return [
                                                                                'status' => '001',
                                                                                'message' => 'jour_ferie_not_found',
                                                                            ];
                                                                        }
                                                                    } else {
                                                                        return [
                                                                            'status' => '001',
                                                                            'message' => 'jour_ferie_key_empty',
                                                                        ];
                                                                    }
                                                                } else {
                                                                    return [
                                                                        'status' => '002',
                                                                        'message' => 'Accès non autorisé.',
                                                                    ];
                                                                }
                                                            } else {
                                                                return [
                                                                    'status' => '002',
                                                                    'message' => 'Type d\'opération non reconnu.',
                                                                ];
                                                            }
                                                        } else {
                                                            return [
                                                                'status' => '002',
                                                                'message' => 'Le type d\'opération ne doit pas être vide.',
                                                            ];
                                                        }
                                                    }else{
                                                        return [
                                                            'status' => '001',
                                                            'message' => 'jour_ferie_date_not_correct',
                                                        ];
                                                    }

                                                }else{
                                                    return [
                                                        'status' => '001',
                                                        'message' => 'jour_ferie_date_invalid',
                                                    ];
                                                }

                                            }else{
                                                return [
                                                    'status' => '001',
                                                    'message' => 'jour_ferie_date_empty',
                                                ];
                                            }

                                        } else {
                                            return [
                                                'status' => '001',
                                                'message' => 'jour_ferie_evenement_empty',
                                            ];
                                        }
                                    }else{
                                        return [
                                            'status' => '001',
                                            'message' => 'jour_ferie_annee_encour_invalid',
                                        ];
                                    }

                                } else {
                                    return [
                                        'status' => '002',
                                        'message' => 'Cet utilisateur n\'a pas accès à ce centre.',
                                    ];
                                }
                            } else {
                                return [
                                    'status' => '002',
                                    'message' => 'id_center est incorrect.',
                                ];
                            }
                        } else {
                            return [
                                'status' => '002',
                                'message' => 'id_center ne doit pas être vide.',
                            ];
                        }
                    } else {
                        return [
                            'status' => '002',
                            'message' => 'Utilisateur non trouvé',
                        ];
                    }
                } else {
                    return [
                        'status' => '002',
                        'message' => 'user_auth_key ne doit pas être vide',
                    ];
                }
            } else {
                return [
                    'status' => '002',
                    'message' => 'Token incorrect',
                ];
            }
        } else {
            return [
                'status' => '002',
                'message' => 'Aucune donnée reçue en POST',
            ];
        }
        exit;
    }

    public function actionJourferie()
    {
        $token = Utils::getApiKey();
        if (Yii::$app->request->post()) {
            if (isset($_POST['token']) && $_POST['token'] == $token) {
                if (isset($_POST['user_auth_key']) && $_POST['user_auth_key'] != '') {
                    $user_auth_key = $_POST['user_auth_key'];
                    $user_exist = User::find()->where(['auth_key' => $user_auth_key, 'status' => 10])->one();
                    if ($user_exist != '') {

                        if (isset($_POST['id_center']) && $_POST['id_center'] != '') {
                            $id_center = $_POST['id_center'];

                            $center = SchoolCenter::find()->where(['id_center' => $id_center, 'etat' => [1, 2, 4]])->one();
                            if ($center != '') {
                                $cont = false;
                                $user_id_center = str_replace('-', '', $user_exist->id_center);
                                $test_center = explode(",", $user_id_center);

                                if ($user_exist->id_center == "1") {
                                    $cont = true;
                                } else if (in_array($id_center, $test_center)) {
                                    $cont = true;
                                }

                                if ($cont == true) {

                                    $manage_jourferie = Utils::have_accessapi("manage_jourferie", $user_exist->idP, $user_exist->id, $id_center);
                                    $test = explode('_', $manage_jourferie);
                                    if ($test[2] == '1') { // INDICE : 0 pour CREATE; 1 pour READ; 2 pour UPDATE; 3 pour DELETE;


                                        if (isset($_POST['jour_ferie_key']) && $_POST['jour_ferie_key'] != '') {
                                            $jour_ferie_key = $_POST['jour_ferie_key'];
                                            //$jour_ferie_exist = Jourferie::find()->where(['keyjourferie' => $jour_ferie_key])->one();

                                            $query = new Query();
                                            $jour_ferie_exist = $query
                                                ->select(['j.*'
                                                ])
                                                ->from('jourferie j')
                                                ->join('INNER JOIN','anneescolaire a','j.idannee = a.id')
                                                ->where(['j.keyjourferie'=> $jour_ferie_key,'a.idcentre'=> $center->id_center])
                                                ->one()
                                            ;
                                            if ($jour_ferie_exist != '') {
                                                return [
                                                    'status' => '000',
                                                    'message' => 'succes_get_jour_ferie',
                                                    'data' => $jour_ferie_exist,
                                                ];
                                            } else {
                                                return [
                                                    'status' => '001',
                                                    'message' => 'jour_ferie_not_found',
                                                ];
                                            }
                                        } else {
                                            return [
                                                'status' => '001',
                                                'message' => 'jour_ferie_key_empty',
                                            ];
                                        }
                                    } else {
                                        return [
                                            'status' => '002',
                                            'message' => 'Accès non autorisé.',
                                        ];
                                    }
                                } else {
                                    return [
                                        'status' => '002',
                                        'message' => 'Cet utilisateur n\'a pas accès à ce centre.',
                                    ];
                                }
                            } else {
                                return [
                                    'status' => '002',
                                    'message' => 'id_center est incorrect.',
                                ];
                            }
                        } else {
                            return [
                                'status' => '002',
                                'message' => 'id_center ne doit pas être vide.',
                            ];
                        }
                    } else {
                        return [
                            'status' => '002',
                            'message' => 'Utilisateur non trouvé.',
                        ];
                    }
                } else {
                    return [
                        'status' => '002',
                        'message' => 'user_auth_key ne doit pas être vide.',
                    ];
                }
            } else {
                return [
                    'status' => '002',
                    'message' => 'Token incorrect.',
                ];
            }
        } else {
            return [
                'status' => '002',
                'message' => 'Aucune donnée reçue en POST.',
            ];
        }
        exit;
    }

    public function actionDeletejourferie()
    {
        $token = Utils::getApiKey();

        if (Yii::$app->request->post()) {
            if (isset($_POST['token']) && $_POST['token'] == $token) {
                if (isset($_POST['user_auth_key']) && $_POST['user_auth_key'] != '') {
                    $user_auth_key = $_POST['user_auth_key'];
                    $user_exist = User::find()->where(['auth_key' => $user_auth_key, 'status' => 10])->one();
                    if ($user_exist != '') {


                        if (isset($_POST['id_center']) && $_POST['id_center'] != '') {
                            $id_center = $_POST['id_center'];

                            $center = SchoolCenter::find()->where(['id_center' => $id_center, 'etat' => [1, 2, 4]])->one();
                            if ($center != '') {
                                $cont = false;
                                $user_id_center = str_replace('-', '', $user_exist->id_center);
                                $test_center = explode(",", $user_id_center);

                                if ($user_exist->id_center == "1") {
                                    $cont = true;
                                } else if (in_array($id_center, $test_center)) {
                                    $cont = true;
                                }

                                if ($cont == true) {

                                    $manage_jourferie = Utils::have_accessapi("manage_jourferie", $user_exist->idP, $user_exist->id, $id_center);
                                    $test = explode('_', $manage_jourferie);
                                    if ($test[3] == '1') { // INDICE : 0 pour CREATE; 1 pour READ; 2 pour UPDATE; 3 pour DELETE;

                                        $anneeEncours = Anneescolaire::find()->where(['idcentre'=>$center->id_center,'status' =>1])->one();
                                        if (isset($_POST['jour_ferie_key']) && $_POST['jour_ferie_key'] != '') {
                                            $jour_ferie_key = $_POST['jour_ferie_key'];
                                            $old_record = Jourferie::find()->where(['idannee'=>$anneeEncours->id,'keyjourferie' => $jour_ferie_key])->one();
                                            /*$query = new Query();
                                            $old_record = $query
                                                ->select(['j.*'
                                                ])
                                                ->from('jourferie j')
                                                ->join('INNER JOIN','anneescolaire a','j.idannee = a.id')
                                                ->where(['j.keyjourferie'=> $jour_ferie_key,'a.idcentre'=> $center->id_center])
                                                ->one()
                                            ;*/

                                            if ($old_record != '') {

                                                //On vérifie si le jour férié n'est pas déjà passé
                                                $dateJour = date("Y-m-d");
                                                if($old_record->datejourferie > $dateJour){

                                                    $old_record->status = 3;
                                                    $old_record->updated_at = time();
                                                    $old_record->updated_by = $user_exist->id;
                                                    if ($old_record->save()) {
                                                        $data = Jourferie::find()->where(['idannee'=>$anneeEncours->id,'status' => [1, 2]])->orderBy(['datejourferie' => SORT_DESC])->all();
                                                        return [
                                                            'status' => '000',
                                                            'message' => 'delete_jour_ferie_success',
                                                            'data' => $data,
                                                        ];
                                                    } else {
                                                        return [
                                                            'status' => '002',
                                                            'message' => 'update_error',
                                                        ];
                                                    }
                                                }else{
                                                    return [
                                                        'status' => '001',
                                                        'message' => 'jour_ferie_deja_passe',
                                                    ];
                                                }

                                            } else {
                                                return [
                                                    'status' => '001',
                                                    'message' => 'jour_ferie_not_found',
                                                ];
                                            }
                                        } else {
                                            return [
                                                'status' => '001',
                                                'message' => 'jour_ferie_key_empty',
                                            ];
                                        }

                                    } else {
                                        return [
                                            'status' => '002',
                                            'message' => 'Accès non autorisé.',
                                        ];
                                    }
                                } else {
                                    return [
                                        'status' => '002',
                                        'message' => 'Cet utilisateur n\'a pas accès à ce centre.',
                                    ];
                                }
                            } else {
                                return [
                                    'status' => '002',
                                    'message' => 'id_center est incorrect.',
                                ];
                            }
                        } else {
                            return [
                                'status' => '002',
                                'message' => 'id_center ne doit pas être vide.',
                            ];
                        }
                    } else {
                        return [
                            'status' => '002',
                            'message' => 'Utilisateur non trouvé.',
                        ];
                    }
                } else {
                    return [
                        'status' => '002',
                        'message' => 'user_auth_key ne doit pas être vide.',
                    ];
                }
            } else {
                return [
                    'status' => '002',
                    'message' => 'Token incorrect.',
                ];
            }
        } else {
            return [
                'status' => '002',
                'message' => 'Aucune donnée reçue en POST.',
            ];
        }
        exit;
    }

}
