<?php

namespace api\modules\geschool\v1\controllers;
use api\modules\geschool\v1\models\Section;
use api\modules\geschool\v1\models\Specialite;
use Yii;
use yii\db\Query;
use yii\rest\ActiveController;
use api\modules\geschool\v1\models\User;
use api\modules\geschool\v1\models\SchoolCenter;

/**
 * Geschool Controller API
 *
 */

class SpecialiteController extends ActiveController
{
    public $modelClass = 'api\modules\geschool\v1\models\Specialite';

    #000 -> Tout est bien
    #001 -> Message client
    #002 -> Message dev


    //Cette méthode nous permet de récupérer les spécialités
    public function actionSpecialites()
    {
        $token = Utils::getApiKey();
        if (Yii::$app->request->post()) {
            if (isset($_POST['token']) && $_POST['token'] == $token) {
                if (isset($_POST['user_auth_key']) && $_POST['user_auth_key'] != '') {
                    $user_auth_key = $_POST['user_auth_key'];
                    $user_exist = User::find()->where(['auth_key' => $user_auth_key, 'status' => 10])->one();
                    if ($user_exist != '') {

                        if (isset($_POST['id_center']) && $_POST['id_center'] != '') {
                            $id_center = $_POST['id_center'];

                            $center = SchoolCenter::find()->where(['id_center' => $id_center, 'etat' => [1, 2, 4]])->one();
                            if ($center != '') {
                                $cont = false;
                                $user_id_center = str_replace('-', '', $user_exist->id_center);
                                $test_center = explode(",", $user_id_center);

                                if ($user_exist->id_center == "1") {
                                    $cont = true;
                                } else if (in_array($id_center, $test_center)) {
                                    $cont = true;
                                }

                                if ($cont == true) {

                                    $manage_specialite = Utils::have_accessapi("manage_specialite", $user_exist->idP, $user_exist->id, $id_center);
                                    $test = explode('_', $manage_specialite);
                                    if ($test[1] == '1') { // INDICE : 0 pour CREATE; 1 pour READ; 2 pour UPDATE; 3 pour DELETE;

                                        //$data = Specialite::find()->where(['status' => [1, 2]])->orderBy(['libellespecialite' => SORT_ASC])->all();

                                        $query = new Query();
                                        $data = $query
                                            ->select(['d.*'
                                            ])
                                            ->from('specialite d')
                                            ->join('INNER JOIN','section s','s.id = d.idsection')
                                            ->where(['d.status'=> [1, 2],
                                                //'s.status'=>1
                                            ])
                                            ->orderBy(['libellespecialite' => SORT_ASC])
                                            ->all();
                                        if (sizeof($data) > 0) {
                                            return [
                                                'status' => '000',
                                                'message' => 'succes_get_specialite',
                                                'data' => $data
                                            ];
                                        } else {
                                            return [
                                                'status' => '001',
                                                'message' => 'empty_liste_specialite',
                                            ];
                                        }

                                    } else {
                                        return [
                                            'status' => '002',
                                            'message' => 'Accès non autorisé.',
                                        ];
                                    }
                                } else {
                                    return [
                                        'status' => '002',
                                        'message' => 'Cet utilisateur n\'a pas accès à ce centre.',
                                    ];
                                }
                            } else {
                                return [
                                    'status' => '002',
                                    'message' => 'id_center est incorrect.',
                                ];
                            }
                        } else {
                            return [
                                'status' => '002',
                                'message' => 'id_center ne doit pas être vide.',
                            ];
                        }
                    } else {
                        return [
                            'status' => '002',
                            'message' => 'Utilisateur non trouvé.',
                        ];
                    }
                } else {
                    return [
                        'status' => '002',
                        'message' => 'user_auth_key ne doit pas être vide.',
                    ];
                }
            } else {
                return [
                    'status' => '002',
                    'message' => 'Token incorrect',
                ];
            }
        } else {
            return [
                'status' => '002',
                'message' => 'Aucune donnée reçue en POST',
            ];
        }
        exit;
    }

    //Cette méthode nous permet d'ajouter une spécialité
    public function actionAddspecialite()
    {
        $token = Utils::getApiKey();
        if (Yii::$app->request->post()) {

            if (isset($_POST['token']) && $_POST['token'] == $token) {
                if (isset($_POST['user_auth_key']) && $_POST['user_auth_key'] != '') {
                    $user_auth_key = $_POST['user_auth_key'];
                    $user_exist = User::find()->where(['auth_key' => $user_auth_key, 'status' => 10])->one();
                    if ($user_exist != '') {

                        if (isset($_POST['id_center']) && $_POST['id_center'] != '') {
                            $id_center = $_POST['id_center'];

                            $center = SchoolCenter::find()->where(['id_center' => $id_center, 'etat' => [1, 2, 4]])->one();
                            if ($center != '') {
                                $cont = false;
                                $user_id_center = str_replace('-', '', $user_exist->id_center);
                                $test_center = explode(",", $user_id_center);

                                if ($user_exist->id_center == "1") {
                                    $cont = true;
                                } else if (in_array($id_center, $test_center)) {
                                    $cont = true;
                                }

                                if ($cont == true) {

                                    $manage_specialite = Utils::have_accessapi("manage_specialite", $user_exist->idP, $user_exist->id, $id_center);
                                    $test = explode('_', $manage_specialite);

                                    if (isset($_POST['libelle']) && $_POST['libelle'] != '') {
                                        $libelle = $_POST['libelle'];

                                        if (isset($_POST['idsection']) && $_POST['idsection'] != '') {
                                            $idsection = $_POST['idsection'];
                                            $findSection = Section::findOne($idsection);
                                            if($findSection != '' or $findSection != null){

                                                if (isset($_POST['operation']) && $_POST['operation'] != '') {
                                                    $operation = $_POST['operation'];
                                                    if ($operation == 1) { // AJOUT
                                                        if ($test[0] == '1') { // INDICE : 0 pour CREATE; 1 pour READ; 2 pour UPDATE; 3 pour DELETE;

                                                            $exist = Specialite::find()->where([
                                                                'libellespecialite' => $libelle,
                                                                'status' => [1, 2]
                                                            ])->one();
                                                            if ($exist == '' or $exist == null) {
                                                                $new_record = new Specialite();
                                                                $new_record->libellespecialite = $libelle;
                                                                $new_record->idsection = $findSection->id;
                                                                $new_record->keyspecialite = Yii::$app->security->generateRandomString(32);
                                                                $new_record->status = 1;
                                                                $new_record->created_at = time();
                                                                $new_record->updated_at = '';
                                                                $new_record->create_by = $user_exist->id;
                                                                $new_record->updated_by = '';
                                                                //print_r($new_record);exit;
                                                                if ($new_record->save()) {

                                                                    //$data = Specialite::find()->where(['status' => [1, 2]])->orderBy(['libellespecialite' => SORT_ASC])->all();

                                                                    $query = new Query();
                                                                    $data = $query
                                                                        ->select(['d.*'
                                                                        ])
                                                                        ->from('specialite d')
                                                                        ->join('INNER JOIN','section s','s.id = d.idsection')
                                                                        ->where(['d.status'=> [1, 2],
                                                                            's.status'=>1])
                                                                        ->orderBy(['libellespecialite' => SORT_ASC])
                                                                        ->all();

                                                                    return [
                                                                        'status' => '000',
                                                                        'message' => 'succes_creation_specialite',
                                                                        'data' => $data,
                                                                    ];
                                                                } else {
                                                                    return [
                                                                        'status' => '002',
                                                                        'message' => 'update_error',
                                                                    ];
                                                                }
                                                            } else {
                                                                return [
                                                                    'status' => '003',
                                                                    'message' => 'specialite_used',
                                                                ];
                                                            }
                                                        } else {
                                                            return [
                                                                'status' => '002',
                                                                'message' => 'Accès non autorisé.',
                                                            ];
                                                        }
                                                    } else if ($operation == 2) { // MISE A JOUR
                                                        if ($test[2] == '1') { // INDICE : 0 pour CREATE; 1 pour READ; 2 pour UPDATE; 3 pour DELETE;

                                                            if (isset($_POST['specialite_key']) && $_POST['specialite_key'] != '') {
                                                                $specialite_key = $_POST['specialite_key'];
                                                                $old_record = Specialite::find()->where(['keyspecialite' => $specialite_key, 'status' => [1,2]])->one();
                                                                if ($old_record != '') {
                                                                    //Pour vérifier s'il existe une autre occurence avec les mêmes informations
                                                                    $exist = Specialite::find()->where([
                                                                        'libellespecialite'=>$libelle,
                                                                        'status' => [1, 2]
                                                                    ])->one();

                                                                    if ($exist == '' or $exist == null) {
                                                                        $continue = true;
                                                                    } else {
                                                                        ($exist->keyspecialite != $specialite_key) ? $continue = false : $continue = true;
                                                                    }

                                                                    if ($continue == true) {
                                                                        $old_record->libellespecialite = $libelle;
                                                                        $old_record->idsection = $findSection->id;
                                                                        $old_record->updated_at = time();
                                                                        $old_record->updated_by = $user_exist->id;
                                                                        if ($old_record->save()) {
                                                                            //$data = Specialite::find()->where(['status' => [1, 2]])->orderBy(['libellespecialite' => SORT_ASC])->all();

                                                                            $query = new Query();
                                                                            $data = $query
                                                                                ->select(['d.*'
                                                                                ])
                                                                                ->from('specialite d')
                                                                                ->join('INNER JOIN','section s','s.id = d.idsection')
                                                                                ->where(['d.status'=> [1, 2],
                                                                                    //'s.status'=>1
                                                                                    ])
                                                                                ->orderBy(['libellespecialite' => SORT_ASC])
                                                                                ->all();
                                                                            return [
                                                                                'status' => '000',
                                                                                'message' => 'succes_update_specialite',
                                                                                'data' => $data,
                                                                            ];
                                                                        } else {
                                                                            return [
                                                                                'status' => '002',
                                                                                'message' => 'update_error',
                                                                            ];
                                                                        }
                                                                    } else {
                                                                        return [
                                                                            'status' => '003',
                                                                            'message' => 'specialite_used',
                                                                        ];
                                                                    }
                                                                } else {
                                                                    return [
                                                                        'status' => '001',
                                                                        'message' => 'specialite_not_found',
                                                                    ];
                                                                }
                                                            } else {
                                                                return [
                                                                    'status' => '001',
                                                                    'message' => 'specialite_key ne peut pas être vide.',
                                                                ];
                                                            }
                                                        } else {
                                                            return [
                                                                'status' => '002',
                                                                'message' => 'Accès non autorisé.',
                                                            ];
                                                        }
                                                    } else {
                                                        return [
                                                            'status' => '002',
                                                            'message' => 'Type d\'opération non reconnu.',
                                                        ];
                                                    }
                                                } else {
                                                    return [
                                                        'status' => '002',
                                                        'message' => 'Le type d\'opération ne doit pas être vide.',
                                                    ];
                                                }

                                            }else{
                                                return [
                                                    'status' => '001',
                                                    'message' => 'specialite_section_not_found',
                                                ];
                                            }

                                        }else{
                                            return [
                                                'status' => '001',
                                                'message' => 'specialite_section_empty',
                                            ];
                                        }

                                    } else {
                                        return [
                                            'status' => '001',
                                            'message' => 'specialite_libelle_empty',
                                        ];
                                    }

                                } else {
                                    return [
                                        'status' => '002',
                                        'message' => 'Cet utilisateur n\'a pas accès à ce centre.',
                                    ];
                                }
                            } else {
                                return [
                                    'status' => '002',
                                    'message' => 'id_center est incorrect.',
                                ];
                            }
                        } else {
                            return [
                                'status' => '002',
                                'message' => 'id_center ne doit pas être vide.',
                            ];
                        }
                    } else {
                        return [
                            'status' => '002',
                            'message' => 'Utilisateur non trouvé',
                        ];
                    }
                } else {
                    return [
                        'status' => '002',
                        'message' => 'user_auth_key ne doit pas être vide',
                    ];
                }
            } else {
                return [
                    'status' => '002',
                    'message' => 'Token incorrect',
                ];
            }
        } else {
            return [
                'status' => '002',
                'message' => 'Aucune donnée reçue en POST',
            ];
        }
        exit;
    }

    //Cette méthode nous permet de récuperer //Cette méthode nous permet de récuperer
    public function actionSpecialite()
    {
        $token = Utils::getApiKey();
        if (Yii::$app->request->post()) {
            if (isset($_POST['token']) && $_POST['token'] == $token) {
                if (isset($_POST['user_auth_key']) && $_POST['user_auth_key'] != '') {
                    $user_auth_key = $_POST['user_auth_key'];
                    $user_exist = User::find()->where(['auth_key' => $user_auth_key, 'status' => 10])->one();
                    if ($user_exist != '') {

                        if (isset($_POST['id_center']) && $_POST['id_center'] != '') {
                            $id_center = $_POST['id_center'];

                            $center = SchoolCenter::find()->where(['id_center' => $id_center, 'etat' => [1, 2, 4]])->one();
                            if ($center != '') {
                                $cont = false;
                                $user_id_center = str_replace('-', '', $user_exist->id_center);
                                $test_center = explode(",", $user_id_center);

                                if ($user_exist->id_center == "1") {
                                    $cont = true;
                                } else if (in_array($id_center, $test_center)) {
                                    $cont = true;
                                }

                                if ($cont == true) {

                                    $manage_specialite = Utils::have_accessapi("manage_specialite", $user_exist->idP, $user_exist->id, $id_center);
                                    $test = explode('_', $manage_specialite);
                                    if ($test[2] == '1') { // INDICE : 0 pour CREATE; 1 pour READ; 2 pour UPDATE; 3 pour DELETE;


                                        if (isset($_POST['specialite_key']) && $_POST['specialite_key'] != '') {
                                            $specialite_key = $_POST['specialite_key'];
                                            $specialite_exist = Specialite::find()->where(['keyspecialite' => $specialite_key])->one();
                                            if ($specialite_exist != '') {
                                                return [
                                                    'status' => '000',
                                                    'message' => 'Données bien récupérées',
                                                    'data' => $specialite_exist,
                                                ];
                                            } else {
                                                return [
                                                    'status' => '001',
                                                    'message' => 'specialite_not_found',
                                                ];
                                            }
                                        } else {
                                            return [
                                                'status' => '001',
                                                'message' => 'specialite_key_empty',
                                            ];
                                        }
                                    } else {
                                        return [
                                            'status' => '002',
                                            'message' => 'Accès non autorisé.',
                                        ];
                                    }
                                } else {
                                    return [
                                        'status' => '002',
                                        'message' => 'Cet utilisateur n\'a pas accès à ce centre.',
                                    ];
                                }
                            } else {
                                return [
                                    'status' => '002',
                                    'message' => 'id_center est incorrect.',
                                ];
                            }
                        } else {
                            return [
                                'status' => '002',
                                'message' => 'id_center ne doit pas être vide.',
                            ];
                        }
                    } else {
                        return [
                            'status' => '002',
                            'message' => 'Utilisateur non trouvé.',
                        ];
                    }
                } else {
                    return [
                        'status' => '002',
                        'message' => 'user_auth_key ne doit pas être vide.',
                    ];
                }
            } else {
                return [
                    'status' => '002',
                    'message' => 'Token incorrect.',
                ];
            }
        } else {
            return [
                'status' => '002',
                'message' => 'Aucune donnée reçue en POST.',
            ];
        }
        exit;
    }

    //Cette méthode nous permet de supprimer d'activer et désactiver une spécialité
    public function actionDeletespecialite()
    {
        $token = Utils::getApiKey();

        if (Yii::$app->request->post()) {
            if (isset($_POST['token']) && $_POST['token'] == $token) {
                if (isset($_POST['user_auth_key']) && $_POST['user_auth_key'] != '') {
                    $user_auth_key = $_POST['user_auth_key'];
                    $user_exist = User::find()->where(['auth_key' => $user_auth_key, 'status' => 10])->one();
                    if ($user_exist != '') {

                        if (isset($_POST['id_center']) && $_POST['id_center'] != '') {
                            $id_center = $_POST['id_center'];

                            $center = SchoolCenter::find()->where(['id_center' => $id_center, 'etat' => [1, 2, 4]])->one();
                            if ($center != '') {
                                $cont = false;
                                $user_id_center = str_replace('-', '', $user_exist->id_center);
                                $test_center = explode(",", $user_id_center);

                                if ($user_exist->id_center == "1") {
                                    $cont = true;
                                } else if (in_array($id_center, $test_center)) {
                                    $cont = true;
                                }

                                if ($cont == true) {

                                    $manage_specialite = Utils::have_accessapi("manage_specialite", $user_exist->idP, $user_exist->id, $id_center);
                                    $test = explode('_', $manage_specialite);
                                    if ($test[3] == '1') { // INDICE : 0 pour CREATE; 1 pour READ; 2 pour UPDATE; 3 pour DELETE;

                                        if (isset($_POST['operation']) && $_POST['operation'] != '') {
                                            $operation = $_POST['operation'];
                                            if (isset($_POST['specialite_key']) && $_POST['specialite_key'] != '') {
                                                $specialite_key = $_POST['specialite_key'];
                                                $old_record = Specialite::find()->where(['keyspecialite' => $specialite_key])->one();
                                                if ($old_record != '') {

                                                    if ($operation == 3) { // SUPPRESSION
                                                        $old_record->status = 3;
                                                        $msg = "delete_specialite_success";
                                                    } else if ($operation == 2) { // DESACTIVATION
                                                        $old_record->status = 2;
                                                        $msg = "desactive_specialite_success";
                                                    } else if ($operation == 1) { // ACTIVATION
                                                        $old_record->status = 1;
                                                        $msg = "active_specialite_success";
                                                    } else {
                                                        return [
                                                            'status' => '002',
                                                            'message' => 'Type d\'opération non reconnu',
                                                        ];
                                                    }

                                                        $old_record->updated_at = time();
                                                        $old_record->updated_by = $user_exist->id;
                                                        if ($old_record->save()) {

                                                            //$data = Specialite::find()->where(['status' => [1, 2]])->orderBy(['libellespecialite' => SORT_ASC])->all();

                                                            $query = new Query();
                                                            $data = $query
                                                                ->select(['d.*'
                                                                ])
                                                                ->from('specialite d')
                                                                ->join('INNER JOIN','section s','s.id = d.idsection')
                                                                ->where(['d.status'=> [1, 2],
                                                                    //'s.status'=>1
                                                                    ])
                                                                ->orderBy(['libellespecialite' => SORT_ASC])
                                                                ->all();

                                                            return [
                                                                'status' => '000',
                                                                'message' =>$msg,
                                                                'data' => $data,
                                                            ];
                                                        } else {
                                                            return [
                                                                'status' => '002',
                                                                'message' => 'update_error',
                                                            ];
                                                        }

                                                } else {
                                                    return [
                                                        'status' => '001',
                                                        'message' => 'specialite_not_found',
                                                    ];
                                                }
                                            } else {
                                                return [
                                                    'status' => '001',
                                                    'message' => 'specialite_key_empty',
                                                ];
                                            }

                                        } else {
                                            return [
                                                'status' => '002',
                                                'message' => 'Le type d\'opération ne doit pas être vide.',
                                            ];
                                        }
                                    } else {
                                        return [
                                            'status' => '002',
                                            'message' => 'Accès non autorisé.',
                                        ];
                                    }
                                } else {
                                    return [
                                        'status' => '002',
                                        'message' => 'Cet utilisateur n\'a pas accès à ce centre.',
                                    ];
                                }
                            } else {
                                return [
                                    'status' => '002',
                                    'message' => 'id_center est incorrect.',
                                ];
                            }
                        } else {
                            return [
                                'status' => '002',
                                'message' => 'id_center ne doit pas être vide.',
                            ];
                        }
                    } else {
                        return [
                            'status' => '002',
                            'message' => 'Utilisateur non trouvé.',
                        ];
                    }
                } else {
                    return [
                        'status' => '002',
                        'message' => 'user_auth_key ne doit pas être vide.',
                    ];
                }
            } else {
                return [
                    'status' => '002',
                    'message' => 'Token incorrect.',
                ];
            }
        } else {
            return [
                'status' => '002',
                'message' => 'Aucune donnée reçue en POST.',
            ];
        }
        exit;
    }

}
