<?php

namespace api\modules\geschool\v1\controllers;

use api\modules\geschool\v1\models\Absence;
use api\modules\geschool\v1\models\Mission;
use api\modules\geschool\v1\models\Personnel;
use api\modules\geschool\v1\models\SchoolCenter;
use api\modules\geschool\v1\models\Typeabsence;
use api\modules\geschool\v1\models\Typepersonnel;
use api\modules\geschool\v1\models\User;
use DateTime;
use Yii;
use yii\rest\ActiveController;

/**
 * Geschool Controller API
 *
 */
class AbsenceController extends ActiveController
{
	public $modelClass = 'api\modules\geschool\v1\models\Absence';

	#000 -> Tout est bien
	#001 -> Message client
	#002 -> Message dev


	public function actionAbsence()
	{
		$token = Utils::getApiKey();
		if (Yii::$app->request->post()) {
			if (isset($_POST['token']) && $_POST['token'] == $token) {
				if (isset($_POST['user_auth_key']) && $_POST['user_auth_key'] != '') {
					$user_auth_key = $_POST['user_auth_key'];
					$user_exist = User::find()->where(['auth_key' => $user_auth_key, 'status' => 10])->one();
					if ($user_exist != '') {
						if (isset($_POST['id_center']) && $_POST['id_center'] != '') {
							$id_center = $_POST['id_center'];

							$center = SchoolCenter::find()->where(['id_center' => $id_center, 'etat' => [1, 2, 4]])->one();
							if ($center != '') {
								$cont = false;
								$user_id_center = str_replace('-', '', $user_exist->id_center);
								$test_center = explode(",", $user_id_center);

								if ($user_exist->id_center == "1") {
									$cont = true;
								} else if (in_array($id_center, $test_center)) {
									$cont = true;
								}

								if ($cont == true) {

									$manage_absence = Utils::have_accessapi("manage_absence", $user_exist->idP, $user_exist->id, $id_center);
									$test = explode('_', $manage_absence);
									if ($test[2] == '1') { // INDICE : 0 pour CREATE; 1 pour READ; 2 pour UPDATE; 3 pour DELETE;
										if (isset($_POST['abscence_key']) && $_POST['abscence_key'] != '') {
											$abscence_key = $_POST['abscence_key'];
											$abscence_exist = Absence::find()->where(['keyabsence' => $abscence_key])->one();
											if ($abscence_exist != '') {

												$tab_result = array();
												$user = User::find()->where(['id' => $abscence_exist->iduser])->one();

												$tab_result['id'] = $abscence_exist->id;
												$tab_result['idtypeabsence'] = $abscence_exist->idtypeabsence0->keytypeabsence;
												$tab_result['designationtypeabsence'] = $abscence_exist->idtypeabsence0->designationtypeabsence;
												$tab_result['iduser'] = $abscence_exist->iduser0->keypersonnel;
												$tab_result['nom'] = $user->nom;
												$tab_result['prenoms'] = $user->prenoms;
												$tab_result['keyabsence'] = $abscence_exist->keyabsence;
												$tab_result['datedemandeabsence'] = $abscence_exist->datedemandeabsence;
												$tab_result['motifabsence'] = $abscence_exist->motifabsence;
												$tab_result['datedebutabsence'] = $abscence_exist->datedebutabsence;
												$tab_result['heuredebutabsence'] = $abscence_exist->heuredebutabsence;
												$tab_result['datefinabsence'] = $abscence_exist->datefinabsence;
												$tab_result['heurefinabsence'] = $abscence_exist->heurefinabsence;
												$tab_result['coordonneesabsence'] = $abscence_exist->coordonneesabsence;
												$tab_result['lieudestinantionabsence'] = $abscence_exist->lieudestinantionabsence;
												$tab_result['heurearattraper'] = $abscence_exist->heurearattraper;
												$tab_result['status'] = $abscence_exist->status;
												$tab_result['created_at'] = $abscence_exist->created_at;
												$tab_result['updated_at'] = $abscence_exist->updated_at;
												$tab_result['create_by'] = $abscence_exist->create_by;
												$tab_result['updated_by'] = $abscence_exist->updated_by;
												return [
													'status' => '000',
													'message' => 'succes_get_absence',
													'data' => $tab_result
												];
											} else {
												return [
													'status' => '001',
													'message' => 'absence_not_found',
												];
											}
										} else {
											return [
												'status' => '001',
												'message' => 'absence_key_empty',
											];
										}
									} else {
										return [
											'status' => '002',
											'message' => 'Accès non autorisé.',
										];
									}
								} else {
									return [
										'status' => '002',
										'message' => 'Cet utilisateur n\'a pas accès à ce centre.',
									];
								}
							} else {
								return [
									'status' => '002',
									'message' => 'id_center est incorrect.',
								];
							}
						} else {
							return [
								'status' => '002',
								'message' => 'id_center ne doit pas être vide.',
							];
						}
					} else {
						return [
							'status' => '002',
							'message' => 'Utilisateur non trouvé.',
						];
					}
				} else {
					return [
						'status' => '002',
						'message' => 'user_auth_key ne doit pas être vide.',
					];
				}
			} else {
				return [
					'status' => '002',
					'message' => 'Token incorrect.',
				];
			}
		} else {
			return [
				'status' => '002',
				'message' => 'Aucune donnée reçue en POST.',
			];
		}
		exit;
	}

	public function actionAddabsence()
	{
		$token =  Utils::getApiKey();
		$heure_debut = "";
		$heure_fin = "";
		$coordonnees = "";
		$lieu_destinantion = "";
		$heure_a_rattraper = 0;
		if (Yii::$app->request->post()) {
			if (isset($_POST['token']) && $_POST['token'] == $token) {
				if (isset($_POST['user_auth_key']) && $_POST['user_auth_key'] != '') {
					$user_auth_key = $_POST['user_auth_key'];
					$user_exist = User::find()->where(['auth_key' => $user_auth_key, 'status' => 10])->one();
					if ($user_exist != '') {

						if (isset($_POST['id_center']) && $_POST['id_center'] != '') {
							$id_center = $_POST['id_center'];

							$center = SchoolCenter::find()->where(['id_center' => $id_center, 'etat' => [1, 2, 4]])->one();
							if ($center != '') {
								$cont = false;
								$user_id_center = str_replace('-', '', $user_exist->id_center);
								$test_center = explode(",", $user_id_center);

								if ($user_exist->id_center == "1") {
									$cont = true;
								} else if (in_array($id_center, $test_center)) {
									$cont = true;
								}

								if ($cont == true) {

									if (isset($_POST['motif']) && $_POST['motif'] != '') {
										$motif = $_POST['motif'];
										if (isset($_POST['date_demande']) && $_POST['date_demande'] != '') {
											$date_demande = $_POST['date_demande'];
											if (isset($_POST['date_debut']) && $_POST['date_debut'] != '') {
												$date_debut = $_POST['date_debut'];
												if (isset($_POST['date_fin']) && $_POST['date_fin'] != '') {
													$date_fin = $_POST['date_fin'];

													if (strtotime($date_debut) < strtotime($date_fin)) {
														if (isset($_POST['personnel']) && $_POST['personnel'] != '') {
															$keypersonnel = $_POST['personnel'];
															$personnel = Personnel::find()->where(['keypersonnel' => $keypersonnel])->one();
															if ($personnel != '') {
																$personnel_id = $personnel->iduser;
																if (isset($_POST['type_absence']) && $_POST['type_absence'] != '') {
																	$type_absence_key = $_POST['type_absence'];
																	$type_absence = Typeabsence::find()->where(['keytypeabsence' => $type_absence_key])->one();
																	if ($type_absence != '') {
																		$type_absence_id = $type_absence->id;
																		if (isset($_POST['heure_debut'])) $heure_debut = $_POST['heure_debut'];
																		if (isset($_POST['heure_fin'])) $heure_fin = $_POST['heure_fin'];
																		if (isset($_POST['coordonnees'])) $coordonnees = $_POST['coordonnees'];
																		if (isset($_POST['lieu_destinantion'])) $lieu_destinantion = $_POST['lieu_destinantion'];
																		if (isset($_POST['heure_a_rattraper'])) $heure_a_rattraper = $_POST['heure_a_rattraper'];
																		if (isset($_POST['operation']) && $_POST['operation'] != '') {
																			$operation = $_POST['operation'];
																			$manage_absence = Utils::have_accessapi("manage_absence", $user_exist->idP, $user_exist->id, $id_center);
																			$test = explode('_', $manage_absence);
																			if ($operation == 1) { // AJOUT

																				if ($test[0] == '1') { // INDICE : 0 pour CREATE; 1 pour READ; 2 pour UPDATE; 3 pour DELETE;
																					$today = date('Y-m-d');
																					if (strtotime($date_demande) <= strtotime($today)) {

																						$new_record = new Absence();
																						$new_record->iduser = $personnel_id;
																						$new_record->idtypeabsence = $type_absence_id;
																						$new_record->keyabsence = Yii::$app->security->generateRandomString(32);
																						$new_record->motifabsence = $motif;
																						$new_record->datedemandeabsence = $date_demande;
																						$new_record->datedebutabsence = $date_debut;
																						$new_record->datefinabsence = $date_fin;
																						$new_record->heuredebutabsence = $heure_debut;
																						$new_record->heurefinabsence = $heure_fin;
																						$new_record->coordonneesabsence = $coordonnees;
																						$new_record->lieudestinantionabsence = $lieu_destinantion;
																						$new_record->heurearattraper = $heure_a_rattraper;
																						$new_record->status = 1;
																						$new_record->created_at = time();
																						$new_record->updated_at = '';
																						$new_record->create_by = $user_exist->id;
																						$new_record->updated_by = '';
																						if ($new_record->save()) {

																							$data = Absence::find()->where(['status' => [1, 2]])->orderBy(['created_at' => SORT_ASC])->all();

																							$tab_all_result = array();
																							$tab_result = array();
																							foreach ($data as $line) {
																								$user = User::find()->where(['id' => $line->iduser])->one();

																								$tab_result['id'] = $line->id;
																								$tab_result['idtypeabsence'] = $line->idtypeabsence;
																								$tab_result['designationtypeabsence'] = $line->idtypeabsence0->designationtypeabsence;
																								$tab_result['iduser'] = $line->iduser;
																								$tab_result['nom'] = $user->nom;
																								$tab_result['prenoms'] = $user->prenoms;
																								$tab_result['keyabsence'] = $line->keyabsence;
																								$tab_result['datedemandeabsence'] = $line->datedemandeabsence;
																								$tab_result['motifabsence'] = $line->motifabsence;
																								$tab_result['datedebutabsence'] = $line->datedebutabsence;
																								$tab_result['heuredebutabsence'] = $line->heuredebutabsence;
																								$tab_result['datefinabsence'] = $line->datefinabsence;
																								$tab_result['heurefinabsence'] = $line->heurefinabsence;
																								$tab_result['coordonneesabsence'] = $line->coordonneesabsence;
																								$tab_result['lieudestinantionabsence'] = $line->lieudestinantionabsence;
																								$tab_result['heurearattraper'] = $line->heurearattraper;
																								$tab_result['status'] = $line->status;
																								$tab_result['created_at'] = $line->created_at;
																								$tab_result['updated_at'] = $line->updated_at;
																								$tab_result['create_by'] = $line->create_by;
																								$tab_result['updated_by'] = $line->updated_by;
																								$tab_all_result[] = $tab_result;
																							}

																							return [
																								'status' => '000',
																								'message' => 'succes_creation_absence',
																								'data' => $tab_all_result
																							];
																						} else {
																							return [
																								'status' => '002',
																								'message' => 'update_error',
																								'error' => $new_record->getErrors(),
																							];
																						}
																					} else {
																						return [
																							'status' => '001',
																							'message' => 'absence_date_ask_error',
																						];
																					}
																				} else {
																					return [
																						'status' => '002',
																						'message' => 'Accès non autorisé.',
																					];
																				}
																			} else if ($operation == 2) { // MISE A JOUR
																				if ($test[2] == '1') { // INDICE : 0 pour CREATE; 1 pour READ; 2 pour UPDATE; 3 pour DELETE;
																					if (isset($_POST['abscence_key']) && $_POST['abscence_key'] != '') {
																						$abscence_key = $_POST['abscence_key'];
																						$old_record = Absence::find()->where(['keyabsence' => $abscence_key, 'status' => 1])->one();
																						if ($old_record != '') {
																							$old_record->iduser = $personnel_id;
																							$old_record->idtypeabsence = $type_absence_id;
																							$old_record->motifabsence = $motif;
																							$old_record->datedemandeabsence = $date_demande;
																							$old_record->datedebutabsence = $date_debut;
																							$old_record->datefinabsence = $date_fin;
																							$old_record->heuredebutabsence = $heure_debut;
																							$old_record->heurefinabsence = $heure_fin;
																							$old_record->coordonneesabsence = $coordonnees;
																							$old_record->lieudestinantionabsence = $lieu_destinantion;
																							$old_record->heurearattraper = $heure_a_rattraper;
																							$old_record->updated_at = time();
																							$old_record->updated_by = $user_exist->id;
																							if ($old_record->save()) {

																								$data = Absence::find()->where(['status' => [1, 2]])->orderBy(['created_at' => SORT_ASC])->all();

																								$tab_all_result = array();
																								$tab_result = array();
																								foreach ($data as $line) {
																									$user = User::find()->where(['id' => $line->iduser])->one();

																									$tab_result['id'] = $line->id;
																									$tab_result['idtypeabsence'] = $line->idtypeabsence;
																									$tab_result['designationtypeabsence'] = $line->idtypeabsence0->designationtypeabsence;
																									$tab_result['iduser'] = $line->iduser;
																									$tab_result['nom'] = $user->nom;
																									$tab_result['prenoms'] = $user->prenoms;
																									$tab_result['keyabsence'] = $line->keyabsence;
																									$tab_result['datedemandeabsence'] = $line->datedemandeabsence;
																									$tab_result['motifabsence'] = $line->motifabsence;
																									$tab_result['datedebutabsence'] = $line->datedebutabsence;
																									$tab_result['heuredebutabsence'] = $line->heuredebutabsence;
																									$tab_result['datefinabsence'] = $line->datefinabsence;
																									$tab_result['heurefinabsence'] = $line->heurefinabsence;
																									$tab_result['coordonneesabsence'] = $line->coordonneesabsence;
																									$tab_result['lieudestinantionabsence'] = $line->lieudestinantionabsence;
																									$tab_result['heurearattraper'] = $line->heurearattraper;
																									$tab_result['status'] = $line->status;
																									$tab_result['created_at'] = $line->created_at;
																									$tab_result['updated_at'] = $line->updated_at;
																									$tab_result['create_by'] = $line->create_by;
																									$tab_result['updated_by'] = $line->updated_by;
																									$tab_all_result[] = $tab_result;
																								}

																								return [
																									'status' => '000',
																									'message' => 'succes_update_absence',
																									'data' => $tab_all_result
																								];
																							} else {
																								return [
																									'status' => '002',
																									'message' => 'update_error',
																								];
																							}
																						} else {
																							return [
																								'status' => '001',
																								'message' => 'absence_not_found',
																							];
																						}
																					} else {
																						return [
																							'status' => '001',
																							'message' => 'absence_key_empty',
																						];
																					}
																				} else {
																					return [
																						'status' => '002',
																						'message' => 'Accès non autorisé.',
																					];
																				}
																			} else {
																				return [
																					'status' => '002',
																					'message' => 'Type d\'opération non reconnu.',
																				];
																			}
																		} else {
																			return [
																				'status' => '002',
																				'message' => 'Le type d\'opération ne doit pas être vide.',
																			];
																		}
																	} else {
																		return [
																			'status' => '001',
																			'message' => 'type_absence_not_found',
																		];
																	}
																} else {
																	return [
																		'status' => '001',
																		'message' => 'type_absence_key_empty',
																	];
																}
															} else {
																return [
																	'status' => '001',
																	'message' => 'personnel_not_found',
																];
															}
														} else {
															return [
																'status' => '001',
																'message' => 'personnel_key_empty',
															];
														}
													} else {
														return [
															'status' => '001',
															'message' => 'absence_dates_diff',
														];
													}
												} else {
													return [
														'status' => '001',
														'message' => 'absence_date_fin_empty',
													];
												}
											} else {
												return [
													'status' => '001',
													'message' => 'absence_date_debut_empty',
												];
											}
										} else {
											return [
												'status' => '001',
												'message' => 'absence_date_demande_empty',
											];
										}
									} else {
										return [
											'status' => '001',
											'message' => 'absence_motif_empty',
										];
									}
								} else {
									return [
										'status' => '002',
										'message' => 'Cet utilisateur n\'a pas accès à ce centre.',
									];
								}
							} else {
								return [
									'status' => '002',
									'message' => 'id_center est incorrect.',
								];
							}
						} else {
							return [
								'status' => '002',
								'message' => 'id_center ne doit pas être vide.',
							];
						}
					} else {
						return [
							'status' => '002',
							'message' => 'Utilisateur non trouvé.',
						];
					}
				} else {
					return [
						'status' => '002',
						'message' => 'user_auth_key ne doit pas être vide.',
					];
				}
			} else {
				return [
					'status' => '002',
					'message' => 'Token incorrect.',
				];
			}
		} else {
			return [
				'status' => '002',
				'message' => 'Aucune donnée reçue en POST.',
			];
		}
		exit;
	}

	public function actionDeleteabsence()
	{
		$token =  Utils::getApiKey();
		if (Yii::$app->request->post()) {
			if (isset($_POST['token']) && $_POST['token'] == $token) {
				if (isset($_POST['user_auth_key']) && $_POST['user_auth_key'] != '') {
					$user_auth_key = $_POST['user_auth_key'];
					$user_exist = User::find()->where(['auth_key' => $user_auth_key, 'status' => 10])->one();
					if ($user_exist != '') {

						if (isset($_POST['id_center']) && $_POST['id_center'] != '') {
							$id_center = $_POST['id_center'];

							$center = SchoolCenter::find()->where(['id_center' => $id_center, 'etat' => [1, 2, 4]])->one();
							if ($center != '') {
								$cont = false;
								$user_id_center = str_replace('-', '', $user_exist->id_center);
								$test_center = explode(",", $user_id_center);

								if ($user_exist->id_center == "1") {
									$cont = true;
								} else if (in_array($id_center, $test_center)) {
									$cont = true;
								}

								if ($cont == true) {

									$manage_absence = Utils::have_accessapi("manage_absence", $user_exist->idP, $user_exist->id, $id_center);
									$test = explode('_', $manage_absence);
									if ($test[3] == '1') { // INDICE : 0 pour CREATE; 1 pour READ; 2 pour UPDATE; 3 pour DELETE;
										if (isset($_POST['operation']) && $_POST['operation'] != '') {
											$operation = $_POST['operation'];
											if (isset($_POST['abscence_key']) && $_POST['abscence_key'] != '') {
												$abscence_key = $_POST['abscence_key'];
												$old_record = Absence::find()->where(['keyabsence' => $abscence_key])->one();
												if ($old_record != '') {
													if ($operation == 3) { // SUPPRESSION
														$old_record->status = 3;
														$msg = "delete_absence_success";
													} else {
														return [
															'status' => '002',
															'message' => 'Type d\'opération non reconnu',
														];
													}
													$old_record->updated_at = time();
													$old_record->updated_by = $user_exist->id;
													if ($old_record->save()) {
														return [
															'status' => '000',
															'message' => $msg,
														];
													} else {
														return [
															'status' => '002',
															'message' => 'update_error',
														];
													}
												} else {
													return [
														'status' => '001',
														'message' => 'absence_not_found',
													];
												}
											} else {
												return [
													'status' => '001',
													'message' => 'absence_key_empty',
												];
											}
										} else {
											return [
												'status' => '002',
												'message' => 'Le type d\'opération ne doit pas être vide.',
											];
										}
									} else {
										return [
											'status' => '002',
											'message' => 'Accès non autorisé.',
										];
									}
								} else {
									return [
										'status' => '002',
										'message' => 'Cet utilisateur n\'a pas accès à ce centre.',
									];
								}
							} else {
								return [
									'status' => '002',
									'message' => 'id_center est incorrect.',
								];
							}
						} else {
							return [
								'status' => '002',
								'message' => 'id_center ne doit pas être vide.',
							];
						}
					} else {
						return [
							'status' => '002',
							'message' => 'Utilisateur non trouvé.',
						];
					}
				} else {
					return [
						'status' => '002',
						'message' => 'user_auth_key ne doit pas être vide.',
					];
				}
			} else {
				return [
					'status' => '002',
					'message' => 'Token incorrect.',
				];
			}
		} else {
			return [
				'status' => '002',
				'message' => 'Aucune donnée reçue en POST.',
			];
		}
		exit;
	}

	public function actionAbsences()
	{
		$token =  Utils::getApiKey();
		$cont = false;
		if (Yii::$app->request->post()) {
			if (isset($_POST['token']) && $_POST['token'] == $token) {
				if (isset($_POST['user_auth_key']) && $_POST['user_auth_key'] != '') {
					$user_auth_key = $_POST['user_auth_key'];
					$user_exist = User::find()->where(['auth_key' => $user_auth_key, 'status' => 10])->one();
					if ($user_exist != '') {
						if (isset($_POST['id_center']) && $_POST['id_center'] != '') {
							$id_center = $_POST['id_center'];

							$center = SchoolCenter::find()->where(['id_center' => $id_center, 'etat' => [1, 2, 4]])->one();
							if ($center != '') {
								$cont = false;
								$user_id_center = str_replace('-', '', $user_exist->id_center);
								$test_center = explode(",", $user_id_center);

								if ($user_exist->id_center == "1") {
									$cont = true;
								} else if (in_array($id_center, $test_center)) {
									$cont = true;
								}

								if ($cont == true) {


									$manage_absence = Utils::have_accessapi("manage_absence", $user_exist->idP, $user_exist->id, $id_center);
									$test = explode('_', $manage_absence);
									if ($test[1] == '1') {

										$data = Absence::find()->where(['status' => [1, 2]])->orderBy(['created_at' => SORT_ASC])->all();

										$tab_all_result = array();
										$tab_result = array();
										foreach ($data as $line) {
											$user = User::find()->where(['id' => $line->iduser])->one();


											$tab_result['id'] = $line->id;
											$tab_result['idtypeabsence'] = $line->idtypeabsence;
											$tab_result['designationtypeabsence'] = $line->idtypeabsence0->designationtypeabsence;
											$tab_result['iduser'] = $line->iduser;
											$tab_result['nom'] = $user->nom;
											$tab_result['prenoms'] = $user->prenoms;
											$tab_result['keyabsence'] = $line->keyabsence;
											$tab_result['datedemandeabsence'] = $line->datedemandeabsence;
											$tab_result['motifabsence'] = $line->motifabsence;
											$tab_result['datedebutabsence'] = $line->datedebutabsence;
											$tab_result['heuredebutabsence'] = $line->heuredebutabsence;
											$tab_result['datefinabsence'] = $line->datefinabsence;
											$tab_result['heurefinabsence'] = $line->heurefinabsence;
											$tab_result['coordonneesabsence'] = $line->coordonneesabsence;
											$tab_result['lieudestinantionabsence'] = $line->lieudestinantionabsence;
											$tab_result['heurearattraper'] = $line->heurearattraper;
											$tab_result['status'] = $line->status;
											$tab_result['created_at'] = $line->created_at;
											$tab_result['updated_at'] = $line->updated_at;
											$tab_result['create_by'] = $line->create_by;
											$tab_result['updated_by'] = $line->updated_by;
											$tab_all_result[] = $tab_result;
										}

										return [
											'status' => '000',
											'message' => 'succes_get_absence',
											'data' => $tab_all_result
										];
									} else {
										return [
											'status' => '002',
											'message' => 'Accès non autorisé.',
										];
									}
								} else {
									return [
										'status' => '002',
										'message' => 'Cet utilisateur n\'a pas accès à ce centre.',
									];
								}
							} else {
								return [
									'status' => '002',
									'message' => 'id_center est incorrect.',
								];
							}
						} else {
							return [
								'status' => '002',
								'message' => 'id_center ne doit pas être vide.',
							];
						}
					} else {
						return [
							'status' => '002',
							'message' => 'Utilisateur non trouvé.',
						];
					}
				} else {
					return [
						'status' => '002',
						'message' => 'user_auth_key ne doit pas être vide.',
					];
				}
			} else {
				return [
					'status' => '002',
					'message' => 'Token incorrect.',
				];
			}
		} else {
			return [
				'status' => '002',
				'message' => 'Aucune donnée reçue en POST.',
			];
		}
		exit;
	}
}
