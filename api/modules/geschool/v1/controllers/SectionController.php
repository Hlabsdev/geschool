<?php

namespace api\modules\geschool\v1\controllers;
use api\modules\geschool\v1\models\Section;
use api\modules\geschool\v1\models\Sectionformation;
use api\modules\geschool\v1\models\Typeformation;
use Yii;
use yii\db\Query;
use yii\rest\ActiveController;
use api\modules\geschool\v1\models\User;
use api\modules\geschool\v1\models\SchoolCenter;

/**
 * Geschool Controller API
 *
 */

class SectionController extends ActiveController
{
    public $modelClass = 'api\modules\geschool\v1\models\Section';

    #000 -> Tout est bien
    #001 -> Message client
    #002 -> Message dev


    public function actionSections()
    {
        $token = Utils::getApiKey();
        if (Yii::$app->request->post()) {
            if (isset($_POST['token']) && $_POST['token'] == $token) {
                if (isset($_POST['user_auth_key']) && $_POST['user_auth_key'] != '') {
                    $user_auth_key = $_POST['user_auth_key'];
                    $user_exist = User::find()->where(['auth_key' => $user_auth_key, 'status' => 10])->one();
                    if ($user_exist != '') {

                        if (isset($_POST['id_center']) && $_POST['id_center'] != '') {
                            $id_center = $_POST['id_center'];

                            $center = SchoolCenter::find()->where(['id_center' => $id_center, 'etat' => [1, 2, 4]])->one();
                            if ($center != '') {
                                $cont = false;
                                $user_id_center = str_replace('-', '', $user_exist->id_center);
                                $test_center = explode(",", $user_id_center);

                                if ($user_exist->id_center == "1") {
                                    $cont = true;
                                } else if (in_array($id_center, $test_center)) {
                                    $cont = true;
                                }

                                if ($cont == true) {

                                    $manage_section = Utils::have_accessapi("manage_section", $user_exist->idP, $user_exist->id, $id_center);
                                    $test = explode('_', $manage_section);
                                    if ($test[1] == '1') { // INDICE : 0 pour CREATE; 1 pour READ; 2 pour UPDATE; 3 pour DELETE;

                                        //Les sections d'un type de  formation
                                        if (isset($_POST['type_formation']) && $_POST['type_formation'] != '') {
                                            $typeformation = $_POST['type_formation'];
                                            //$data = Section::find()->where(['status' => [1, 2]])->orderBy(['designation' => SORT_ASC])->all();
                                            $query = new Query();
                                            $data = $query
                                                ->select(['s.*',
                                                    'sf.idtypeformation','tf.libelletypeformation'
                                                ])
                                                ->from('section s')
                                                ->join('INNER JOIN','sectionformation sf','sf.idsection = s.id')
                                                ->join('INNER JOIN','typeformation tf','sf.idtypeformation = tf.id')
                                                ->where(['s.status'=> [1, 2],'sf.status'=> [1, 2],'idtypeformation'=>$typeformation])
                                                ->orderBy(['designation' => SORT_ASC])
                                                ->all()
                                            ;

                                        }else{
                                            //Toutes les sections
                                            //$data = Section::find()->where(['status' => [1, 2]])->orderBy(['designation' => SORT_ASC])->all();
                                            $query = new Query();
                                            $data = $query
                                                ->select(['s.*',
                                                    'sf.idtypeformation','tf.libelletypeformation'
                                                ])
                                                ->from('section s')
                                                ->join('INNER JOIN','sectionformation sf','sf.idsection = s.id')
                                                ->join('INNER JOIN','typeformation tf','sf.idtypeformation = tf.id')
                                                ->where(['s.status'=> [1, 2],'sf.status'=> [1, 2]])
                                                ->orderBy(['designation' => SORT_ASC])
                                                ->all()
                                            ;
                                        }


                                        if (sizeof($data) > 0) {
                                            return [
                                                'status' => '000',
                                                'message' => 'succes_get_section',
                                                'data' => $data
                                            ];
                                        } else {
                                            return [
                                                'status' => '001',
                                                'message' => 'empty_liste_section',
                                            ];
                                        }

                                    } else {
                                        return [
                                            'status' => '002',
                                            'message' => 'Accès non autorisé.',
                                        ];
                                    }
                                } else {
                                    return [
                                        'status' => '002',
                                        'message' => 'Cet utilisateur n\'a pas accès à ce centre.',
                                    ];
                                }
                            } else {
                                return [
                                    'status' => '002',
                                    'message' => 'id_center est incorrect.',
                                ];
                            }
                        } else {
                            return [
                                'status' => '002',
                                'message' => 'id_center ne doit pas être vide.',
                            ];
                        }
                    } else {
                        return [
                            'status' => '002',
                            'message' => 'Utilisateur non trouvé.',
                        ];
                    }
                } else {
                    return [
                        'status' => '002',
                        'message' => 'user_auth_key ne doit pas être vide.',
                    ];
                }
            } else {
                return [
                    'status' => '002',
                    'message' => 'Token incorrect',
                ];
            }
        } else {
            return [
                'status' => '002',
                'message' => 'Aucune donnée reçue en POST',
            ];
        }
        exit;
    }

    public function actionAddsection()
    {
        $token = Utils::getApiKey();
        if (Yii::$app->request->post()) {

            if (isset($_POST['token']) && $_POST['token'] == $token) {
                if (isset($_POST['user_auth_key']) && $_POST['user_auth_key'] != '') {
                    $user_auth_key = $_POST['user_auth_key'];
                    $user_exist = User::find()->where(['auth_key' => $user_auth_key, 'status' => 10])->one();
                    if ($user_exist != '') {

                        if (isset($_POST['id_center']) && $_POST['id_center'] != '') {
                            $id_center = $_POST['id_center'];

                            $center = SchoolCenter::find()->where(['id_center' => $id_center, 'etat' => [1, 2, 4]])->one();
                            if ($center != '') {
                                $cont = false;
                                $user_id_center = str_replace('-', '', $user_exist->id_center);
                                $test_center = explode(",", $user_id_center);

                                if ($user_exist->id_center == "1") {
                                    $cont = true;
                                } else if (in_array($id_center, $test_center)) {
                                    $cont = true;
                                }

                                if ($cont == true) {

                                    $manage_section = Utils::have_accessapi("manage_section", $user_exist->idP, $user_exist->id, $id_center);
                                    $test = explode('_', $manage_section);

                                    if (isset($_POST['id_type_formation']) && $_POST['id_type_formation'] != '') {
                                        $id_type_formation = $_POST['id_type_formation'];

                                        $find_type_formation = Typeformation::find()->where(['id' => $id_type_formation, 'status' => [1, 2]])->one();
                                        if ($find_type_formation !== null) {

                                            if (isset($_POST['designation']) && $_POST['designation'] != '') {
                                                $designation = $_POST['designation'];

                                                if (isset($_POST['operation']) && $_POST['operation'] != '') {
                                                    $operation = $_POST['operation'];
                                                    if ($operation == 1) { // AJOUT
                                                        if ($test[0] == '1') { // INDICE : 0 pour CREATE; 1 pour READ; 2 pour UPDATE; 3 pour DELETE;
                                                           /*
                                                            $query = new Query();
                                                            $exist = $query
                                                                ->select(['s.*'
                                                                ])
                                                                ->from('section s')
                                                                ->join('INNER JOIN','sectionformation sf','sf.idsection = s.id')
                                                                ->where(['s.status'=> [1, 2],'s.designation'=> $designation,'idtypeformation'=> $find_type_formation->id])
                                                                ->orderBy(['designation' => SORT_ASC])
                                                                ->one();
                                                            */
                                                            $exist = Section::find()->where([
                                                                'designation' => $designation,
                                                                'status' => [1, 2]
                                                            ])->one();

                                                            if ($exist == '' or $exist == null) {
                                                                $new_record = new Section();
                                                                $new_record->designation = $designation;
                                                                $new_record->keysection = Yii::$app->security->generateRandomString(32);
                                                                $new_record->status = 1;
                                                                $new_record->created_at = time();
                                                                $new_record->updated_at = '';
                                                                $new_record->create_by = $user_exist->id;
                                                                $new_record->updated_by = '';
                                                                //print_r($new_record);exit;
                                                                if ($new_record->save()) {
                                                                    //Lier au type de formation
                                                                    $new_detail_section_formation = new Sectionformation();
                                                                    $new_detail_section_formation->keysectionformation = Yii::$app->security->generateRandomString(32);
                                                                    $new_detail_section_formation->idsection = $new_record->id;
                                                                    $new_detail_section_formation->idtypeformation = $find_type_formation->id;
                                                                    $new_detail_section_formation->status =1;
                                                                    $new_detail_section_formation->created_at = time();
                                                                    $new_detail_section_formation->create_by = $user_exist->id;
                                                                    $new_detail_section_formation->save();

                                                                    //$data = Section::find()->where(['status' => [1, 2]])->orderBy(['designation' => SORT_ASC])->all();
                                                                    $query = new Query();
                                                                    $data = $query
                                                                        ->select(['s.*',
                                                                            'sf.idtypeformation','tf.libelletypeformation'
                                                                        ])
                                                                        ->from('section s')
                                                                        ->join('INNER JOIN','sectionformation sf','sf.idsection = s.id')
                                                                        ->join('INNER JOIN','typeformation tf','sf.idtypeformation = tf.id')
                                                                        ->where(['s.status'=> [1, 2],'sf.status'=> [1, 2]])
                                                                        ->orderBy(['designation' => SORT_ASC])
                                                                        ->all()
                                                                    ;

                                                                    return [
                                                                        'status' => '000',
                                                                        'message' => 'succes_creation_section',
                                                                        'data' => $data,
                                                                    ];
                                                                } else {
                                                                    return [
                                                                        'status' => '002',
                                                                        'message' => 'update_error',
                                                                    ];
                                                                }
                                                            } else {
                                                                return [
                                                                    'status' => '003',
                                                                    'message' => 'section_used',
                                                                ];
                                                            }
                                                        } else {
                                                            return [
                                                                'status' => '002',
                                                                'message' => 'Accès non autorisé.',
                                                            ];
                                                        }
                                                    } else if ($operation == 2) { // MISE A JOUR
                                                        if ($test[2] == '1') { // INDICE : 0 pour CREATE; 1 pour READ; 2 pour UPDATE; 3 pour DELETE;

                                                            if (isset($_POST['section_key']) && $_POST['section_key'] != '') {
                                                                $section_key = $_POST['section_key'];
                                                                $old_record = Section::find()->where(['keysection' => $section_key])->one();
                                                                if ($old_record != '') {
                                                                    //Pour vérifier s'il existe une autre occurence avec les mêmes informations
                                                                    $exist = Section::find()->where([
                                                                        'designation'=>$designation,
                                                                        'status' => [1, 2]
                                                                    ])->one();

                                                                    if ($exist == '' or $exist == null) {
                                                                        $continue = true;
                                                                    } else {
                                                                        ($exist->keysection != $section_key) ? $continue = false : $continue = true;
                                                                    }

                                                                    if ($continue == true) {
                                                                        $old_record->designation = $designation;
                                                                        $old_record->updated_at = time();
                                                                        $old_record->updated_by = $user_exist->id;
                                                                        if ($old_record->save()) {
                                                                            //Msj du détail
                                                                            $detail_sectionformation = Sectionformation::find()->where(['idsection' => $old_record->id])->one();
                                                                            $detail_sectionformation->idtypeformation = $find_type_formation->id;                                                                            $detail_sectionformation->updated_at = time();
                                                                            $detail_sectionformation->updated_at = time();
                                                                            $detail_sectionformation->updated_by = $user_exist->id;
                                                                            $detail_sectionformation->save();

                                                                            //$data = Section::find()->where(['status' => [1, 2]])->orderBy(['designation' => SORT_ASC])->all();

                                                                            $query = new Query();
                                                                            $data = $query
                                                                                ->select(['s.*',
                                                                                    'sf.idtypeformation','tf.libelletypeformation'
                                                                                ])
                                                                                ->from('section s')
                                                                                ->join('INNER JOIN','sectionformation sf','sf.idsection = s.id')
                                                                                ->join('INNER JOIN','typeformation tf','sf.idtypeformation = tf.id')
                                                                                ->where(['s.status'=> [1, 2],'sf.status'=> [1, 2]])
                                                                                ->orderBy(['designation' => SORT_ASC])
                                                                                ->all()
                                                                            ;
                                                                            return [
                                                                                'status' => '000',
                                                                                'message' => 'succes_update_section',
                                                                                'data' => $data,
                                                                            ];
                                                                        } else {
                                                                            return [
                                                                                'status' => '002',
                                                                                'message' => 'update_error',
                                                                            ];
                                                                        }
                                                                    } else {
                                                                        return [
                                                                            'status' => '003',
                                                                            'message' => 'section_used',
                                                                        ];
                                                                    }
                                                                } else {
                                                                    return [
                                                                        'status' => '001',
                                                                        'message' => 'section_not_found',
                                                                    ];
                                                                }
                                                            } else {
                                                                return [
                                                                    'status' => '001',
                                                                    'message' => 'section_key_empty',
                                                                ];
                                                            }
                                                        } else {
                                                            return [
                                                                'status' => '002',
                                                                'message' => 'Accès non autorisé.',
                                                            ];
                                                        }
                                                    } else {
                                                        return [
                                                            'status' => '002',
                                                            'message' => 'Type d\'opération non reconnu.',
                                                        ];
                                                    }
                                                } else {
                                                    return [
                                                        'status' => '002',
                                                        'message' => 'Le type d\'opération ne doit pas être vide.',
                                                    ];
                                                }

                                            } else {
                                                return [
                                                    'status' => '001',
                                                    'message' => 'section_designation_empty',
                                                ];
                                            }


                                        } else {
                                            return [
                                                'status' => '001',
                                                'message' => 'type_formation_not_found',
                                            ];
                                        }
                                    } else {
                                        return [
                                            'status' => '001',
                                            'message' => 'type_formation_key_empty',
                                        ];
                                    }
                                } else {
                                    return [
                                        'status' => '002',
                                        'message' => 'Cet utilisateur n\'a pas accès à ce centre.',
                                    ];
                                }
                            } else {
                                return [
                                    'status' => '002',
                                    'message' => 'id_center est incorrect.',
                                ];
                            }
                        } else {
                            return [
                                'status' => '002',
                                'message' => 'id_center ne doit pas être vide.',
                            ];
                        }
                    } else {
                        return [
                            'status' => '002',
                            'message' => 'Utilisateur non trouvé',
                        ];
                    }
                } else {
                    return [
                        'status' => '002',
                        'message' => 'user_auth_key ne doit pas être vide',
                    ];
                }
            } else {
                return [
                    'status' => '002',
                    'message' => 'Token incorrect',
                ];
            }
        } else {
            return [
                'status' => '002',
                'message' => 'Aucune donnée reçue en POST',
            ];
        }
        exit;
    }

    public function actionSection()
    {
        $token = Utils::getApiKey();
        if (Yii::$app->request->post()) {
            if (isset($_POST['token']) && $_POST['token'] == $token) {
                if (isset($_POST['user_auth_key']) && $_POST['user_auth_key'] != '') {
                    $user_auth_key = $_POST['user_auth_key'];
                    $user_exist = User::find()->where(['auth_key' => $user_auth_key, 'status' => 10])->one();
                    if ($user_exist != '') {

                        if (isset($_POST['id_center']) && $_POST['id_center'] != '') {
                            $id_center = $_POST['id_center'];

                            $center = SchoolCenter::find()->where(['id_center' => $id_center, 'etat' => [1, 2, 4]])->one();
                            if ($center != '') {
                                $cont = false;
                                $user_id_center = str_replace('-', '', $user_exist->id_center);
                                $test_center = explode(",", $user_id_center);

                                if ($user_exist->id_center == "1") {
                                    $cont = true;
                                } else if (in_array($id_center, $test_center)) {
                                    $cont = true;
                                }

                                if ($cont == true) {

                                    $manage_section = Utils::have_accessapi("manage_section", $user_exist->idP, $user_exist->id, $id_center);
                                    $test = explode('_', $manage_section);
                                    if ($test[2] == '1') { // INDICE : 0 pour CREATE; 1 pour READ; 2 pour UPDATE; 3 pour DELETE;


                                        if (isset($_POST['section_key']) && $_POST['section_key'] != '') {
                                            $section_key = $_POST['section_key'];
                                            //$section_exist = Section::find()->where(['keysection' => $section_key])->one();
                                            $query = new Query();
                                            $section_exist = $query
                                                ->select(['s.*',
                                                    'sf.idtypeformation','tf.libelletypeformation'
                                                ])
                                                ->from('section s')
                                                ->join('INNER JOIN','sectionformation sf','sf.idsection = s.id')
                                                ->join('INNER JOIN','typeformation tf','sf.idtypeformation = tf.id')
                                                ->where(['keysection' => $section_key,'sf.status'=> [1, 2]])
                                                ->orderBy(['designation' => SORT_ASC])
                                                ->one()
                                            ;
                                            if ($section_exist != '') {
                                                return [
                                                    'status' => '000',
                                                    'message' => 'succes_get_section',
                                                    'data' => $section_exist,
                                                ];
                                            } else {
                                                return [
                                                    'status' => '001',
                                                    'message' => 'section_not_found',
                                                ];
                                            }
                                        } else {
                                            return [
                                                'status' => '001',
                                                'message' => 'section_key_empty',
                                            ];
                                        }
                                    } else {
                                        return [
                                            'status' => '002',
                                            'message' => 'Accès non autorisé.',
                                        ];
                                    }
                                } else {
                                    return [
                                        'status' => '002',
                                        'message' => 'Cet utilisateur n\'a pas accès à ce centre.',
                                    ];
                                }
                            } else {
                                return [
                                    'status' => '002',
                                    'message' => 'id_center est incorrect.',
                                ];
                            }
                        } else {
                            return [
                                'status' => '002',
                                'message' => 'id_center ne doit pas être vide.',
                            ];
                        }
                    } else {
                        return [
                            'status' => '002',
                            'message' => 'Utilisateur non trouvé.',
                        ];
                    }
                } else {
                    return [
                        'status' => '002',
                        'message' => 'user_auth_key ne doit pas être vide.',
                    ];
                }
            } else {
                return [
                    'status' => '002',
                    'message' => 'Token incorrect.',
                ];
            }
        } else {
            return [
                'status' => '002',
                'message' => 'Aucune donnée reçue en POST.',
            ];
        }
        exit;
    }

    public function actionDeletesection()
    {
        $token = Utils::getApiKey();

        if (Yii::$app->request->post()) {
            if (isset($_POST['token']) && $_POST['token'] == $token) {
                if (isset($_POST['user_auth_key']) && $_POST['user_auth_key'] != '') {
                    $user_auth_key = $_POST['user_auth_key'];
                    $user_exist = User::find()->where(['auth_key' => $user_auth_key, 'status' => 10])->one();
                    if ($user_exist != '') {

                        if (isset($_POST['id_center']) && $_POST['id_center'] != '') {
                            $id_center = $_POST['id_center'];

                            $center = SchoolCenter::find()->where(['id_center' => $id_center, 'etat' => [1, 2, 4]])->one();
                            if ($center != '') {
                                $cont = false;
                                $user_id_center = str_replace('-', '', $user_exist->id_center);
                                $test_center = explode(",", $user_id_center);

                                if ($user_exist->id_center == "1") {
                                    $cont = true;
                                } else if (in_array($id_center, $test_center)) {
                                    $cont = true;
                                }

                                if ($cont == true) {

                                    $manage_section = Utils::have_accessapi("manage_section", $user_exist->idP, $user_exist->id, $id_center);
                                    $test = explode('_', $manage_section);
                                    if ($test[3] == '1') { // INDICE : 0 pour CREATE; 1 pour READ; 2 pour UPDATE; 3 pour DELETE;

                                        if (isset($_POST['operation']) && $_POST['operation'] != '') {
                                            $operation = $_POST['operation'];
                                            if (isset($_POST['section_key']) && $_POST['section_key'] != '') {
                                                $section_key = $_POST['section_key'];
                                                $old_record = Section::find()->where(['keysection' => $section_key])->one();
                                                if ($old_record != '') {

                                                    if ($operation == 3) { // SUPPRESSION
                                                        $old_record->status = 3;
                                                        $msg = "delete_section_success";
                                                    } else if ($operation == 2) { // DESACTIVATION
                                                        $old_record->status = 2;
                                                        $msg = "desactive_section_success";
                                                    } else if ($operation == 1) { // ACTIVATION
                                                        $old_record->status = 1;
                                                        $msg = "active_section_success";
                                                    } else {
                                                        return [
                                                            'status' => '002',
                                                            'message' => 'Type d\'opération non reconnu',
                                                        ];
                                                    }

                                                        $old_record->updated_at = time();
                                                        $old_record->updated_by = $user_exist->id;
                                                        if ($old_record->save()) {
                                                            //$data = Section::find()->where(['status' => [1, 2]])->orderBy(['designation' => SORT_ASC])->all();

                                                            //Msj du détail
                                                            $detail_sectionformation = Sectionformation::find()->where(['idsection' => $old_record->id])->one();
                                                            $detail_sectionformation->status = $operation;
                                                            $detail_sectionformation->updated_at = time();
                                                            $detail_sectionformation->updated_by = $user_exist->id;
                                                            $detail_sectionformation->save();


                                                            $query = new Query();
                                                            $data = $query
                                                                ->select(['s.*',
                                                                    'sf.idtypeformation','tf.libelletypeformation'
                                                                ])
                                                                ->from('section s')
                                                                ->join('INNER JOIN','sectionformation sf','sf.idsection = s.id')
                                                                ->join('INNER JOIN','typeformation tf','sf.idtypeformation = tf.id')
                                                                ->where(['s.status'=> [1, 2],'sf.status'=> [1, 2]])
                                                                ->orderBy(['designation' => SORT_ASC])
                                                                ->all()
                                                            ;

                                                            return [
                                                                'status' => '000',
                                                                'message' =>$msg,
                                                                'data' => $data,
                                                            ];
                                                        } else {
                                                            return [
                                                                'status' => '002',
                                                                'message' => 'update_error',
                                                            ];
                                                        }

                                                } else {
                                                    return [
                                                        'status' => '001',
                                                        'message' => 'section_not_found',
                                                    ];
                                                }
                                            } else {
                                                return [
                                                    'status' => '001',
                                                    'message' => 'section_key_empty',
                                                ];
                                            }

                                        } else {
                                            return [
                                                'status' => '002',
                                                'message' => 'Le type d\'opération ne doit pas être vide.',
                                            ];
                                        }
                                    } else {
                                        return [
                                            'status' => '002',
                                            'message' => 'Accès non autorisé.',
                                        ];
                                    }
                                } else {
                                    return [
                                        'status' => '002',
                                        'message' => 'Cet utilisateur n\'a pas accès à ce centre.',
                                    ];
                                }
                            } else {
                                return [
                                    'status' => '002',
                                    'message' => 'id_center est incorrect.',
                                ];
                            }
                        } else {
                            return [
                                'status' => '002',
                                'message' => 'id_center ne doit pas être vide.',
                            ];
                        }
                    } else {
                        return [
                            'status' => '002',
                            'message' => 'Utilisateur non trouvé.',
                        ];
                    }
                } else {
                    return [
                        'status' => '002',
                        'message' => 'user_auth_key ne doit pas être vide.',
                    ];
                }
            } else {
                return [
                    'status' => '002',
                    'message' => 'Token incorrect.',
                ];
            }
        } else {
            return [
                'status' => '002',
                'message' => 'Aucune donnée reçue en POST.',
            ];
        }
        exit;
    }

}
