<?php

namespace api\modules\geschool\v1\controllers;
use api\modules\geschool\v1\models\Cours;
use api\modules\geschool\v1\models\Courscentre;
use api\modules\geschool\v1\models\Typecours;
use Yii;
use yii\db\Query;
use yii\rest\ActiveController;
use api\modules\geschool\v1\models\User;
use api\modules\geschool\v1\models\SchoolCenter;

/**
 * Geschool Controller API
 *
 */

class CoursController extends ActiveController
{
    public $modelClass = 'api\modules\geschool\v1\models\Cours';

    #000 -> Tout est bien
    #001 -> Message client
    #002 -> Message dev

    //Cette méthode nous permet de récupérer les types de cours du système
    public function actionTypecours()
    {
        $token = Utils::getApiKey();

        if (Yii::$app->request->post()) {
            if (isset($_POST['token']) && $_POST['token'] == $token) {
                if (isset($_POST['user_auth_key']) && $_POST['user_auth_key'] != '') {
                    $user_auth_key = $_POST['user_auth_key'];
                    $user_exist = User::find()->where(['auth_key' => $user_auth_key, 'status' => 10])->one();
                    if ($user_exist != '') {

                        if (isset($_POST['id_center']) && $_POST['id_center'] != '') {
                            $id_center = $_POST['id_center'];

                            $center = SchoolCenter::find()->where(['id_center' => $id_center, 'etat' => [1, 2, 4]])->one();
                            if ($center != '') {
                                $cont = false;
                                $user_id_center = str_replace('-', '', $user_exist->id_center);
                                $test_center = explode(",", $user_id_center);

                                if ($user_exist->id_center == "1") {
                                    $cont = true;
                                } else if (in_array($id_center, $test_center)) {
                                    $cont = true;
                                }

                                if ($cont == true) {

                                    $manage_cours = Utils::have_accessapi("manage_cours", $user_exist->idP, $user_exist->id, $id_center);
                                    $test = explode('_', $manage_cours);
                                    if ($test[1] == '1') { // INDICE : 0 pour CREATE; 1 pour READ; 2 pour UPDATE; 3 pour DELETE;

                                        $data = Typecours::find()->where(['status' => [1]])->orderBy(['libelletypecours' => SORT_ASC])->all();
                                        if (sizeof($data) > 0) {
                                            return [
                                                'status' => '000',
                                                'message' => 'succes_get_type_cours',
                                                'data' => $data
                                            ];
                                        } else {
                                            return [
                                                'status' => '001',
                                                'message' => 'empty_liste_type_cours',
                                            ];
                                        }
                                    } else {
                                        return [
                                            'status' => '002',
                                            'message' => 'Accès non autorisé.',
                                        ];
                                    }
                                } else {
                                    return [
                                        'status' => '002',
                                        'message' => 'Cet utilisateur n\'a pas accès à ce centre.',
                                    ];
                                }
                            } else {
                                return [
                                    'status' => '002',
                                    'message' => 'id_center est incorrect.',
                                ];
                            }
                        } else {
                            return [
                                'status' => '002',
                                'message' => 'id_center ne doit pas être vide.',
                            ];
                        }
                    } else {
                        return [
                            'status' => '002',
                            'message' => 'Utilisateur non trouvé.',
                        ];
                    }
                } else {
                    return [
                        'status' => '002',
                        'message' => 'user_auth_key ne doit pas être vide.',
                    ];
                }
            } else {
                return [
                    'status' => '002',
                    'message' => 'Token incorrect',
                ];
            }
        } else {
            return [
                'status' => '002',
                'message' => 'Aucune donnée reçue en POST',
            ];
        }

        exit;
    }

    //Cette méthode nous permet de récupérer les cours d'un centre
    public function actionCours()
    {
        $token = Utils::getApiKey();
        if (Yii::$app->request->post()) {
            if (isset($_POST['token']) && $_POST['token'] == $token) {
                if (isset($_POST['user_auth_key']) && $_POST['user_auth_key'] != '') {
                    $user_auth_key = $_POST['user_auth_key'];
                    $user_exist = User::find()->where(['auth_key' => $user_auth_key, 'status' => 10])->one();
                    if ($user_exist != '') {

                        if (isset($_POST['id_center']) && $_POST['id_center'] != '') {
                            $id_center = $_POST['id_center'];

                            $center = SchoolCenter::find()->where(['id_center' => $id_center, 'etat' => [1, 2, 4]])->one();
                            if ($center != '') {
                                $cont = false;
                                $user_id_center = str_replace('-', '', $user_exist->id_center);
                                $test_center = explode(",", $user_id_center);

                                if ($user_exist->id_center == "1") {
                                    $cont = true;
                                } else if (in_array($id_center, $test_center)) {
                                    $cont = true;
                                }

                                if ($cont == true) {

                                    $manage_cours = Utils::have_accessapi("manage_cours", $user_exist->idP, $user_exist->id, $id_center);
                                    $test = explode('_', $manage_cours);
                                    if ($test[1] == '1') { // INDICE : 0 pour CREATE; 1 pour READ; 2 pour UPDATE; 3 pour DELETE;

                                        $query = new Query();
                                        $data = $query
                                            ->select(['c.*'
                                            ])
                                            ->from('cours c')
                                            ->join('INNER JOIN','courscentre cc','c.id = cc.idcours')
                                            ->where(['c.status'=> [1, 2],
                                                'cc.idcentre'=>[1,$center->id_center]])
                                            ->orderBy(['designation' => SORT_ASC])
                                            ->all()
                                        ;

                                        if (sizeof($data) > 0) {
                                            return [
                                                'status' => '000',
                                                'message' => 'succes_get_cours',
                                                'data' => $data
                                            ];
                                        } else {
                                            return [
                                                'status' => '001',
                                                'message' => 'empty_liste_cours',
                                            ];
                                        }



                                    } else {
                                        return [
                                            'status' => '002',
                                            'message' => 'Accès non autorisé.',
                                        ];
                                    }
                                } else {
                                    return [
                                        'status' => '002',
                                        'message' => 'Cet utilisateur n\'a pas accès à ce centre.',
                                    ];
                                }
                            } else {
                                return [
                                    'status' => '002',
                                    'message' => 'id_center est incorrect.',
                                ];
                            }
                        } else {
                            return [
                                'status' => '002',
                                'message' => 'id_center ne doit pas être vide.',
                            ];
                        }
                    } else {
                        return [
                            'status' => '002',
                            'message' => 'Utilisateur non trouvé.',
                        ];
                    }
                } else {
                    return [
                        'status' => '002',
                        'message' => 'user_auth_key ne doit pas être vide.',
                    ];
                }
            } else {
                return [
                    'status' => '002',
                    'message' => 'Token incorrect',
                ];
            }
        } else {
            return [
                'status' => '002',
                'message' => 'Aucune donnée reçue en POST',
            ];
        }
        exit;
    }

    //Cette méthode nous permet d'ajouter les cours d'un centre
    public function actionAddcours()
    {
        $token =Utils::getApiKey();
        if (Yii::$app->request->post()) {

            if (isset($_POST['token']) && $_POST['token'] == $token) {
                if (isset($_POST['user_auth_key']) && $_POST['user_auth_key'] != '') {
                    $user_auth_key = $_POST['user_auth_key'];
                    $user_exist = User::find()->where(['auth_key' => $user_auth_key, 'status' => 10])->one();
                    if ($user_exist != '') {

                        if (isset($_POST['id_center']) && $_POST['id_center'] != '') {
                            $id_center = $_POST['id_center'];

                            $center = SchoolCenter::find()->where(['id_center' => $id_center, 'etat' => [1, 2, 4]])->one();
                            if ($center != '') {
                                $cont = false;
                                $user_id_center = str_replace('-', '', $user_exist->id_center);
                                $test_center = explode(",", $user_id_center);

                                if ($user_exist->id_center == "1") {
                                    $cont = true;
                                } else if (in_array($id_center, $test_center)) {
                                    $cont = true;
                                }

                                if ($cont == true) {

                                    $manage_cours = Utils::have_accessapi("manage_cours", $user_exist->idP, $user_exist->id, $id_center);
                                    $test = explode('_', $manage_cours);

                                    if (isset($_POST['designation']) && $_POST['designation'] != '') {
                                        $designation = $_POST['designation'];
                                        if(isset($_POST['typecours']) && $_POST['typecours'] != ''){
                                            $idtypecours = $_POST['typecours'];

                                            $typecours = Typecours::find()->where(['id' => $idtypecours, 'status' => 1])->one();
                                            if ($typecours !== null) {

                                                if (isset($_POST['operation']) && $_POST['operation'] != '') {
                                                    $operation = $_POST['operation'];
                                                    if ($operation == 1) { // AJOUT
                                                        if ($test[0] == '1') { // INDICE : 0 pour CREATE; 1 pour READ; 2 pour UPDATE; 3 pour DELETE;

                                                            $exist = Cours::find()->where([
                                                                'designation' => $designation,
                                                                'idtypecours'=>$typecours,
                                                                'status' => [1, 2]
                                                            ])->one();
                                                            if ($exist == '' or $exist == null) {
                                                                $new_record = new Cours();
                                                                $new_record->designation = $designation;
                                                                $new_record->idtypecours = $typecours->id;
                                                                $new_record->keycours = Yii::$app->security->generateRandomString(32);
                                                                $new_record->status = 1;
                                                                $new_record->created_at = time();
                                                                $new_record->updated_at = '';
                                                                $new_record->create_by = $user_exist->id;
                                                                $new_record->updated_by = '';
                                                                //print_r($new_record);exit;
                                                                if ($new_record->save()) {
                                                                    //Ajout du cours au centre concernée
                                                                    $newCourscentre = new Courscentre();
                                                                    $newCourscentre->idcours = $new_record->id;
                                                                    $newCourscentre->idcentre = $center->id_center;
                                                                    $newCourscentre->created_at = time();
                                                                    $newCourscentre->create_by = $user_exist->id;
                                                                    $newCourscentre->save();

                                                                    $query = new Query();
                                                                    $data = $query
                                                                        ->select(['c.*'
                                                                        ])
                                                                        ->from('cours c')
                                                                        ->join('INNER JOIN','courscentre cc','c.id = cc.idcours')
                                                                        ->where(['c.status'=> [1, 2],
                                                                            'cc.idcentre'=>[1,$center->id_center]])
                                                                        ->orderBy(['designation' => SORT_ASC])
                                                                        ->all()
                                                                    ;
                                                                    return [
                                                                        'status' => '000',
                                                                        'message' => 'succes_creation_cours',
                                                                        'data' => $data,
                                                                    ];
                                                                } else {
                                                                    return [
                                                                        'status' => '002',
                                                                        'message' => 'update_error',
                                                                    ];
                                                                }
                                                            } else {
                                                                return [
                                                                    'status' => '003',
                                                                    'message' => 'cours_used',
                                                                ];
                                                            }
                                                        } else {
                                                            return [
                                                                'status' => '002',
                                                                'message' => 'Accès non autorisé.',
                                                            ];
                                                        }
                                                    } else if ($operation == 2) { // MISE A JOUR
                                                        if ($test[2] == '1') { // INDICE : 0 pour CREATE; 1 pour READ; 2 pour UPDATE; 3 pour DELETE;

                                                            if (isset($_POST['cours_key']) && $_POST['cours_key'] != '') {
                                                                $cours_key = $_POST['cours_key'];
                                                                $old_record = Cours::find()->where(['keycours' => $cours_key, 'status' => [1,2]])->one();
                                                                if ($old_record != '') {
                                                                    //Pour vérifier s'il existe une autre occurence avec les mêmes informations du cours
                                                                    $exist = Cours::find()->where([
                                                                        'designation'=>$designation,
                                                                        'status' => [1, 2]
                                                                    ])->one();

                                                                    if ($exist == '' or $exist == null) {
                                                                        $continue = true;
                                                                    } else {
                                                                        ($exist->keycours != $cours_key) ? $continue = false : $continue = true;
                                                                    }

                                                                    if ($continue == true) {
                                                                        $old_record->designation = $designation;
                                                                        $old_record->idtypecours = $typecours->id;
                                                                        $old_record->updated_at = time();
                                                                        $old_record->updated_by = $user_exist->id;
                                                                        if ($old_record->save()) {
                                                                            $query = new Query();
                                                                            $data = $query
                                                                                ->select(['c.*'
                                                                                ])
                                                                                ->from('cours c')
                                                                                ->join('INNER JOIN','courscentre cc','c.id = cc.idcours')
                                                                                ->where(['c.status'=> [1, 2],
                                                                                    'cc.idcentre'=>[1,$center->id_center]])
                                                                                ->orderBy(['designation' => SORT_ASC])
                                                                                ->all()
                                                                            ;
                                                                            return [
                                                                                'status' => '000',
                                                                                'message' => 'succes_update_cours',
                                                                                'data' => $data,
                                                                            ];
                                                                        } else {
                                                                            return [
                                                                                'status' => '002',
                                                                                'message' => 'update_error',
                                                                            ];
                                                                        }
                                                                    } else {
                                                                        return [
                                                                            'status' => '003',
                                                                            'message' => 'cours_used',
                                                                        ];
                                                                    }
                                                                } else {
                                                                    return [
                                                                        'status' => '001',
                                                                        'message' => 'cours_not_found',
                                                                    ];
                                                                }
                                                            } else {
                                                                return [
                                                                    'status' => '001',
                                                                    'message' => 'cours_key ne peut pas être vide.',
                                                                ];
                                                            }
                                                        } else {
                                                            return [
                                                                'status' => '002',
                                                                'message' => 'Accès non autorisé.',
                                                            ];
                                                        }
                                                    } else {
                                                        return [
                                                            'status' => '002',
                                                            'message' => 'Type d\'opération non reconnu.',
                                                        ];
                                                    }
                                                } else {
                                                    return [
                                                        'status' => '002',
                                                        'message' => 'Le type d\'opération ne doit pas être vide.',
                                                    ];
                                                }

                                            } else {
                                                return [
                                                    'status' => '001',
                                                    'message' => 'cours_type_not_found',
                                                ];
                                            }


                                        }else{
                                            return [
                                                'status' => '001',
                                                'message' => 'cours_type_empty',
                                            ];
                                        }

                                    } else {
                                        return [
                                            'status' => '001',
                                            'message' => 'cours_designation_empty',
                                        ];
                                    }

                                } else {
                                    return [
                                        'status' => '002',
                                        'message' => 'Cet utilisateur n\'a pas accès à ce centre.',
                                    ];
                                }
                            } else {
                                return [
                                    'status' => '002',
                                    'message' => 'id_center est incorrect.',
                                ];
                            }
                        } else {
                            return [
                                'status' => '002',
                                'message' => 'id_center ne doit pas être vide.',
                            ];
                        }
                    } else {
                        return [
                            'status' => '002',
                            'message' => 'Utilisateur non trouvé',
                        ];
                    }
                } else {
                    return [
                        'status' => '002',
                        'message' => 'user_auth_key ne doit pas être vide',
                    ];
                }
            } else {
                return [
                    'status' => '002',
                    'message' => 'Token incorrect',
                ];
            }
        } else {
            return [
                'status' => '002',
                'message' => 'Aucune donnée reçue en POST',
            ];
        }
        exit;
    }

    //Cette méthode nous permet de récupérer un cours spécifique
    public function actionCour()
    {
        $token =Utils::getApiKey();
        if (Yii::$app->request->post()) {
            if (isset($_POST['token']) && $_POST['token'] == $token) {
                if (isset($_POST['user_auth_key']) && $_POST['user_auth_key'] != '') {
                    $user_auth_key = $_POST['user_auth_key'];
                    $user_exist = User::find()->where(['auth_key' => $user_auth_key, 'status' => 10])->one();
                    if ($user_exist != '') {

                        if (isset($_POST['id_center']) && $_POST['id_center'] != '') {
                            $id_center = $_POST['id_center'];

                            $center = SchoolCenter::find()->where(['id_center' => $id_center, 'etat' => [1, 2, 4]])->one();
                            if ($center != '') {
                                $cont = false;
                                $user_id_center = str_replace('-', '', $user_exist->id_center);
                                $test_center = explode(",", $user_id_center);

                                if ($user_exist->id_center == "1") {
                                    $cont = true;
                                } else if (in_array($id_center, $test_center)) {
                                    $cont = true;
                                }

                                if ($cont == true) {

                                    $manage_cours = Utils::have_accessapi("manage_cours", $user_exist->idP, $user_exist->id, $id_center);
                                    $test = explode('_', $manage_cours);
                                    if ($test[2] == '1') { // INDICE : 0 pour CREATE; 1 pour READ; 2 pour UPDATE; 3 pour DELETE;


                                        if (isset($_POST['cours_key']) && $_POST['cours_key'] != '') {
                                            $cours_key = $_POST['cours_key'];
                                            $cours_exist = Cours::find()->where(['keycours' => $cours_key])->one();
                                            if ($cours_exist != '') {
                                                return [
                                                    'status' => '000',
                                                    'message' => 'succes_get_cours',
                                                    'data' => $cours_exist,
                                                ];
                                            } else {
                                                return [
                                                    'status' => '001',
                                                    'message' => 'cours_not_found',
                                                ];
                                            }
                                        } else {
                                            return [
                                                'status' => '001',
                                                'message' => 'cours_key_empty',
                                            ];
                                        }
                                    } else {
                                        return [
                                            'status' => '002',
                                            'message' => 'Accès non autorisé.',
                                        ];
                                    }
                                } else {
                                    return [
                                        'status' => '002',
                                        'message' => 'Cet utilisateur n\'a pas accès à ce centre.',
                                    ];
                                }
                            } else {
                                return [
                                    'status' => '002',
                                    'message' => 'id_center est incorrect.',
                                ];
                            }
                        } else {
                            return [
                                'status' => '002',
                                'message' => 'id_center ne doit pas être vide.',
                            ];
                        }
                    } else {
                        return [
                            'status' => '002',
                            'message' => 'Utilisateur non trouvé.',
                        ];
                    }
                } else {
                    return [
                        'status' => '002',
                        'message' => 'user_auth_key ne doit pas être vide.',
                    ];
                }
            } else {
                return [
                    'status' => '002',
                    'message' => 'Token incorrect.',
                ];
            }
        } else {
            return [
                'status' => '002',
                'message' => 'Aucune donnée reçue en POST.',
            ];
        }
        exit;
    }

    //Cette méthode nous permet de suprimmer, de désactiver et activer un cours spécifique
    public function actionDeletecours()
    {
        $token =Utils::getApiKey();

        if (Yii::$app->request->post()) {
            if (isset($_POST['token']) && $_POST['token'] == $token) {
                if (isset($_POST['user_auth_key']) && $_POST['user_auth_key'] != '') {
                    $user_auth_key = $_POST['user_auth_key'];
                    $user_exist = User::find()->where(['auth_key' => $user_auth_key, 'status' => 10])->one();
                    if ($user_exist != '') {

                        if (isset($_POST['id_center']) && $_POST['id_center'] != '') {
                            $id_center = $_POST['id_center'];

                            $center = SchoolCenter::find()->where(['id_center' => $id_center, 'etat' => [1, 2, 4]])->one();
                            if ($center != '') {
                                $cont = false;
                                $user_id_center = str_replace('-', '', $user_exist->id_center);
                                $test_center = explode(",", $user_id_center);

                                if ($user_exist->id_center == "1") {
                                    $cont = true;
                                } else if (in_array($id_center, $test_center)) {
                                    $cont = true;
                                }

                                if ($cont == true) {

                                    $manage_cours = Utils::have_accessapi("manage_cours", $user_exist->idP, $user_exist->id, $id_center);
                                    $test = explode('_', $manage_cours);
                                    if ($test[3] == '1') { // INDICE : 0 pour CREATE; 1 pour READ; 2 pour UPDATE; 3 pour DELETE;

                                        if (isset($_POST['operation']) && $_POST['operation'] != '') {
                                            $operation = $_POST['operation'];
                                            if (isset($_POST['cours_key']) && $_POST['cours_key'] != '') {
                                                $cours_key = $_POST['cours_key'];
                                                $old_record = Cours::find()->where(['keycours' => $cours_key])->one();
                                                if ($old_record != '') {

                                                    if ($operation == 3) { // SUPPRESSION
                                                        $old_record->status = 3;
                                                        $msg = "delete_cours_success";
                                                    } else if ($operation == 2) { // DESACTIVATION
                                                        $old_record->status = 2;
                                                        $msg = "desactive_cours_success";
                                                    } else if ($operation == 1) { // ACTIVATION
                                                        $old_record->status = 1;
                                                        $msg = "active_cours_success";
                                                    } else {
                                                        return [
                                                            'status' => '002',
                                                            'message' => 'Type d\'opération non reconnu',
                                                        ];
                                                    }

                                                        $old_record->updated_at = time();
                                                        $old_record->updated_by = $user_exist->id;
                                                        if ($old_record->save()) {

                                                            $query = new Query();
                                                            $data = $query
                                                                ->select(['c.*'
                                                                ])
                                                                ->from('cours c')
                                                                ->join('INNER JOIN','courscentre cc','c.id = cc.idcours')
                                                                ->where(['c.status'=> [1, 2],
                                                                    'cc.idcentre'=>[1,$center->id_center]])
                                                                ->orderBy(['designation' => SORT_ASC])
                                                                ->all();



                                                            return [
                                                                'status' => '000',
                                                                'message' =>$msg,
                                                                'data' => $data,
                                                            ];
                                                        } else {
                                                            return [
                                                                'status' => '002',
                                                                'message' => 'update_error',
                                                            ];
                                                        }

                                                } else {
                                                    return [
                                                        'status' => '001',
                                                        'message' => 'cours_not_found',
                                                    ];
                                                }
                                            } else {
                                                return [
                                                    'status' => '001',
                                                    'message' => 'cours_key_empty',
                                                ];
                                            }

                                        } else {
                                            return [
                                                'status' => '002',
                                                'message' => 'Le type d\'opération ne doit pas être vide.',
                                            ];
                                        }
                                    } else {
                                        return [
                                            'status' => '002',
                                            'message' => 'Accès non autorisé.',
                                        ];
                                    }
                                } else {
                                    return [
                                        'status' => '002',
                                        'message' => 'Cet utilisateur n\'a pas accès à ce centre.',
                                    ];
                                }
                            } else {
                                return [
                                    'status' => '002',
                                    'message' => 'id_center est incorrect.',
                                ];
                            }
                        } else {
                            return [
                                'status' => '002',
                                'message' => 'id_center ne doit pas être vide.',
                            ];
                        }
                    } else {
                        return [
                            'status' => '002',
                            'message' => 'Utilisateur non trouvé.',
                        ];
                    }
                } else {
                    return [
                        'status' => '002',
                        'message' => 'user_auth_key ne doit pas être vide.',
                    ];
                }
            } else {
                return [
                    'status' => '002',
                    'message' => 'Token incorrect.',
                ];
            }
        } else {
            return [
                'status' => '002',
                'message' => 'Aucune donnée reçue en POST.',
            ];
        }
        exit;
    }

}
