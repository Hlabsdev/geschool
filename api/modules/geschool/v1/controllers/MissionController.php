<?php

namespace api\modules\geschool\v1\controllers;

use api\modules\geschool\v1\models\Absence;
use api\modules\geschool\v1\models\Detailmission;
use api\modules\geschool\v1\models\Mission;
use api\modules\geschool\v1\models\Personnel;
use api\modules\geschool\v1\models\SchoolCenter;
use api\modules\geschool\v1\models\Typeabsence;
use api\modules\geschool\v1\models\Typepersonnel;
use api\modules\geschool\v1\models\User;
use DateTime;
use Yii;
use yii\db\Query;
use yii\rest\ActiveController;

/**
 * Geschool Controller API
 *
 */
class MissionController extends ActiveController
{
	public $modelClass = 'api\modules\geschool\v1\models\Mission';

	#000 -> Tout est bien
	#001 -> Message client
	#002 -> Message dev

	//Cette méthode permet de récuperer les missions d'un centre
	public function actionMissions()
	{
		$token = Utils::getApiKey();
		if (Yii::$app->request->post()) {
			if (isset($_POST['token']) && $_POST['token'] == $token) {
				if (isset($_POST['user_auth_key']) && $_POST['user_auth_key'] != '') {
					$user_auth_key = $_POST['user_auth_key'];
					$user_exist = User::find()->where(['auth_key' => $user_auth_key, 'status' => 10])->one();
					if ($user_exist != '') {

						if (isset($_POST['id_center']) && $_POST['id_center'] != '') {
							$id_center = $_POST['id_center'];

							$center = SchoolCenter::find()->where(['id_center' => $id_center, 'etat' => [1, 2, 4]])->one();
							if ($center != '') {
								$cont = false;
								$user_id_center = str_replace('-', '', $user_exist->id_center);
								$test_center = explode(",", $user_id_center);

								if ($user_exist->id_center == "1") {
									$cont = true;
								} else if (in_array($id_center, $test_center)) {
									$cont = true;
								}

								if ($cont == true) {


									$manage_mission = Utils::have_accessapi("manage_mission", $user_exist->idP, $user_exist->id, $id_center);
									$test = explode('_', $manage_mission);
									if ($test[1] == '1') { // INDICE : 0 pour CREATE; 1 pour READ; 2 pour UPDATE; 3 pour DELETE;
										$data = Mission::find()->where(['status' => [1, 2], 'idcentre' => $center->id_center])->orderBy(['created_at' => SORT_ASC])->all();
										if (sizeof($data) > 0) {
											return [
												'status' => '000',
												'message' => 'succes_get_mission',
												'data' => $data
											];
										} else {
											return [
												'status' => '001',
												'message' => 'empty_liste_mission',
											];
										}
									} else {
										return [
											'status' => '002',
											'message' => 'Accès non autorisé.',
										];
									}
								} else {
									return [
										'status' => '002',
										'message' => 'Cet utilisateur n\'a pas accès à ce centre.',
									];
								}
							} else {
								return [
									'status' => '002',
									'message' => 'id_center est incorrect.',
								];
							}
						} else {
							return [
								'status' => '002',
								'message' => 'id_center ne doit pas être vide.',
							];
						}
					} else {
						return [
							'status' => '002',
							'message' => 'Utilisateur non trouvé.',
						];
					}
				} else {
					return [
						'status' => '002',
						'message' => 'user_auth_key ne doit pas être vide.',
					];
				}
			} else {
				return [
					'status' => '002',
					'message' => 'Token incorrect.',
				];
			}
		} else {
			return [
				'status' => '002',
				'message' => 'Aucune donnée reçue en POST.',
			];
		}
		exit;
	}

	//Cette méthode permet de récuperer une mission spécifique
	public function actionMission()
	{
		$token = Utils::getApiKey();
		if (Yii::$app->request->post()) {
			if (isset($_POST['token']) && $_POST['token'] == $token) {
				if (isset($_POST['user_auth_key']) && $_POST['user_auth_key'] != '') {
					$user_auth_key = $_POST['user_auth_key'];
					$user_exist = User::find()->where(['auth_key' => $user_auth_key, 'status' => 10])->one();
					if ($user_exist != '') {

						if (isset($_POST['id_center']) && $_POST['id_center'] != '') {
							$id_center = $_POST['id_center'];

							$center = SchoolCenter::find()->where(['id_center' => $id_center, 'etat' => [1, 2, 4]])->one();
							if ($center != '') {
								$cont = false;
								$user_id_center = str_replace('-', '', $user_exist->id_center);
								$test_center = explode(",", $user_id_center);

								if ($user_exist->id_center == "1") {
									$cont = true;
								} else if (in_array($id_center, $test_center)) {
									$cont = true;
								}

								if ($cont == true) {

									$manage_mission = Utils::have_accessapi("manage_mission", $user_exist->idP, $user_exist->id, $id_center);
									$test = explode('_', $manage_mission);
									if ($test[2] == '1') { // INDICE : 0 pour CREATE; 1 pour READ; 2 pour UPDATE; 3 pour DELETE;
										if (isset($_POST['mission_key']) && $_POST['mission_key'] != '') {
											$mission_key = $_POST['mission_key'];
											$mission_exist = Mission::find()->where(['keymission' => $mission_key])->one();
											if ($mission_exist != '') {
												return [
													'status' => '000',
													'message' => 'succes_get_mission',
													'data' => $mission_exist,
												];
											} else {
												return [
													'status' => '001',
													'message' => 'mission_not_found',
												];
											}
										} else {
											return [
												'status' => '001',
												'message' => 'mission_key_empty',
											];
										}
									} else {
										return [
											'status' => '002',
											'message' => 'Accès non autorisé.',
										];
									}
								} else {
									return [
										'status' => '002',
										'message' => 'Cet utilisateur n\'a pas accès à ce centre.',
									];
								}
							} else {
								return [
									'status' => '002',
									'message' => 'id_center est incorrect.',
								];
							}
						} else {
							return [
								'status' => '002',
								'message' => 'id_center ne doit pas être vide.',
							];
						}
					} else {
						return [
							'status' => '002',
							'message' => 'Utilisateur non trouvé.',
						];
					}
				} else {
					return [
						'status' => '002',
						'message' => 'user_auth_key ne doit pas être vide.',
					];
				}
			} else {
				return [
					'status' => '002',
					'message' => 'Token incorrect.',
				];
			}
		} else {
			return [
				'status' => '002',
				'message' => 'Aucune donnée reçue en POST.',
			];
		}
		exit;
	}

	//Cette méthode permet d'ajouter une missions avec ces détails
	public function actionAddmission()
	{
		$token = Utils::getApiKey();
		$motif = "";
		$itineraire_retenu = "";
		if (Yii::$app->request->post()) {
			if (isset($_POST['token']) && $_POST['token'] == $token) {
				if (isset($_POST['user_auth_key']) && $_POST['user_auth_key'] != '') {
					$user_auth_key = $_POST['user_auth_key'];
					$user_exist = User::find()->where(['auth_key' => $user_auth_key, 'status' => 10])->one();
					if ($user_exist != '') {

						if (isset($_POST['id_center']) && $_POST['id_center'] != '') {
							$id_center = $_POST['id_center'];

							$center = SchoolCenter::find()->where(['id_center' => $id_center, 'etat' => [1, 2, 4]])->one();
							if ($center != '') {
								$cont = false;
								$user_id_center = str_replace('-', '', $user_exist->id_center);
								$test_center = explode(",", $user_id_center);

								if ($user_exist->id_center == "1") {
									$cont = true;
								} else if (in_array($id_center, $test_center)) {
									$cont = true;
								}

								if ($cont == true) {

									if (isset($_POST['lieu_mission']) && $_POST['lieu_mission'] != '') {
										$lieu_mission = $_POST['lieu_mission'];
										if (isset($_POST['date_depart']) && $_POST['date_depart'] != '') {
											$date_depart = $_POST['date_depart'];
											if (isset($_POST['date_retour_probable']) && $_POST['date_retour_probable'] != '') {
												$date_retour_probable = $_POST['date_retour_probable'];

												if (strtotime($date_depart) > strtotime($date_retour_probable)) {
													return [
														'status' => '001',
														'message' => 'mission_date_debut_sup_retour',
													];
												} else {
													if (isset($_POST['itineraire_retenu'])) $itineraire_retenu = $_POST['itineraire_retenu'];
													if (isset($_POST['motif'])) $motif = $_POST['motif'];
													if (isset($_POST['operation']) && $_POST['operation'] != '') {
														$operation = $_POST['operation'];
														$manage_mission = Utils::have_accessapi("manage_mission", $user_exist->idP, $user_exist->id, $id_center);
														$test = explode('_', $manage_mission);
														if ($operation == 1) { // AJOUT

															if ($test[0] == '1') { // INDICE : 0 pour CREATE; 1 pour READ; 2 pour UPDATE; 3 pour DELETE;
																$mission_exist = Mission::find()->where(['datedepart' => $date_depart, 'lieumission' => $lieu_mission, 'motif' => $motif, 'status' => [1, 2]])->one();
																if ($mission_exist == "" or $mission_exist == null) {

																	if (isset($_POST['detail_mission']) && $_POST['detail_mission'] != '') {

																		$detail_mission = json_decode($_POST['detail_mission']);

																		//Données test
																		/*$detail_mission = array();
																		for($i=0; $i< 3; $i++){

																			if($i+7 == 7){
																				$detail_ligne['keypersonnel'] = 'SzrQt6TazKv2ayDHqs754qvvAvD';
																			}

																			elseif($i+7 == 8){
																				$detail_ligne['keypersonnel'] = 'f-0l8GzMY9ovbmmsP6DV-gpJQOoZBadJ';
																			}

																			else{
																				$detail_ligne['keypersonnel'] = 'C_TF0ZhBEP0v7Uw4Zgz0txKBC3u-lfqc';
																			}

																			$detail_ligne['iduser'] = $i+7;
																			$detail_ligne['frais'] = 100000*$detail_ligne['iduser'];
																			$detail_ligne['contactabsence'] = '22890839666';

																			array_push($detail_mission,$detail_ligne);
																		}*/

																		//On vérifie les détails
																		$result_verif = 0;
																		$msg = "";
																		$unDetail = array();
																		foreach ($detail_mission as $unDetails) {
																			$unDetailCass = explode(";", $unDetails);
																			$unDetail['keypersonnel'] = $unDetailCass[0];
																			$unDetail['frais'] = $unDetailCass[1];
																			$unDetail['contactabsence'] = $unDetailCass[2];
																			$find_personnel = null;
																			$find_personnel = Personnel::find()->where(['keypersonnel' => $unDetail['keypersonnel']])->one();
																			// $find_personnel = Personnel::find()->where(['iduser' => $unDetail['iduser']])->one();
																			if ($find_personnel !== null) {
																				//Personnel Ok
																				if ($unDetail['frais'] != '' && $unDetail['contactabsence'] != '') {
																					// Frais et contact correct
																					$result_verif = 1;
																				} else {
																					$result_verif = 0;
																					$msg = 'detailmission_error';
																				}
																			} else {
																				$result_verif = 0;
																				$msg = 'detailmission_personnel_not_found';
																			}
																		}

																		//Fin verification
																		if ($result_verif) {
																			//Données correctes
																			$new_record = new Mission();
																			$new_record->keymission = Yii::$app->security->generateRandomString(32);
																			$new_record->motif = $motif;
																			$new_record->itineraireretenu = $itineraire_retenu;
																			$new_record->lieumission = $lieu_mission;
																			$new_record->datedepart = $date_depart;
																			$new_record->dateretourprob = $date_retour_probable;
																			$new_record->idcentre = $id_center;
																			$new_record->status = 1;
																			$new_record->created_at = time();
																			$new_record->create_by = $user_exist->id;
																			if ($new_record->save()) {
																				//On enregistre les détails
																				foreach ($detail_mission as $unDetails) {
																					$unDetailCass = explode(";", $unDetails);
																					$unDetail['iduser'] = $unDetailCass[0];
																					$unDetail['frais'] = $unDetailCass[1];
																					$unDetail['contactabsence'] = $unDetailCass[2];

																					$new_detail_mission = new Detailmission();
																					$new_detail_mission->keydetailmission = Yii::$app->security->generateRandomString(32);
																					// $new_detail_mission->iduser = $unDetail['iduser'];
																					$personnel = Personnel::find()->where(['keypersonnel' => $unDetail['keypersonnel']])->one();
																					$new_detail_mission->iduser = $personnel->iduser;

																					$new_detail_mission->idmission = $new_record->id;
																					$new_detail_mission->contactabsence = $unDetail['contactabsence'];
																					$new_detail_mission->frais = $unDetail['frais'];
																					$new_detail_mission->status = 1;
																					$new_detail_mission->create_by = $user_exist->id;
																					$new_detail_mission->created_at = time();
																					$new_detail_mission->save();
																				}

																				$data = Mission::find()->where(['status' => [1, 2], 'idcentre' => $center->id_center])->orderBy(['created_at' => SORT_ASC])->all();
																				return [
																					'status' => '000',
																					'message' => 'succes_creation_mission',
																					'data' => $data,
																				];
																			} else {
																				return [
																					'status' => '002',
																					'message' => 'update_error',
																				];
																			}
																		} else {
																			return [
																				'status' => '001',
																				'message' => $msg,
																			];
																		}
																	} else {
																		return [
																			'status' => '001',
																			'message' => 'mission_detail_empty',
																		];
																	}
																} else {
																	return [
																		'status' => '003',
																		'message' => 'mission_used',
																	];
																}
															} else {
																return [
																	'status' => '002',
																	'message' => 'Accès non autorisé.',
																];
															}
														} else if ($operation == 2) { // MODIFICATION

															if ($test[2] == '1') { // INDICE : 0 pour CREATE; 1 pour READ; 2 pour UPDATE; 3 pour DELETE;
																if (isset($_POST['mission_key']) && $_POST['mission_key'] != '') {
																	$mission_key = $_POST['mission_key'];
																	$old_record = Mission::find()->where(['keymission' => $mission_key, 'status' => [1, 2]])->one();
																	if ($old_record != '') {
																		$old_record->motif = $motif;
																		$old_record->itineraireretenu = $itineraire_retenu;
																		$old_record->lieumission = $lieu_mission;
																		$old_record->datedepart = $date_depart;
																		$old_record->dateretourprob = $date_retour_probable;
																		$old_record->updated_at = time();
																		$old_record->updated_by = $user_exist->id;
																		if ($old_record->save()) {
																			$data = Mission::find()->where(['status' => [1, 2], 'idcentre' => $center->id_center])->orderBy(['created_at' => SORT_ASC])->all();
																			return [
																				'status' => '000',
																				'message' => 'succes_update_mission',
																				'data' => $data,
																			];
																		} else {
																			return [
																				'status' => '002',
																				'message' => 'update_error',
																			];
																		}
																	} else {
																		return [
																			'status' => '003',
																			'message' => 'mission_not_found',
																		];
																	}
																} else {
																	return [
																		'status' => '001',
																		'message' => 'mission_key_empty',
																	];
																}
															} else {
																return [
																	'status' => '002',
																	'message' => 'Accès non autorisé.',
																];
															}
														} else {
															return [
																'status' => '002',
																'message' => 'Type d\'opération non reconnu.',
															];
														}
													} else {
														return [
															'status' => '002',
															'message' => 'Le type d\'opération ne doit pas être vide.',
														];
													}
												}
											} else {
												return [
													'status' => '001',
													'message' => 'mission_date_retour_empty',
												];
											}
										} else {
											return [
												'status' => '001',
												'message' => 'mission_date_depart_empty',
											];
										}
									} else {
										return [
											'status' => '001',
											'message' => 'mission_lieu_empty',
										];
									}
								} else {
									return [
										'status' => '002',
										'message' => 'Cet utilisateur n\'a pas accès à ce centre.',
									];
								}
							} else {
								return [
									'status' => '002',
									'message' => 'id_center est incorrect.',
								];
							}
						} else {
							return [
								'status' => '002',
								'message' => 'id_center ne doit pas être vide.',
							];
						}
					} else {
						return [
							'status' => '002',
							'message' => 'Utilisateur non trouvé.',
						];
					}
				} else {
					return [
						'status' => '002',
						'message' => 'user_auth_key ne doit pas être vide.',
					];
				}
			} else {
				return [
					'status' => '002',
					'message' => 'Token incorrect.',
				];
			}
		} else {
			return [
				'status' => '002',
				'message' => 'Aucune donnée reçue en POST.',
			];
		}
		exit;
	}


	//Cette méthode permet d'ajouter une missions avec ces détails
	public function actionAddmission_()
	{
		$token = Utils::getApiKey();
		$motif = "";
		$itineraire_retenu = "";
		if (Yii::$app->request->post()) {
			if (isset($_POST['token']) && $_POST['token'] == $token) {
				if (isset($_POST['user_auth_key']) && $_POST['user_auth_key'] != '') {
					$user_auth_key = $_POST['user_auth_key'];
					$user_exist = User::find()->where(['auth_key' => $user_auth_key, 'status' => 10])->one();
					if ($user_exist != '') {

						if (isset($_POST['id_center']) && $_POST['id_center'] != '') {
							$id_center = $_POST['id_center'];

							$center = SchoolCenter::find()->where(['id_center' => $id_center, 'etat' => [1, 2, 4]])->one();
							if ($center != '') {
								$cont = false;
								$user_id_center = str_replace('-', '', $user_exist->id_center);
								$test_center = explode(",", $user_id_center);

								if ($user_exist->id_center == "1") {
									$cont = true;
								} else if (in_array($id_center, $test_center)) {
									$cont = true;
								}

								if ($cont == true) {

									if (isset($_POST['lieu_mission']) && $_POST['lieu_mission'] != '') {
										$lieu_mission = $_POST['lieu_mission'];
										if (isset($_POST['date_depart']) && $_POST['date_depart'] != '') {
											$date_depart = $_POST['date_depart'];
											if (isset($_POST['date_retour_probable']) && $_POST['date_retour_probable'] != '') {
												$date_retour_probable = $_POST['date_retour_probable'];

												if (strtotime($date_depart) > strtotime($date_retour_probable)) {
													return [
														'status' => '001',
														'message' => 'mission_date_debut_sup_retour',
													];
												} else {
													if (isset($_POST['itineraire_retenu'])) $itineraire_retenu = $_POST['itineraire_retenu'];
													if (isset($_POST['motif'])) $motif = $_POST['motif'];
													if (isset($_POST['operation']) && $_POST['operation'] != '') {
														$operation = $_POST['operation'];
														$manage_mission = Utils::have_accessapi("manage_mission", $user_exist->idP, $user_exist->id, $id_center);
														$test = explode('_', $manage_mission);
														if ($operation == 1) { // AJOUT

															if ($test[0] == '1') { // INDICE : 0 pour CREATE; 1 pour READ; 2 pour UPDATE; 3 pour DELETE;
																$mission_exist = Mission::find()->where(['datedepart' => $date_depart, 'lieumission' => $lieu_mission, 'motif' => $motif, 'status' => [1, 2]])->one();
																if ($mission_exist == "" or $mission_exist == null) {

																	if (isset($_POST['detail_mission']) && $_POST['detail_mission'] != '') {

																		//$detail_mission = $_POST['detail_mission'];

																		//Données test
																		$detail_mission = array();
																		for ($i = 0; $i < 3; $i++) {

																			if ($i + 7 == 7) {
																				$detail_ligne['keypersonnel'] = 'SzrQt6TazKv2ayDHqs754qvvAvD';
																			} elseif ($i + 7 == 8) {
																				$detail_ligne['keypersonnel'] = 'f-0l8GzMY9ovbmmsP6DV-gpJQOoZBadJ';
																			} else {
																				$detail_ligne['keypersonnel'] = 'C_TF0ZhBEP0v7Uw4Zgz0txKBC3u-lfqc';
																			}

																			$detail_ligne['iduser'] = $i + 7;
																			$detail_ligne['frais'] = 100000 * $detail_ligne['iduser'];
																			$detail_ligne['contactabsence'] = '22890839666';

																			array_push($detail_mission, $detail_ligne);
																		}

																		//On vérifie les détails
																		$result_verif = 0;
																		$msg = "";
																		foreach ($detail_mission as $unDetail) {
																			$personnel = null;
																			//$find_personnel = Personnel::find()->where(['keypersonnel' => $unDetail['keypersonnel']])->one();
																			$find_personnel = Personnel::find()->where(['iduser' => $unDetail['iduser']])->one();
																			if ($find_personnel !== null) {
																				//Personnel Ok
																				if ($detail_ligne['frais'] != '' && $detail_ligne['contactabsence'] != '') {
																					// Frais et contact correct
																					$result_verif = 1;
																				} else {
																					$result_verif = 0;
																					$msg = 'detailmission_error';
																				}
																			} else {
																				$result_verif = 0;
																				$msg = 'detailmission_personnel_not_found';
																			}
																		}

																		//Fin verification
																		if ($result_verif) {
																			//Données correctes
																			$new_record = new Mission();
																			$new_record->keymission = Yii::$app->security->generateRandomString(32);
																			$new_record->motif = $motif;
																			$new_record->itineraireretenu = $itineraire_retenu;
																			$new_record->lieumission = $lieu_mission;
																			$new_record->datedepart = $date_depart;
																			$new_record->dateretourprob = $date_retour_probable;
																			$new_record->status = 1;
																			$new_record->created_at = time();
																			$new_record->create_by = $user_exist->id;
																			if ($new_record->save()) {
																				//On enregistre les détails
																				foreach ($detail_mission as $unDetail) {
																					$new_detail_mission = new Detailmission();
																					$new_detail_mission->keydetailmission = Yii::$app->security->generateRandomString(32);
																					$new_detail_mission->iduser = $unDetail['iduser'];
																					//$personnel = Personnel::find()->where(['keypersonnel' => $unDetail['keypersonnel']])->one();
																					//$new_detail_mission->iduser =$personnel->iduser ;

																					$new_detail_mission->idmission = $new_record->id;
																					$new_detail_mission->contactabsence = $unDetail['contactabsence'];
																					$new_detail_mission->frais = $unDetail['frais'];
																					$new_detail_mission->status = 1;
																					$new_detail_mission->create_by = $user_exist->id;
																					$new_detail_mission->created_at = time();
																					$new_detail_mission->save();
																				}

																				$data = Mission::find()->where(['status' => [1, 2]])->orderBy(['created_at' => SORT_ASC])->all();
																				return [
																					'status' => '000',
																					'message' => 'succes_creation_mission',
																					'data' => $data,
																				];
																			} else {
																				return [
																					'status' => '002',
																					'message' => 'update_error',
																				];
																			}
																		} else {
																			return [
																				'status' => '001',
																				'message' => $msg,
																			];
																		}
																	} else {
																		return [
																			'status' => '001',
																			'message' => 'mission_detail_empty',
																		];
																	}
																} else {
																	return [
																		'status' => '003',
																		'message' => 'mission_used',
																	];
																}
															} else {
																return [
																	'status' => '002',
																	'message' => 'Accès non autorisé.',
																];
															}
														} else if ($operation == 2) { // MODIFICATION

															if ($test[2] == '1') { // INDICE : 0 pour CREATE; 1 pour READ; 2 pour UPDATE; 3 pour DELETE;
																if (isset($_POST['mission_key']) && $_POST['mission_key'] != '') {
																	$mission_key = $_POST['mission_key'];
																	$old_record = Mission::find()->where(['keymission' => $mission_key, 'status' => [1, 2]])->one();
																	if ($old_record != '') {
																		$old_record->motif = $motif;
																		$old_record->itineraireretenu = $itineraire_retenu;
																		$old_record->lieumission = $lieu_mission;
																		$old_record->datedepart = $date_depart;
																		$old_record->dateretourprob = $date_retour_probable;
																		$old_record->updated_at = time();
																		$old_record->updated_by = $user_exist->id;
																		if ($old_record->save()) {
																			$data = Mission::find()->where(['status' => [1, 2]])->orderBy(['created_at' => SORT_ASC])->all();
																			return [
																				'status' => '000',
																				'message' => 'succes_update_mission',
																				'data' => $data,
																			];
																		} else {
																			return [
																				'status' => '002',
																				'message' => 'update_error',
																			];
																		}
																	} else {
																		return [
																			'status' => '003',
																			'message' => 'mission_not_found',
																		];
																	}
																} else {
																	return [
																		'status' => '001',
																		'message' => 'mission_key_empty',
																	];
																}
															} else {
																return [
																	'status' => '002',
																	'message' => 'Accès non autorisé.',
																];
															}
														} else {
															return [
																'status' => '002',
																'message' => 'Type d\'opération non reconnu.',
															];
														}
													} else {
														return [
															'status' => '002',
															'message' => 'Le type d\'opération ne doit pas être vide.',
														];
													}
												}
											} else {
												return [
													'status' => '001',
													'message' => 'mission_date_retour_empty',
												];
											}
										} else {
											return [
												'status' => '001',
												'message' => 'mission_date_depart_empty',
											];
										}
									} else {
										return [
											'status' => '001',
											'message' => 'mission_lieu_empty',
										];
									}
								} else {
									return [
										'status' => '002',
										'message' => 'Cet utilisateur n\'a pas accès à ce centre.',
									];
								}
							} else {
								return [
									'status' => '002',
									'message' => 'id_center est incorrect.',
								];
							}
						} else {
							return [
								'status' => '002',
								'message' => 'id_center ne doit pas être vide.',
							];
						}
					} else {
						return [
							'status' => '002',
							'message' => 'Utilisateur non trouvé.',
						];
					}
				} else {
					return [
						'status' => '002',
						'message' => 'user_auth_key ne doit pas être vide.',
					];
				}
			} else {
				return [
					'status' => '002',
					'message' => 'Token incorrect.',
				];
			}
		} else {
			return [
				'status' => '002',
				'message' => 'Aucune donnée reçue en POST.',
			];
		}
		exit;
	}


	//Cette fonction permet de supprimer une mission
	public function actionDeletemission()
	{
		$token = Utils::getApiKey();
		$msg = "";
		if (Yii::$app->request->post()) {
			if (isset($_POST['token']) && $_POST['token'] == $token) {
				if (isset($_POST['user_auth_key']) && $_POST['user_auth_key'] != '') {
					$user_auth_key = $_POST['user_auth_key'];
					$user_exist = User::find()->where(['auth_key' => $user_auth_key, 'status' => 10])->one();
					if ($user_exist != '') {

						if (isset($_POST['id_center']) && $_POST['id_center'] != '') {
							$id_center = $_POST['id_center'];

							$center = SchoolCenter::find()->where(['id_center' => $id_center, 'etat' => [1, 2, 4]])->one();
							if ($center != '') {
								$cont = false;
								$user_id_center = str_replace('-', '', $user_exist->id_center);
								$test_center = explode(",", $user_id_center);

								if ($user_exist->id_center == "1") {
									$cont = true;
								} else if (in_array($id_center, $test_center)) {
									$cont = true;
								}

								if ($cont == true) {

									$manage_mission = Utils::have_accessapi("manage_mission", $user_exist->idP, $user_exist->id, $id_center);
									$test = explode('_', $manage_mission);
									if ($test[3] == '1') { // INDICE : 0 pour CREATE; 1 pour READ; 2 pour UPDATE; 3 pour DELETE;
										if (isset($_POST['operation']) && $_POST['operation'] != '') {
											$operation = $_POST['operation'];
											if (isset($_POST['mission_key']) && $_POST['mission_key'] != '') {
												$mission_key = $_POST['mission_key'];
												$old_record = Mission::find()->where(['keymission' => $mission_key])->one();
												if ($old_record != '') {
													if ($operation == 3) { // SUPPRESSION
														$old_record->status = 3;
														$msg = "delete_mission_success";
														/*} else if ($operation == 2) { // DESACTIVATION
														$old_record->status = 2;
														$msg = "desactive_mission_success";
													} else if ($operation == 1) { // ACTIVATION
														$old_record->status = 1;
														$msg = "active_mission_success";*/
													} else {
														return [
															'status' => '002',
															'message' => 'Type d\'opération non reconnu',
														];
													}
													$old_record->updated_at = time();
													$old_record->updated_by = $user_exist->id;
													if ($old_record->save()) {
														//On supprime les détails
														$detailmissions = $old_record->detailmissions;
														foreach ($detailmissions as $unDetail) {
															$unDetail->status = 3;
															$unDetail->updated_by = $user_exist->id;
															$unDetail->updated_at = time();
															$unDetail->save();
														}
														return [
															'status' => '000',
															'message' => $msg,
															//'data' =>$detailmissions,
														];
													} else {
														return [
															'status' => '002',
															'message' => 'update_error',
														];
													}
												} else {
													return [
														'status' => '001',
														'message' => 'mission_not_found',
													];
												}
											} else {
												return [
													'status' => '001',
													'message' => 'mission_key_empty',
												];
											}
										} else {
											return [
												'status' => '002',
												'message' => 'Le type d\'opération ne doit pas être vide.',
											];
										}
									} else {
										return [
											'status' => '002',
											'message' => 'Accès non autorisé.',
										];
									}
								} else {
									return [
										'status' => '002',
										'message' => 'Cet utilisateur n\'a pas accès à ce centre.',
									];
								}
							} else {
								return [
									'status' => '002',
									'message' => 'id_center est incorrect.',
								];
							}
						} else {
							return [
								'status' => '002',
								'message' => 'id_center ne doit pas être vide.',
							];
						}
					} else {
						return [
							'status' => '002',
							'message' => 'Utilisateur non trouvé.',
						];
					}
				} else {
					return [
						'status' => '002',
						'message' => 'user_auth_key ne doit pas être vide.',
					];
				}
			} else {
				return [
					'status' => '002',
					'message' => 'Token incorrect.',
				];
			}
		} else {
			return [
				'status' => '002',
				'message' => 'Aucune donnée reçue en POST.',
			];
		}
		exit;
	}

	//Cette méthode permet de récuperer les détails d'une mission
	public function actionDetailmissions()
	{
		$token = Utils::getApiKey();
		if (Yii::$app->request->post()) {
			if (isset($_POST['token']) && $_POST['token'] == $token) {
				if (isset($_POST['user_auth_key']) && $_POST['user_auth_key'] != '') {
					$user_auth_key = $_POST['user_auth_key'];
					$user_exist = User::find()->where(['auth_key' => $user_auth_key, 'status' => 10])->one();
					if ($user_exist != '') {

						if (isset($_POST['id_center']) && $_POST['id_center'] != '') {
							$id_center = $_POST['id_center'];

							$center = SchoolCenter::find()->where(['id_center' => $id_center, 'etat' => [1, 2, 4]])->one();
							if ($center != '') {
								$cont = false;
								$user_id_center = str_replace('-', '', $user_exist->id_center);
								$test_center = explode(",", $user_id_center);

								if ($user_exist->id_center == "1") {
									$cont = true;
								} else if (in_array($id_center, $test_center)) {
									$cont = true;
								}

								if ($cont == true) {

									$manage_mission = Utils::have_accessapi("manage_mission", $user_exist->idP, $user_exist->id, $id_center);
									$test = explode('_', $manage_mission);
									if ($test[2] == '1') { // INDICE : 0 pour CREATE; 1 pour READ; 2 pour UPDATE; 3 pour DELETE;
										if (isset($_POST['mission_key']) && $_POST['mission_key'] != '') {
											$mission_key = $_POST['mission_key'];
											$mission_exist = Mission::find()->where(['keymission' => $mission_key])->one();
											if ($mission_exist !== null) {
												$data['mission'] = $mission_exist;
												//$data['detailmissions'] = $mission_exist->detailmissions;
												$query = new Query();
												$data['detailmissions'] = $query
													->select(['d.*',
														'u.nom','u.prenoms',
													])
													->from('detailmission d')
													//->join('INNER JOIN','mission m','m.id = d.idmission')
													->join('INNER JOIN','user u','u.id = d.iduser')
													->where(['d.status'=> 1,
														//'idcentre'=>$center->id_center,
														'idmission'=>$mission_exist->id
													])
													->orderBy(['u.nom' => SORT_ASC,'u.prenoms' => SORT_ASC])
													->all()
												;

												return [
													'status' => '000',
													'message' => 'succes_get_mission',
													'data' => $data,
												];
											} else {
												return [
													'status' => '001',
													'message' => 'mission_not_found',
												];
											}
										} else {
											return [
												'status' => '001',
												'message' => 'mission_key_empty',
											];
										}
									} else {
										return [
											'status' => '002',
											'message' => 'Accès non autorisé.',
										];
									}
								} else {
									return [
										'status' => '002',
										'message' => 'Cet utilisateur n\'a pas accès à ce centre.',
									];
								}
							} else {
								return [
									'status' => '002',
									'message' => 'id_center est incorrect.',
								];
							}
						} else {
							return [
								'status' => '002',
								'message' => 'id_center ne doit pas être vide.',
							];
						}
					} else {
						return [
							'status' => '002',
							'message' => 'Utilisateur non trouvé.',
						];
					}
				} else {
					return [
						'status' => '002',
						'message' => 'user_auth_key ne doit pas être vide.',
					];
				}
			} else {
				return [
					'status' => '002',
					'message' => 'Token incorrect.',
				];
			}
		} else {
			return [
				'status' => '002',
				'message' => 'Aucune donnée reçue en POST.',
			];
		}
		exit;
	}

	//Cette méthode permet d'ajouter un détail à une mission
	public function actionAdddetailmission()
	{
		$token = Utils::getApiKey();
		if (Yii::$app->request->post()) {
			if (isset($_POST['token']) && $_POST['token'] == $token) {
				if (isset($_POST['user_auth_key']) && $_POST['user_auth_key'] != '') {
					$user_auth_key = $_POST['user_auth_key'];
					$user_exist = User::find()->where(['auth_key' => $user_auth_key, 'status' => 10])->one();
					if ($user_exist != '') {

						if (isset($_POST['id_center']) && $_POST['id_center'] != '') {
							$id_center = $_POST['id_center'];

							$center = SchoolCenter::find()->where(['id_center' => $id_center, 'etat' => [1, 2, 4]])->one();
							if ($center != '') {
								$cont = false;
								$user_id_center = str_replace('-', '', $user_exist->id_center);
								$test_center = explode(",", $user_id_center);

								if ($user_exist->id_center == "1") {
									$cont = true;
								} else if (in_array($id_center, $test_center)) {
									$cont = true;
								}

								if ($cont == true) {

									$manage_mission = Utils::have_accessapi("manage_mission", $user_exist->idP, $user_exist->id, $id_center);
									$test = explode('_', $manage_mission);
									if ($test[0] == '1') { // INDICE : 0 pour CREATE; 1 pour READ; 2 pour UPDATE; 3 pour DELETE;
										if (isset($_POST['mission_key']) && $_POST['mission_key'] != '') {
											$mission_key = $_POST['mission_key'];
											$mission_exist = Mission::find()->where(['keymission' => $mission_key])->one();
											if ($mission_exist !== null) {
												$data['mission'] = $mission_exist;

												if (isset($_POST['iduser']) && $_POST['iduser'] != '') {
													$iduser = $_POST['iduser'];

													$find_personnel = Personnel::find()->where(['iduser' => $iduser])->one();
													if ($find_personnel !== null) {
														if (isset($_POST['frais']) && $_POST['frais'] != '') {
															$frais = $_POST['frais'];

															if (isset($_POST['contact']) && $_POST['contact'] != '') {
																$contact = $_POST['contact'];

																if (isset($_POST['operation']) && $_POST['operation'] != '') {
																	$operation = $_POST['operation'];

																	if ($operation == 1) { // AJOUT

																		$new_detail_mission = new Detailmission();
																		$new_detail_mission->keydetailmission = Yii::$app->security->generateRandomString(32);
																		$new_detail_mission->iduser = $find_personnel->iduser;

																		$new_detail_mission->idmission = $mission_exist->id;
																		$new_detail_mission->contactabsence = $contact;
																		$new_detail_mission->frais = $frais;
																		$new_detail_mission->status = 1;
																		$new_detail_mission->create_by = $user_exist->id;
																		$new_detail_mission->created_at = time();
																		if ($new_detail_mission->save()) {
																			$data['mission'] = $mission_exist;
																			//$data['detailmissions'] = $mission_exist->detailmissions;
																			$query = new Query();
																			$data['detailmissions'] = $query
																				->select(['d.*',
																					'u.nom','u.prenoms',
																				])
																				->from('detailmission d')
																				//->join('INNER JOIN','mission m','m.id = d.idmission')
																				->join('INNER JOIN','user u','u.id = d.iduser')
																				->where(['d.status'=> 1,
																					//'idcentre'=>$center->id_center,
																					'idmission'=>$mission_exist->id
																				])
																				->orderBy(['u.nom' => SORT_ASC,'u.prenoms' => SORT_ASC])
																				->all()
																			;
																			return [
																				'status' => '000',
																				'message' => 'succes_ajout_detail_mission',
																				'data' => $data,
																			];
																		} else {
																			return [
																				'status' => '002',
																				'message' => 'update_error',
																			];
																		}
																	} else if ($operation == 2) { // MODIFICATION
																		if (isset($_POST['keydetail']) && $_POST['keydetail'] != '') {
																			$keydetail = $_POST['keydetail'];

																			$old_record = Detailmission::find()->where(['keydetailmission' => $keydetail, 'idmission' => $mission_exist->id])->one();
																			if ($old_record !== null) {
																				$donne_ok = 1;
																				if (isset($_POST['dateretour']) && $_POST['dateretour'] != '') {
																					$donne_ok = 0;
																					$dateretoureff = $_POST['dateretour'];
																					if (strtotime($dateretoureff) >= strtotime($mission_exist->datedepart)) {
																						$donne_ok = 1;
																						$old_record->dateretoureffmission = $dateretoureff;
																					}
																				}
																				if ($donne_ok == 1) {
																					//Modification
																					$old_record->iduser = $find_personnel->iduser;
																					$old_record->frais = $frais;
																					$old_record->contactabsence = $contact;
																					$old_record->updated_at = time();
																					$old_record->updated_by = $user_exist->id;
																					if ($old_record->save()) {
																						$data['mission'] = $mission_exist;
																						//$data['detailmissions'] = $mission_exist->detailmissions;
																						$query = new Query();
																						$data['detailmissions'] = $query
																							->select(['d.*',
																								'u.nom','u.prenoms',
																							])
																							->from('detailmission d')
																							//->join('INNER JOIN','mission m','m.id = d.idmission')
																							->join('INNER JOIN','user u','u.id = d.iduser')
																							->where(['d.status'=> 1,
																								//'idcentre'=>$center->id_center,
																								'idmission'=>$mission_exist->id
																							])
																							->orderBy(['u.nom' => SORT_ASC,'u.prenoms' => SORT_ASC])
																							->all()
																						;
																						return [
																							'status' => '000',
																							'message' => 'succes_update_detail_mission',
																							'data' => $data,
																						];
																					} else {
																						return [
																							'status' => '002',
																							'message' => 'update_error',
																						];
																					}
																				} else {
																					return [
																						'status' => '001',
																						'message' => 'detailmission_date_retour_error',
																					];
																				}
																			} else {
																				return [
																					'status' => '001',
																					'message' => 'detailmission_not_found',
																				];
																			}
																		} else {
																			return [
																				'status' => '001',
																				'message' => 'detailmission_key_empty',
																			];
																		}
																	} else {
																		return [
																			'status' => '002',
																			'message' => 'Type d\'opération non reconnu.',
																		];
																	}
																} else {
																	return [
																		'status' => '002',
																		'message' => 'Le type d\'opération ne doit pas être vide.',
																	];
																}
															} else {
																return [
																	'status' => '001',
																	'message' => 'detailmission_error',
																];
															}
														} else {
															return [
																'status' => '001',
																'message' => 'detailmission_error',
															];
														}
													} else {
														return [
															'status' => '001',
															'message' => 'detailmission_personnel_not_found',
														];
													}
												} else {
													return [
														'status' => '001',
														'message' => 'detailmission_personnel_empty',
													];
												}
											} else {
												return [
													'status' => '001',
													'message' => 'mission_not_found',
												];
											}
										} else {
											return [
												'status' => '001',
												'message' => 'mission_key_empty',
											];
										}
									} else {
										return [
											'status' => '002',
											'message' => 'Accès non autorisé.',
										];
									}
								} else {
									return [
										'status' => '002',
										'message' => 'Cet utilisateur n\'a pas accès à ce centre.',
									];
								}
							} else {
								return [
									'status' => '002',
									'message' => 'id_center est incorrect.',
								];
							}
						} else {
							return [
								'status' => '002',
								'message' => 'id_center ne doit pas être vide.',
							];
						}
					} else {
						return [
							'status' => '002',
							'message' => 'Utilisateur non trouvé.',
						];
					}
				} else {
					return [
						'status' => '002',
						'message' => 'user_auth_key ne doit pas être vide.',
					];
				}
			} else {
				return [
					'status' => '002',
					'message' => 'Token incorrect.',
				];
			}
		} else {
			return [
				'status' => '002',
				'message' => 'Aucune donnée reçue en POST.',
			];
		}
		exit;
	}

	//Cette méthode permet de récuperer un détail d'une mission spécifique
	public function actionDetailmission()
	{
		$token = Utils::getApiKey();
		if (Yii::$app->request->post()) {
			if (isset($_POST['token']) && $_POST['token'] == $token) {
				if (isset($_POST['user_auth_key']) && $_POST['user_auth_key'] != '') {
					$user_auth_key = $_POST['user_auth_key'];
					$user_exist = User::find()->where(['auth_key' => $user_auth_key, 'status' => 10])->one();
					if ($user_exist != '') {

						if (isset($_POST['id_center']) && $_POST['id_center'] != '') {
							$id_center = $_POST['id_center'];

							$center = SchoolCenter::find()->where(['id_center' => $id_center, 'etat' => [1, 2, 4]])->one();
							if ($center != '') {
								$cont = false;
								$user_id_center = str_replace('-', '', $user_exist->id_center);
								$test_center = explode(",", $user_id_center);

								if ($user_exist->id_center == "1") {
									$cont = true;
								} else if (in_array($id_center, $test_center)) {
									$cont = true;
								}

								if ($cont == true) {

									$manage_mission = Utils::have_accessapi("manage_mission", $user_exist->idP, $user_exist->id, $id_center);
									$test = explode('_', $manage_mission);
									if ($test[2] == '1') { // INDICE : 0 pour CREATE; 1 pour READ; 2 pour UPDATE; 3 pour DELETE;
										if (isset($_POST['detail_key']) && $_POST['detail_key'] != '') {
											$detail_key = $_POST['detail_key'];
											$detail_mission_exist = Detailmission::find()->where(['keydetailmission' => $detail_key])->one();
											if ($detail_mission_exist !== null) {
												return [
													'status' => '000',
													'message' => 'succes_get_detail_mission',
													'data' => $detail_mission_exist,
												];
											} else {
												return [
													'status' => '001',
													'message' => 'detailmission_not_found',
												];
											}
										} else {
											return [
												'status' => '001',
												'message' => 'detaildetail_key_empty',
											];
										}
									} else {
										return [
											'status' => '002',
											'message' => 'Accès non autorisé.',
										];
									}
								} else {
									return [
										'status' => '002',
										'message' => 'Cet utilisateur n\'a pas accès à ce centre.',
									];
								}
							} else {
								return [
									'status' => '002',
									'message' => 'id_center est incorrect.',
								];
							}
						} else {
							return [
								'status' => '002',
								'message' => 'id_center ne doit pas être vide.',
							];
						}
					} else {
						return [
							'status' => '002',
							'message' => 'Utilisateur non trouvé.',
						];
					}
				} else {
					return [
						'status' => '002',
						'message' => 'user_auth_key ne doit pas être vide.',
					];
				}
			} else {
				return [
					'status' => '002',
					'message' => 'Token incorrect.',
				];
			}
		} else {
			return [
				'status' => '002',
				'message' => 'Aucune donnée reçue en POST.',
			];
		}
		exit;
	}

	//Cette fonction permet de supprimer un détail de mission
	public function actionDeletedetailmission()
	{
		$token = Utils::getApiKey();
		if (Yii::$app->request->post()) {
			if (isset($_POST['token']) && $_POST['token'] == $token) {
				if (isset($_POST['user_auth_key']) && $_POST['user_auth_key'] != '') {
					$user_auth_key = $_POST['user_auth_key'];
					$user_exist = User::find()->where(['auth_key' => $user_auth_key, 'status' => 10])->one();
					if ($user_exist != '') {

						if (isset($_POST['id_center']) && $_POST['id_center'] != '') {
							$id_center = $_POST['id_center'];

							$center = SchoolCenter::find()->where(['id_center' => $id_center, 'etat' => [1, 2, 4]])->one();
							if ($center != '') {
								$cont = false;
								$user_id_center = str_replace('-', '', $user_exist->id_center);
								$test_center = explode(",", $user_id_center);

								if ($user_exist->id_center == "1") {
									$cont = true;
								} else if (in_array($id_center, $test_center)) {
									$cont = true;
								}

								if ($cont == true) {

									$manage_mission = Utils::have_accessapi("manage_mission", $user_exist->idP, $user_exist->id, $id_center);
									$test = explode('_', $manage_mission);
									if ($test[3] == '1') { // INDICE : 0 pour CREATE; 1 pour READ; 2 pour UPDATE; 3 pour DELETE;
										if (isset($_POST['operation']) && $_POST['operation'] != '') {
											$operation = $_POST['operation'];
											if (isset($_POST['detail_key']) && $_POST['detail_key'] != '') {
												$detail_key = $_POST['detail_key'];
												$old_record = Detailmission::find()->where(['keydetailmission' => $detail_key])->one();
												if ($old_record != '') {
													if ($operation == 3) { // SUPPRESSION
														$old_record->status = 3;
														$msg = "delete_detail_mission_success";
														/*} else if ($operation == 2) { // DESACTIVATION
                                                            $old_record->status = 2;
                                                            $msg = "desactive_mission_success";
                                                        } else if ($operation == 1) { // ACTIVATION
                                                            $old_record->status = 1;
                                                            $msg = "active_mission_success";*/
													} else {
														return [
															'status' => '002',
															'message' => 'Type d\'opération non reconnu',
														];
													}
													$old_record->updated_at = time();
													$old_record->updated_by = $user_exist->id;
													if ($old_record->save()) {
														return [
															'status' => '000',
															'message' => $msg,
														];
													} else {
														return [
															'status' => '002',
															'message' => 'update_error',
														];
													}
												} else {
													return [
														'status' => '001',
														'message' => 'detailmission_not_found',
													];
												}
											} else {
												return [
													'status' => '001',
													'message' => 'detailmission_key_empty',
												];
											}
										} else {
											return [
												'status' => '002',
												'message' => 'Le type d\'opération ne doit pas être vide.',
											];
										}
									} else {
										return [
											'status' => '002',
											'message' => 'Accès non autorisé.',
										];
									}
								} else {
									return [
										'status' => '002',
										'message' => 'Cet utilisateur n\'a pas accès à ce centre.',
									];
								}
							} else {
								return [
									'status' => '002',
									'message' => 'id_center est incorrect.',
								];
							}
						} else {
							return [
								'status' => '002',
								'message' => 'id_center ne doit pas être vide.',
							];
						}
					} else {
						return [
							'status' => '002',
							'message' => 'Utilisateur non trouvé.',
						];
					}
				} else {
					return [
						'status' => '002',
						'message' => 'user_auth_key ne doit pas être vide.',
					];
				}
			} else {
				return [
					'status' => '002',
					'message' => 'Token incorrect.',
				];
			}
		} else {
			return [
				'status' => '002',
				'message' => 'Aucune donnée reçue en POST.',
			];
		}
		exit;
	}
}
