<?php

namespace api\modules\geschool\v1\controllers;
use api\modules\geschool\v1\models\Etatusage;
use api\modules\geschool\v1\models\Ressource;
use Yii;
use yii\db\Query;
use yii\rest\ActiveController;
use api\modules\geschool\v1\models\User;
use api\modules\geschool\v1\models\SchoolCenter;

/**
 * Geschool Controller API
 *
 */

class RessourceController extends ActiveController
{
    public $modelClass = 'api\modules\geschool\v1\models\Ressource';

    #000 -> Tout est bien
    #001 -> Message client
    #002 -> Message dev


    //Cette méthode nous permet de récupérer les etats d'usage du système
    public function actionEtatusages()
    {
        $token = Utils::getApiKey();

        if (Yii::$app->request->post()) {
            if (isset($_POST['token']) && $_POST['token'] == $token) {
                if (isset($_POST['user_auth_key']) && $_POST['user_auth_key'] != '') {
                    $user_auth_key = $_POST['user_auth_key'];
                    $user_exist = User::find()->where(['auth_key' => $user_auth_key, 'status' => 10])->one();
                    if ($user_exist != '') {

                        if (isset($_POST['id_center']) && $_POST['id_center'] != '') {
                            $id_center = $_POST['id_center'];

                            $center = SchoolCenter::find()->where(['id_center' => $id_center, 'etat' => [1, 2, 4]])->one();
                            if ($center != '') {
                                $cont = false;
                                $user_id_center = str_replace('-', '', $user_exist->id_center);
                                $test_center = explode(",", $user_id_center);

                                if ($user_exist->id_center == "1") {
                                    $cont = true;
                                } else if (in_array($id_center, $test_center)) {
                                    $cont = true;
                                }

                                if ($cont == true) {

                                    $manage_resource = Utils::have_accessapi("manage_resource", $user_exist->idP, $user_exist->id, $id_center);
                                    $test = explode('_', $manage_resource);
                                    if ($test[1] == '1') { // INDICE : 0 pour CREATE; 1 pour READ; 2 pour UPDATE; 3 pour DELETE;

                                        $data = Etatusage::find()->where(['status' => 1,'idcentre'=>$id_center])->orderBy(['libelle' => SORT_ASC])->all();
                                        if (sizeof($data) > 0) {
                                            return [
                                                'status' => '000',
                                                'message' => 'succes_get_etat_usage',
                                                'data' => $data
                                            ];
                                        } else {
                                            return [
                                                'status' => '001',
                                                'message' => 'empty_liste_etat_usage',
                                            ];
                                        }
                                    } else {
                                        return [
                                            'status' => '002',
                                            'message' => 'Accès non autorisé.',
                                        ];
                                    }
                                } else {
                                    return [
                                        'status' => '002',
                                        'message' => 'Cet utilisateur n\'a pas accès à ce centre.',
                                    ];
                                }
                            } else {
                                return [
                                    'status' => '002',
                                    'message' => 'id_center est incorrect.',
                                ];
                            }
                        } else {
                            return [
                                'status' => '002',
                                'message' => 'id_center ne doit pas être vide.',
                            ];
                        }
                    } else {
                        return [
                            'status' => '002',
                            'message' => 'Utilisateur non trouvé.',
                        ];
                    }
                } else {
                    return [
                        'status' => '002',
                        'message' => 'user_auth_key ne doit pas être vide.',
                    ];
                }
            } else {
                return [
                    'status' => '002',
                    'message' => 'Token incorrect',
                ];
            }
        } else {
            return [
                'status' => '002',
                'message' => 'Aucune donnée reçue en POST',
            ];
        }

        exit;
    }

    //Cette méthode nous permet de récupérer les ressources  d'un centre
    public function actionRessources()
    {
        $token = Utils::getApiKey();
        if (Yii::$app->request->post()) {
            if (isset($_POST['token']) && $_POST['token'] == $token) {
                if (isset($_POST['user_auth_key']) && $_POST['user_auth_key'] != '') {
                    $user_auth_key = $_POST['user_auth_key'];
                    $user_exist = User::find()->where(['auth_key' => $user_auth_key, 'status' => 10])->one();
                    if ($user_exist != '') {

                        if (isset($_POST['id_center']) && $_POST['id_center'] != '') {
                            $id_center = $_POST['id_center'];

                            $center = SchoolCenter::find()->where(['id_center' => $id_center, 'etat' => [1, 2, 4]])->one();
                            if ($center != '') {
                                $cont = false;
                                $user_id_center = str_replace('-', '', $user_exist->id_center);
                                $test_center = explode(",", $user_id_center);

                                if ($user_exist->id_center == "1") {
                                    $cont = true;
                                } else if (in_array($id_center, $test_center)) {
                                    $cont = true;
                                }

                                if ($cont == true) {

                                    $manage_ressource = Utils::have_accessapi("manage_ressource", $user_exist->idP, $user_exist->id, $id_center);
                                    $test = explode('_', $manage_ressource);
                                    if ($test[1] == '1') { // INDICE : 0 pour CREATE; 1 pour READ; 2 pour UPDATE; 3 pour DELETE;


                                        //$data = Ressource::find()->where(['idcentre'=>$center->id_center,'status' => [1, 2]])->orderBy(['designation' => SORT_ASC])->all();
                                        $query = new Query();
                                        $data = $query
                                            ->select(['r.*',
                                                'e.keyetatusage',
                                                'e.libelle',
                                            ])
                                            ->from('ressource r')
                                            ->join('INNER JOIN','etatusage e','e.id = r.idetatusage')
                                            ->where(['r.status'=> [1,2],
                                                'r.idcentre'=>$center->id_center,
                                            ])
                                            ->orderBy(['designation' => SORT_ASC])
                                            ->all()
                                        ;
                                        if (sizeof($data) > 0) {
                                            return [
                                                'status' => '000',
                                                'message' => 'succes_get_ressource',
                                                'data' => $data
                                            ];
                                        } else {
                                            return [
                                                'status' => '001',
                                                'message' => 'empty_ressource',
                                            ];
                                        }

                                    } else {
                                        return [
                                            'status' => '002',
                                            'message' => 'Accès non autorisé.',
                                        ];
                                    }
                                } else {
                                    return [
                                        'status' => '002',
                                        'message' => 'Cet utilisateur n\'a pas accès à ce centre.',
                                    ];
                                }
                            } else {
                                return [
                                    'status' => '002',
                                    'message' => 'id_center est incorrect.',
                                ];
                            }
                        } else {
                            return [
                                'status' => '002',
                                'message' => 'id_center ne doit pas être vide.',
                            ];
                        }
                    } else {
                        return [
                            'status' => '002',
                            'message' => 'Utilisateur non trouvé.',
                        ];
                    }
                } else {
                    return [
                        'status' => '002',
                        'message' => 'user_auth_key ne doit pas être vide.',
                    ];
                }
            } else {
                return [
                    'status' => '002',
                    'message' => 'Token incorrect',
                ];
            }
        } else {
            return [
                'status' => '002',
                'message' => 'Aucune donnée reçue en POST',
            ];
        }
        exit;
    }

    //Cette méthode nous permet d'ajouter une ressource  à un centre
    public function actionAddressource()
    {
        $token = Utils::getApiKey();
        if (Yii::$app->request->post()) {

            if (isset($_POST['token']) && $_POST['token'] == $token) {
                if (isset($_POST['user_auth_key']) && $_POST['user_auth_key'] != '') {
                    $user_auth_key = $_POST['user_auth_key'];
                    $user_exist = User::find()->where(['auth_key' => $user_auth_key, 'status' => 10])->one();
                    if ($user_exist != '') {


                        if (isset($_POST['id_center']) && $_POST['id_center'] != '') {
                            $id_center = $_POST['id_center'];

                            $center = SchoolCenter::find()->where(['id_center' => $id_center, 'etat' => [1, 2, 4]])->one();
                            if ($center != '') {
                                $cont = false;
                                $user_id_center = str_replace('-', '', $user_exist->id_center);
                                $test_center = explode(",", $user_id_center);

                                if ($user_exist->id_center == "1") {
                                    $cont = true;
                                } else if (in_array($id_center, $test_center)) {
                                    $cont = true;
                                }

                                if ($cont == true) {

                                    $manage_ressource = Utils::have_accessapi("manage_ressource", $user_exist->idP, $user_exist->id, $id_center);
                                    $test = explode('_', $manage_ressource);

                                    if (isset($_POST['etat']) && $_POST['etat'] != '') {
                                        $key_etat = $_POST['etat'];
                                        $etatusage = Etatusage::find()->where(['idcentre' => $center->id_center, 'keyetatusage' => $key_etat])->one();
                                        if ($etatusage !== null) {

                                            if (isset($_POST['designation']) && $_POST['designation'] != '') {
                                                $designation = $_POST['designation'];
                                                if (isset($_POST['code']) && $_POST['code'] != '') {
                                                    $coderessource = $_POST['code'];
                                                    if (strlen($coderessource)<= 10){
                                                        if (isset($_POST['operation']) && $_POST['operation'] != '') {
                                                            $operation = $_POST['operation'];
                                                            if ($operation == 1) { // AJOUT
                                                                if ($test[0] == '1') { // INDICE : 0 pour CREATE; 1 pour READ; 2 pour UPDATE; 3 pour DELETE;

                                                                    $exist = Ressource::find()->where(['idcentre'=>$center->id_center,'designation' => $designation, 'status' => [1, 2]])->one();
                                                                    if ($exist == '' or $exist == null) {
                                                                        $new_record = new Ressource();
                                                                        $new_record->designation = $designation;
                                                                        $new_record->idetatusage = $etatusage->id;
                                                                        $new_record->coderessource = $coderessource;
                                                                        $new_record->keyressource = Yii::$app->security->generateRandomString(32);
                                                                        $new_record->idcentre = $center->id_center;
                                                                        $new_record->status = 1;
                                                                        $new_record->created_at = time();
                                                                        $new_record->updated_at = '';
                                                                        $new_record->create_by = $user_exist->id;
                                                                        $new_record->updated_by = '';
                                                                        if ($new_record->save()) {
                                                                            //$data = Ressource::find()->where(['idcentre'=>$center->id_center,'status' => [1, 2]])->orderBy(['designation' => SORT_ASC])->all();
                                                                            $query = new Query();
                                                                            $data = $query
                                                                                ->select(['r.*',
                                                                                    'e.keyetatusage',
                                                                                    'e.libelle',
                                                                                ])
                                                                                ->from('ressource r')
                                                                                ->join('INNER JOIN','etatusage e','e.id = r.idetatusage')
                                                                                ->where(['r.status'=> [1,2],
                                                                                    'r.idcentre'=>$center->id_center,
                                                                                ])
                                                                                ->orderBy(['designation' => SORT_ASC])
                                                                                ->all()
                                                                            ;
                                                                            return [
                                                                                'status' => '000',
                                                                                'message' => 'succes_creation_ressource',
                                                                                'data' => $data,
                                                                            ];
                                                                        } else {
                                                                            return [
                                                                                'status' => '002',
                                                                                'message' => 'update_error',
                                                                            ];
                                                                        }
                                                                    } else {
                                                                        return [
                                                                            'status' => '003',
                                                                            'message' => 'ressource_used',
                                                                        ];
                                                                    }
                                                                } else {
                                                                    return [
                                                                        'status' => '002',
                                                                        'message' => 'Accès non autorisé.',
                                                                    ];
                                                                }
                                                            } else if ($operation == 2) { // MISE A JOUR
                                                                if ($test[2] == '1') { // INDICE : 0 pour CREATE; 1 pour READ; 2 pour UPDATE; 3 pour DELETE;

                                                                    $continue = false;
                                                                    if (isset($_POST['ressource_key']) && $_POST['ressource_key'] != '') {
                                                                        $ressource_key = $_POST['ressource_key'];
                                                                        $old_record = Ressource::find()->where(['idcentre' => $center->id_center,'keyressource' => $ressource_key, 'status' => [1, 2]])->one();
                                                                        if ($old_record != '') {

                                                                            $exist = Ressource::find()->where(['idcentre'=>$center->id_center,'designation' => $designation, 'status' => [1, 2]])->one();
                                                                            if ($exist == '' or $exist == null) {
                                                                                $continue = true;
                                                                            } else {
                                                                                ($exist->keyressource != $ressource_key) ? $continue = false : $continue = true;
                                                                            }

                                                                            if ($continue == true) {
                                                                                $old_record->designation = $designation;
                                                                                $old_record->idetatusage = $etatusage->id;
                                                                                $old_record->coderessource = $coderessource;
                                                                                $old_record->updated_at = time();
                                                                                $old_record->updated_by = $user_exist->id;
                                                                                if ($old_record->save()) {
                                                                                    //$data = Ressource::find()->where(['idcentre'=>$center->id_center,'status' => [1, 2]])->orderBy(['designation' => SORT_ASC])->all();
                                                                                    $query = new Query();
                                                                                    $data = $query
                                                                                        ->select(['r.*',
                                                                                            'e.keyetatusage',
                                                                                            'e.libelle',
                                                                                        ])
                                                                                        ->from('ressource r')
                                                                                        ->join('INNER JOIN','etatusage e','e.id = r.idetatusage')
                                                                                        ->where(['r.status'=> [1,2],
                                                                                            'r.idcentre'=>$center->id_center,
                                                                                        ])
                                                                                        ->orderBy(['designation' => SORT_ASC])
                                                                                        ->all()
                                                                                    ;
                                                                                    return [
                                                                                        'status' => '000',
                                                                                        'message' => 'succes_update_ressource',
                                                                                        'data' => $data,
                                                                                    ];
                                                                                } else {
                                                                                    return [
                                                                                        'status' => '002',
                                                                                        'message' => 'update_error',
                                                                                    ];
                                                                                }
                                                                            } else {
                                                                                return [
                                                                                    'status' => '003',
                                                                                    'message' => 'ressource_used',
                                                                                ];
                                                                            }
                                                                        } else {
                                                                            return [
                                                                                'status' => '001',
                                                                                'message' => 'ressource_not_found',
                                                                            ];
                                                                        }
                                                                    } else {
                                                                        return [
                                                                            'status' => '001',
                                                                            'message' => 'ressource_key_empty',
                                                                        ];
                                                                    }
                                                                } else {
                                                                    return [
                                                                        'status' => '002',
                                                                        'message' => 'Accès non autorisé.',
                                                                    ];
                                                                }
                                                            } else {
                                                                return [
                                                                    'status' => '002',
                                                                    'message' => 'Type d\'opération non reconnu.',
                                                                ];
                                                            }
                                                        } else {
                                                            return [
                                                                'status' => '002',
                                                                'message' => 'Le type d\'opération ne doit pas être vide.',
                                                            ];
                                                        }
                                                    }else{
                                                        return [
                                                            'status' => '001',
                                                            'message' => 'code_lenght_over',
                                                        ];
                                                    }



                                                } else {
                                                    return [
                                                        'status' => '001',
                                                        'message' => 'ressource_code_empty',
                                                    ];
                                                }

                                            } else {
                                                return [
                                                    'status' => '001',
                                                    'message' => 'ressource_designation_empty',
                                                ];
                                            }

                                        } else {
                                            return [
                                                'status' => '001',
                                                'message' => 'etat_usage_not_found',
                                            ];
                                        }
                                    } else {
                                        return [
                                            'status' => '001',
                                            'message' => 'etat_usage_key_empty',
                                        ];
                                    }
                                } else {
                                    return [
                                        'status' => '002',
                                        'message' => 'Cet utilisateur n\'a pas accès à ce centre.',
                                    ];
                                }
                            } else {
                                return [
                                    'status' => '002',
                                    'message' => 'id_center est incorrect.',
                                ];
                            }
                        } else {
                            return [
                                'status' => '002',
                                'message' => 'id_center ne doit pas être vide.',
                            ];
                        }
                    } else {
                        return [
                            'status' => '002',
                            'message' => 'Utilisateur non trouvé.',
                        ];
                    }
                } else {
                    return [
                        'status' => '002',
                        'message' => 'user_auth_key ne doit pas être vide.',
                    ];
                }
            } else {
                return [
                    'status' => '002',
                    'message' => 'Token incorrect.',
                ];
            }
        } else {
            return [
                'status' => '002',
                'message' => 'Aucune donnée reçue en POST.',
            ];
        }
        exit;
    }

    //Cette méthode nous permet de récuperer une ressource d'un centre
    public function actionRessource()
    {
        $token = Utils::getApiKey();
        if (Yii::$app->request->post()) {
            if (isset($_POST['token']) && $_POST['token'] == $token) {
                if (isset($_POST['user_auth_key']) && $_POST['user_auth_key'] != '') {
                    $user_auth_key = $_POST['user_auth_key'];
                    $user_exist = User::find()->where(['auth_key' => $user_auth_key, 'status' => 10])->one();
                    if ($user_exist != '') {

                        if (isset($_POST['id_center']) && $_POST['id_center'] != '') {
                            $id_center = $_POST['id_center'];

                            $center = SchoolCenter::find()->where(['id_center' => $id_center, 'etat' => [1, 2, 4]])->one();
                            if ($center != '') {
                                $cont = false;
                                $user_id_center = str_replace('-', '', $user_exist->id_center);
                                $test_center = explode(",", $user_id_center);

                                if ($user_exist->id_center == "1") {
                                    $cont = true;
                                } else if (in_array($id_center, $test_center)) {
                                    $cont = true;
                                }

                                if ($cont == true) {

                                    $manage_ressource = Utils::have_accessapi("manage_ressource", $user_exist->idP, $user_exist->id, $id_center);
                                    $test = explode('_', $manage_ressource);
                                    if ($test[2] == '1') { // INDICE : 0 pour CREATE; 1 pour READ; 2 pour UPDATE; 3 pour DELETE;

                                        if (isset($_POST['ressource_key']) && $_POST['ressource_key'] != '') {
                                            $ressource_key = $_POST['ressource_key'];
                                           // $ressource_exist = Ressource::find()->where(['keyressource'=>$ressource_key,'idcentre'=>$center->id_center,'status' => [1,2]])->one();
                                            $query = new Query();
                                            $ressource_exist= $query
                                                ->select(['r.*',
                                                    'e.keyetatusage',
                                                    'e.libelle',
                                                ])
                                                ->from('ressource r')
                                                ->join('INNER JOIN','etatusage e','e.id = r.idetatusage')
                                                ->where(['r.status'=> [1,2],
                                                    'r.idcentre'=>$center->id_center,
                                                    'keyressource'=>$ressource_key,
                                                ])
                                                ->orderBy(['designation' => SORT_ASC])
                                                ->one()
                                            ;
                                            if ($ressource_exist != '') {
                                                return [
                                                    'status' => '000',
                                                    'message' => 'succes_get_ressource',
                                                    'data' => $ressource_exist,
                                                ];
                                            } else {
                                                return [
                                                    'status' => '001',
                                                    'message' => 'ressource_not_found',
                                                ];
                                            }
                                        } else {
                                            return [
                                                'status' => '001',
                                                'message' => 'ressource_key_empty',
                                            ];
                                        }
                                    } else {
                                        return [
                                            'status' => '002',
                                            'message' => 'Accès non autorisé.',
                                        ];
                                    }
                                } else {
                                    return [
                                        'status' => '002',
                                        'message' => 'Cet utilisateur n\'a pas accès à ce centre.',
                                    ];
                                }
                            } else {
                                return [
                                    'status' => '002',
                                    'message' => 'id_center est incorrect.',
                                ];
                            }
                        } else {
                            return [
                                'status' => '002',
                                'message' => 'id_center ne doit pas être vide.',
                            ];
                        }
                    } else {
                        return [
                            'status' => '002',
                            'message' => 'Utilisateur non trouvé.',
                        ];
                    }
                } else {
                    return [
                        'status' => '002',
                        'message' => 'user_auth_key ne doit pas être vide.',
                    ];
                }
            } else {
                return [
                    'status' => '002',
                    'message' => 'Token incorrect.',
                ];
            }
        } else {
            return [
                'status' => '002',
                'message' => 'Aucune donnée reçue en POST.',
            ];
        }
        exit;
    }

    //Cette méthode nous permet de supprimer / desactiver / activer une ressource de cours d'un centre
    public function actionDeleteressource()
    {
        $token = Utils::getApiKey();
        $msg = "";
        if (Yii::$app->request->post()) {
            if (isset($_POST['token']) && $_POST['token'] == $token) {
                if (isset($_POST['user_auth_key']) && $_POST['user_auth_key'] != '') {
                    $user_auth_key = $_POST['user_auth_key'];
                    $user_exist = User::find()->where(['auth_key' => $user_auth_key, 'status' => 10])->one();
                    if ($user_exist != '') {

                        if (isset($_POST['id_center']) && $_POST['id_center'] != '') {
                            $id_center = $_POST['id_center'];

                            $center = SchoolCenter::find()->where(['id_center' => $id_center, 'etat' => [1, 2, 4]])->one();
                            if ($center != '') {
                                $cont = false;
                                $user_id_center = str_replace('-', '', $user_exist->id_center);
                                $test_center = explode(",", $user_id_center);

                                if ($user_exist->id_center == "1") {
                                    $cont = true;
                                } else if (in_array($id_center, $test_center)) {
                                    $cont = true;
                                }

                                if ($cont == true) {

                                    $manage_ressource = Utils::have_accessapi("manage_ressource", $user_exist->idP, $user_exist->id, $id_center);
                                    $test = explode('_', $manage_ressource);

                                    if ($test[3] == '1') { // INDICE : 0 pour CREATE; 1 pour READ; 2 pour UPDATE; 3 pour DELETE;

                                        if (isset($_POST['operation']) && $_POST['operation'] != '') {
                                            $operation = $_POST['operation'];
                                            if (isset($_POST['ressource_key']) && $_POST['ressource_key'] != '') {
                                                $ressource_key = $_POST['ressource_key'];
                                                $old_record = Ressource::find()->where(['idcentre'=>$center->id_center,'keyressource' => $ressource_key])->one();
                                                if ($old_record != '') {

                                                    if ($operation == 3) { // SUPPRESSION
                                                        $old_record->status = 3;
                                                        $msg = "delete_ressource_success";
                                                    } else if ($operation == 2) { // DESACTIVATION
                                                        $old_record->status = 2;
                                                        $msg = "desactive_ressource_success";
                                                    } else if ($operation == 1) { // ACTIVATION
                                                        $old_record->status = 1;
                                                        $msg = "active_ressource_success";
                                                    } else {
                                                        return [
                                                            'status' => '002',
                                                            'message' => 'Type d\'opération non reconnu',
                                                        ];
                                                    }

                                                    $old_record->updated_at = time();
                                                    $old_record->updated_by = $user_exist->id;
                                                    if ($old_record->save()) {
                                                        //$data = Ressource::find()->where(['idcentre'=>$center->id_center,'status' => [1, 2]])->orderBy(['designation' => SORT_ASC])->all();
                                                        $query = new Query();
                                                        $data = $query
                                                            ->select(['r.*',
                                                                'e.keyetatusage',
                                                                'e.libelle',
                                                            ])
                                                            ->from('ressource r')
                                                            ->join('INNER JOIN','etatusage e','e.id = r.idetatusage')
                                                            ->where(['r.status'=> [1,2],
                                                                'r.idcentre'=>$center->id_center,
                                                            ])
                                                            ->orderBy(['designation' => SORT_ASC])
                                                            ->all()
                                                        ;
                                                        return [
                                                            'status' => '000',
                                                            'message' => $msg,
                                                            'data' => $data,
                                                        ];
                                                    } else {
                                                        return [
                                                            'status' => '002',
                                                            'message' => 'update_error',
                                                        ];
                                                    }
                                                } else {
                                                    return [
                                                        'status' => '001',
                                                        'message' => 'ressource_not_found',
                                                    ];
                                                }
                                            } else {
                                                return [
                                                    'status' => '001',
                                                    'message' => 'ressource_key_empty',
                                                ];
                                            }
                                        } else {
                                            return [
                                                'status' => '002',
                                                'message' => 'Le type d\'opération ne doit pas être vide.',
                                            ];
                                        }
                                    } else {
                                        return [
                                            'status' => '002',
                                            'message' => 'Accès non autorisé.',
                                        ];
                                    }
                                } else {
                                    return [
                                        'status' => '002',
                                        'message' => 'Cet utilisateur n\'a pas accès à ce centre.',
                                    ];
                                }
                            } else {
                                return [
                                    'status' => '002',
                                    'message' => 'id_center est incorrect.',
                                ];
                            }
                        } else {
                            return [
                                'status' => '002',
                                'message' => 'id_center ne doit pas être vide.',
                            ];
                        }
                    } else {
                        return [
                            'status' => '002',
                            'message' => 'Utilisateur non trouvé.',
                        ];
                    }
                } else {
                    return [
                        'status' => '002',
                        'message' => 'user_auth_key ne doit pas être vide.',
                    ];
                }
            } else {
                return [
                    'status' => '002',
                    'message' => 'Token incorrect.',
                ];
            }
        } else {
            return [
                'status' => '002',
                'message' => 'Aucune donnée reçue en POST.',
            ];
        }
        exit;
    }
}
