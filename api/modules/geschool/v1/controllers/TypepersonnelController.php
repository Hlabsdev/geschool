<?php

namespace api\modules\geschool\v1\controllers;

use api\modules\geschool\v1\models\SchoolCenter;
use api\modules\geschool\v1\models\Typepersonnel;
use api\modules\geschool\v1\models\User;
use Yii;
use yii\rest\ActiveController;

/**
 * Geschool Controller API
 *
 */
class TypepersonnelController extends ActiveController
{
	public $modelClass = 'api\modules\geschool\v1\models\Typepersonnel';

	#000 -> Tout est bien
	#001 -> Message client
	#002 -> Message dev


	public function actionTypepersonnels()
	{
		$token = Utils::getApiKey();
		// $manage_typepersonnel = Utils::have_access("manage_typepersonnel");
		// $manage_typepersonnel = Utils::have_accessapi("manage_typepersonnel", $userIdP, $userId, $id_center);
		// $test = explode('_', $manage_typepersonnel);
		// if ($test[1] == '1') {
		if (Yii::$app->request->post()) {
			if (isset($_POST['token']) && $_POST['token'] == $token) {
				if (isset($_POST['user_auth_key']) && $_POST['user_auth_key'] != '') {
					$user_auth_key = $_POST['user_auth_key'];
					$user_exist = User::find()->where(['auth_key' => $user_auth_key, 'status' => 10])->one();
					if ($user_exist != '') {



						if (isset($_POST['id_center']) && $_POST['id_center'] != '') {
							$id_center = $_POST['id_center'];

							$center = SchoolCenter::find()->where(['id_center' => $id_center, 'etat' => [1, 2, 4]])->one();
							if ($center != '') {
								$cont = false;
								$user_id_center = str_replace('-', '', $user_exist->id_center);
								$test_center = explode(",", $user_id_center);

								if ($user_exist->id_center == "1") {
									$cont = true;
								} else if (in_array($id_center, $test_center)) {
									$cont = true;
								}

								if ($cont == true) {

									$manage_typepersonnel = Utils::have_accessapi("manage_typepersonnel", $user_exist->idP, $user_exist->id, $id_center);
									$test = explode('_', $manage_typepersonnel);
									if ($test[1] == '1') { // INDICE : 0 pour CREATE; 1 pour READ; 2 pour UPDATE; 3 pour DELETE;

										$data = Typepersonnel::find()->where(['status' => [1, 2]])->orderBy(['libelle' => SORT_ASC])->all();
										if (sizeof($data) > 0) {
											return [
												'status' => '000',
												'message' => 'succes_get_type_personnel',
												'data' => $data
											];
										} else {
											return [
												'status' => '001',
												'message' => 'empty_liste_type_personnel',
											];
										}
									} else {
										return [
											'status' => '002',
											'message' => 'Accès non autorisé.',
										];
									}
								} else {
									return [
										'status' => '002',
										'message' => 'Cet utilisateur n\'a pas accès à ce centre.',
									];
								}
							} else {
								return [
									'status' => '002',
									'message' => 'id_center est incorrect.',
								];
							}
						} else {
							return [
								'status' => '002',
								'message' => 'id_center ne doit pas être vide.',
							];
						}
					} else {
						return [
							'status' => '002',
							'message' => 'Utilisateur non trouvé.',
						];
					}
				} else {
					return [
						'status' => '002',
						'message' => 'user_auth_key ne doit pas être vide.',
					];
				}
			} else {
				return [
					'status' => '002',
					'message' => 'Token incorrect',
				];
			}
		} else {
			return [
				'status' => '002',
				'message' => 'Aucune donnée reçue en POST',
			];
		}
		// } else {
		// 	return [
		// 		'status' => '001',
		// 		'message' => 'Accès non autorisé.',
		// 	];
		// }
		exit;
	}

	public function actionAddtypepersonnel()
	{
		$token = Utils::getApiKey();
		if (Yii::$app->request->post()) {

			if (isset($_POST['token']) && $_POST['token'] == $token) {
				if (isset($_POST['user_auth_key']) && $_POST['user_auth_key'] != '') {
					$user_auth_key = $_POST['user_auth_key'];
					$user_exist = User::find()->where(['auth_key' => $user_auth_key, 'status' => 10])->one();
					if ($user_exist != '') {

						if (isset($_POST['id_center']) && $_POST['id_center'] != '') {
							$id_center = $_POST['id_center'];

							$center = SchoolCenter::find()->where(['id_center' => $id_center, 'etat' => [1, 2, 4]])->one();
							if ($center != '') {
								$cont = false;
								$user_id_center = str_replace('-', '', $user_exist->id_center);
								$test_center = explode(",", $user_id_center);

								if ($user_exist->id_center == "1") {
									$cont = true;
								} else if (in_array($id_center, $test_center)) {
									$cont = true;
								}

								if ($cont == true) {

									$manage_typepersonnel = Utils::have_accessapi("manage_typepersonnel", $user_exist->idP, $user_exist->id, $id_center);
									$test = explode('_', $manage_typepersonnel);

									if (isset($_POST['libelle']) && $_POST['libelle'] != '') {
										$libelle = $_POST['libelle'];
										if (isset($_POST['operation']) && $_POST['operation'] != '') {
											$operation = $_POST['operation'];
											if ($operation == 1) { // AJOUT
												if ($test[0] == '1') { // INDICE : 0 pour CREATE; 1 pour READ; 2 pour UPDATE; 3 pour DELETE;
													$exist = Typepersonnel::find()->where(['libelle' => $libelle, 'status' => [1, 2]])->one();
													if ($exist == '' or $exist == null) {
														$new_record = new Typepersonnel();
														$new_record->libelle = $libelle;
														$new_record->keytypepersonnel = Yii::$app->security->generateRandomString(32);
														$new_record->status = 1;
														$new_record->created_at = time();
														$new_record->updated_at = '';
														$new_record->create_by = $user_exist->id;
														$new_record->updated_by = '';
														if ($new_record->save()) {
															$data = Typepersonnel::find()->where(['status' => [1, 2]])->orderBy(['libelle' => SORT_ASC])->all();
															return [
																'status' => '000',
																'message' => 'succes_creation_type_personnel',
																'data' => $data,
															];
														} else {
															return [
																'status' => '002',
																'message' => 'update_error',
															];
														}
													} else {
														return [
															'status' => '003',
															'message' => 'type_personnel_used',
														];
													}
												} else {
													return [
														'status' => '002',
														'message' => 'Accès non autorisé.',
													];
												}
											} else if ($operation == 2) { // MISE A JOUR
												if ($test[2] == '1') { // INDICE : 0 pour CREATE; 1 pour READ; 2 pour UPDATE; 3 pour DELETE;
													$continue = false;
													if (isset($_POST['type_personnel_key']) && $_POST['type_personnel_key'] != '') {
														$type_personnel_key = $_POST['type_personnel_key'];
														$old_record = Typepersonnel::find()->where(['keytypepersonnel' => $type_personnel_key, 'status' => [1, 2]])->one();
														if ($old_record != '') {
															$exist = Typepersonnel::find()->where(['libelle' => $libelle, 'status' => [1, 2]])->one();
															if ($exist == '' or $exist == null) {
																$continue = true;
															} else {
																($exist->keytypepersonnel != $type_personnel_key) ? $continue = false : $continue = true;
															}

															if ($continue == true) {
																$old_record->libelle = $libelle;
																$old_record->updated_at = time();
																$old_record->updated_by = $user_exist->id;
																if ($old_record->save()) {
																	$data = Typepersonnel::find()->where(['status' => [1, 2]])->orderBy(['libelle' => SORT_ASC])->all();
																	return [
																		'status' => '000',
																		'message' => 'succes_update_type_personnel',
																		'data' => $data,
																	];
																} else {
																	return [
																		'status' => '002',
																		'message' => 'update_error',
																	];
																}
															} else {
																return [
																	'status' => '003',
																	'message' => 'type_personnel_used',
																];
															}
														} else {
															return [
																'status' => '001',
																'message' => 'type_personnel_not_found',
															];
														}
													} else {
														return [
															'status' => '001',
															'message' => 'type_personnel_key_empty',
														];
													}
												} else {
													return [
														'status' => '002',
														'message' => 'Accès non autorisé.',
													];
												}
											} else {
												return [
													'status' => '002',
													'message' => 'Type d\'opération non reconnu.',
												];
											}
										} else {
											return [
												'status' => '002',
												'message' => 'Le type d\'opération ne doit pas être vide.',
											];
										}
									} else {
										return [
											'status' => '001',
											'message' => 'type_personnel_libelle_empty',
										];
									}
								} else {
									return [
										'status' => '002',
										'message' => 'Cet utilisateur n\'a pas accès à ce centre.',
									];
								}
							} else {
								return [
									'status' => '002',
									'message' => 'id_center est incorrect.',
								];
							}
						} else {
							return [
								'status' => '002',
								'message' => 'id_center ne doit pas être vide.',
							];
						}
					} else {
						return [
							'status' => '002',
							'message' => 'Utilisateur non trouvé',
						];
					}
				} else {
					return [
						'status' => '002',
						'message' => 'user_auth_key ne doit pas être vide',
					];
				}
			} else {
				return [
					'status' => '002',
					'message' => 'Token incorrect',
				];
			}
		} else {
			return [
				'status' => '002',
				'message' => 'Aucune donnée reçue en POST',
			];
		}
		exit;
	}

	public function actionTypepersonnel()
	{
		$token = Utils::getApiKey();
		if (Yii::$app->request->post()) {
			if (isset($_POST['token']) && $_POST['token'] == $token) {
				if (isset($_POST['user_auth_key']) && $_POST['user_auth_key'] != '') {
					$user_auth_key = $_POST['user_auth_key'];
					$user_exist = User::find()->where(['auth_key' => $user_auth_key, 'status' => 10])->one();
					if ($user_exist != '') {


						if (isset($_POST['id_center']) && $_POST['id_center'] != '') {
							$id_center = $_POST['id_center'];

							$center = SchoolCenter::find()->where(['id_center' => $id_center, 'etat' => [1, 2, 4]])->one();
							if ($center != '') {
								$cont = false;
								$user_id_center = str_replace('-', '', $user_exist->id_center);
								$test_center = explode(",", $user_id_center);

								if ($user_exist->id_center == "1") {
									$cont = true;
								} else if (in_array($id_center, $test_center)) {
									$cont = true;
								}

								if ($cont == true) {

									$manage_typepersonnel = Utils::have_accessapi("manage_typepersonnel", $user_exist->idP, $user_exist->id, $id_center);
									$test = explode('_', $manage_typepersonnel);
									if ($test[2] == '1') { // INDICE : 0 pour CREATE; 1 pour READ; 2 pour UPDATE; 3 pour DELETE;


										if (isset($_POST['type_personnel_key']) && $_POST['type_personnel_key'] != '') {
											$type_personnel_key = $_POST['type_personnel_key'];
											$type_abscence_exist = Typepersonnel::find()->where(['keytypepersonnel' => $type_personnel_key])->one();
											if ($type_abscence_exist != '') {
												return [
													'status' => '000',
													'message' => 'succes_get_type_personnel',
													'data' => $type_abscence_exist,
												];
											} else {
												return [
													'status' => '001',
													'message' => 'type_personnel_not_found',
												];
											}
										} else {
											return [
												'status' => '001',
												'message' => 'type_personnel_key_empty',
											];
										}
									} else {
										return [
											'status' => '002',
											'message' => 'Accès non autorisé.',
										];
									}
								} else {
									return [
										'status' => '002',
										'message' => 'Cet utilisateur n\'a pas accès à ce centre.',
									];
								}
							} else {
								return [
									'status' => '002',
									'message' => 'id_center est incorrect.',
								];
							}
						} else {
							return [
								'status' => '002',
								'message' => 'id_center ne doit pas être vide.',
							];
						}
					} else {
						return [
							'status' => '002',
							'message' => 'Utilisateur non trouvé.',
						];
					}
				} else {
					return [
						'status' => '002',
						'message' => 'user_auth_key ne doit pas être vide.',
					];
				}
			} else {
				return [
					'status' => '002',
					'message' => 'Token incorrect.',
				];
			}
		} else {
			return [
				'status' => '002',
				'message' => 'Aucune donnée reçue en POST.',
			];
		}
		exit;
	}

	public function actionDeletetypepersonnel()
	{
		$token = Utils::getApiKey();
		$msg = "";
		if (Yii::$app->request->post()) {
			if (isset($_POST['token']) && $_POST['token'] == $token) {
				if (isset($_POST['user_auth_key']) && $_POST['user_auth_key'] != '') {
					$user_auth_key = $_POST['user_auth_key'];
					$user_exist = User::find()->where(['auth_key' => $user_auth_key, 'status' => 10])->one();
					if ($user_exist != '') {

						if (isset($_POST['id_center']) && $_POST['id_center'] != '') {
							$id_center = $_POST['id_center'];

							$center = SchoolCenter::find()->where(['id_center' => $id_center, 'etat' => [1, 2, 4]])->one();
							if ($center != '') {
								$cont = false;
								$user_id_center = str_replace('-', '', $user_exist->id_center);
								$test_center = explode(",", $user_id_center);

								if ($user_exist->id_center == "1") {
									$cont = true;
								} else if (in_array($id_center, $test_center)) {
									$cont = true;
								}

								if ($cont == true) {

									$manage_typepersonnel = Utils::have_accessapi("manage_typepersonnel", $user_exist->idP, $user_exist->id, $id_center);
									$test = explode('_', $manage_typepersonnel);
									if ($test[3] == '1') { // INDICE : 0 pour CREATE; 1 pour READ; 2 pour UPDATE; 3 pour DELETE;

										if (isset($_POST['operation']) && $_POST['operation'] != '') {
											$operation = $_POST['operation'];
											if (isset($_POST['type_personnel_key']) && $_POST['type_personnel_key'] != '') {
												$type_personnel_key = $_POST['type_personnel_key'];
												$old_record = Typepersonnel::find()->where(['keytypepersonnel' => $type_personnel_key])->one();
												if ($old_record != '') {

													if ($operation == 3) { // SUPPRESSION
														$old_record->status = 3;
														$msg = "delete_type_personnel_success";
													} else if ($operation == 2) { // DESACTIVATION
														$old_record->status = 2;
														$msg = "desactive_type_personnel_success";
													} else if ($operation == 1) { // ACTIVATION
														$old_record->status = 1;
														$msg = "active_type_personnel_success";
													} else {
														return [
															'status' => '002',
															'message' => 'Type d\'opération non reconnu',
														];
													}

													$old_record->updated_at = time();
													$old_record->updated_by = $user_exist->id;
													if ($old_record->save()) {
														$data = Typepersonnel::find()->where(['status' => [1,2]])->orderBy(['libelle' => SORT_ASC])->all();
														return [
															'status' => '000',
															'message' => $msg,
															'data' => $data,
														];
													} else {
														return [
															'status' => '002',
															'message' => 'update_error',
														];
													}
												} else {
													return [
														'status' => '001',
														'message' => 'type_personnel_not_found',
													];
												}
											} else {
												return [
													'status' => '001',
													'message' => 'type_personnel_key_empty',
												];
											}
										} else {
											return [
												'status' => '002',
												'message' => 'Le type d\'opération ne doit pas être vide.',
											];
										}
									} else {
										return [
											'status' => '002',
											'message' => 'Accès non autorisé.',
										];
									}
								} else {
									return [
										'status' => '002',
										'message' => 'Cet utilisateur n\'a pas accès à ce centre.',
									];
								}
							} else {
								return [
									'status' => '002',
									'message' => 'id_center est incorrect.',
								];
							}
						} else {
							return [
								'status' => '002',
								'message' => 'id_center ne doit pas être vide.',
							];
						}
					} else {
						return [
							'status' => '002',
							'message' => 'Utilisateur non trouvé.',
						];
					}
				} else {
					return [
						'status' => '002',
						'message' => 'user_auth_key ne doit pas être vide.',
					];
				}
			} else {
				return [
					'status' => '002',
					'message' => 'Token incorrect.',
				];
			}
		} else {
			return [
				'status' => '002',
				'message' => 'Aucune donnée reçue en POST.',
			];
		}
		exit;
	}
}
