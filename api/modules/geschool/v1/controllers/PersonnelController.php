<?php

namespace api\modules\geschool\v1\controllers;

use api\modules\geschool\v1\models\Personnel;
use api\modules\geschool\v1\models\SchoolCenter;
use api\modules\geschool\v1\models\Typepersonnel;
use api\modules\geschool\v1\models\User;
use api\modules\geschool\v1\models\Usermodelapiresponse;
use Yii;
use yii\db\Query;
use yii\rest\ActiveController;

/**
 * Geschool Controller API
 *
 */
class PersonnelController extends ActiveController
{
	public $modelClass = 'api\modules\geschool\v1\models\Personnel';

	#000 -> Tout est bien
	#001 -> Message client
	#002 -> Message dev

	public function actionAddpersonnel()
	{
		$token = Utils::getApiKey();
		$situation_matrimoniale = "";
		$section_concerne = "";
		$email = '';
		$date_naissance = '';
		$telephone_user = '';
		$adresse_user = '';
		$nom = '';
		$prenoms = '';
		$photo = '';
		$password = '';
		$phone = '';

		if (Yii::$app->request->post()) {
			if (isset($_POST['token']) && $_POST['token'] == $token) {
				if (isset($_POST['user_auth_key']) && $_POST['user_auth_key'] != '') {
					$user_auth_key = $_POST['user_auth_key'];
					$user_exist = User::find()->where(['auth_key' => $user_auth_key, 'status' => 10])->one();
					if ($user_exist != '') {
						if (isset($_POST['id_center']) && $_POST['id_center'] != '') {
							$id_center = $_POST['id_center'];

							$center = SchoolCenter::find()->where(['id_center' => $id_center, 'etat' => [1, 2, 4]])->one();
							if ($center != '') {
								$cont = false;
								$user_id_center = str_replace('-', '', $user_exist->id_center);
								$test_center = explode(",", $user_id_center);

								if ($user_exist->id_center == "1") {
									$cont = true;
								} else if (in_array($id_center, $test_center)) {
									$cont = true;
								}

								if ($cont == true) {
									if (isset($_POST['operation']) && $_POST['operation'] != '') {
										$operation = $_POST['operation'];
										if ($operation == 1) { // AJOUT

											if (isset($_POST['username']) && $_POST['username'] != '') {
												$username = $_POST['username'];
												$user = User::find()->where(['username' => $username, 'status' => 10])->one();

												if ($user == '' or $user == null) {
													$new_user = new User();
													if (isset($_POST['password']) && $_POST['password'] != '') {
														$password = $_POST['password'];
														if (isset($_POST['nom']) && $_POST['nom'] != '') $nom = $_POST['nom'];
														if (isset($_POST['prenoms']) && $_POST['prenoms'] != '') $prenoms = $_POST['prenoms'];
														if (isset($_POST['email']) && $_POST['email'] != '') $email = $_POST['email'];
														if (isset($_POST['date_naissance']) && $_POST['date_naissance'] != '') $date_naissance = $_POST['date_naissance'];
														if (isset($_POST['telephone_user']) && $_POST['telephone_user'] != '') $telephone_user = $_POST['telephone_user'];
														if (isset($_POST['adresse_user']) && $_POST['adresse_user'] != '') $adresse_user = $_POST['adresse_user'];
														if (isset($_POST['photo']) && $_POST['photo'] != '') $photo = $_POST['photo'];
														if ($photo != '') {
															$url = Yii::$app->basePath;
															$recup = explode(DIRECTORY_SEPARATOR . "api", $url);
															$target_path = $recup[0] . DIRECTORY_SEPARATOR . "backend" . DIRECTORY_SEPARATOR . "web" . DIRECTORY_SEPARATOR . "images" . DIRECTORY_SEPARATOR . "user" . DIRECTORY_SEPARATOR;
															$image_name = "INC_" . time() . date('s') . ".jpg";
															if (move_uploaded_file($photo, $target_path . $image_name)) {
																$new_user->photo = $photo;
															} else {
																return [
																	'status' => '001',
																	'message' => 'error_get_personnel_photo',
																];
															}
														}
														if ($telephone_user != '') {
															$tete_phone = substr($telephone_user, 0, 3);
															if ($tete_phone == '228') {
																$phone = $telephone_user;
															} else {
																$phone = '228' . $telephone_user;
															}
														}

														$recup_center = str_replace('-', '', $id_center);
														$test_center = explode(",", $recup_center);

														$new_user->idP = $user_exist->id;
														$new_user->id_center = $id_center;
														$new_user->default_center = $test_center[0];
														$new_user->nom = $nom;
														$new_user->prenoms = $prenoms;
														$new_user->username = $username;
														$new_user->auth_key = Yii::$app->security->generateRandomString(32);
														$new_user->password_hash = Yii::$app->security->generatePasswordHash($password);
														$new_user->password_reset_token = '';
														$new_user->email = $email;
														$new_user->datenaissance = $date_naissance;
														$new_user->telephoneuser = $phone;
														$new_user->adresseuser = $adresse_user;
														$new_user->role = 10;
														$new_user->status = 10;
														$new_user->created_at = time();
														$new_user->updated_at = '';
														$new_user->create_by = $user_exist->id;
														$new_user->update_by = '';

														if (isset($_POST['indice_grade']) && $_POST['indice_grade'] != '') {
															$indice_grade = $_POST['indice_grade'];
															if (isset($_POST['type_personnel']) && $_POST['type_personnel'] != '') {
																$type_personnel_id = $_POST['type_personnel'];
																$type_personnel = Typepersonnel::find()->where(['id' => $type_personnel_id])->one();
																if ($type_personnel != "") {
																	if (isset($_POST['date_embauche']) && $_POST['date_embauche'] != '') {
																		$date_embauche = $_POST['date_embauche'];
																		if (isset($_POST['situation_matrimoniale']) && $_POST['situation_matrimoniale'] != '') $situation_matrimoniale = $_POST['situation_matrimoniale'];
																		if (isset($_POST['section_concerne']) && $_POST['section_concerne'] != '') $section_concerne = $_POST['section_concerne'];
																		$matricule = rand(1000, 3000) . rand(4000, 7000) . rand(8000, 9999);
																		$matricule_exist = true;
																		while ($matricule_exist) {
																			$info_personnel = Personnel::findOne(['matricule' => $matricule]);
																			if ($info_personnel == null) {
																				$matricule_exist = false;
																			} else {
																				$matricule = rand(1000, 3000) . rand(4000, 7000) . rand(8000, 9999);
																			}
																		}

																		$new_record = new Personnel();
																		$new_record->keypersonnel = Yii::$app->security->generateRandomString(32);
																		$new_record->indicegrade = $indice_grade;
																		$new_record->situationmatrimoniale = $situation_matrimoniale;
																		$new_record->dateembauche = $date_embauche;
																		$new_record->matricule = (string)$matricule;
																		$new_record->sectionconcerne = $section_concerne;
																		$new_record->typepersonnel = $type_personnel->id;
																		$new_record->status = 1;
																		$new_record->created_at = time();
																		$new_record->updated_at = '';
																		$new_record->create_by = $user_exist->id;
																		$new_record->updated_by = '';

																		if ($new_user->save()) {
																			$new_record->iduser = $new_user->id;
																			if ($new_record->save()) {
																				$data = Personnel::find()->where(['status' => [1, 2]])->orderBy(['created_at' => SORT_ASC])->all();

																				$tab_all_result = array();
																				$tab_result = array();
																				foreach ($data as $line) {
																					$user = User::find()->where(['id' => $line->iduser])->one();
																					$typepersonnel = Typepersonnel::find()->where(['id' => $line->typepersonnel])->one();
																					$tab_result['typepersonnellibelle'] = $typepersonnel->libelle;
																					$tab_result['iduser'] = $line->iduser;
																					$tab_result['nom'] = $user->nom;
																					$tab_result['prenoms'] = $user->prenoms;
																					$tab_result['keypersonnel'] = $line->keypersonnel;
																					$tab_result['indicegrade'] = $line->indicegrade;
																					$tab_result['situationmatrimoniale'] = $line->situationmatrimoniale;
																					$tab_result['dateembauche'] = $line->dateembauche;
																					$tab_result['matricule'] = $line->matricule;
																					$tab_result['sectionconcerne'] = $line->sectionconcerne;
																					$tab_result['typepersonnel'] = $line->typepersonnel;
																					$tab_result['status'] = $line->status;
																					$tab_result['created_at'] = $line->created_at;
																					$tab_result['updated_at'] = $line->updated_at;
																					$tab_result['create_by'] = $line->create_by;
																					$tab_result['updated_by'] = $line->updated_by;
																					$tab_all_result[] = $tab_result;
																				}
																				return [
																					'status' => '000',
																					'message' => 'succes_creation_personnel',
																					'data' => $tab_all_result
																				];
																			} else {
																				return [
																					'status' => '002',
																					'message' => 'update_error',
																					'message' => $new_record->getErrors(),
																				];
																			}
																		} else {
																			return [
																				'status' => '002',
																				'message' => $new_user->getErrors(),
																			];
																		}
																	} else {
																		return [
																			'status' => '001',
																			'message' => 'personnel_date_embauche_empty',
																		];
																	}
																} else {
																	return [
																		'status' => '001',
																		'message' => 'type_personnel_not_found',
																	];
																}
															} else {
																return [
																	'status' => '001',
																	'message' => 'type_personnel_key_empty',
																];
															}
														} else {
															return [
																'status' => '001',
																'message' => 'personnel_indice_grade_empty',
															];
														}
													} else {
														return [
															'status' => '001',
															'message' => 'personnel_password_empty',
														];
													}
												} else {
													return [
														'status' => '001',
														'message' => 'personnel_username_used',
														'data' => $user,
													];
												}
											} else {
												return [
													'status' => '001',
													'message' => 'personnel_username_empty',
												];
											}
										} else if ($operation == 2) { // MODIFICATION
											if (isset($_POST['user']) && $_POST['user'] != '') {
												$user = $_POST['user'];
												$user_exisst = User::find()->where(['id' => $user, 'status' => 10])->one();
												if ($user_exisst != '') {
													if (isset($_POST['username']) && $_POST['username'] != '') $username = $_POST['username'];
													if (isset($_POST['password']) && $_POST['password'] != '') $password = $_POST['password'];
													if (isset($_POST['nom']) && $_POST['nom'] != '') $nom = $_POST['nom'];
													if (isset($_POST['prenoms']) && $_POST['prenoms'] != '') $prenoms = $_POST['prenoms'];
													if (isset($_POST['email']) && $_POST['email'] != '') $email =  $_POST['email'];
													if (isset($_POST['date_naissance']) && $_POST['date_naissance'] != '') $date_naissance = $_POST['date_naissance'];
													if (isset($_POST['telephone_user']) && $_POST['telephone_user'] != '') $telephone_user = $_POST['telephone_user'];
													if (isset($_POST['adresse_user']) && $_POST['adresse_user'] != '') $adresse_user = $_POST['adresse_user'];
													if (isset($_POST['photo']) && $_POST['photo'] != '') $photo = $_POST['photo'];
													if ($photo != '') {
														$url = Yii::$app->basePath;
														$recup = explode(DIRECTORY_SEPARATOR . "api", $url);
														$target_path = $recup[0] . DIRECTORY_SEPARATOR . "backend" . DIRECTORY_SEPARATOR . "web" . DIRECTORY_SEPARATOR . "images" . DIRECTORY_SEPARATOR . "user" . DIRECTORY_SEPARATOR;
														$image_name = "INC_" . time() . date('s') . ".jpg";
														if (move_uploaded_file($photo, $target_path . $image_name)) {
															$user_exisst->photo = $photo;
														} else {
															return [
																'status' => '001',
																'message' => 'error_get_personnel_photo',
															];
														}
													}
													if ($telephone_user != '') {
														$tete_phone = substr($telephone_user, 0, 3);
														if ($tete_phone == '228') {
															$phone = $telephone_user;
														} else {
															$phone = '228' . $telephone_user;
														}
													}

													$recup_center = str_replace('-', '', $id_center);
													$test_center = explode(",", $recup_center);

													$user_exisst->id_center = $id_center;
													$user_exisst->default_center = $test_center[0];
													$user_exisst->nom = $nom;
													$user_exisst->prenoms = $prenoms;
													$user_exisst->username = $username;
													if ($password != '') $user_exisst->password_hash = Yii::$app->security->generatePasswordHash($password);
													$user_exisst->email = $email;
													$user_exisst->datenaissance = $date_naissance;
													$user_exisst->telephoneuser = $phone;
													$user_exisst->adresseuser = $adresse_user;
													$user_exisst->updated_at = time();
													$user_exisst->update_by = $user_exist->id;

													if (isset($_POST['personnel_key']) && $_POST['personnel_key'] != '') {
														$personnel_key = $_POST['personnel_key'];
														$old_record = Personnel::find()->where(['keypersonnel' => $personnel_key, 'status' => 1])->one();
														if ($old_record != '') {
															if ($user_exisst->id == $old_record->iduser) {
																$user_id = $user_exisst->id;
																if (isset($_POST['indice_grade']) && $_POST['indice_grade'] != '') {
																	$indice_grade = $_POST['indice_grade'];
																	if (isset($_POST['type_personnel']) && $_POST['type_personnel'] != '') {
																		$type_personnel_id = $_POST['type_personnel'];
																		$type_personnel = Typepersonnel::find()->where(['id' => $type_personnel_id])->one();
																		if ($type_personnel != "") {
																			if (isset($_POST['date_embauche']) && $_POST['date_embauche'] != '') {
																				$date_embauche = $_POST['date_embauche'];
																				if (isset($_POST['situation_matrimoniale']) && $_POST['situation_matrimoniale'] != '') $situation_matrimoniale = $_POST['situation_matrimoniale'];
																				if (isset($_POST['section_concerne']) && $_POST['section_concerne'] != '') $section_concerne = $_POST['section_concerne'];

																				$old_record->iduser = $user_id;
																				$old_record->indicegrade = $indice_grade;
																				$old_record->situationmatrimoniale = $situation_matrimoniale;
																				$old_record->dateembauche = $date_embauche;
																				$old_record->sectionconcerne = $section_concerne;
																				$old_record->typepersonnel = $type_personnel->id;
																				$old_record->updated_at = time();
																				$old_record->updated_by = $user_exist->id;

																				if ($user_exisst->save()) {
																					if ($old_record->save()) {
																						$data = Personnel::find()->where(['status' => [1, 2]])->orderBy(['created_at' => SORT_ASC])->all();

																						$tab_all_result = array();
																						$tab_result = array();
																						foreach ($data as $line) {
																							$user = User::find()->where(['id' => $line->iduser])->one();
																							$typepersonnel = Typepersonnel::find()->where(['id' => $line->typepersonnel])->one();
																							$tab_result['typepersonnellibelle'] = $typepersonnel->libelle;
																							$tab_result['iduser'] = $line->iduser;
																							$tab_result['nom'] = $user->nom;
																							$tab_result['prenoms'] = $user->prenoms;
																							$tab_result['keypersonnel'] = $line->keypersonnel;
																							$tab_result['indicegrade'] = $line->indicegrade;
																							$tab_result['situationmatrimoniale'] = $line->situationmatrimoniale;
																							$tab_result['dateembauche'] = $line->dateembauche;
																							$tab_result['matricule'] = $line->matricule;
																							$tab_result['sectionconcerne'] = $line->sectionconcerne;
																							$tab_result['typepersonnel'] = $line->typepersonnel;
																							$tab_result['status'] = $line->status;
																							$tab_result['created_at'] = $line->created_at;
																							$tab_result['updated_at'] = $line->updated_at;
																							$tab_result['create_by'] = $line->create_by;
																							$tab_result['updated_by'] = $line->updated_by;
																							$tab_all_result[] = $tab_result;
																						}
																						return [
																							'status' => '000',
																							'message' => 'succes_update_personnel',
																							'data' => $tab_all_result
																						];
																					} else {
																						return [
																							'status' => '002',
																							'message' => 'update_error',
																						];
																					}
																				} else {
																					return [
																						'status' => '002',
																						'message' => $user_exisst->getErrors(),
																					];
																				}
																			} else {
																				return [
																					'status' => '001',
																					'message' => 'personnel_date_embauche_error',
																				];
																			}
																		} else {
																			return [
																				'status' => '001',
																				'message' => 'type_personnel_not_found',
																			];
																		}
																	} else {
																		return [
																			'status' => '001',
																			'message' => 'type_personnel_key_empty',
																		];
																	}
																} else {
																	return [
																		'status' => '001',
																		'message' => 'personnel_indice_grade_empty',
																	];
																}
															} else {
																return [
																	'status' => '001',
																	'message' => 'personnel_not_found',
																];
															}
														} else {
															return [
																'status' => '001',
																'message' => 'personnel_not_found',
															];
														}
													} else {
														return [
															'status' => '001',
															'message' => 'personnel_key_empty',
														];
													}
												} else {
													return [
														'status' => '001',
														'message' => 'personnel_not_found',
													];
												}
											} else {
												return [
													'status' => '001',
													'message' => 'personnel_key_empty',
												];
											}
										} else {
											return [
												'status' => '002',
												'message' => 'Type d\'opération non reconnu.',
											];
										}
									} else {
										return [
											'status' => '002',
											'message' => 'Le type d\'opération ne peut pas être vide.',
										];
									}
								} else {
									return [
										'status' => '002',
										'message' => 'Cet utilisateur n\'a pas accès à ce centre.',
									];
								}
							} else {
								return [
									'status' => '002',
									'message' => 'id_center est incorrect.',
								];
							}
						} else {
							return [
								'status' => '002',
								'message' => 'id_center ne doit pas être vide.',
							];
						}
					} else {
						return [
							'status' => '002',
							'message' => 'Utilisateur non trouvé.',
						];
					}
				} else {
					return [
						'status' => '002',
						'message' => 'user_auth_key ne doit pas être vide.',
					];
				}
			} else {
				return [
					'status' => '002',
					'message' => 'Token incorrect.',
				];
			}
		} else {
			return [
				'status' => '002',
				'message' => 'Aucune donnée reçue en POST.',
			];
		}
		exit;
	}

	public function actionDeletepersonnel()
	{
		$token = Utils::getApiKey();
		$msg = "";
		if (Yii::$app->request->post()) {
			if (isset($_POST['token']) && $_POST['token'] == $token) {
				if (isset($_POST['user_auth_key']) && $_POST['user_auth_key'] != '') {
					$user_auth_key = $_POST['user_auth_key'];
					$user_exist = User::find()->where(['auth_key' => $user_auth_key, 'status' => 10])->one();
					if ($user_exist != '') {

						if (isset($_POST['id_center']) && $_POST['id_center'] != '') {
							$id_center = $_POST['id_center'];

							$center = SchoolCenter::find()->where(['id_center' => $id_center, 'etat' => [1, 2, 4]])->one();
							if ($center != '') {
								$cont = false;
								$user_id_center = str_replace('-', '', $user_exist->id_center);
								$test_center = explode(",", $user_id_center);

								if ($user_exist->id_center == "1") {
									$cont = true;
								} else if (in_array($id_center, $test_center)) {
									$cont = true;
								}

								if ($cont == true) {

									if (isset($_POST['operation']) && $_POST['operation'] != '') {
										$operation = $_POST['operation'];
										if (isset($_POST['personnel_key']) && $_POST['personnel_key'] != '') {
											$personnel_key = $_POST['personnel_key'];
											$old_record = Personnel::find()->where(['keypersonnel' => $personnel_key])->one();
											if ($old_record != '') {
												if ($operation == 3) { // SUPPRESSION
													$old_record->status = 3;
													$msg = "delete_personnel_success";
												} else if ($operation == 2) { // DESACTIVATION
													$old_record->status = 2;
													$msg = "desactive_personnel_success";
												} else if ($operation == 1) { // ACTIVATION
													$old_record->status = 1;
													$msg = "active_personnel_success";
												} else {
													return [
														'status' => '002',
														'message' => 'Type d\'opération non reconnu.',
													];
												}
												$old_record->updated_at = time();
												$old_record->updated_by = $user_exist->id;
												if ($old_record->save()) {
													return [
														'status' => '000',
														'message' => $msg,
													];
												} else {
													return [
														'status' => '002',
														'message' => 'update_error',
													];
												}
											} else {
												return [
													'status' => '001',
													'message' => 'personnel_not_found',
												];
											}
										} else {
											return [
												'status' => '001',
												'message' => 'personnel_key_empty',
											];
										}
									} else {
										return [
											'status' => '002',
											'message' => 'Le type d\'opération ne doit pas être vide.',
										];
									}
								} else {
									return [
										'status' => '002',
										'message' => 'Cet utilisateur n\'a pas accès à ce centre.',
									];
								}
							} else {
								return [
									'status' => '002',
									'message' => 'id_center est incorrect.',
								];
							}
						} else {
							return [
								'status' => '002',
								'message' => 'id_center ne doit pas être vide.',
							];
						}
					} else {
						return [
							'status' => '002',
							'message' => 'Utilisateur non trouvé.',
						];
					}
				} else {
					return [
						'status' => '002',
						'message' => 'user_auth_key ne doit pas être vide.',
					];
				}
			} else {
				return [
					'status' => '002',
					'message' => 'Token incorrect.',
				];
			}
		} else {
			return [
				'status' => '002',
				'message' => 'Aucune donnée reçue en POST.',
			];
		}
		exit;
	}

	public function actionPersonnel()
	{
		$token = Utils::getApiKey();
		if (Yii::$app->request->post()) {
			if (isset($_POST['token']) && $_POST['token'] == $token) {
				if (isset($_POST['user_auth_key']) && $_POST['user_auth_key'] != '') {
					$user_auth_key = $_POST['user_auth_key'];
					$user_exist = User::find()->where(['auth_key' => $user_auth_key, 'status' => 10])->one();
					if ($user_exist != '') {

						if (isset($_POST['id_center']) && $_POST['id_center'] != '') {
							$id_center = $_POST['id_center'];

							$center = SchoolCenter::find()->where(['id_center' => $id_center, 'etat' => [1, 2, 4]])->one();
							if ($center != '') {
								$cont = false;
								$user_id_center = str_replace('-', '', $user_exist->id_center);
								$test_center = explode(",", $user_id_center);

								if ($user_exist->id_center == "1") {
									$cont = true;
								} else if (in_array($id_center, $test_center)) {
									$cont = true;
								}

								if ($cont == true) {

									if (isset($_POST['personnel_key']) && $_POST['personnel_key'] != '') {
										$personnel_key = $_POST['personnel_key'];
										$personnel_exist = Personnel::find()->where(['keypersonnel' => $personnel_key])->one();
										if ($personnel_exist != '') {
											$tab_result = array();
											$user = User::find()->where(['id' => $personnel_exist->iduser])->one();
											$typepersonnel = Typepersonnel::find()->where(['id' => $personnel_exist->typepersonnel])->one();
											$tab_result['typepersonnellibelle'] = $typepersonnel->libelle;
											$tab_result['iduser'] = $personnel_exist->iduser;
											$tab_result['nom'] = $user->nom;
											$tab_result['prenoms'] = $user->prenoms;
											$tab_result['keypersonnel'] = $personnel_exist->keypersonnel;
											$tab_result['indicegrade'] = $personnel_exist->indicegrade;
											$tab_result['situationmatrimoniale'] = $personnel_exist->situationmatrimoniale;
											$tab_result['dateembauche'] = $personnel_exist->dateembauche;
											$tab_result['matricule'] = $personnel_exist->matricule;
											$tab_result['sectionconcerne'] = $personnel_exist->sectionconcerne;
											$tab_result['typepersonnel'] = $personnel_exist->typepersonnel;
											$tab_result['status'] = $personnel_exist->status;
											$tab_result['created_at'] = $personnel_exist->created_at;
											$tab_result['updated_at'] = $personnel_exist->updated_at;
											$tab_result['create_by'] = $personnel_exist->create_by;
											$tab_result['updated_by'] = $personnel_exist->updated_by;
											return [
												'status' => '000',
												'message' => 'succes_get_personnel',
												'data' => $tab_result,
											];
										} else {
											return [
												'status' => '001',
												'message' => 'personnel_not_found',
											];
										}
									} else {
										return [
											'status' => '001',
											'message' => 'personnel_key_empty',
										];
									}
								} else {
									return [
										'status' => '002',
										'message' => 'Cet utilisateur n\'a pas accès à ce centre.',
									];
								}
							} else {
								return [
									'status' => '002',
									'message' => 'id_center est incorrect.',
								];
							}
						} else {
							return [
								'status' => '002',
								'message' => 'id_center ne doit pas être vide.',
							];
						}
					} else {
						return [
							'status' => '002',
							'message' => 'Utilisateur non trouvé.',
						];
					}
				} else {
					return [
						'status' => '002',
						'message' => 'user_auth_key ne doit pas être vide.',
					];
				}
			} else {
				return [
					'status' => '002',
					'message' => 'Token incorrect.',
				];
			}
		} else {
			return [
				'status' => '002',
				'message' => 'Aucune donnée reçue en POST.',
			];
		}
		exit;
	}

	public function actionPersonnels()
	{
		$token = Utils::getApiKey();
		if (Yii::$app->request->post()) {
			if (isset($_POST['token']) && $_POST['token'] == $token) {
				if (isset($_POST['user_auth_key']) && $_POST['user_auth_key'] != '') {
					$user_auth_key = $_POST['user_auth_key'];
					$user_exist = User::find()->where(['auth_key' => $user_auth_key, 'status' => 10])->one();
					if ($user_exist != '') {

						if (isset($_POST['id_center']) && $_POST['id_center'] != '') {
							$id_center = $_POST['id_center'];


							$center = SchoolCenter::find()->where(['id_center' => $id_center, 'etat' => [1, 2, 4]])->one();
							if ($center != '') {
								$cont = false;
								$user_id_center = str_replace('-', '', $user_exist->id_center);
								$test_center = explode(",", $user_id_center);

								if ($user_exist->id_center == "1") {
									$cont = true;
								} else if (in_array($id_center, $test_center)) {
									$cont = true;
								}

								if ($cont == true) {

									$idCentre = "-" . $id_center . "-";
									// $tab_all_result = User::find()->where(['status' => ['10', '20']])
									// 	->andWhere(['!=', 'idP', 0])
									// 	->andWhere(['like', 'id_center', $idCentre])
									// 	->all();

									$query = new Query();
									$data = $query
										->select([
											'u.*',
											'p.keypersonnel',
										])
										->from('user u')
										->join('INNER JOIN', 'personnel p', 'p.iduser = u.id')
										->where(['u.status' => ['10', '20'], 'p.status' => ['1', '2']])
										->andWhere(['!=', 'u.idP', 0])
										->andWhere(['like', 'u.id_center', $idCentre])
										->orderBy(['created_at' => SORT_ASC])
										->all();

									return [
										'status' => '000',
										'message' => 'succes_get_personnel',
										'data' => $data
									];
								} else {
									return [
										'status' => '002',
										'message' => 'Cet utilisateur n\'a pas accès à ce centre.',
									];
								}
							} else {
								return [
									'status' => '002',
									'message' => 'id_center est incorrect.',
								];
							}
						} else {
							return [
								'status' => '002',
								'message' => 'id_center ne doit pas être vide.',
							];
						}
					} else {
						return [
							'status' => '002',
							'message' => 'Utilisateur non trouvé.',
						];
					}
				} else {
					return [
						'status' => '002',
						'message' => 'user_auth_key ne doit pas être vide.',
					];
				}
			} else {
				return [
					'status' => '002',
					'message' => 'Token incorrect.',
				];
			}
		} else {
			return [
				'status' => '002',
				'message' => 'Aucune donnée reçue en POST.',
			];
		}
		exit;
	}
}
