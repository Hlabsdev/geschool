<?php
namespace api\modules\geschool\v1\controllers;

use Yii;
use yii\web\Controller;
use backend\models\HistoriqueWhatsapp;
use backend\modules\users\models\UserProfil;
use backend\modules\users\models\UserAccess;
use backend\models\MessageHistorique;

class Utils 
{	

	//const wassa_apiurl="http://www.wassasms.com/wassasms/api/web/v3/sends?access-token=0QA7tbbIfJDIFV7je4-RKITJYyYgg_kg&sender=";
	const wassa_apiurl="http://www.wassasms.com/wassasms/api/web/v3/sends?access-token=YWKI8ZzXJVckJTZ4X-OyXWvOOSIU-XAi&sender=";
	const apiToken="SzrQt6TazKv2ayDHqs754qvvAvD";

	public static function resetbtn(){
		return '<input type="reset" name="Reset" value="'.Yii::t('app', 'resetbutton').'" class="btn btn-sm btn-dark" style="margin-right:100px">';		
	}
	
	public static function required(){
		return '<span style="color:red">**</span>';		
	}
	
	public static function emptyContent(){
		
		
		return '<div class="alert alert-danger alert-dismissible show fade" style="margin-bottom: 30px">
				  <div class="alert-body">
					<button class="close" data-dismiss="alert">
					  <span>×</span>
					</button>
					'.Yii::t('app', 'liste_empty').'
				  </div>
				</div>';	
		
	}
	
	public static function show_nbre($recup){
		$nbre=trim($recup);
		if($nbre>0){
			$recup=explode(".",$nbre);
			if(sizeof($recup)>1 && (int)$recup[1]!=0){
				return number_format($nbre, 2, '.', ' ');
			}else{
				return number_format($recup[0], 0, '.', ' ');				
				//return $nbre."BF";				
			}
		}else if($nbre<0){
			$convert=(-1)*$nbre;
			$recup=explode(".",$convert);
			if(sizeof($recup)>1 && (int)$recup[1]!=0){
				return "-".number_format($convert, 2, '.', ' ');
			}else{
				return "-".number_format($convert, 0, '.', ' ');				
			}
		}else return 0 ;
	}
	
	public static function Rule ($lib_right,$type_operation){
		   
		    $user=Yii::$app->user->identity;
		    $UserIdP=$user->idP;
			
			$plus="";
			$position= 0;
			if($UserIdP==0){
				$position= 1;
			}else{	

				$id_center=Yii::$app->session->get('default_center');
				$infoProfil=UserAccess::find()->select(['id_user_profil'])->where(['id_user'=>$user->id,'etat'=>1,'id_user_profil'=>UserProfil::find()->select(['id_user_profil'])->where(['id_center'=>$id_center,'etat_user_profil'=>1])])->one();

				if($infoProfil!=null){
					
					$id_profil=$infoProfil->id_user_profil;
					 
					if($type_operation=="CREATE")$plus=" and profil_access.pcreate='1' ";
					if($type_operation=="UPDATE")$plus=" and profil_access.pupdate='1' ";
					if($type_operation=="DELETE")$plus=" and profil_access.pdelete='1'";
					if($type_operation=="READ")$plus=" and  (profil_access.pupdate='1' or profil_access.pdelete='1' or profil_access.pread='1')";
					if($type_operation=="ALL")$plus=" and  (profil_access.pcreate='1' and profil_access.pupdate='1' and profil_access.pdelete='1' and profil_access.pread='1')";
				   
				   $qlmodule="select id_right from school_module where id_center='".$id_center."' and etat=1";
				   $sql="select* from profil_access,access_right where access_right.id_right not in (".$qlmodule.") and
					   access_right.lib_right='".$lib_right."' and access_right.id_right=profil_access.id_right 
					   and profil_access.id_profil='".$id_profil."' and profil_access.etat_profil_access='1' $plus";
				  
					$connection=Yii::$app->db;
					$command=$connection->createCommand($sql);
					$users=$command->queryAll();	
					$position= sizeof($users) ;
				
				}
				
			}
			if($position!=1){				
				return Yii::$app->getResponse()->redirect("account");
			}
	}
		
	public static function have_access ($lib_right){
	  
	   $user=Yii::$app->user->identity;
       
       $UserIdP=$user->idP;
		
		$position= "0_0_0_0";
		if($UserIdP==0){
			$position= "1_1_1_1";
		}else{		

				//recuperer le profil de la personne dans ce centre
				
				$id_center=Yii::$app->session->get('default_center');
				$infoProfil=UserAccess::find()->select(['id_user_profil'])->where(['id_user'=>$user->id,'etat'=>1,'id_user_profil'=>UserProfil::find()->select(['id_user_profil'])->where(['id_center'=>$id_center,'etat_user_profil'=>1])])->one();

				if($infoProfil!=null){
					 $qlmodule="select id_right from school_module where id_center='".$id_center."' and etat=1";
				   $id_profil=$infoProfil->id_user_profil;
				   $sql="select profil_access.* from profil_access,access_right where access_right.id_right not in (".$qlmodule.") and access_right.etat_right='1' and
					   access_right.lib_right='".$lib_right."' and access_right.id_right=profil_access.id_right 
					   and profil_access.id_profil='".$id_profil."' and profil_access.etat_profil_access='1'";
				  
					$connection=Yii::$app->db;
					$command=$connection->createCommand($sql);
					$users=$command->queryAll();
					
					
					
					if(sizeof($users)==1){
						$position=$users[0]['pcreate']."_".$users[0]['pread']."_".$users[0]['pupdate']."_".$users[0]['pdelete'];
					}
				}
		}		
		return $position ;
	}
		
	public static function date_convert($date_send,$type=0){ 
         $date= explode(" ",$date_send);
	     $recup=explode("-",$date[0]);
		
		if(sizeof($date)==1){
			if(sizeof($recup)>=3){
				return $recup[2].' '.Utils::convertir_mois($recup[1]).' '.$recup[0] ;
			}else return $date_send ; 
		}else if(sizeof($date)==2){
			if(sizeof($recup)>=3){
				if($type==1){
					return $recup[2].' '.Utils::convertir_mois($recup[1]).' '.$recup[0];
				}else{
					return $recup[2].' '.Utils::convertir_mois($recup[1]).' '.$recup[0].' - '.$date[1] ;
				}
			}else return $date_send ;
		}else return $date_send ;
	}

	public static function conversion_date($date)
   {
	   $recup=explode("/",$date);
       $annee=$recup[2];
       $mois=$recup[1];
       $jour=$recup[0];
             
       return $annee."-".$mois."-".$jour;  
   }
 
   public static function reconversion_date($date)
   {
	   $recup=explode("-",$date);
       $annee=$recup[0];
       $mois=$recup[1];
       $jour=$recup[2];
             
       return $jour."/".$mois."/".$annee;  
   }
	
    public static	function convertir_mois($mois)
    {
       
       switch ($mois) {
           case "1": $mois=Yii::t('app', 'month01');
               break;
           case "2": $mois=Yii::t('app', 'month02');
               break;
           case "3": $mois=Yii::t('app', 'month03');
               break;
           case "4": $mois=Yii::t('app', 'month04');
               break;
           case "5": $mois=Yii::t('app', 'month05');
               break;
           case "6": $mois=Yii::t('app', 'month06');
               break;
           case "7": $mois=Yii::t('app', 'month07');
               break;
           case "8": $mois=Yii::t('app', 'month08');
               break;
           case "9": $mois=Yii::t('app', 'month09');
               break;
           case "10": $mois=Yii::t('app', 'month10');
               break;
           case "11": $mois=Yii::t('app', 'month11');
               break;
           case "12": $mois=Yii::t('app', 'month12');
               break;
       }
       return $mois ;
   }
   
    public static	function all_years()
    {
		$tab=array();
		$years_begin=2021;
		$years_end=date("Y")+1;
		for($i=$years_begin;$i<=$years_end;$i++){
			$tab[$i]=$i;
		}		
		return $tab;
	}	
	
	public static	function all_mois()
    {
       
	   $tab_mois["01"]=Yii::t('app', 'month01');
	   $tab_mois["02"]=Yii::t('app', 'month02');
	   $tab_mois["03"]=Yii::t('app', 'month03');
	   $tab_mois["04"]=Yii::t('app', 'month04');
	   $tab_mois["05"]=Yii::t('app', 'month05');
	   $tab_mois["06"]=Yii::t('app', 'month06');
	   $tab_mois["07"]=Yii::t('app', 'month07');
	   $tab_mois["08"]=Yii::t('app', 'month08');
	   $tab_mois["09"]=Yii::t('app', 'month09');
	   $tab_mois["10"]=Yii::t('app', 'month10');
	   $tab_mois["11"]=Yii::t('app', 'month11');
	   $tab_mois["12"]=Yii::t('app', 'month12');
       
       return $tab_mois ;
   }
	 
	public static function retour($recup)
	{	
			$retour="";
			if(sizeof($recup)>0){
				$title=Yii::t('app', 'dashboard');
				
				$content='';
				
				$i=1;					
				$content.='<li class="breadcrumb-item"><a  href="'.Yii::$app->request->baseUrl.'/account">'.Yii::t('app','dashboard').'</a></li>';
				foreach ($recup as $key => $value){
						if($i==sizeof($recup)){
							$content.='<li class="breadcrumb-item active" aria-current="page">'.$value.'</li>';
							$title=$value;
						}else{
							$content.='<li class="breadcrumb-item"><a href="'.Yii::$app->request->baseUrl.'/'.$key.'">'.$value.'</a></li>';								
						}
					$i++;
				}
				
				
						
				$retour= '<div class="card">
					  <div class="card-header">
						<h4>'.$title.'</h4>
					  </div>
					  <div class="card-body">
						<nav aria-label="breadcrumb">
						  <ol class="breadcrumb">
							'.$content.'
						  </ol>
						</nav>
					  </div>
					</div>';
					
			}
			
			return $retour ;
			
	}
	

     
	public static function hit_sms($message,$to) {
   
   
		$message=str_replace("+","%2B",$message);
		$nbre_caractere=strlen($message);
		$nbre_page=(int)($nbre_caractere/160)+1;
		
		$new_message=new MessageHistorique ;
		$new_message->destinataire=$to;
		$new_message->content=$message;
		$new_message->nbre_page=$nbre_page;
		$new_message->datemessage=date("Y-m-d H:i:s");
		$new_message->response_message="";
		$new_message->operateur="wassasms";
		$new_message->save();
		
		
		
		$from="Geschool";
		$textMsg = urlencode($message);
		$dlrurl = "";
		$url = Utils::wassa_apiurl.$from."&receiver=".$to."&text=".$textMsg."&dlr_url=".$dlrurl;
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_COOKIESESSION, true);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		$resultat = curl_exec($curl);
		
		if($errno = curl_errno($curl)) {
			$error_message = curl_strerror($errno);
			 "\n\ncURL error ({$errno}):\n {$error_message}";
		}else{
			$error_message =$resultat;
		}
		curl_close($curl);
		$new_message->response_message=$error_message;
		$new_message->save();
		return $error_message ;
		
	}

	public static function getApiUrl(){
		return 'http://192.168.20.12/geschool/api/web/school_v1/';
	}

	public static function getApiKey(){
		return 'SzrQt6TazKv2ayDHqs754qvvAvD';
	}

	public static function have_accessapi($lib_right, $userIdP, $userId, $id_center)
	{

		$position = "0_0_0_0";
		if ($userIdP == 0) {
			$position = "1_1_1_1";
		} else {
			//recuperer le profil de la personne dans ce centre

			$infoProfil = UserAccess::find()->select(['id_user_profil'])->where(['id_user' => $userId, 'etat' => 1, 'id_user_profil' => UserProfil::find()->select(['id_user_profil'])->where(['id_center' => $id_center, 'etat_user_profil' => 1])])->one();

			if ($infoProfil != null) {
				$qlmodule = "select id_right from school_module where id_center='" . $id_center . "' and etat=1";
				$id_profil = $infoProfil->id_user_profil;
				$sql = "select profil_access.* from profil_access,access_right where access_right.id_right not in (" . $qlmodule . ") and access_right.etat_right='1' and
					   access_right.lib_right='" . $lib_right . "' and access_right.id_right=profil_access.id_right
					   and profil_access.id_profil='" . $id_profil . "' and profil_access.etat_profil_access='1'";

				$connection = Yii::$app->db;
				$command = $connection->createCommand($sql);
				$users = $command->queryAll();



				if (sizeof($users) == 1) {
					$position = $users[0]['pcreate'] . "" . $users[0]['pread'] . "" . $users[0]['pupdate'] . "_" . $users[0]['pdelete'];
				}
			}
		}
		return $position;
	}


	//Vérifier la validité d'une date
	public static function isValidDate($date, $format = 'Y-m-d'){
		$dt = \DateTime::createFromFormat($format, $date);
		return $dt && $dt->format($format) === $date;
	}
	

}
