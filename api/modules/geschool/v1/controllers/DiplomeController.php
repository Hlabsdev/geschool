<?php

namespace api\modules\geschool\v1\controllers;

use api\modules\geschool\v1\models\Typeabsence;
use api\modules\geschool\v1\models\Diplome;
use api\modules\geschool\v1\models\SchoolCenter;
use api\modules\geschool\v1\models\User;
use Yii;
use yii\rest\ActiveController;

/**
 * Geschool Controller API
 *
 */
class DiplomeController extends ActiveController
{
	public $modelClass = 'api\modules\geschool\v1\models\Diplome';

	#000 -> Tout est bien
	#001 -> Message client
	#002 -> Message dev


	public function actionDiplomes()
	{
		$token = Utils::getApiKey();
		if (Yii::$app->request->post()) {
			if (isset($_POST['token']) && $_POST['token'] == $token) {
				if (isset($_POST['user_auth_key']) && $_POST['user_auth_key'] != '') {
					$user_auth_key = $_POST['user_auth_key'];
					$user_exist = User::find()->where(['auth_key' => $user_auth_key, 'status' => 10])->one();

					if ($user_exist != '') {

						if (isset($_POST['id_center']) && $_POST['id_center'] != '') {
							$id_center = $_POST['id_center'];

							$center = SchoolCenter::find()->where(['id_center' => $id_center, 'etat' => [1, 2, 4]])->one();
							if ($center != '') {
								$cont = false;
								$user_id_center = str_replace('-', '', $user_exist->id_center);
								$test_center = explode(",", $user_id_center);

								if ($user_exist->id_center == "1") {
									$cont = true;
								} else if (in_array($id_center, $test_center)) {
									$cont = true;
								}

								if ($cont == true) {

									$manage_diplome = Utils::have_accessapi("manage_diplome", $user_exist->idP, $user_exist->id, $id_center);
									$test = explode('_', $manage_diplome);


									if ($test[1] == '1') { // INDICE : 0 pour CREATE; 1 pour READ; 2 pour UPDATE; 3 pour DELETE;


										$data = Diplome::find()->where(['status' => [1, 2]])->orderBy(['libellediplome' => SORT_ASC])->all();

										return [
											'status' => '000',
											'message' => 'succes_get_diplome',
											'data' => $data
										];

									} else {
										return [
											'status' => '002',
											'message' => 'Accès non autorisé.',
										];
									}
								} else {
									return [
										'status' => '002',
										'message' => 'Cet utilisateur n\'a pas accès à ce centre.',
									];
								}
							} else {
								return [
									'status' => '002',
									'message' => 'id_center est incorrect.',
								];
							}
						} else {
							return [
								'status' => '002',
								'message' => 'id_center ne doit pas être vide.',
							];
						}
					} else {
						return [
							'status' => '002',
							'message' => 'Utilisateur non trouvé.',
						];
					}
				} else {
					return [
						'status' => '002',
						'message' => 'user_auth_key ne doit pas être vide.',
					];
				}
			} else {
				return [
					'status' => '002',
					'message' => 'Token incorrect',
				];
			}
		} else {
			return [
				'status' => '002',
				'message' => 'Aucune donnée reçue en POST',
			];
		}
		exit;
	}

	public function actionAdddiplome()
	{
		$token = Utils::getApiKey();
		if (Yii::$app->request->post()) {

			if (isset($_POST['token']) && $_POST['token'] == $token) {
				if (isset($_POST['user_auth_key']) && $_POST['user_auth_key'] != '') {
					$user_auth_key = $_POST['user_auth_key'];
					$user_exist = User::find()->where(['auth_key' => $user_auth_key, 'status' => 10])->one();
					if ($user_exist != '') {


						if (isset($_POST['id_center']) && $_POST['id_center'] != '') {
							$id_center = $_POST['id_center'];

							$center = SchoolCenter::find()->where(['id_center' => $id_center, 'etat' => [1, 2, 4]])->one();
							if ($center != '') {
								$cont = false;
								$user_id_center = str_replace('-', '', $user_exist->id_center);
								$test_center = explode(",", $user_id_center);

								if ($user_exist->id_center == "1") {
									$cont = true;
								} else if (in_array($id_center, $test_center)) {
									$cont = true;
								}

								if ($cont == true) {

									$manage_diplome = Utils::have_accessapi("manage_diplome", $user_exist->idP, $user_exist->id, $id_center);
									$test = explode('_', $manage_diplome);

									if (isset($_POST['libelle']) && $_POST['libelle'] != '') {
										$libelle = $_POST['libelle'];
										if (isset($_POST['operation']) && $_POST['operation'] != '') {
											$operation = $_POST['operation'];
											if ($operation == 1) { // AJOUT
												if ($test[0] == '1') { // INDICE : 0 pour CREATE; 1 pour READ; 2 pour UPDATE; 3 pour DELETE;

													$exist = Diplome::find()->where(['libellediplome' => $libelle, 'status' => [1, 2]])->one();
													if ($exist == '' or $exist == null) {
														$new_record = new Diplome();
														$new_record->libellediplome = $libelle;
														$new_record->keydiplome = Yii::$app->security->generateRandomString(32);
														$new_record->status = 1;
														$new_record->created_at = time();
														$new_record->updated_at = '';
														$new_record->create_by = $user_exist->id;
														$new_record->updated_by = '';
														if ($new_record->save()) {
															$data = Diplome::find()->where(['status' => [1, 2]])->orderBy(['libellediplome' => SORT_ASC])->all();
															return [
																'status' => '000',
																'message' => 'succes_creation_diplome',
																'data' => $data,
															];
														} else {
															return [
																'status' => '002',
																'message' => 'update_error',
															];
														}
													} else {
														return [
															'status' => '003',
															'message' => 'diplome_used',
														];
													}
												} else {
													return [
														'status' => '002',
														'message' => 'Accès non autorisé.',
													];
												}
											} else if ($operation == 2) { // MISE A JOUR
												if ($test[2] == '1') { // INDICE : 0 pour CREATE; 1 pour READ; 2 pour UPDATE; 3 pour DELETE;


													$continue = false;
													if (isset($_POST['diplome_key']) && $_POST['diplome_key'] != '') {
														$diplome_key = $_POST['diplome_key'];
														$old_record = Diplome::find()->where(['keydiplome' => $diplome_key, 'status' => [1, 2]])->one();
														if ($old_record != '') {

															$exist = Diplome::find()->where(['libellediplome' => $libelle, 'status' => [1, 2]])->one();
															if ($exist == '' or $exist == null) {
																$continue = true;
															} else {
																($exist->keydiplome != $diplome_key) ? $continue = false : $continue = true;
															}

															if ($continue == true) {
																$old_record->libellediplome = $libelle;
																$old_record->updated_at = time();
																$old_record->updated_by = $user_exist->id;
																if ($old_record->save()) {
																	$data = Diplome::find()->where(['status' => [1, 2]])->orderBy(['libellediplome' => SORT_ASC])->all();
																	return [
																		'status' => '000',
																		'message' => 'succes_update_diplome',
																		'data' => $data,
																	];
																} else {
																	return [
																		'status' => '002',
																		'message' => 'update_error',
																	];
																}
															} else {
																return [
																	'status' => '003',
																	'message' => 'diplome_used',
																];
															}
														} else {
															return [
																'status' => '001',
																'message' => 'diplome_not_found',
															];
														}
													} else {
														return [
															'status' => '001',
															'message' => 'diplome_key_empty',
														];
													}
												} else {
													return [
														'status' => '002',
														'message' => 'Accès non autorisé.',
													];
												}
											} else {
												return [
													'status' => '002',
													'message' => 'Type d\'opération non reconnu.',
												];
											}
										} else {
											return [
												'status' => '002',
												'message' => 'Le type d\'opération ne doit pas être vide.',
											];
										}
									} else {
										return [
											'status' => '001',
											'message' => 'diplome_libelle_empty',
										];
									}
								} else {
									return [
										'status' => '002',
										'message' => 'Cet utilisateur n\'a pas accès à ce centre.',
									];
								}
							} else {
								return [
									'status' => '002',
									'message' => 'id_center est incorrect.',
								];
							}
						} else {
							return [
								'status' => '002',
								'message' => 'id_center ne doit pas être vide.',
							];
						}
					} else {
						return [
							'status' => '002',
							'message' => 'Utilisateur non trouvé.',
						];
					}
				} else {
					return [
						'status' => '002',
						'message' => 'user_auth_key ne doit pas être vide.',
					];
				}
			} else {
				return [
					'status' => '002',
					'message' => 'Token incorrect.',
				];
			}
		} else {
			return [
				'status' => '002',
				'message' => 'Aucune donnée reçue en POST.',
			];
		}
		exit;
	}

	public function actionDiplome()
	{
		$token = Utils::getApiKey();
		if (Yii::$app->request->post()) {
			if (isset($_POST['token']) && $_POST['token'] == $token) {
				if (isset($_POST['user_auth_key']) && $_POST['user_auth_key'] != '') {
					$user_auth_key = $_POST['user_auth_key'];
					$user_exist = User::find()->where(['auth_key' => $user_auth_key, 'status' => 10])->one();
					if ($user_exist != '') {

						if (isset($_POST['id_center']) && $_POST['id_center'] != '') {
							$id_center = $_POST['id_center'];

							$center = SchoolCenter::find()->where(['id_center' => $id_center, 'etat' => [1, 2, 4]])->one();
							if ($center != '') {
								$cont = false;
								$user_id_center = str_replace('-', '', $user_exist->id_center);
								$test_center = explode(",", $user_id_center);

								if ($user_exist->id_center == "1") {
									$cont = true;
								} else if (in_array($id_center, $test_center)) {
									$cont = true;
								}

								if ($cont == true) {

									$manage_diplome = Utils::have_accessapi("manage_diplome", $user_exist->idP, $user_exist->id, $id_center);
									$test = explode('_', $manage_diplome);


									if ($test[2] == '1') { // INDICE : 0 pour CREATE; 1 pour READ; 2 pour UPDATE; 3 pour DELETE;


										if (isset($_POST['diplome_key']) && $_POST['diplome_key'] != '') {
											$diplome_key = $_POST['diplome_key'];
											$diplome_exist = Diplome::find()->where(['keydiplome' => $diplome_key])->one();
											if ($diplome_exist != '') {
												return [
													'status' => '000',
													'message' => 'succes_get_diplome',
													'data' => $diplome_exist,
												];
											} else {
												return [
													'status' => '001',
													'message' => 'diplome_not_found',
												];
											}
										} else {
											return [
												'status' => '001',
												'message' => 'diplome_key_empty',
											];
										}
									} else {
										return [
											'status' => '002',
											'message' => 'Accès non autorisé.',
										];
									}
								} else {
									return [
										'status' => '002',
										'message' => 'Cet utilisateur n\'a pas accès à ce centre.',
									];
								}
							} else {
								return [
									'status' => '002',
									'message' => 'id_center est incorrect.',
								];
							}
						} else {
							return [
								'status' => '002',
								'message' => 'id_center ne doit pas être vide.',
							];
						}
					} else {
						return [
							'status' => '002',
							'message' => 'Utilisateur non trouvé.',
						];
					}
				} else {
					return [
						'status' => '002',
						'message' => 'user_auth_key ne doit pas être vide.',
					];
				}
			} else {
				return [
					'status' => '002',
					'message' => 'Token incorrect.',
				];
			}
		} else {
			return [
				'status' => '002',
				'message' => 'Aucune donnée reçue en POST.',
			];
		}
		exit;
	}

	public function actionDeletediplome()
	{
		$token = Utils::getApiKey();
		$msg = "";
		if (Yii::$app->request->post()) {
			if (isset($_POST['token']) && $_POST['token'] == $token) {
				if (isset($_POST['user_auth_key']) && $_POST['user_auth_key'] != '') {
					$user_auth_key = $_POST['user_auth_key'];
					$user_exist = User::find()->where(['auth_key' => $user_auth_key, 'status' => 10])->one();
					if ($user_exist != '') {

						if (isset($_POST['id_center']) && $_POST['id_center'] != '') {
							$id_center = $_POST['id_center'];

							$center = SchoolCenter::find()->where(['id_center' => $id_center, 'etat' => [1, 2, 4]])->one();
							if ($center != '') {
								$cont = false;
								$user_id_center = str_replace('-', '', $user_exist->id_center);
								$test_center = explode(",", $user_id_center);

								if ($user_exist->id_center == "1") {
									$cont = true;
								} else if (in_array($id_center, $test_center)) {
									$cont = true;
								}

								if ($cont == true) {

									$manage_diplome = Utils::have_accessapi("manage_diplome", $user_exist->idP, $user_exist->id, $id_center);
									$test = explode('_', $manage_diplome);

									if ($test[3] == '1') { // INDICE : 0 pour CREATE; 1 pour READ; 2 pour UPDATE; 3 pour DELETE;

										if (isset($_POST['operation']) && $_POST['operation'] != '') {
											$operation = $_POST['operation'];
											if (isset($_POST['diplome_key']) && $_POST['diplome_key'] != '') {
												$diplome_key = $_POST['diplome_key'];
												$old_record = Diplome::find()->where(['keydiplome' => $diplome_key])->one();
												if ($old_record != '') {

													if ($operation == 3) { // SUPPRESSION
														$old_record->status = 3;
														$msg = "delete_diplome_success";
													} else if ($operation == 2) { // DESACTIVATION
														$old_record->status = 2;
														$msg = "desactive_diplome_success";
													} else if ($operation == 1) { // ACTIVATION
														$old_record->status = 1;
														$msg = "active_diplome_success";
													} else {
														return [
															'status' => '002',
															'message' => 'Type d\'opération non reconnu',
														];
													}

													$old_record->updated_at = time();
													$old_record->updated_by = $user_exist->id;
													if ($old_record->save()) {
														return [
															'status' => '000',
															'message' => $msg,
														];
													} else {
														return [
															'status' => '002',
															'message' => 'update_error',
														];
													}
												} else {
													return [
														'status' => '001',
														'message' => 'diplome_not_found',
													];
												}
											} else {
												return [
													'status' => '001',
													'message' => 'diplome_key_empty',
												];
											}
										} else {
											return [
												'status' => '002',
												'message' => 'Le type d\'opération ne doit pas être vide.',
											];
										}
									} else {
										return [
											'status' => '002',
											'message' => 'Accès non autorisé.',
										];
									}
								} else {
									return [
										'status' => '002',
										'message' => 'Cet utilisateur n\'a pas accès à ce centre.',
									];
								}
							} else {
								return [
									'status' => '002',
									'message' => 'id_center est incorrect.',
								];
							}
						} else {
							return [
								'status' => '002',
								'message' => 'id_center ne doit pas être vide.',
							];
						}
					} else {
						return [
							'status' => '002',
							'message' => 'Utilisateur non trouvé.',
						];
					}
				} else {
					return [
						'status' => '002',
						'message' => 'user_auth_key ne doit pas être vide.',
					];
				}
			} else {
				return [
					'status' => '002',
					'message' => 'Token incorrect.',
				];
			}
		} else {
			return [
				'status' => '002',
				'message' => 'Aucune donnée reçue en POST.',
			];
		}
		exit;
	}
}
