<?php

namespace api\modules\geschool\v1\controllers;

use api\modules\geschool\v1\models\Absence;
use api\modules\geschool\v1\models\Mission;
use api\modules\geschool\v1\models\Personnel;
use api\modules\geschool\v1\models\Typeabsence;
use api\modules\geschool\v1\models\Typepersonnel;
use api\modules\geschool\v1\models\User;
use DateTime;
use Yii;
use yii\rest\ActiveController;

/**
 * Geschool Controller API
 *
 */
class GeschoolController extends ActiveController
{
	public $modelClass = 'api\modules\geschool\v1\models\Parametrage';

	#000 -> Tout est bien
	#001 -> Message client
	#002 -> Message dev

	public function actionTest()
	{
		echo 'test';
		exit;
	}
}
