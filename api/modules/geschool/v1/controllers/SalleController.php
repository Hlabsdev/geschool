<?php

namespace api\modules\geschool\v1\controllers;
use api\modules\geschool\v1\models\Salle;
use Yii;
use yii\rest\ActiveController;
use api\modules\geschool\v1\models\User;
use api\modules\geschool\v1\models\SchoolCenter;

/**
 * Geschool Controller API
 *
 */

class SalleController extends ActiveController
{
    public $modelClass = 'api\modules\geschool\v1\models\Salle';

    #000 -> Tout est bien
    #001 -> Message client
    #002 -> Message dev


    //Cette méthode nous permet de récupérer les salles de cours d'un centre
    public function actionSalles()
    {
        $token = Utils::getApiKey();
        if (Yii::$app->request->post()) {
            if (isset($_POST['token']) && $_POST['token'] == $token) {
                if (isset($_POST['user_auth_key']) && $_POST['user_auth_key'] != '') {
                    $user_auth_key = $_POST['user_auth_key'];
                    $user_exist = User::find()->where(['auth_key' => $user_auth_key, 'status' => 10])->one();
                    if ($user_exist != '') {

                        if (isset($_POST['id_center']) && $_POST['id_center'] != '') {
                            $id_center = $_POST['id_center'];

                            $center = SchoolCenter::find()->where(['id_center' => $id_center, 'etat' => [1, 2, 4]])->one();
                            if ($center != '') {
                                $cont = false;
                                $user_id_center = str_replace('-', '', $user_exist->id_center);
                                $test_center = explode(",", $user_id_center);

                                if ($user_exist->id_center == "1") {
                                    $cont = true;
                                } else if (in_array($id_center, $test_center)) {
                                    $cont = true;
                                }

                                if ($cont == true) {

                                    $manage_salle = Utils::have_accessapi("manage_salle", $user_exist->idP, $user_exist->id, $id_center);
                                    $test = explode('_', $manage_salle);
                                    if ($test[1] == '1') { // INDICE : 0 pour CREATE; 1 pour READ; 2 pour UPDATE; 3 pour DELETE;


                                        $data = Salle::find()->where(['idcentre'=>$center->id_center,'status' => [1, 2]])->orderBy(['libelle' => SORT_ASC])->all();

                                        if (sizeof($data) > 0) {
                                            return [
                                                'status' => '000',
                                                'message' => 'succes_get_salle',
                                                'data' => $data
                                            ];
                                        } else {
                                            return [
                                                'status' => '001',
                                                'message' => 'empty_salle',
                                            ];
                                        }

                                    } else {
                                        return [
                                            'status' => '002',
                                            'message' => 'Accès non autorisé.',
                                        ];
                                    }
                                } else {
                                    return [
                                        'status' => '002',
                                        'message' => 'Cet utilisateur n\'a pas accès à ce centre.',
                                    ];
                                }
                            } else {
                                return [
                                    'status' => '002',
                                    'message' => 'id_center est incorrect.',
                                ];
                            }
                        } else {
                            return [
                                'status' => '002',
                                'message' => 'id_center ne doit pas être vide.',
                            ];
                        }
                    } else {
                        return [
                            'status' => '002',
                            'message' => 'Utilisateur non trouvé.',
                        ];
                    }
                } else {
                    return [
                        'status' => '002',
                        'message' => 'user_auth_key ne doit pas être vide.',
                    ];
                }
            } else {
                return [
                    'status' => '002',
                    'message' => 'Token incorrect',
                ];
            }
        } else {
            return [
                'status' => '002',
                'message' => 'Aucune donnée reçue en POST',
            ];
        }
        exit;
    }

    //Cette méthode nous permet d'ajouter une salle de cours à un centre
    public function actionAddsalle()
    {
        $token = Utils::getApiKey();
        if (Yii::$app->request->post()) {

            if (isset($_POST['token']) && $_POST['token'] == $token) {
                if (isset($_POST['user_auth_key']) && $_POST['user_auth_key'] != '') {
                    $user_auth_key = $_POST['user_auth_key'];
                    $user_exist = User::find()->where(['auth_key' => $user_auth_key, 'status' => 10])->one();
                    if ($user_exist != '') {


                        if (isset($_POST['id_center']) && $_POST['id_center'] != '') {
                            $id_center = $_POST['id_center'];

                            $center = SchoolCenter::find()->where(['id_center' => $id_center, 'etat' => [1, 2, 4]])->one();
                            if ($center != '') {
                                $cont = false;
                                $user_id_center = str_replace('-', '', $user_exist->id_center);
                                $test_center = explode(",", $user_id_center);

                                if ($user_exist->id_center == "1") {
                                    $cont = true;
                                } else if (in_array($id_center, $test_center)) {
                                    $cont = true;
                                }

                                if ($cont == true) {

                                    $manage_salle = Utils::have_accessapi("manage_salle", $user_exist->idP, $user_exist->id, $id_center);
                                    $test = explode('_', $manage_salle);

                                    if (isset($_POST['libelle']) && $_POST['libelle'] != '') {
                                        $libelle = $_POST['libelle'];
                                        if (isset($_POST['capacite']) && $_POST['capacite'] != '') {
                                            $capacite = $_POST['capacite'];

                                            if (isset($_POST['operation']) && $_POST['operation'] != '') {
                                                $operation = $_POST['operation'];
                                                if ($operation == 1) { // AJOUT
                                                    if ($test[0] == '1') { // INDICE : 0 pour CREATE; 1 pour READ; 2 pour UPDATE; 3 pour DELETE;

                                                        $exist = Salle::find()->where(['idcentre'=>$center->id_center,'libelle' => $libelle, 'status' => [1, 2]])->one();
                                                        if ($exist == '' or $exist == null) {
                                                            $new_record = new Salle();
                                                            $new_record->libelle = $libelle;
                                                            $new_record->nbreplacemaxi = $capacite;
                                                            $new_record->keysalle = Yii::$app->security->generateRandomString(32);
                                                            $new_record->idcentre = $center->id_center;
                                                            $new_record->status = 1;
                                                            $new_record->created_at = time();
                                                            $new_record->updated_at = '';
                                                            $new_record->create_by = $user_exist->id;
                                                            $new_record->updated_by = '';
                                                            if ($new_record->save()) {
                                                                $data = Salle::find()->where(['idcentre'=>$center->id_center,'status' => [1, 2]])->orderBy(['libelle' => SORT_ASC])->all();
                                                                return [
                                                                    'status' => '000',
                                                                    'message' => 'succes_creation_salle',
                                                                    'data' => $data,
                                                                ];
                                                            } else {
                                                                return [
                                                                    'status' => '002',
                                                                    'message' => 'update_error',
                                                                ];
                                                            }
                                                        } else {
                                                            return [
                                                                'status' => '003',
                                                                'message' => 'salle_used',
                                                            ];
                                                        }
                                                    } else {
                                                        return [
                                                            'status' => '002',
                                                            'message' => 'Accès non autorisé.',
                                                        ];
                                                    }
                                                } else if ($operation == 2) { // MISE A JOUR
                                                    if ($test[2] == '1') { // INDICE : 0 pour CREATE; 1 pour READ; 2 pour UPDATE; 3 pour DELETE;

                                                        $continue = false;
                                                        if (isset($_POST['salle_key']) && $_POST['salle_key'] != '') {
                                                            $salle_key = $_POST['salle_key'];
                                                            $old_record = Salle::find()->where(['keysalle' => $salle_key, 'status' => [1, 2]])->one();
                                                            if ($old_record != '') {

                                                                $exist = Salle::find()->where(['idcentre'=>$center->id_center,'libelle' => $libelle, 'status' => [1, 2]])->one();
                                                                if ($exist == '' or $exist == null) {
                                                                    $continue = true;
                                                                } else {
                                                                    ($exist->keysalle != $salle_key) ? $continue = false : $continue = true;
                                                                }

                                                                if ($continue == true) {
                                                                    $old_record->libelle = $libelle;
                                                                    $old_record->nbreplacemaxi = $capacite;
                                                                    $old_record->updated_at = time();
                                                                    $old_record->updated_by = $user_exist->id;
                                                                    if ($old_record->save()) {
                                                                        $data = Salle::find()->where(['idcentre'=>$center->id_center,'status' => [1, 2]])->orderBy(['libelle' => SORT_ASC])->all();
                                                                        return [
                                                                            'status' => '000',
                                                                            'message' => 'succes_update_salle',
                                                                            'data' => $data,
                                                                        ];
                                                                    } else {
                                                                        return [
                                                                            'status' => '002',
                                                                            'message' => 'update_error',
                                                                        ];
                                                                    }
                                                                } else {
                                                                    return [
                                                                        'status' => '003',
                                                                        'message' => 'salle_used',
                                                                    ];
                                                                }
                                                            } else {
                                                                return [
                                                                    'status' => '001',
                                                                    'message' => 'salle_not_found',
                                                                ];
                                                            }
                                                        } else {
                                                            return [
                                                                'status' => '001',
                                                                'message' => 'salle_key_empty',
                                                            ];
                                                        }
                                                    } else {
                                                        return [
                                                            'status' => '002',
                                                            'message' => 'Accès non autorisé.',
                                                        ];
                                                    }
                                                } else {
                                                    return [
                                                        'status' => '002',
                                                        'message' => 'Type d\'opération non reconnu.',
                                                    ];
                                                }
                                            } else {
                                                return [
                                                    'status' => '002',
                                                    'message' => 'Le type d\'opération ne doit pas être vide.',
                                                ];
                                            }

                                        } else {
                                        return [
                                            'status' => '001',
                                            'message' => 'salle_capacite_empty',
                                        ];
                                    }

                                    } else {
                                        return [
                                            'status' => '001',
                                            'message' => 'salle_libelle_empty',
                                        ];
                                    }
                                } else {
                                    return [
                                        'status' => '002',
                                        'message' => 'Cet utilisateur n\'a pas accès à ce centre.',
                                    ];
                                }
                            } else {
                                return [
                                    'status' => '002',
                                    'message' => 'id_center est incorrect.',
                                ];
                            }
                        } else {
                            return [
                                'status' => '002',
                                'message' => 'id_center ne doit pas être vide.',
                            ];
                        }
                    } else {
                        return [
                            'status' => '002',
                            'message' => 'Utilisateur non trouvé.',
                        ];
                    }
                } else {
                    return [
                        'status' => '002',
                        'message' => 'user_auth_key ne doit pas être vide.',
                    ];
                }
            } else {
                return [
                    'status' => '002',
                    'message' => 'Token incorrect.',
                ];
            }
        } else {
            return [
                'status' => '002',
                'message' => 'Aucune donnée reçue en POST.',
            ];
        }
        exit;
    }

    //Cette méthode nous permet de récuperer une salle d'un centre
    public function actionSalle()
    {
        $token = Utils::getApiKey();
        if (Yii::$app->request->post()) {
            if (isset($_POST['token']) && $_POST['token'] == $token) {
                if (isset($_POST['user_auth_key']) && $_POST['user_auth_key'] != '') {
                    $user_auth_key = $_POST['user_auth_key'];
                    $user_exist = User::find()->where(['auth_key' => $user_auth_key, 'status' => 10])->one();
                    if ($user_exist != '') {

                        if (isset($_POST['id_center']) && $_POST['id_center'] != '') {
                            $id_center = $_POST['id_center'];

                            $center = SchoolCenter::find()->where(['id_center' => $id_center, 'etat' => [1, 2, 4]])->one();
                            if ($center != '') {
                                $cont = false;
                                $user_id_center = str_replace('-', '', $user_exist->id_center);
                                $test_center = explode(",", $user_id_center);

                                if ($user_exist->id_center == "1") {
                                    $cont = true;
                                } else if (in_array($id_center, $test_center)) {
                                    $cont = true;
                                }

                                if ($cont == true) {

                                    $manage_salle = Utils::have_accessapi("manage_salle", $user_exist->idP, $user_exist->id, $id_center);
                                    $test = explode('_', $manage_salle);
                                    if ($test[2] == '1') { // INDICE : 0 pour CREATE; 1 pour READ; 2 pour UPDATE; 3 pour DELETE;

                                        if (isset($_POST['salle_key']) && $_POST['salle_key'] != '') {
                                            $salle_key = $_POST['salle_key'];
                                            $salle_exist = Salle::find()->where(['keysalle'=>$salle_key,'idcentre'=>$center->id_center,'status' => [1,2]])->one();
                                            if ($salle_exist != '') {
                                                return [
                                                    'status' => '000',
                                                    'message' => 'succes_get_salle',
                                                    'data' => $salle_exist,
                                                ];
                                            } else {
                                                return [
                                                    'status' => '001',
                                                    'message' => 'salle_not_found',
                                                ];
                                            }
                                        } else {
                                            return [
                                                'status' => '001',
                                                'message' => 'salle_key_empty',
                                            ];
                                        }
                                    } else {
                                        return [
                                            'status' => '002',
                                            'message' => 'Accès non autorisé.',
                                        ];
                                    }
                                } else {
                                    return [
                                        'status' => '002',
                                        'message' => 'Cet utilisateur n\'a pas accès à ce centre.',
                                    ];
                                }
                            } else {
                                return [
                                    'status' => '002',
                                    'message' => 'id_center est incorrect.',
                                ];
                            }
                        } else {
                            return [
                                'status' => '002',
                                'message' => 'id_center ne doit pas être vide.',
                            ];
                        }
                    } else {
                        return [
                            'status' => '002',
                            'message' => 'Utilisateur non trouvé.',
                        ];
                    }
                } else {
                    return [
                        'status' => '002',
                        'message' => 'user_auth_key ne doit pas être vide.',
                    ];
                }
            } else {
                return [
                    'status' => '002',
                    'message' => 'Token incorrect.',
                ];
            }
        } else {
            return [
                'status' => '002',
                'message' => 'Aucune donnée reçue en POST.',
            ];
        }
        exit;
    }

    //Cette méthode nous permet de supprimer / desactiver / activer une salle de cours d'un centre
    public function actionDeletesalle()
    {
        $token = Utils::getApiKey();
        $msg = "";
        if (Yii::$app->request->post()) {
            if (isset($_POST['token']) && $_POST['token'] == $token) {
                if (isset($_POST['user_auth_key']) && $_POST['user_auth_key'] != '') {
                    $user_auth_key = $_POST['user_auth_key'];
                    $user_exist = User::find()->where(['auth_key' => $user_auth_key, 'status' => 10])->one();
                    if ($user_exist != '') {

                        if (isset($_POST['id_center']) && $_POST['id_center'] != '') {
                            $id_center = $_POST['id_center'];

                            $center = SchoolCenter::find()->where(['id_center' => $id_center, 'etat' => [1, 2, 4]])->one();
                            if ($center != '') {
                                $cont = false;
                                $user_id_center = str_replace('-', '', $user_exist->id_center);
                                $test_center = explode(",", $user_id_center);

                                if ($user_exist->id_center == "1") {
                                    $cont = true;
                                } else if (in_array($id_center, $test_center)) {
                                    $cont = true;
                                }

                                if ($cont == true) {

                                    $manage_salle = Utils::have_accessapi("manage_salle", $user_exist->idP, $user_exist->id, $id_center);
                                    $test = explode('_', $manage_salle);

                                    if ($test[3] == '1') { // INDICE : 0 pour CREATE; 1 pour READ; 2 pour UPDATE; 3 pour DELETE;

                                        if (isset($_POST['operation']) && $_POST['operation'] != '') {
                                            $operation = $_POST['operation'];
                                            if (isset($_POST['salle_key']) && $_POST['salle_key'] != '') {
                                                $salle_key = $_POST['salle_key'];
                                                $old_record = Salle::find()->where(['idcentre'=>$center->id_center,'keysalle' => $salle_key])->one();
                                                if ($old_record != '') {

                                                    if ($operation == 3) { // SUPPRESSION
                                                        $old_record->status = 3;
                                                        $msg = "delete_salle_success";
                                                    } else if ($operation == 2) { // DESACTIVATION
                                                        $old_record->status = 2;
                                                        $msg = "desactive_salle_success";
                                                    } else if ($operation == 1) { // ACTIVATION
                                                        $old_record->status = 1;
                                                        $msg = "active_salle_success";
                                                    } else {
                                                        return [
                                                            'status' => '002',
                                                            'message' => 'Type d\'opération non reconnu',
                                                        ];
                                                    }

                                                    $old_record->updated_at = time();
                                                    $old_record->updated_by = $user_exist->id;
                                                    if ($old_record->save()) {
                                                        $data = Salle::find()->where(['idcentre'=>$center->id_center,'status' => [1, 2]])->orderBy(['libelle' => SORT_ASC])->all();
                                                        return [
                                                            'status' => '000',
                                                            'message' => $msg,
                                                            'data' => $data,
                                                        ];
                                                    } else {
                                                        return [
                                                            'status' => '002',
                                                            'message' => 'update_error',
                                                        ];
                                                    }
                                                } else {
                                                    return [
                                                        'status' => '001',
                                                        'message' => 'salle_not_found',
                                                    ];
                                                }
                                            } else {
                                                return [
                                                    'status' => '001',
                                                    'message' => 'salle_key_empty',
                                                ];
                                            }
                                        } else {
                                            return [
                                                'status' => '002',
                                                'message' => 'Le type d\'opération ne doit pas être vide.',
                                            ];
                                        }
                                    } else {
                                        return [
                                            'status' => '002',
                                            'message' => 'Accès non autorisé.',
                                        ];
                                    }
                                } else {
                                    return [
                                        'status' => '002',
                                        'message' => 'Cet utilisateur n\'a pas accès à ce centre.',
                                    ];
                                }
                            } else {
                                return [
                                    'status' => '002',
                                    'message' => 'id_center est incorrect.',
                                ];
                            }
                        } else {
                            return [
                                'status' => '002',
                                'message' => 'id_center ne doit pas être vide.',
                            ];
                        }
                    } else {
                        return [
                            'status' => '002',
                            'message' => 'Utilisateur non trouvé.',
                        ];
                    }
                } else {
                    return [
                        'status' => '002',
                        'message' => 'user_auth_key ne doit pas être vide.',
                    ];
                }
            } else {
                return [
                    'status' => '002',
                    'message' => 'Token incorrect.',
                ];
            }
        } else {
            return [
                'status' => '002',
                'message' => 'Aucune donnée reçue en POST.',
            ];
        }
        exit;
    }
}
