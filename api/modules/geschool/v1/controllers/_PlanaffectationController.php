<?php

namespace api\modules\geschool\v1\controllers;

use api\modules\geschool\v1\models\Mission;
use api\modules\geschool\v1\models\Personnel;
use api\modules\geschool\v1\models\Planaffectation;
use api\modules\geschool\v1\models\SchoolCenter;
use api\modules\geschool\v1\models\User;
use DateTime;
use Yii;
use yii\rest\ActiveController;

/**
 * Geschool Controller API
 *
 */
class PlanaffectationController extends ActiveController
{
	public $modelClass = 'api\modules\geschool\v1\models\Planaffectation';

	#000 -> Tout est bien
	#001 -> Message client
	#002 -> Message dev

	//Cette méthode nous permet de récupérer les plans d'affectation d'un personnel
	public function actionPlanaffectations()
	{
		$token = Utils::getApiKey();
		if (Yii::$app->request->post()) {
			if (isset($_POST['token']) && $_POST['token'] == $token) {
				if (isset($_POST['user_auth_key']) && $_POST['user_auth_key'] != '') {
					$user_auth_key = $_POST['user_auth_key'];
					$user_exist = User::find()->where(['auth_key' => $user_auth_key, 'status' => 10])->one();
					if ($user_exist != '') {

						if (isset($_POST['id_center']) && $_POST['id_center'] != '') {
							$id_center = $_POST['id_center'];

							$center = SchoolCenter::find()->where(['id_center' => $id_center, 'etat' => [1, 2, 4]])->one();
							if ($center != '') {
								$cont = false;
								$user_id_center = str_replace('-', '', $user_exist->id_center);
								$test_center = explode(",", $user_id_center);

								if ($user_exist->id_center == "1") {
									$cont = true;
								} else if (in_array($id_center, $test_center)) {
									$cont = true;
								}

								if ($cont == true) {

									$manage_planaffectation = Utils::have_accessapi("manage_planaffectation", $user_exist->idP, $user_exist->id, $id_center);
									$test = explode('_', $manage_planaffectation);
									if ($test[1] == '1') { // INDICE : 0 pour CREATE; 1 pour READ; 2 pour UPDATE; 3 pour DELETE;
										if (isset($_POST['iduser']) && $_POST['iduser'] != '') {
											$iduser = $_POST['iduser'];

											$find_personnel = Personnel::find()->where(['iduser' => $iduser])->one();
											if ($find_personnel !== null) {
												//Les plans d'affectation du personnel
												$data = Planaffectation::find()
													->where([
														'status' => 1,
														'iduser'=>$find_personnel->iduser
													])
													->orderBy(['created_at' => SORT_ASC])->all();
												if (sizeof($data) > 0) {
													return [
														'status' => '000',
														'message' => 'succes_get_plan_affectation.',
														'data' => $data
													];
												} else {
													return [
														'status' => '001',
														'message' => 'empty_liste_plan_affectation',
													];
												}

											} else {
												return [
													'status' => '002',
													'message' => 'personnel_not_found',
												];
											}
										} else {
											return [
												'status' => '002',
												'message' => 'personnel_key_empty',
											];
										}

									} else {
										return [
											'status' => '002',
											'message' => 'Accès non autorisé.',
										];
									}
								} else {
									return [
										'status' => '002',
										'message' => 'Cet utilisateur n\'a pas accès à ce centre.',
									];
								}
							} else {
								return [
									'status' => '002',
									'message' => 'id_center est incorrect.',
								];
							}
						} else {
							return [
								'status' => '002',
								'message' => 'id_center ne doit pas être vide.',
							];
						}
					} else {
						return [
							'status' => '002',
							'message' => 'Utilisateur non trouvé.',
						];
					}
				} else {
					return [
						'status' => '002',
						'message' => 'user_auth_key ne doit pas être vide.',
					];
				}
			} else {
				return [
					'status' => '002',
					'message' => 'Token incorrect.',
				];
			}
		} else {
			return [
				'status' => '002',
				'message' => 'Aucune donnée reçue en POST.',
			];
		}
		exit;
	}

	//Cette méthode nous permet d'obtenir un plan d'affectation
	public function actionPlanaffectation()
	{
		$token = Utils::getApiKey();
		if (Yii::$app->request->post()) {
			if (isset($_POST['token']) && $_POST['token'] == $token) {
				if (isset($_POST['user_auth_key']) && $_POST['user_auth_key'] != '') {
					$user_auth_key = $_POST['user_auth_key'];
					$user_exist = User::find()->where(['auth_key' => $user_auth_key, 'status' => 10])->one();
					if ($user_exist != '') {

						if (isset($_POST['id_center']) && $_POST['id_center'] != '') {
							$id_center = $_POST['id_center'];

							$center = SchoolCenter::find()->where(['id_center' => $id_center, 'etat' => [1, 2, 4]])->one();
							if ($center != '') {
								$cont = false;
								$user_id_center = str_replace('-', '', $user_exist->id_center);
								$test_center = explode(",", $user_id_center);

								if ($user_exist->id_center == "1") {
									$cont = true;
								} else if (in_array($id_center, $test_center)) {
									$cont = true;
								}

								if ($cont == true) {

									$manage_planaffectation = Utils::have_accessapi("manage_planaffectation", $user_exist->idP, $user_exist->id, $id_center);
									$test = explode('_', $manage_planaffectation);
									if ($test[2] == '1') { // INDICE : 0 pour CREATE; 1 pour READ; 2 pour UPDATE; 3 pour DELETE;
										if (isset($_POST['planaffectation_key']) && $_POST['planaffectation_key'] != '') {
											$planaffectation_key = $_POST['planaffectation_key'];
											$planaffectation_exist = Planaffectation::find()->where(['keyplanaffectation' => $planaffectation_key])->one();
											if ($planaffectation_exist !== null) {
												return [
													'status' => '000',
													'message' => 'succes_get_plan_affectation',
													'data' => $planaffectation_exist,
												];
											} else {
												return [
													'status' => '001',
													'message' => 'plan_affectation_not_found',
												];
											}
										} else {
											return [
												'status' => '001',
												'message' => 'plan_affectation_key_empty',
											];
										}
									} else {
										return [
											'status' => '002',
											'message' => 'Accès non autorisé.',
										];
									}
								} else {
									return [
										'status' => '002',
										'message' => 'Cet utilisateur n\'a pas accès à ce centre.',
									];
								}
							} else {
								return [
									'status' => '002',
									'message' => 'id_center est incorrect.',
								];
							}
						} else {
							return [
								'status' => '002',
								'message' => 'id_center ne doit pas être vide.',
							];
						}
					} else {
						return [
							'status' => '002',
							'message' => 'Utilisateur non trouvé.',
						];
					}
				} else {
					return [
						'status' => '002',
						'message' => 'user_auth_key ne doit pas être vide.',
					];
				}
			} else {
				return [
					'status' => '002',
					'message' => 'Token incorrect.',
				];
			}
		} else {
			return [
				'status' => '002',
				'message' => 'Aucune donnée reçue en POST.',
			];
		}
		exit;
	}

	//Cette méthode permet d'ajouter un plan d'affectation pour un personnel
	public function actionAddplanaffectation()
	{
		$token = Utils::getApiKey();
		$motif = "";
		$itineraire_retenu = "";
		if (Yii::$app->request->post()) {
			if (isset($_POST['token']) && $_POST['token'] == $token) {
				if (isset($_POST['user_auth_key']) && $_POST['user_auth_key'] != '') {
					$user_auth_key = $_POST['user_auth_key'];
					$user_exist = User::find()->where(['auth_key' => $user_auth_key, 'status' => 10])->one();
					if ($user_exist != '') {

						if (isset($_POST['id_center']) && $_POST['id_center'] != '') {
							$id_center = $_POST['id_center'];

							$center = SchoolCenter::find()->where(['id_center' => $id_center, 'etat' => [1, 2, 4]])->one();
							if ($center != '') {
								$cont = false;
								$user_id_center = str_replace('-', '', $user_exist->id_center);
								$test_center = explode(",", $user_id_center);

								if ($user_exist->id_center == "1") {
									$cont = true;
								} else if (in_array($id_center, $test_center)) {
									$cont = true;
								}

								if ($cont == true) {

									if (isset($_POST['centreaffectation_key']) && $_POST['centreaffectation_key'] != '') {
										$centreaffectation_key = $_POST['centreaffectation_key'];

										$find_center = SchoolCenter::find()->where(['key_center' => $centreaffectation_key])->one();
										if ($find_center !==null) {

											if (isset($_POST['datedebut']) && $_POST['datedebut'] != '') {
												$datedebutaffectation = $_POST['datedebut'];
												if (isset($_POST['datefin']) && $_POST['datefin'] != '') {
													$datefinaffectation = $_POST['datefin'];

													if (strtotime($datedebutaffectation) >strtotime($datefinaffectation) ) {
														return [
															'status' => '001',
															'message' => 'plan_affectation_date_debut_sup_retour',
														];
													} else {

														if (isset($_POST['operation']) && $_POST['operation'] != '') {
															$operation = $_POST['operation'];
															$manage_planaffectation = Utils::have_accessapi("manage_planaffectation", $user_exist->idP, $user_exist->id, $centreaffectation_key);
															$test = explode('_', $manage_planaffectation);
															if ($operation == 1) { // AJOUT

																if ($test[0] == '1') { // INDICE : 0 pour CREATE; 1 pour READ; 2 pour UPDATE; 3 pour DELETE;
																	//On vérifie si le personnel est renseigné
																	if (isset($_POST['useraffectation_id']) && $_POST['useraffectation_id'] != '') {
																		$useraffectation_id = $_POST['useraffectation_id'];

																		$find_personnel = Personnel::find()->where(['iduser' => $useraffectation_id])->one();
																		if ($find_personnel !== null) {
																			//A revoir concordance des données
																			//On enrégistre
																			$new_record = new Planaffectation();

																			$new_record->keyplanaffectation = Yii::$app->security->generateRandomString(32);
																			$new_record->iduser= $find_personnel->iduser;
																			$new_record->idcenter=$find_center->id_center;
																			$new_record->datedebutaffectation=$datedebutaffectation;
																			$new_record->datefinaffectation= $datefinaffectation;
																			$new_record->status= 1;
																			$new_record->created_at= date('Y-m-d H:i:s');
																			$new_record->created_by= $user_exist->id;
																			if($new_record->save()){
																				$data = Planaffectation::find()
																					->where([
																						'status' => 1,
																						'iduser'=>$find_personnel->iduser
																					])
																					->orderBy(['created_at' => SORT_ASC])->all();
																				return [
																					'status' => '000',
																					'message' => 'succes_creation_plan_affectation',
																					'data' => $data,
																				];
																			}else{
																				return [
																					'status' => '002',
																					'message' => 'update_error',
																				];
																			}

																		} else {
																			return [
																				'status' => '002',
																				'message' => 'personnel_not_found',
																			];
																		}
																	} else {
																		return [
																			'status' => '002',
																			'message' => 'personnel_key_empty',
																		];
																	}


																} else {
																	return [
																		'status' => '002',
																		'message' => 'Accès non autorisé.',
																	];
																}
															} else if ($operation == 2) { // MODIFICATION

																if ($test[2] == '1') { // INDICE : 0 pour CREATE; 1 pour READ; 2 pour UPDATE; 3 pour DELETE;
																	if (isset($_POST['planaffectation_key']) && $_POST['planaffectation_key'] != '') {
																		$planaffectation_key = $_POST['planaffectation_key'];
																		$old_record = Planaffectation::find()->where(['keyplanaffectation' => $planaffectation_key, 'status' => 1])->one();
																		if ($old_record !== null) {
																			$old_record->idcenter=$find_center->id_center;
																			$old_record->datedebutaffectation=$datedebutaffectation;
																			$old_record->datefinaffectation= $datefinaffectation;
																			$old_record->updated_at = date('Y-m-d H:i:s');
																			$old_record->updated_by = $user_exist->id;
																			if (isset($_POST['datedebuteffective']) && $_POST['datedebuteffective'] != '')
																				$old_record->datedebuteffective = $_POST['datedebuteffective'];

																			if (isset($_POST['datedfineffective']) && $_POST['datedfineffective'] != '')
																				$old_record->datedfineffective = $_POST['datedfineffective'];

																			if ($old_record->save()) {
																				$data = Planaffectation::find()
																					->where([
																						'status' => 1,
																						'iduser'=>$old_record->iduser
																					])
																					->orderBy(['created_at' => SORT_ASC])->all();																				return [
																					'status' => '000',
																					'message' => 'succes_update_plan_affectation',
																					'data' => $data,
																				];
																			} else {
																				return [
																					'status' => '002',
																					'message' => 'update_error',
																				];
																			}
																		} else {
																			return [
																				'status' => '001',
																				'message' => 'plan_affectation_not_found',
																			];
																		}
																	} else {
																		return [
																			'status' => '001',
																			'message' => 'plan_affectation_key_empty',
																		];
																	}
																} else {
																	return [
																		'status' => '002',
																		'message' => 'Accès non autorisé.',
																	];
																}
															} else {
																return [
																	'status' => '002',
																	'message' => 'Type d\'opération non reconnu.',
																];
															}
														} else {
															return [
																'status' => '002',
																'message' => 'Le type d\'opération ne doit pas être vide.',
															];
														}
													}
												} else {
													return [
														'status' => '001',
														'message' => 'plan_affectation_date_fin_empty.',
													];
												}
											} else {
												return [
													'status' => '001',
													'message' => 'plan_affectation_date_debut_empty',
												];
											}


										} else {
											return [
												'status' => '001',
												'message' => 'centre_affectation_not_found',
											];
										}
									} else {
										return [
											'status' => '001',
											'message' => 'centre_affectation_key_empty',
										];
									}


								} else {
									return [
										'status' => '001',
										'message' => 'Cet utilisateur n\'a pas accès à ce centre.',
									];
								}
							} else {
								return [
									'status' => '002',
									'message' => 'id_center est incorrect.',
								];
							}
						} else {
							return [
								'status' => '002',
								'message' => 'id_center ne doit pas être vide.',
							];
						}
					} else {
						return [
							'status' => '002',
							'message' => 'Utilisateur non trouvé.',
						];
					}
				} else {
					return [
						'status' => '002',
						'message' => 'user_auth_key ne doit pas être vide.',
					];
				}
			} else {
				return [
					'status' => '002',
					'message' => 'Token incorrect.',
				];
			}
		} else {
			return [
				'status' => '002',
				'message' => 'Aucune donnée reçue en POST.',
			];
		}
		exit;
	}

	//Cette méthode permet de supprimer un plan d'affectation pour un personnel
	public function actionDeleteplanaffectation()
	{
		$token =  Utils::getApiKey();
		if (Yii::$app->request->post()) {
			if (isset($_POST['token']) && $_POST['token'] == $token) {
				if (isset($_POST['user_auth_key']) && $_POST['user_auth_key'] != '') {
					$user_auth_key = $_POST['user_auth_key'];
					$user_exist = User::find()->where(['auth_key' => $user_auth_key, 'status' => 10])->one();
					if ($user_exist != '') {

						if (isset($_POST['id_center']) && $_POST['id_center'] != '') {
							$id_center = $_POST['id_center'];

							$center = SchoolCenter::find()->where(['id_center' => $id_center, 'etat' => [1, 2, 4]])->one();
							if ($center != '') {
								$cont = false;
								$user_id_center = str_replace('-', '', $user_exist->id_center);
								$test_center = explode(",", $user_id_center);

								if ($user_exist->id_center == "1") {
									$cont = true;
								} else if (in_array($id_center, $test_center)) {
									$cont = true;
								}

								if ($cont == true) {

									$manage_planaffectation = Utils::have_accessapi("manage_planaffectation", $user_exist->idP, $user_exist->id, $id_center);
									$test = explode('_', $manage_planaffectation);
									if ($test[3] == '1') { // INDICE : 0 pour CREATE; 1 pour READ; 2 pour UPDATE; 3 pour DELETE;
										//if (isset($_POST['operation']) && $_POST['operation'] != '') {
											//$operation = $_POST['operation'];
											if (isset($_POST['planaffectation_key']) && $_POST['planaffectation_key'] != '') {
												$planaffectation_key = $_POST['planaffectation_key'];
												$old_record = Planaffectation::find()->where(['keyplanaffectation' => $planaffectation_key])->one();
												if ($old_record !== null) {
													//if ($operation == 3) { // SUPPRESSION
														$old_record->status = 3;
														$msg = "delete_plan_affectation_success";
													/*} else {
														return [
															'status' => '002',
															'message' => 'Type d\'opération non reconnu',
														];
													}*/
													$old_record->updated_at = date('Y-m-d H:i:s');
													$old_record->updated_by = $user_exist->id;
													if ($old_record->save()) {
														return [
															'status' => '000',
															'message' => $msg,
														];
													} else {
														return [
															'status' => '002',
															'message' => 'update_error',
														];
													}
												} else {
													return [
														'status' => '001',
														'message' => 'plan_affectation_not_found',
													];
												}
											} else {
												return [
													'status' => '001',
													'message' => 'plan_affectation_key_empty',
												];
											}
										/*} else {
											return [
												'status' => '002',
												'message' => 'Le type d\'opération ne doit pas être vide.',
											];
										}*/
									} else {
										return [
											'status' => '002',
											'message' => 'Accès non autorisé.',
										];
									}
								} else {
									return [
										'status' => '002',
										'message' => 'Cet utilisateur n\'a pas accès à ce centre.',
									];
								}
							} else {
								return [
									'status' => '002',
									'message' => 'id_center est incorrect.',
								];
							}
						} else {
							return [
								'status' => '002',
								'message' => 'id_center ne doit pas être vide.',
							];
						}
					} else {
						return [
							'status' => '002',
							'message' => 'Utilisateur non trouvé.',
						];
					}
				} else {
					return [
						'status' => '002',
						'message' => 'user_auth_key ne doit pas être vide.',
					];
				}
			} else {
				return [
					'status' => '002',
					'message' => 'Token incorrect.',
				];
			}
		} else {
			return [
				'status' => '002',
				'message' => 'Aucune donnée reçue en POST.',
			];
		}
		exit;
	}






}
