<?php

namespace api\modules\geschool\v1\models;

use Yii;

/**
 * This is the model class for table "ressource".
 *
 * @property integer $id
 * @property integer $idcentre
 * @property string $designation
 * @property string $keyressource
 * @property string $coderessource
 * @property integer $ididetatusage
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $create_by
 * @property integer $updated_by
 */
class Ressource extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ressource';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idcentre', 'designation', 'keyressource', 'idetatusage', 'created_at', 'create_by'], 'required'],
            [['idcentre', 'idetatusage', 'status', 'created_at', 'updated_at', 'create_by', 'updated_by'], 'integer'],
            [['designation'], 'string', 'max' => 254],
            [['keyressource'], 'string', 'max' => 32],
            [['coderessource'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idcentre' => 'Idcentre',
            'designation' => 'Designation',
            'keyressource' => 'Keyressource',
            'coderessource' => 'Coderessource',
            'idetatusage' => 'Etatusage',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'create_by' => 'Create By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCentre()
    {
        return $this->hasOne(SchoolCenter::className(), ['id_center' => 'idcenter']);
    }
}
