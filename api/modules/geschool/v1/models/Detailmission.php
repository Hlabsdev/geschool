<?php

namespace api\modules\geschool\v1\models;

use Yii;

/**
 * This is the model class for table "detailmission".
 *
 * @property string $id
 * @property string $idmission
 * @property integer $iduser
 * @property string $keydetailmission
 * @property string $frais
 * @property string $contactabsence
 * @property string $dateretoureffmission
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $create_by
 * @property integer $updated_by
 */
class Detailmission extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'detailmission';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idmission', 'iduser', 'created_at', 'create_by'], 'required'],
            [['idmission', 'iduser', 'status', 'created_at', 'updated_at', 'create_by', 'updated_by'], 'integer'],
            [['frais'], 'number'],
            [['dateretoureffmission'], 'safe'],
            [['keydetailmission'], 'string', 'max' => 32],
            [['contactabsence'], 'string', 'max' => 254],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idmission' => 'Idmission',
            'iduser' => 'Iduser',
            'keydetailmission' => 'Keydetailmission',
            'frais' => 'Frais',
            'contactabsence' => 'Contactabsence',
            'dateretoureffmission' => 'Dateretoureffmission',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'create_by' => 'Create By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMission()
    {
        return $this->hasOne(Mission::className(), ['id' => 'idmission']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPersonnel()
    {
        return $this->hasOne(Personnel::className(), ['id' => 'iduser']);
    }
}
