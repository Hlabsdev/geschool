<?php

namespace api\modules\geschool\v1\models;

use Yii;

/**
 * This is the model class for table "salle".
 *
 * @property integer $id
 * @property integer $idcentre
 * @property string $libelle
 * @property string $keysalle
 * @property integer $nbreplacemaxi
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $create_by
 * @property integer $updated_by
 */
class Salle extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'salle';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idcentre', 'created_at', 'create_by'], 'required'],
            [['idcentre', 'nbreplacemaxi', 'status', 'created_at', 'updated_at', 'create_by', 'updated_by'], 'integer'],
            [['libelle'], 'string', 'max' => 50],
            [['keysalle'], 'string', 'max' => 32],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idcentre' => 'Idcentre',
            'libelle' => 'Libelle',
            'keysalle' => 'Keysalle',
            'nbreplacemaxi' => 'Nbreplacemaxi',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'create_by' => 'Create By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCentre()
    {
        return $this->hasOne(SchoolCenter::className(), ['id_center' => 'idcenter']);
    }
}
