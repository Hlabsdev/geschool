<?php

namespace api\modules\geschool\v1\models;

use Yii;

/**
 * This is the model class for table "etatusage".
 *
 * @property integer $id
 * @property string $libelle
 * @property string $keyetatusage
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $create_by
 * @property integer $updated_by
 * @property integer $idcentre
 *
 *@property Ressource[] $ressources
 */
class Etatusage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'etatusage';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status', 'created_at', 'updated_at', 'create_by', 'updated_by'], 'integer'],
            [['created_at', 'create_by'], 'required'],
            [['libelle'], 'string', 'max' => 254],
            [['keyetatusage'], 'string', 'max' => 32],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'libelle' => 'Libelle',
            'keyetatusage' => 'Keyetatusage',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'create_by' => 'Create By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRessources()
    {
        return $this->hasMany(Ressource::className(), ['idetatusage' => 'id']);
    }
}
