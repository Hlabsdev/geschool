<?php

namespace api\modules\geschool\v1\models;

use Yii;

/**
 * This is the model class for table "cours".
 *
 * @property integer $id
 * @property string $designation
 * @property string $keycours
 * @property integer $idtypecours
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $create_by
 * @property integer $updated_by
 *
 * @property Classecours[] $classecours
 * @property Courscentreclasse[] $courscentreclasses
 */
class Cours extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cours';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['designation', 'keycours', 'idtypecours', 'created_at', 'create_by'], 'required'],
            [['status', 'created_at', 'updated_at', 'create_by', 'updated_by', 'idtypecours'], 'integer'],
            [['designation'], 'string', 'max' => 254],
            [['keycours'], 'string', 'max' => 32],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'designation' => 'Designation',
            'keycours' => 'Keycours',
            'idtypecours' => 'Typecours',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'create_by' => 'Create By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTypecours()
    {
        return $this->hasOne(Typecours::className(), ['id' => 'idtypecours']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClassecours()
    {
        return $this->hasMany(Classecours::className(), ['idcours' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourscentreclasses()
    {
        return $this->hasMany(Courscentreclasse::className(), ['idcours' => 'id']);
    }
}
