<?php

namespace api\modules\geschool\v1\models;

use Yii;

/**
 * This is the model class for table "sectionformation".
 *
 * @property integer $id
 * @property string $keysectionformation
 * @property integer $idsection
 * @property integer $idtypeformation
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $create_by
 * @property integer $updated_by
 */
class Sectionformation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sectionformation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idsection', 'idtypeformation', 'created_at', 'create_by'], 'required'],
            [['idsection', 'idtypeformation', 'status', 'created_at', 'updated_at', 'create_by', 'updated_by'], 'integer'],
            [['keysectionformation'], 'string', 'max' => 32],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'keysectionformation' => 'Keysectionformation',
            'idsection' => 'Idsection',
            'idtypeformation' => 'Idtypeformation',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'create_by' => 'Create By',
            'updated_by' => 'Updated By',
        ];
    }
}
