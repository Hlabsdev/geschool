<?php

namespace api\modules\geschool\v1\models;

use Yii;

/**
 * This is the model class for table "school_center".
 *
 * @property integer $id_center
 * @property string $key_center
 * @property string $denomination_center
 * @property string $localite_center
 * @property integer $sms_disponible
 * @property integer $idprefecture
 * @property string $nomcourt
 * @property string $codecentre
 * @property string $telephonecentre
 * @property string $emailcentre
 * @property string $sitewebcentre
 * @property string $reseausociauxcentre
 * @property string $inspection
 * @property string $lienlogo
 * @property double $coordonnegpsx
 * @property double $coordonnegpsy
 * @property integer $etat
 * @property string $date_create
 * @property integer $created_by
 * @property string $date_update
 * @property integer $updated_by
 *
 * @property Centreannee[] $centreannees
 * @property Centreclasse[] $centreclasses
 * @property Centresection[] $centresections
 * @property Courscentre[] $courscentres
 * @property Prefecture $idprefecture0
 * @property UserProfil[] $userProfils
 */
class SchoolCenter extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'school_center';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['key_center', 'denomination_center', 'localite_center', 'etat', 'created_by'], 'required'],
            [['sms_disponible', 'idprefecture', 'etat', 'created_by', 'updated_by'], 'integer'],
            [['reseausociauxcentre', 'inspection'], 'string'],
            [['coordonnegpsx', 'coordonnegpsy'], 'number'],
            [['date_create', 'date_update'], 'safe'],
            [['key_center'], 'string', 'max' => 50],
            [['denomination_center', 'localite_center'], 'string', 'max' => 255],
            [['nomcourt'], 'string', 'max' => 25],
            [['codecentre'], 'string', 'max' => 10],
            [['telephonecentre'], 'string', 'max' => 11],
            [['emailcentre', 'sitewebcentre', 'lienlogo'], 'string', 'max' => 254],
            [['idprefecture'], 'exist', 'skipOnError' => true, 'targetClass' => Prefecture::className(), 'targetAttribute' => ['idprefecture' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_center' => 'Id Center',
            'key_center' => 'Key Center',
            'denomination_center' => 'Denomination Center',
            'localite_center' => 'Localite Center',
            'sms_disponible' => 'Sms Disponible',
            'idprefecture' => 'Idprefecture',
            'nomcourt' => 'Nomcourt',
            'codecentre' => 'Codecentre',
            'telephonecentre' => 'Telephonecentre',
            'emailcentre' => 'Emailcentre',
            'sitewebcentre' => 'Sitewebcentre',
            'reseausociauxcentre' => 'Reseausociauxcentre',
            'inspection' => 'Inspection',
            'lienlogo' => 'Lienlogo',
            'coordonnegpsx' => 'Coordonnegpsx',
            'coordonnegpsy' => 'Coordonnegpsy',
            'etat' => 'Etat',
            'date_create' => 'Date Create',
            'created_by' => 'Created By',
            'date_update' => 'Date Update',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCentreannees()
    {
        return $this->hasMany(Centreannee::className(), ['idcentre' => 'id_center']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCentreclasses()
    {
        return $this->hasMany(Centreclasse::className(), ['idcentre' => 'id_center']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCentresections()
    {
        return $this->hasMany(Centresection::className(), ['idcentre' => 'id_center']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourscentres()
    {
        return $this->hasMany(Courscentre::className(), ['idcentre' => 'id_center']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdprefecture0()
    {
        return $this->hasOne(Prefecture::className(), ['id' => 'idprefecture']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserProfils()
    {
        return $this->hasMany(UserProfil::className(), ['id_center' => 'id_center']);
    }
}
