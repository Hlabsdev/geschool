<?php

namespace api\modules\geschool\v1\models;

use Yii;

/**
 * This is the model class for table "typepersonnel".
 *
 * @property integer $id
 * @property string $libelle
 * @property string $keytypepersonnel
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $create_by
 * @property integer $updated_by
 *
 * @property User $createBy
 * @property User $updatedBy
 */
class Typepersonnel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'typepersonnel';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['libelle', 'keytypepersonnel', 'created_at', 'create_by'], 'required'],
            [['status', 'created_at', 'updated_at', 'create_by', 'updated_by'], 'integer'],
            [['libelle'], 'string', 'max' => 50],
            [['keytypepersonnel'], 'string', 'max' => 32],
            [['create_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['create_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'libelle' => 'Libelle',
            'keytypepersonnel' => 'Keytypepersonnel',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'create_by' => 'Create By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreateBy()
    {
        return $this->hasOne(User::className(), ['id' => 'create_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }
}
