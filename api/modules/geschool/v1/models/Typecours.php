<?php

namespace api\modules\geschool\v1\models;

use Yii;

/**
 * This is the model class for table "typecours".
 *
 * @property integer $id
 * @property string $libelletypecours
 * @property string $keytypecours
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $create_by
 * @property integer $updated_by
 */
class Typecours extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'typecours';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status', 'created_at', 'updated_at', 'create_by', 'updated_by'], 'integer'],
            [['created_at', 'create_by'], 'required'],
            [['libelletypecours'], 'string', 'max' => 254],
            [['keytypecours'], 'string', 'max' => 32],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'libelletypecours' => 'Libelletypecours',
            'keytypecours' => 'Keytypecours',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'create_by' => 'Create By',
            'updated_by' => 'Updated By',
        ];
    }
}
