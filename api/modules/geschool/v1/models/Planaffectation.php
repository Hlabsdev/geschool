<?php

namespace api\modules\geschool\v1\models;

use Yii;

/**
 * This is the model class for table "planaffectation".
 *
 * @property integer $id
 * @property string $keyplanaffectation
 * @property integer $iduser
 * @property integer $idcenter
 * @property string $datedebutaffectation
 * @property string $datedebuteffective
 * @property string $datefinaffectation
 * @property string $datedfineffective
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 */
class Planaffectation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'planaffectation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['keyplanaffectation', 'iduser', 'idcenter', 'datedebutaffectation', 'datefinaffectation', 'created_by'], 'required'],
            [['iduser', 'idcenter', 'status', 'created_by', 'updated_by'], 'integer'],
            [['datedebutaffectation', 'datedebuteffective', 'datefinaffectation', 'datedfineffective', 'created_at', 'updated_at'], 'safe'],
            [['keyplanaffectation'], 'string', 'max' => 32],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'keyplanaffectation' => 'keyplanaffectation',
            'iduser' => 'Iduser',
            'idcenter' => 'Idcenter',
            'datedebutaffectation' => 'Datedebutaffectation',
            'datedebuteffective' => 'Datedebuteffective',
            'datefinaffectation' => 'Datefinaffectation',
            'datedfineffective' => 'Datedfineffective',
            'status' => 'Status',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Personnel::className(), ['iduser' => 'iduser']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCentre()
    {
        return $this->hasOne(SchoolCenter::className(), ['id_center' => 'idcenter']);
    }
}
