<?php

namespace api\modules\geschool\v1\models;

use Yii;

/**
 * This is the model class for table "parametrage".
 *
 * @property integer $id
 * @property integer $idanneescolaire
 * @property integer $idcentreclasse
 * @property double $moyennepassage
 * @property double $moyenneexclusion
 * @property integer $titulaire
 * @property integer $titulaireadjoint
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $create_by
 * @property integer $updated_by
 */
class Parametrage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'parametrage';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idanneescolaire', 'idcentreclasse', 'created_at', 'create_by'], 'required'],
            [['idanneescolaire', 'idcentreclasse', 'titulaire', 'titulaireadjoint', 'status', 'created_at', 'updated_at', 'create_by', 'updated_by'], 'integer'],
            [['moyennepassage', 'moyenneexclusion'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idanneescolaire' => 'Idanneescolaire',
            'idcentreclasse' => 'Idcentreclasse',
            'moyennepassage' => 'Moyennepassage',
            'moyenneexclusion' => 'Moyenneexclusion',
            'titulaire' => 'Titulaire',
            'titulaireadjoint' => 'Titulaireadjoint',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'create_by' => 'Create By',
            'updated_by' => 'Updated By',
        ];
    }
}
