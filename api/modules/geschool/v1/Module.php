<?php

namespace api\modules\geschool\v1;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'api\modules\geschool\v1\controllers';
    public $modelNamespace = 'api\modules\geschool\v1\models';

    public function init()
    {
        parent::init();
    }
}
