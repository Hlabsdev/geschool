<?php

namespace api\modules\v1\controllers;

use yii\rest\ActiveController;

/**
 * Country Controller API
 *
 * @author Budi Irawan <deerawan@gmail.com>
 */
class CountryController extends ActiveController
{
    public $modelClass = 'api\modules\v1\models\Country';   
    
    
    public function actionCountries(){

        $someJSON = '[{"name":"Jonathan Suh","gender":"male"},{"name":"William Philbin","gender":"male"},{"name":"Allison McKinnery","gender":"female"}]';
        $someArray = json_decode($someJSON, true);

        foreach ($someArray as $key => $value) {
            echo $value["name"] . ", " . $value["gender"] . "<br>";
        }
    }
}


