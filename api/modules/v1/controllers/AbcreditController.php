<?php

namespace api\modules\v1\controllers;

use api\modules\v1\models\AbcAssuranceDeces;
use api\modules\v1\models\AbcAssurancePerte;
use api\modules\v1\models\AbcBanque;
use api\modules\v1\models\AbcCashout;
use api\modules\v1\models\AbcIncident;
use api\modules\v1\models\AbcRetrait;
use api\modules\v1\models\AbcSouscriptio;
use Yii;
use yii\rest\ActiveController;
use api\modules\v1\models\Information;
use api\modules\v1\models\AbcVente;
use api\modules\v1\models\Etiquette;
use api\modules\v1\models\AbcSouscription;
use api\modules\v1\models\AbcSouscriptionMarchand;
use api\modules\v1\models\AbcTypePaiement;
use api\modules\v1\models\AbcTypeSouscription as ModelsAbcTypeSouscription;
use api\modules\v1\models\ActeurUser as ModelsActeurUser;
use api\modules\v1\models\Carte;
// use api\modules\v1\controllers\AbcCarte;
use backend\modules\abcvente\models\AbcHistorique;
use backend\modules\abcvente\models\AbcTypeSouscription;
use backend\modules\acteurs\models\ActeurUser;
use backend\modules\acteurs\models\User;
use DateTime;
use Exception;
use backend\controllers\Utils;
use console\controllers\Common;
use yii\db\Query;
use yii\helpers\Json;

/**
 * Ussdtransation Controller API
 *
 */
class AbcreditController extends ActiveController
{
	public $modelClass = 'api\modules\v1\models\AbcVente';
	public $lien_whatsapp = "https://wa.me/22891047373?text=";
	public $lien_telegram = "https://t.me/abtogo_bot?start=";

	public function set($data, $param)
	{
		//print_r($data->$param);exit;
		if (isset($data[$param])) {
			return $data[$param];
		} else {
			return null;
		}
	}

	public function actionVerif_solde()
	{
		$token = "SzrQt6TazKv2ayDHqs754qvvAvD";
		if (Yii::$app->request->post()) {
			if (isset($_POST['token']) && $_POST['token'] == $token) {
				if (isset($_POST['telephone']) && $_POST['telephone'] != '') {
					$verif_exist = AbcSouscription::find()->where(['customer' => $_POST['telephone']])->one();
					if ($verif_exist != '') {
						return [
							'status' => 'ok',
							'message' => $verif_exist->solde,
						];
					} else {
						return [
							'status' => 'ko',
							'message' => 'Ce client n\'est pas souscrit à ABCrédit',
						];
					}
				} else {
					return [
						'status' => 'ko',
						'message' => 'Telephone non reçu en post',
					];
				}
			} else {
				return [
					'status' => 'ko',
					'message' => 'Token incorrect',
				];
			}
		}
		exit;
	}

	public function actionPrepare_compte()
	{
		$token = "SzrQt6TazKv2aAvDyDHqs754qvv";
		if (Yii::$app->request->post()) {
			if (isset($_POST['token']) && $_POST['token'] == $token) {
				AbcSouscription::updateAll(['etat_reset' => 1], 'etat_reset = 0');
			}
		}
	}

	public function actionReset_compte()
	{
		$token = "SzrQt6TazKv2aAvDyDHqs754qvv";
		if (Yii::$app->request->post()) {
			if (isset($_POST['token']) && $_POST['token'] == $token) {

				$date_echeance = date('Y-m-1');
				$date_todays = date('Y-m-d');
				$firstDate  = new DateTime($date_echeance);
				$secondDate = new DateTime($date_todays);
				$intvl = $firstDate->diff($secondDate);
				if ($intvl->days == 0) {
					$all_data = AbcSouscription::find()->where(['etat_reset' => 1])->limit(100)->orderBy(['id_souscription' => SORT_ASC])->all();
					for ($i = 0; $i < sizeof($all_data); $i++) {

						$histo = new AbcHistorique;
						$histo->key_historique = Yii::$app->security->generateRandomString(32);
						$histo->mois_operation = date('Y-m');
						$histo->montant = $all_data[$i]->type_souscription->solde;
						$histo->montant_operation = 0;
						$histo->id_souscription = $all_data[$i]->id_souscription;

						if ($histo->save()) {
							// $typ = AbcTypeSouscription::find()->where(['id' => $all_data[$i]->type_souscription])->one();
							$all_data[$i]->solde_disponible = $all_data[$i]->type_souscription->solde;
							$all_data[$i]->etat_reset = 0;
							$all_data[$i]->save();
						}
					}
				}
			} else {
				return [
					'status' => 'ko',
					'message' => 'Token incorrect',
				];
			}
		}
		exit;
	}

	public function actionGet_data()
	{
		if (yii::$app->request->post()) {
			$params = Yii::$app->request->post();
			//10 = Commerces
			//20 = Taxes collecte
			//30 = Controle taxe
			if (isset($params['form_type'])) {
				$form_type = $params['form_type'];
				if (($form_type != "") && ($form_type != null)) {
					return $this->get_form_data($form_type);
				} else {
					return [
						'status' => "006",
						'message' => "Type de formulaire non reconnu",
					];
				}
			} else {
				return [
					'status' => "006",
					'message' => "Type du formulaire non reçu",
				];
			}
		}
		exit;
	}

	public function get_form_data($form_type)
	{
		set_time_limit(0);
		date_default_timezone_set('UTC');
		$nbre_show = 200;
		$default_url = 'https://kf.datawiseforcorridorbj.datawise.site';

		$formulaires_ids = [
			'100' => 'aawgNYi63TmoYTAnsq83bL',
		];


		if (array_key_exists($form_type, $formulaires_ids)) {
			$formId = $formulaires_ids[$form_type];
			$username = 'clinadmin';
			$password = 'clin@2021!';
			//$formId = 'apTRX4ezmEYKKXAtxYyKgE';

			// $old_limit = Information::find()->select(['count(idInformation) as idInformation'])->where(['type' => $form_type])->one();
			$old_limit = Information::find()->where(['type' => $form_type])->count();
			$old_limit = 0;

			// $begin = $old_limit->idInformation;
			$begin = $old_limit;
			$url_api = $default_url . "/assets/" . $formId . "/submissions.json?start=" . $begin . "&limit=" . $nbre_show;

			// print_r($url_api);exit;

			$curl = curl_init();
			curl_setopt_array($curl, array(
				CURLOPT_URL => $url_api,
				CURLOPT_ENCODING => "",
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 30,
				CURLOPT_SSL_VERIFYPEER => 0,
				CURLOPT_USERPWD => $username . ":" . $password,
				CURLOPT_USERAGENT => 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.17 (KHTML, like Gecko) Chrome/24.0.1312.52 Safari/537.17',
				CURLOPT_AUTOREFERER => true,
				CURLOPT_RETURNTRANSFER => 1,
				CURLOPT_FOLLOWLOCATION => 1,
				CURLOPT_VERBOSE => 1,
				CURLOPT_CUSTOMREQUEST => "GET",
			));

			$response = curl_exec($curl);
			$err = curl_error($curl);


			curl_close($curl);

			if ($err) {
				//echo "cURL Error #:" . $err;
				return "cURL Error #: false";
			} else {
				$datas = json_decode($response);
				$this->save_information($datas, $form_type);
				// print_r($response);
				// die;
			}
		} else {
			return [
				'status' => "006",
				'message' => "Le formulaire n'existe pas",
			];
		}
		exit;
	}

	public function save_information($datas, $type)
	{
		foreach ($datas as $data) {
			// print_r($data);exit;
			$information = new Information();
			$information->content = json_encode($data);
			$information->type = $type;
			$information->extract = 0;
			$information->dateRegistration = date('Y-m-d H:i:s');
			if (!$information->save()) {
				print_r($information->getErrors());
				exit;
			}
		}
		exit;
	}

	public function actionExtract()
	{
		if (yii::$app->request->post()) {
			$params = yii::$app->request->post();
			//10 = ancien
			//20 nouveau
			if (isset($params['form_type'])) {
				$form_type = $params['form_type'];
				if ($params['form_type'] == 100) {
					return $this->process($form_type);
				} else {
					return [
						'status' => "006",
						'message' => "Type de formulaire non reconnu",
					];
				}
			}
		} else {
			return [
				'status' => "006",
				'message' => "Type du formulaire non reçu",
			];
		}
		exit();
	}

	public function actionVerif_sms()
	{
		if (Yii::$app->request->post()) {
			$token = '1ElXH7V02KFlqqev7t9Lo7KfTeOr2HpnCreditAB';
			if ($_POST['access_token'] == $token) {
				//$carte_code = $_POST['carte_code'];
				//$find_carte = Carte::find()->where(['code_barre_carte' => $carte_code, 'etat' => 1])->one();
				$tete_phone = substr($_POST['telephone'], 0, 3);
				if ($tete_phone == '228') {
					$username = $_POST['telephone'];
				} else {
					$username = '228' . $_POST['telephone'];
				}
				$montant_transaction = $_POST['montant_transaction'];
				$find_user = User::find()->where(['username' => $username])->one();
				if ($find_user != null) {
					$phone_number = $find_user->username;
					$find_souscription = AbcSouscription::find()->where(['id_user' => $find_user->id])->one();
					if ($find_souscription != null) {
						$find_historique = AbcHistorique::find()->where(['id_souscription' => $find_souscription->id_souscription])->one();
						$reel_out = $montant_transaction;
						if ($find_souscription->status_souscription == 1) {

							// CECI EST UN SCAN POUR UN RETRAIT D'ARGENT
							if (isset($_POST["cashout"]) && ($_POST["cashout"] == '1')) {
								$param_cashout = AbcCashout::find()->where(['status' => 1])->one();

								$cout = ($montant_transaction * $param_cashout->cout) / 100;
								$reel_out = $montant_transaction + $cout;
							}

							if ($find_historique->montant_restant >= $reel_out) {
								$code = rand(1000, 9999);
								$find_souscription->code_verif = $code;
								if ($find_souscription->save()) {
									$content = 'Votre code de confirmation est : ' . $code;
									Common::hit_sms($content, $find_souscription->user->username, "ABCrédit");
									return json_encode([
										'status' => '000',
										'code' => $code,
										'message' => 'Un code de confirmation été envoyé au client.',
									]);
								}
							} else {
								$message = 'Le solde disponible sur votre compte est insuffisant pour cette operation. Solde actuel : ' . $find_historique->montant_restant . ' FCFA.';
								Common::hit_sms($message, $find_souscription->user->username, "ABCrédit");
								return json_encode([
									'status' => '002',
									'message' => 'Le solde du client est insuffisant pour cette operation.',
								]);
							}
						} else {
							$content = 'Votre compte est inactif, contactez votre banque.';
							Common::hit_sms($content, $phone_number, "ABCrédit");
							return json_encode([
								'status' => '001',
								'message' => 'Le compte du client n\'est pas opérationnel',
							]);
						}
					} else {
						return json_encode([
							'status' => '002',
							'message' => 'Ce client n\'est pas sur ABCrédit.',
						]);
					}
				} else {
					return json_encode([
						'status' => '002',
						'message' => 'Utilisateur non trouvé dans le système.',
					]);
				}
			} else {
				return json_encode([
					'status' => '002',
					'message' => 'token incorrect.',
				]);
			}
		}
	}

	public function actionVerif_cartescan()
	{
		if (Yii::$app->request->post()) {
			$token = '1ElXH7V02KFlqqev7t9Lo7KfTeOr2HpnCreditAB';
			if ($_POST['access_token'] == $token) {
				//$carte_code = $_POST['carte_code'];
				//$find_carte = Carte::find()->where(['code_barre_carte' => $carte_code, 'etat' => 1])->one();
				$etiquette_code = $_POST['carte_code'];
				$tete_phone = substr($_POST['telephone'], 0, 3);
				if ($tete_phone == '228') {
					$username = $_POST['telephone'];
				} else {
					$username = '228' . $_POST['telephone'];
				}

				$montant_transaction = $_POST['montant_transaction'];
				$find_user = User::find()->where(['username' => $username])->one();
				if ($find_user != null) {
					$phone_number = $find_user->username;
					$find_etiquette = Etiquette::find()->where(['code_barre_etiquette' => $etiquette_code, 'etat' => 1])->one();
					if ($find_etiquette != null) {
						$find_souscription = AbcSouscription::find()->where(['etiquette_id' => $find_etiquette->id, 'id_user' => $find_user->id])->one();
						if ($find_souscription != null) {
							$find_historique = AbcHistorique::find()->where(['id_souscription' => $find_souscription->id_souscription])->one();
							//print_r($find_souscription); die;
							$reel_out = $montant_transaction;
							if ($find_souscription->status_souscription == 1) {

								// CECI EST UN SCAN POUR UN RETRAIT D'ARGENT
								if (isset($_POST["cashout"]) && ($_POST["cashout"] == '1')) {
									$param_cashout = AbcCashout::find()->where(['status' => 1])->one();

									$cout = ($montant_transaction * $param_cashout->cout) / 100;
									$reel_out = $montant_transaction + $cout;
								}

								if ($find_historique->montant_restant >= $reel_out) {
									$code = rand(1000, 9999);
									$find_souscription->code_verif = $code;

									if ($find_souscription->save()) {
										$content = 'Votre code de confirmation est : ' . $code;
										Common::hit_sms($content, $find_souscription->user->username, "ABCrédit");
										return json_encode([
											'status' => '000',
											'code' => $code,
											'message' => 'Un code de confirmation a été envoyé au client.',
										]);
									}
								} else {
									$message = 'Le solde disponible sur votre compte est insuffisant pour cette operation. Solde actuel : ' . $find_historique->montant_restant . ' FCFA.';
									Common::hit_sms($message, $find_souscription->user->username, "ABCrédit");
									return json_encode([
										'status' => '001',
										'message' => 'Le solde du client est insuffisant pour cette operation.',
									]);
								}
							} else {
								$content = 'Votre compte est inactif, contactez votre banque.';
								Common::hit_sms($content, $phone_number, "ABCrédit");
								return json_encode([
									'status' => '001',
									'message' => 'Le compte du client n\'est pas opérationnel.',
								]);
							}
						} else {
							return json_encode([
								'status' => '001',
								'message' => 'Ce client n\'est pas sur ABCrédit.',
							]);
						}
					} else {
						return json_encode([
							'status' => '001',
							'message' => 'Carte non valide.',
						]);
					}
				} else {
					return json_encode([
						'status' => '001',
						'message' => 'Utilisateur non trouvé dans le système.',
					]);
				}
			}
		}
	}

	public function actionReset_forget_password()
	{
		if (Yii::$app->request->post()) {
			$token = '1ElXH7V02KFlqqev7t9Lo7KfTeOr2HpnCreditAB';
			if ($_POST['access_token'] == $token) {

				$new_password = $_POST['new_password'];
				$telephone = $_POST['telephone'];
				$tete_phone = substr($telephone, 0, 3);
				if ($tete_phone == '228') {
					$telephone = $_POST['telephone'];
				} else {
					$telephone = '228' . $_POST['telephone'];
				}
				$find_user = User::find()->where(['username' => $telephone])->one();
				$find_user->password_hashpassword_hashpassword_hash = Yii::$app->security->generatePasswordHash($new_password);
				if ($find_user->save()) {
					return json_encode([
						'status' => '000',
						'message' => 'Mot de passe changé avec succès.',
					]);
				}
			} else {
				return json_encode([
					'status' => '001',
					'message' => 'Token incorrect.',
				]);
			}
		}
	}

	public function actionVerify_number()
	{
		if (Yii::$app->request->post()) {
			$token = '1ElXH7V02KFlqqev7t9Lo7KfTeOr2HpnCreditAB';
			if ($_POST['access_token'] == $token) {
				$telephone = $_POST['telephone'];
				$tete_phone = substr($telephone, 0, 3);
				if ($tete_phone == '228') {
					$telephone = $_POST['telephone'];
				} else {
					$telephone = '228' . $_POST['telephone'];
				}
				$find_user = User::find()->where(['username' => $telephone])->one();
				if ($find_user != null) {
					$find_marchand = ActeurUser::find()->where(['id_user' =>  $find_user->id])->one();
					if ($find_marchand != null) {
						return json_encode([
							'status' => '000',
							'message' => 'Compte marchand trouvé',
							'username' => $telephone,
							'nom' => $find_user->nom,
							'prenoms' => $find_user->prenoms,
						]);
					} else {
						return json_encode([
							'status' => '001',
							'message' => 'Vous n\'êtes pas marchand sur ABCrédit.',
						]);
					}
				} else {
					return json_encode([
						'status' => '001',
						'message' => 'Utilisateur non trouvé dans le système.',
					]);
				}
			} else {
				return json_encode([
					'status' => '001',
					'message' => 'Token incorrect.',
				]);
			}
		}
	}

	public function actionCash_out()
	{
		if (Yii::$app->request->post()) {
			$token = '1ElXH7V02KFlqqev7t9Lo7KfTeOr2HpnCreditAB';
			if ($_POST['access_token'] == $token) {
				$montant_out = $_POST['montant'];
				$telephone = $_POST['telephone'];
				$marchand = $_POST['marchand'];
				$type_paiement = $_POST['type_paiement'];
				$telephone_beneficiaire = $_POST['beneficiaire'];

				$tete_phone = substr($telephone, 0, 3);
				if ($tete_phone == '228') {
					$telephone = $_POST['telephone'];
				} else {
					$telephone = '228' . $_POST['telephone'];
				}

				$tete_phoneb = substr($telephone_beneficiaire, 0, 3);
				if ($tete_phoneb == '228') {
					$telephone_beneficiaire = $_POST['beneficiaire'];
				} else {
					$telephone_beneficiaire = '228' . $_POST['beneficiaire'];
				}

				$ref = 'ABC' . substr(uniqid(rand(), true), 0, 4);
				$exist = AbcRetrait::find()->where(['ref_retrait' => $ref])->one();
				if ($exist != null) {
					$ref = 'ABC' . substr(uniqid(rand(), true), 0, 4);
				} else {

					$find_user = User::find()->where(['username' => $telephone])->one();
					if ($find_user != null) {
						$info_souscrip = AbcSouscription::find()->where(['id_user' => $find_user->id])->one();
						if ($info_souscrip != null) {
							if ($info_souscrip->status_souscription == 1) {

								$acteur = ActeurUser::find()->where(['id_user' =>  $marchand])->one();
								$param_cashout = AbcCashout::find()->where(['status' => 1])->one();

								$cout = ($montant_out * $param_cashout->cout) / 100;
								$reel_out = $montant_out + $cout;

								$marchand_benef = ($cout * $param_cashout->percent_marchand) / 100;
								$our_benef = $cout - $marchand_benef;

								// print_r($reel_out);
								// die;

								if ($info_souscrip->solde_disponible >= $reel_out) {

									$model = new AbcRetrait();
									$model->key_retrait_abc = Yii::$app->security->generateRandomString(32);
									$model->ref_retrait = $ref;
									$model->montant_retrait = $montant_out;
									$model->date_create = date('Y-m-d H:i:s');
									$model->status_retrait = 0;
									$model->id_acteur_user = $acteur->id_acteur_user;
									$model->idclient = $info_souscrip->id_souscription;
									$model->marchand_benef = $marchand_benef;
									$model->abc_benef = $our_benef;
									$model->reel_out = $reel_out;
									$model->numero_beneficiare = $telephone_beneficiaire;
									$model->type_envoiepaiement = $type_paiement;

									// $type_paiement = AbcTypePaiement::find()->where(['status'=> 1])->all();
									if (strtoupper($_POST['canal']) == 'WHATSAPP') {
										$type_envoiepaiement = '1';
									} else if (strtoupper($_POST['canal']) == 'TELEGRAM') {
										$type_envoiepaiement = '2';
									} else if (strtoupper($_POST['canal']) == 'SMS') {
										$type_envoiepaiement = '3';
										if ($_POST['verifCode'] != $info_souscrip->code_verif) {
											return json_encode([
												'status' => '002',
												'message' => 'Code de vérification incorrect',
											]);
										}
									} else if (strtoupper($_POST['canal']) == 'SCAN') {
										$type_envoiepaiement = '4'; // LE CODE QR A ETE SCANNE
										if ($_POST['verifCode'] != $info_souscrip->code_verif) {
											return json_encode([
												'status' => '002',
												'message' => 'Code de vérification incorrect',
											]);
										}
									}



									$code_paiement = "ABSELLERCRD" . $ref . "CRD" . $acteur->code_reference . "ETC" . $acteur->id_acteur_user . "ETC" . $info_souscrip->ref_souscription . "CRD2ABSELLER";

									$full_name = $find_user->nom . ' ' . $find_user->prenoms;
									$phone_number = $find_user->username;
									$canal_key = $find_user->canal_key;

									if ((isset($_POST['isScanned']) && $_POST['isScanned'] == 'true') or (isset($_POST['isSms']) && $_POST['isSms'] == 'true')) {
										// if (($_POST['isScanned'] == 'true') or ($_POST['isSms'] == 'true')) {

										$model->status_retrait = 1;
										if ($model->save()) {
											$maj_historique = AbcHistorique::find()->where(['id_souscription' => $info_souscrip->id_souscription])->one();
											$maj_historique->montant_operation += $reel_out;
											$maj_historique->montant_restant -= $reel_out;
											$maj_historique->save();

											$info_souscrip->solde_disponible -= $reel_out;
											$info_souscrip->code_verif = '';
											$info_souscrip->save();

											$content_client = "Cher Client " . $full_name;
											$content_client .= "\nVous venez d'effectuer une operation de retrait de " . $model->montant_retrait . " FCFA aupres du marchand " . $acteur->denomination . ", " . $acteur->address_acteur;

											Common::hit_sms(strtoupper($content_client), $phone_number, "ABCrédit");

											$message = 'Opération de retrait éffectuée avec succès';
											return json_encode([
												'status' => '000',
												'message' => $message,
											]);
										}
									} else {

										$content = "Cher Client " . $full_name;
										$content .= ". Nous avons bien reçu votre demande de retrait.";
										$content .= "\nLe Marchand " . $acteur->denomination . ", " . $acteur->address_acteur . " a initié une opération de retrait depuis votre carte ABCrédit.";
										$content .= "\nMontant : " . $montant_out . " FCFA";
										if ($param_cashout->cout != 0) $content .= "\nFrais : " . $cout . " FCFA";
										$content .= "\nBénéficiaire : " . $telephone_beneficiaire;
										$content_link = "\n\nPour approuver l'operation et autoriser le retrait, cliquez sur le lien et envoyer le contenu : \n";
										// $content_confirm_sms = "\nVotre code de confirmation est : ";
										//Common::hit_sms($content, $telephone, "ABCrédit");

										if ($type_envoiepaiement == "1") {
											$paiement_url = $this->lien_whatsapp . $code_paiement;
											$content .= $content_link . $paiement_url;
											Utils::send_information($phone_number, $content, "WHATSAPP");
										} else if ($type_envoiepaiement == "2") {
											$paiement_url = $this->lien_telegram . $code_paiement;
											$content .= $content_link . $paiement_url;
											if ($canal_key != "") {
												Utils::send_information($canal_key, $content, "TELEGRAM");
											} else {
												Common::hit_sms(strtoupper($content), $phone_number, "ABCrédit");
											}
										}
										/*else if (in_array($model->type_envoiepaiement, array("3", "4"))) {
											$content .= $content_confirm_sms;
											Common::hit_sms(strtoupper($content), $phone_number, "ABCrédit");
										}*/
										if ($model->save()) {

											if ($model->typeEnvoiepaiement->ref != "ABC03") {
												Common::save_cashout($model);
											}

											$message = 'Opération de retrait initié avec succès';
											return json_encode([
												'status' => '000',
												'message' => $message,
											]);
										} else {
											return json_encode([
												'status' => '000',
												'message' => json_encode($model->getErrors()),
											]);
										}
									}
								} else {
									return json_encode([
										'status' => '001',
										'message' => 'Le solde du client est insuffisant pour cette opération',
									]);
								}
							} else {
								$content = 'Votre compte est inactif, contactez votre banque.';
								Common::hit_sms($content, $telephone, "ABCrédit");
								return json_encode([
									'status' => '001',
									'message' => 'Le compte du client n\'est pas activé',
								]);
							}
						} else {
							return json_encode([
								'status' => '001',
								'message' => 'Ce client n\'est pas sur ABCrédit.',
							]);
						}
					} else {
						return json_encode([
							'status' => '001',
							'message' => 'Utilisateur non trouvé dans le système.',
						]);
					}
				}
			} else {
				return json_encode([
					'status' => '001',
					'message' => 'Token incorrect.',
				]);
			}
		}
	}

	public function actionAdd_vente()
	{
		if (Yii::$app->request->post()) {
			$token = '1ElXH7V02KFlqqev7t9Lo7KfTeOr2HpnCreditAB';

			if ($_POST['access_token'] == $token) {
				$model = new AbcVente();
				$ref = '';
				if (isset($_POST['ref_vente']) && $_POST['ref_vente'] == "1") {
					$model = AbcVente::find()->where(['ref_vente' => $_POST['reference']])->one();
					if ($model == null) {
						return json_encode([
							'status' => '002',
							'message' => 'Une erreur s\'est produite, veuillez réessayer plus tard.',
						]);
					} else {
						$ref = $model->ref_vente;
					}
				} else {

					$ref = 'ABC' . substr(uniqid(rand(), true), 0, 4);
					$exist = AbcVente::find()->where(['ref_vente' => $ref])->one();
					if ($exist != '') {
						$ref = 'ABC' . substr(uniqid(rand(), true), 0, 4);
					}
				}

				if (
					$_POST['description'] == "" || $_POST['montant'] == ""
					|| $_POST['client'] == "" || $_POST['marchand'] == ""
					|| $_POST['canal'] == ""
				) {
					$message = Yii::t('app', 'Veuillez renseigner tous les champs');
					return json_encode([
						'status' => '002',
						'message' => $message,
					]);
				} else {
					$info_marchand = ActeurUser::find()->where(['id_user' =>  $_POST['marchand']])->one();

					$tete_phone = substr($_POST['client'], 0, 3);
					if ($tete_phone == '228') {
						$phone = $_POST['client'];
					} else {
						$phone = '228' . $_POST['client'];
					}
					$info_souscrip = AbcSouscription::find()->where(['customer' =>  $phone])->one();
					if ($info_souscrip != null) {
						$info_user = User::find()->where(['id' => $info_souscrip->id_user])->one();
						$phone_number = $info_user->username;
						if ($info_souscrip->status_souscription == 1) {
							if ($info_souscrip->solde_disponible >= $_POST['montant']) {

								$model->key_vente_abc = Yii::$app->security->generateRandomString(32);
								$model->ref_vente = $ref;
								$model->libelle_vente_abc = $_POST['description'];
								$model->montant_vente_abc = $_POST['montant'];
								$model->date_create = date('Y-m-d H:i:s');
								$model->status_vente_abc = 0;
								$model->id_acteur_user = $info_marchand->id_acteur_user;
								$model->idclient = $info_souscrip->id_souscription;
								if (strtoupper($_POST['canal']) == 'WHATSAPP') {
									$model->type_envoiepaiement = '1';
								} else if (strtoupper($_POST['canal']) == 'TELEGRAM') {
									$model->type_envoiepaiement = '2';
								} else if (strtoupper($_POST['canal']) == 'SMS') {
									$model->type_envoiepaiement = '3';
									if ($_POST['verifCode'] != $info_souscrip->code_verif) {
										return json_encode([
											'status' => '002',
											'message' => 'Code de validation incorrect',
										]);
									}
								} else if (strtoupper($_POST['canal']) == 'SCAN') {
									$model->type_envoiepaiement = '4'; // LE CODE QR A ETE SCANNE


									if (trim($_POST['verifCode']) != trim($info_souscrip->code_verif)) {
										return json_encode([
											'status' => '002',
											'message' => 'Code de validation incorrect',
										]);
									}
									/*else {
										return json_encode([
											'status' => '002',
											'message' => $_POST['verifCode'] . '----' . $info_souscrip->code_verif,
										]);
									}*/
								} else {
									return json_encode([
										'status' => '002',
										'message' => strtoupper($_POST['canal'])
									]);
								}
								$model->information_id = "";
								$model->provenance = "APP MOBILLE";
								$model->submission_time = date('Y-m-d H:i:s');

								// print_r($model->key_vente_abc);
								// die;

								$code_paiement = "ABSELLERCRD" . $ref . "CRD" . $info_marchand->code_reference . "ETC" . $info_marchand->id_acteur_user . "ETC" . $info_souscrip->ref_souscription . "CRD1ABSELLER";
								$full_name = $info_user->nom . ' ' . $info_user->prenoms;

								// return gettype($_POST['isScanned']);
								if ((isset($_POST['isScanned']) && $_POST['isScanned'] == 'true') or (isset($_POST['isSms']) && $_POST['isSms'] == 'true')) {

									$model->status_vente_abc = 1;

									if ($model->save()) {

										$maj_historique = AbcHistorique::find()->where(['id_souscription' => $info_souscrip->id_souscription])->one();
										$maj_historique->montant_operation += $model->montant_vente_abc;
										$maj_historique->montant_restant -= $model->montant_vente_abc;
										$maj_historique->save();

										$info_souscrip->solde_disponible -= $model->montant_vente_abc;
										$info_souscrip->code_verif = '';
										$info_souscrip->save();

										$content_client = "Cher Client " . $full_name;
										$content_client .= "\nVous venez d'effectuer une operation aupres du marchand " . $info_marchand->denomination . ", " . $info_marchand->address_acteur;
										$content_client .= "\nDescription : " . $model->libelle_vente_abc;
										$content_client .= "\nMontant : " . $model->montant_vente_abc . " FCFA";

										$content_marchand = "Cher Marchand " . $info_marchand->denomination . ", " . $info_marchand->address_acteur;
										$content_marchand .= "\nClient : " . $full_name;
										$content_marchand .= "\nReference : " . $ref;
										$content_marchand .= "\nDescription : " . $model->libelle_vente_abc;
										$content_marchand .= "\n\nOperaton terminee avec succes.";
										$phone_number_marchand = $info_marchand->idUser->username;

										Common::hit_sms(strtoupper($content_client), $phone_number, "ABCrédit");
										Common::hit_sms(strtoupper($content_marchand), $phone_number_marchand, "ABCrédit");

										$message = 'Transaction éffectuée avec succès';
										return json_encode([
											'status' => '000',
											'message' => $message,
										]);
									} else {
										return json_encode([
											'status' => '002',
											'message' => json_encode($model->getErrors()),
										]);
									}
								} else {

									if ($model->save()) {

										// SEND PAY INFO TO THE CUSTOMER
										// ABSELLER
										// CRD
										// ABC4510 reference vente
										// CRD
										// CF63391 reference marchand
										// ETC
										// 1 id marchand
										// ETC
										// SPT1021 reference souscription
										// CRD
										// ABSELLER

										$content = "Cher Client " . $full_name;
										$content .= ". Nous avons bien reçu votre commande.";
										$content .= "\nLe Marchand " . $info_marchand->denomination . ", " . $info_marchand->address_acteur . " a initié une commande depuis votre carte ABCrédit.";
										$content .= "\nDescription : " . $model->libelle_vente_abc;
										$content .= "\nMontant : " . $model->montant_vente_abc . " FCFA";
										$content_link = "\n\nPour approuver la commande et autoriser la livraison, cliquez sur le lien et envoyer le contenu :\n";
										//$content_confirm_sms = "\n\nVotre code de confirmation est : ";

										$phone_number = $info_user->username;
										$canal_key = $info_user->canal_key;

										if ($model->type_envoiepaiement == "1") {
											$paiement_url = $this->lien_whatsapp . $code_paiement;
											$content .= $content_link . $paiement_url;
											Utils::send_information($phone_number, $content, "WHATSAPP");
										} else if ($model->type_envoiepaiement == "2") {
											$paiement_url = $this->lien_telegram . $code_paiement;
											$content .= $content_link . $paiement_url;
											if ($canal_key != "") {
												Utils::send_information($canal_key, $content, "TELEGRAM");
											} else {
												Common::hit_sms(strtoupper($content), $phone_number, "ABCrédit");
											}
										}
										/*else if (in_array($model->type_envoiepaiement, array("3", "4"))) {
											$content .= $content_confirm_sms;
											Common::hit_sms(strtoupper($content), $phone_number, "ABCrédit");
										}*/

										$message = 'Transaction ajoutée avec succès';
										return json_encode([
											'status' => '000',
											'message' => $message,
										]);
									}
								}
							} else {
								return json_encode([
									'status' => '002',
									'message' => 'Le solde du client est insuffisant pour cette opération',
								]);
							}
						} else {
							$content = 'Votre compte est inactif, contactez votre banque.';
							Common::hit_sms($content, $phone_number, "ABCrédit");
							return json_encode([
								'status' => '002',
								'message' => 'Le compte du client n\'est pas activé',
							]);
						}
					} else {
						return json_encode([
							'status' => '002',
							'message' => 'Client introuvable',
						]);
					}
				}
			} else {
				return json_encode([
					'status' => '001',
					'message' => 'Token incorrect',
				]);
			}
		}
	}

	public function actionAdd_vente_brouillon()
	{
		if (Yii::$app->request->post()) {
			//return json_encode($_POST['files']);
			// return json_encode($_FILES);
			$token = '1ElXH7V02KFlqqev7t9Lo7KfTeOr2HpnCreditAB';

			if ($_POST['access_token'] == $token) {

				$model = new AbcVente();
				$description = '';
				$montant = '';
				$canal = '';
				if (isset($_POST['description']) && $_POST['description'] != '') $description = $_POST['description'];
				if (isset($_POST['montant']) && $_POST['montant'] != '') $montant = $_POST['montant'];
				if (isset($_POST['canal']) && $_POST['canal'] != '') $canal = $_POST['canal'];

				$ref = 'ABC' . substr(uniqid(rand(), true), 0, 4);
				$exist = AbcVente::find()->where(['ref_vente' => $ref])->one();
				if ($exist != '') {
					$ref = 'ABC' . substr(uniqid(rand(), true), 0, 4);
				} else {

					$info_marchand = ActeurUser::find()->where(['id_user' =>  $_POST['marchand']])->one();

					if ($_POST['client'] != '') {
						$tete_phone = substr($_POST['client'], 0, 3);
						if ($tete_phone == '228') {
							$phone = $_POST['client'];
						} else {
							$phone = '228' . $_POST['client'];
						}
						// print_r($phone);die;

						$info_souscrip = AbcSouscription::find()->where(['customer' =>  $phone])->one();
						// print_r($info_souscrip);
						// die;
						if ($info_souscrip != null) {
							$info_user = User::find()->where(['id' => $info_souscrip->id_user])->one();
							$phone_number = $info_user->username;
							if ($info_souscrip->status_souscription == 1) {
								if ($info_souscrip->solde_disponible >= $montant) {

									$model->key_vente_abc = Yii::$app->security->generateRandomString(32);
									$model->ref_vente = $ref;
									$model->libelle_vente_abc = $description;
									$model->montant_vente_abc = $montant;
									$model->date_create = date('Y-m-d H:i:s');
									$model->status_vente_abc = 2;
									$model->id_acteur_user = $info_marchand->id_acteur_user;
									$model->idclient = $info_souscrip->id_souscription;
									if (strtoupper($canal) == 'WHATSAPP') {
										$model->type_envoiepaiement = '1';
									} else if (strtoupper($canal) == 'TELEGRAM') {
										$model->type_envoiepaiement = '2';
									} else if (strtoupper($canal) == 'SMS') {
										$model->type_envoiepaiement = '3';
									} else if (strtoupper($canal) == 'SCAN') {
										$model->type_envoiepaiement = '4'; // LE CODE QR A ETE SCANNE
									}
									$model->information_id = "";
									$model->provenance = "APP MOBILLE";
									$model->submission_time = date('Y-m-d H:i:s');

									if ($model->save()) {
										return json_encode([
											'status' => '000',
											'message' => 'Broulloin bien sauvegardé',
										]);
									} else {
										return json_encode([
											'status' => '002',
											'message' => $model->getErrors(),
										]);
									}
								} else {
									return json_encode([
										'status' => '002',
										'message' => 'Le solde du client est insuffisant pour cette opération',
									]);
								}
							} else {
								return json_encode([
									'status' => '002',
									'message' => 'Le compte du client n\'est pas opérationnel',
								]);
								$content = 'Votre compte est inactif, contacter votre banque.';
								Common::hit_sms($content, $phone_number, "ABCrédit");
							}
						} else {
							return json_encode([
								'status' => '002',
								'message' => 'Ce client n\'a pas de souscription',
							]);
						}
					} else {
						return json_encode([
							'status' => '002',
							'message' => 'Le téléphone du client est requis',
						]);
					}
				}
			} else {
				return json_encode([
					'status' => '001',
					'message' => 'Token incorrect',
				]);
			}
		}
	}


	public function actionUpdate_vente()
	{
		if (Yii::$app->request->post()) {
			//return json_encode($_POST['files']);
			// return json_encode($_FILES);
			$token = '1ElXH7V02KFlqqev7t9Lo7KfTeOr2HpnCreditAB';

			if ($_POST['access_token'] == $token) {

				$ref = $_POST['reference'];
				$exist = AbcVente::find()->where(['ref_vente' => $ref])->one();
				if ($exist == null) {
					return json_encode([
						'status' => '001',
						'message' => 'Transaction non trouvée',
					]);
				} else {

					$info_marchand = ActeurUser::find()->where(['id_user' =>  $_POST['marchand']])->one();

					$tete_phone = substr($_POST['client'], 0, 3);
					if ($tete_phone == '228') {
						$phone = $_POST['client'];
					} else {
						$phone = '228' . $_POST['client'];
					}
					$info_souscrip = AbcSouscription::find()->where(['customer' =>  $phone])->one();
					if ($info_souscrip != null) {
						$info_user = User::find()->where(['id' => $info_souscrip->id_user])->one();
						$phone_number = $info_user->username;
						if ($info_souscrip->status_souscription == 1) {
							if ($info_souscrip->solde_disponible >= $_POST['montant']) {

								$exist->key_vente_abc = Yii::$app->security->generateRandomString(32);
								$exist->ref_vente = $ref;
								$exist->libelle_vente_abc = $_POST['description'];
								$exist->montant_vente_abc = $_POST['montant'];
								$exist->date_create = date('Y-m-d H:i:s');
								$exist->status_vente_abc = 0;
								$exist->id_acteur_user = $info_marchand->id_acteur_user;
								$exist->idclient = $info_souscrip->id_souscription;

								if (strtoupper($_POST['canal']) == 'WHATSAPP') {
									$exist->type_envoiepaiement = '1';
								} else if (strtoupper($_POST['canal']) == 'TELEGRAM') {
									$exist->type_envoiepaiement = '2';
								} else if (strtoupper($_POST['canal']) == 'SMS') {
									$exist->type_envoiepaiement = '3';
									if ($_POST['verifCode'] != $info_souscrip->code_verif) {
										return json_encode([
											'status' => '002',
											'message' => 'Code de validation incorrect',
										]);
									}
								} else if (strtoupper($_POST['canal']) == 'SCAN') {
									$exist->type_envoiepaiement = '4'; // LE CODE QR A ETE SCANNE
									if ($_POST['verifCode'] != $info_souscrip->code_verif) {
										return json_encode([
											'status' => '002',
											'message' => 'Code de validation incorrect',
										]);
									}
								}


								$exist->information_id = "";
								$exist->provenance = "APP MOBILLE";
								$exist->submission_time = date('Y-m-d H:i:s');

								// print_r($exist->key_vente_abc);
								// die;

								$maj_historique = AbcHistorique::find()->where(['id_souscription' => $info_souscrip->id_souscription])->one();
								$maj_historique->montant_operation += $exist->montant_vente_abc;
								$maj_historique->montant_restant -= $exist->montant_vente_abc;

								if ($maj_historique->save()) {

									$code_paiement = "ABSELLERCRD" . $ref . "CRD" . $info_marchand->code_reference . "ETC" . $info_marchand->id_acteur_user . "ETC" . $info_souscrip->ref_souscription . "CRDABSELLER";
									$full_name = $info_user->nom . ' ' . $info_user->prenoms;

									// return gettype($_POST['isScanned']);

									if ((isset($_POST['isScanned']) && $_POST['isScanned'] == 'true') or (isset($_POST['isSms']) && $_POST['isSms'] == 'true')) {
										if ($exist->save()) {
											$content_client = "Cher Client " . $full_name;
											$content_client .= "\nVous venez d'effectuer une operation aupres du marchand " . $info_marchand->denomination . ", " . $info_marchand->address_acteur;
											$content_client .= "\nDescription : " . $exist->libelle_vente_abc;
											$content_client .= "\nMontant : " . $exist->montant_vente_abc;

											$content_marchand = "Cher Marchand " . $info_marchand->denomination . ", " . $info_marchand->address_acteur;
											$content_marchand .= "\nClient : " . $full_name;
											$content_marchand .= "\nReference : " . $ref;
											$content_marchand .= "\nDescription : " . $exist->libelle_vente_abc;
											$content_marchand .= "\n\nOperaton terminee avec succes.";
											$phone_number_marchand = $info_marchand->idUser->username;

											Common::hit_sms($content_client, $phone_number, "ABCrédit");
											Common::hit_sms($content_marchand, $phone_number_marchand, "ABCrédit");

											$message = 'Transaction éffectuée avec succès';
											return json_encode([
												'status' => '000',
												'message' => $message,
											]);
										}
									} else {

										if ($exist->save()) {

											// $info_marchand = ActeurUser::find()->where(['id_acteur_user' => $exist->id_acteur_user])->one();
											// $info_souscrip = AbcSouscription::find()->where(['id_souscription' => $exist->idclient])->one();
											// $info_user = User::find()->where(['id' => $info_souscrip->id_user])->one();
											// $info_souscrip->id_user->username;
											// $info_user->username;
											// SEND PAY INFO TO THE CUSTOMER
											// ABSELLER
											// CRD
											// ABC4510 reference vente
											// CRD
											// CF63391 reference marchand
											// ETC
											// 1 id marchand
											// ETC
											// SPT1021 reference souscription
											// CRD
											// ABSELLER

											$content = "Cher Client " . $full_name;
											$content .= ". Nous avons bien reçu votre commande.";
											$content .= "\nLe Marchand : " . $info_marchand->denomination . ", " . $info_marchand->address_acteur . " a initié une commande depuis votre carte ABCrédit.";
											$content .= "\nDescription : " . $exist->libelle_vente_abc;
											$content .= "\nMontant : " . $exist->montant_vente_abc . " FCFA";
											$content_link = "\n\nPour approuver la commande et autoriser la livraison, cliquez sur le lien et envoyer le contenu :\n";
											$content_confirm_sms = "\n\nVotre code de confirmation est : ";

											$phone_number = $info_user->username;
											$canal_key = $info_user->canal_key;

											if ($exist->type_envoiepaiement == "1") {
												$paiement_url = $this->lien_whatsapp . $code_paiement;
												$content .= $content_link . $paiement_url;
												Utils::send_information($phone_number, $content, "WHATSAPP");
											} else if ($exist->type_envoiepaiement == "2") {
												$paiement_url = $this->lien_telegram . $code_paiement;
												$content .= $content_link . $paiement_url;
												if ($canal_key != "") {
													Utils::send_information($canal_key, $content, "TELEGRAM");
												} else {
													Common::hit_sms($content, $phone_number, "ABCrédit");
												}
											} else if (in_array($exist->type_envoiepaiement, array("3", "4"))) {
												$content .= $content_confirm_sms;
												Common::hit_sms($content, $phone_number, "ABCrédit");
											}

											$message = 'Transaction ajoutée avec succès';
											return json_encode([
												'status' => '000',
												'message' => $message,
											]);
										}
									}
								} else {
									$message = '';
									foreach ($exist->getErrors() as $val) {
										$message .= $val[0] . '\n';
									}
									// $message = 'Une erreur est survenue, veuillez réessayer plus tard';
									return json_encode([
										'status' => '002',
										'message' => $message,
									]);
								}
							} else {
								return json_encode([
									'status' => '002',
									'message' => 'Le solde du client est insuffisant pour cette opération',
								]);
							}
						} else {
							$content = 'Votre compte est inactif, contacter votre banque.';
							Common::hit_sms($content, $phone_number, "ABCrédit");
							return json_encode([
								'status' => '002',
								'message' => 'Le compte du client n\'est pas opérationnel',
							]);
						}
					} else {
						return json_encode([
							'status' => '002',
							'message' => 'Client introuvable',
						]);
					}
				}
			} else {
				return json_encode([
					'status' => '001',
					'message' => 'Token incorrect',
				]);
			}
		}
	}

	public function actionListe_transaction()
	{
		if (Yii::$app->request->post()) {
			if ($_POST['marchand']) {
				$acteur = ActeurUser::find()->where(['id_user' =>  $_POST['marchand']])->one();
				// $return = AbcVente::find()->where(['status_vente_abc' => [0, 1], 'id_acteur_user' => $acteur->id_acteur_user])->orderBy(['date_create' => SORT_DESC])->all();

				$sql = "SELECT `abc_souscription`.`customer` as telephone, `abc_vente`.`idclient`, `abc_vente`.`id_vente_abc`, `abc_vente`.`ref_vente`, `abc_vente`.`libelle_vente_abc`, `abc_vente`.`status_vente_abc`, `abc_vente`.`montant_vente_abc`, `abc_vente`.`id_acteur_user`, `abc_vente`.`type_envoiepaiement` FROM `abc_vente` LEFT JOIN `abc_souscription` ON abc_souscription.id_souscription = abc_vente.idclient WHERE (`status_vente_abc` = 0 OR `status_vente_abc` = 1) AND (`id_acteur_user`=" . $acteur->id_acteur_user . ") ORDER BY `abc_vente`.`date_create` DESC";
				$return = Yii::$app->db->createCommand($sql)->queryAll();

				if (sizeof($return) > 0) {
					echo Json::encode([
						'status' => '000',
						'information' => $return,
					]);
				} else {
					echo Json::encode([
						'status' => '001',
						'message' => 'Votre liste de transaction est vide',
					]);
				}
			} else {
				echo Json::encode([
					'status' => '002',
					'message' => 'Aucune donnée reçu par post',
				]);
			}
		} else {
			echo Json::encode([
				'status' => '002',
				'message' => 'Missing post',
			]);
		}
		exit;
	}

	public function actionListe_transaction_broulloin()
	{
		if (Yii::$app->request->post()) {
			if ($_POST['marchand']) {
				$acteur = ActeurUser::find()->where(['id_user' =>  $_POST['marchand']])->one();
				// print_r($acteur);die;
				if ($acteur != null) {
					// $return = AbcVente::find()->with('abc_souscription')->where(['status_vente_abc' => 2, 'id_acteur_user' => $acteur->id_acteur_user, 'abc_souscription.id_souscription = abc_vente.idclient'])->orderBy(['date_create' => SORT_DESC])->all();
					// $return = AbcVente::find()->where(['status_vente_abc' => 2, 'id_acteur_user' => $acteur->id_acteur_user])->orderBy(['date_create' => SORT_DESC])->all();

					$sql = "SELECT `abc_souscription`.`customer` as telephone, `abc_vente`.`idclient`, `abc_vente`.`id_vente_abc`, `abc_vente`.`ref_vente`, `abc_vente`.`libelle_vente_abc`, `abc_vente`.`montant_vente_abc`, `abc_vente`.`date_create`, `abc_vente`.`id_acteur_user`, `abc_vente`.`type_envoiepaiement` FROM `abc_vente` LEFT JOIN `abc_souscription` ON abc_souscription.id_souscription = abc_vente.idclient WHERE (`status_vente_abc`=2) AND (`id_acteur_user`=" . $acteur->id_acteur_user . ") ORDER BY `abc_vente`.`date_create` DESC";
					$return = Yii::$app->db->createCommand($sql)->queryAll();
					// print_r($data);die;


					/*$return = AbcVente::find()
						->select([
							'abc_souscription.*',
							'abc_vente.idclient',
							'abc_vente.id_vente_abc',
							'abc_vente.ref_vente',
							'abc_vente.libelle_vente_abc',
							'abc_vente.montant_vente_abc',
							'abc_vente.id_acteur_user',
							'abc_vente.type_envoiepaiement',
						])
						// ->leftJoin('idclient0')
						->leftJoin('abc_souscription', 'abc_souscription.id_souscription = abc_vente.idclient')
						->where(['status_vente_abc' => 2, 'id_acteur_user' => $acteur->id_acteur_user])
						// ->andWhere('abc_souscription.id_souscription = abc_vente.idclient')
						->with('idclient0')
						->all();*/

					if (sizeof($return) > 0) {
						echo Json::encode([
							'status' => '000',
							'information' => $return,
						]);
					} else {
						echo Json::encode([
							'status' => '001',
							'message' => 'Votre liste des broulloins est vide',
						]);
					}
				} else {
					echo Json::encode([
						'status' => '001',
						'message' => 'Marchand non trouvé',
					]);
				}
			} else {
				echo Json::encode([
					'status' => '002',
					'message' => 'Aucune donnée reçu par post',
				]);
			}
		} else {
			echo Json::encode([
				'status' => '002',
				'message' => 'Missing post',
			]);
		}
		exit;
	}

	public function actionAll_transaction_stats()
	{
		if ($_POST['marchand']) {

			$the_date = date('Y-m-d');
			$the_day_of_week = date("w", strtotime($the_date)); //sunday is 0

			$date_first_mounth = date('Y-m-1');
			$dateObj = new DateTime($the_date);
			$dateObj->modify('last day of');
			$date_last_mounth = $dateObj->format('Y-m-d');

			$first_day_of_week = date("Y-m-d", strtotime($the_date) - 60 * 60 * 24 * ($the_day_of_week) + 60 * 60 * 24 * 1);
			$last_day_of_week = date("Y-m-d", strtotime($first_day_of_week) + 60 * 60 * 24 * 4);

			$acteur = ActeurUser::find()->where(['id_user' =>  $_POST['marchand']])->one();
			$total = AbcVente::find()->where(['status_vente_abc' => [0, 1, 2], 'id_acteur_user' => $acteur->id_acteur_user])->count();
			$totalSemaine = AbcVente::find()->where(['id_acteur_user' => $acteur->id_acteur_user])->andWhere(['between', 'date_create', $first_day_of_week, $last_day_of_week])->count();
			$totalToday = AbcVente::find()->where(['id_acteur_user' => $acteur->id_acteur_user])->andWhere(['like', 'date_create', $the_date])->count();
			// $totalToday = AbcVente::find()->where(['like', 'date_create',  $the_date])->count();
			$totalMois = AbcVente::find()->where(['id_acteur_user' => $acteur->id_acteur_user])->andWhere(['between', 'date_create', $date_first_mounth, $date_last_mounth])->count();
			$totalAttente = AbcVente::find()->where(['status_vente_abc' => 0, 'id_acteur_user' => $acteur->id_acteur_user])->count();
			$totalFinalise = AbcVente::find()->where(['status_vente_abc' => 1, 'id_acteur_user' => $acteur->id_acteur_user])->count();
			$totalFinaliseCout = AbcVente::find()->where(['status_vente_abc' => 1, 'id_acteur_user' => $acteur->id_acteur_user])->sum('montant_vente_abc');
			$totalBroulloin = AbcVente::find()->where(['status_vente_abc' => 2, 'id_acteur_user' => $acteur->id_acteur_user])->count();

			//$sql = 'SELECT sum(abc_vente.montant_vente_abc) FROM abc_vente WHERE `status_vente_abc` = 1 AND id_acteur_user = ' . $acteur->id_acteur_user;
			//$totalFinaliseCout = Yii::$app->db->createCommand($sql)->queryScalar();
			if ($totalFinaliseCout == null) $totalFinaliseCout = "0";

			$return = array(
				'total' => $total,
				'totalSemaine' => $totalSemaine,
				'totalToday' => $totalToday,
				'totalMois' => $totalMois,
				'totalAttente' => $totalAttente,
				'totalFinalise' => $totalFinalise,
				'totalFinaliseCout' => $totalFinaliseCout,
				'totalBroulloin' => $totalBroulloin,
			);

			if (sizeof($return) > 0) {
				echo Json::encode([
					'status' => '000',
					'information' => $return,
				]);
			} else {
				echo Json::encode([
					'status' => '001',
					'message' => 'Votre liste de transaction est vide',
				]);
			}
		} else {
			echo Json::encode([
				'status' => '002',
				'message' => 'Aucune donnée reçu par post',
			]);
		}
		exit;
	}

	public function actionListe_client()
	{
		echo Json::encode(AbcSouscription::find()->where(['status_souscription' => 1])->all());
		exit;
	}

	public function actionListe_marchand()
	{
		// $array = array();
		$data = ActeurUser::find()->joinWith('abc_souscription_marchand')->where(['type_acteur' => 1, 'etat' => [0, 1]])->andWhere('`abc_souscription_marchand`.`id_acteur_user` = `acteur_user`.`id_acteur_user`')->all();
		// foreach ($data as $value) {
		// 	$array[] = array(
		// 		'id_acteur_user' => $value['id_acteur_user'],
		// 		'denomination' => $value['denomination'],
		// 		'id_user' => $value['id_user']
		// 	);
		// }
		echo  Json::encode($data);
		exit;
	}


	public function actionLogin_marchand()
	{
		if (Yii::$app->request->post()) {
			$token = '1ElXH7V02KFlqqev7t9Lo7KfTeOr2HpnCreditAB';
			if ($_POST['token'] == $token) {
				$username = $_POST['telephone'];
				$password = $_POST['password'];
				$tete_phone = substr($username, 0, 3);
				if ($tete_phone == '228') {
					$phone = $username;
				} else {
					$phone = '228' . $username;
				}
				$user = User::find()->where(['username' => $phone, 'status' => [10, 40]])->one();
				if ($user != "") {
					$acteur = ActeurUser::find()->where(['id_user' =>  $user->id])->one();
					if ($acteur != "") {
						$acteur_abc = AbcSouscriptionMarchand::find()->where(['id_acteur_user' =>  $acteur->id_acteur_user])->one();
						if ($acteur_abc != "") {
							$password_hash = Yii::$app->security->validatePassword($password, $user->password_hash);;
							if ($password_hash) {
								echo Json::encode([
									'status' => '000',
									'information' => $user
								]);
							} else {
								echo Json::encode([
									'status' => '001',
									'message' => 'Mot de passe incorrect'
								]);
							}
						} else {
							echo Json::encode([
								'status' => '001',
								'message' => 'Vous n\'etes pas souscript à ABCrédit'
							]);
						}
					} else {
						echo Json::encode([
							'status' => '001',
							'message' => 'Vous n\'etes pas un marchand ABCrédit'
						]);
					}
				} else {
					echo Json::encode([
						'status' => '001',
						'message' => 'Téléphone ou mot de passe incorrect'
					]);
				}
			} else {
				echo Json::encode([
					'status' => '002',
					'message' => 'Token incorrect'
				]);
			}
		}
		exit;
	}


	public function actionAll_card()
	{
		if (Yii::$app->request->post()) {
			if (isset($_POST['customerPhone']) && $_POST['customerPhone'] != '') {
				$tete_phone = substr($_POST['customerPhone'], 0, 3);
				if ($tete_phone == '228') {
					$phone = $_POST['customerPhone'];
				} else {
					$phone = '228' . $_POST['customerPhone'];
				}
				$datas = AbcSouscription::find()->where(['status_souscription' => 1, 'customer' => $phone])->all();
				if (sizeof($datas) > 0) {
					$tab_sous = array();
					$tab_all_sous = array();
					foreach ($datas as $data) {
						$banque = AbcBanque::findOne($data->banque);
						$tab_sous['id_souscription'] = $data->id_souscription;
						$tab_sous['id_user'] = $data->id_user;
						$tab_sous['customer'] = $data->customer;
						$tab_sous['solde_disponible'] = $data->solde_disponible;
						$tab_sous['banque'] = $banque->libelle;
						$tab_all_sous[] = $tab_sous;
					}
					echo Json::encode([
						'status' => '000',
						'information' => $tab_all_sous,
					]);
				} else {
					echo Json::encode([
						'status' => '001',
						'message' => 'Aucune carte trouvée',
					]);
				}
			} else {
				echo Json::encode([
					'status' => '002',
					'message' => 'customerPhone non reçu',
				]);
			}
		}
		exit;
	}

	public function actionAll_abc_type()
	{
		//	if (Yii::$app->request->post()) {
		echo Json::encode(AbcTypeSouscription::find()->where(['status' => 1])->all());
		exit;
		//	}
	}

	public function actionAll_abc_type_paiement()
	{
		//	if (Yii::$app->request->post()) {
		echo Json::encode(AbcTypePaiement::find()->where(['status' => 1])->all());
		exit;
		//	}
	}

	public function actionAll_abc_banque()
	{
		// if (Yii::$app->request->post()) {
		echo Json::encode(AbcBanque::find()->where(['status' => 1])->all());
		exit;
		// }
	}

	public function actionAll_assurance_deces()
	{
		// if (Yii::$app->request->post()) {
		echo Json::encode(AbcAssuranceDeces::find()->where(['status' => 1])->all());
		exit;
		// }
	}

	public function actionAll_assurance_perte()
	{
		// if (Yii::$app->request->post()) {
		echo Json::encode(AbcAssurancePerte::find()->where(['status' => 1])->all());
		exit;
		// }
	}

	public function actionBlock_carte()
	{
		if (Yii::$app->request->post()) {
			$token = '1ElXH7V02KFlqqev7t9Lo7KfTeOr2HpnCreditAB';
			if ($_POST['access_token'] == $token) {
				if (isset($_POST['customer']) && $_POST['customer'] != '') {
					$tete_phone = substr($_POST['customer'], 0, 3);
					if ($tete_phone == '228') {
						$phone = $_POST['customer'];
					} else {
						$phone = '228' . $_POST['customer'];
					}
					$abc_user = AbcSouscription::find()->where(['customer' => $phone, 'status_souscription' => 1])->one();
					$user = User::find()->where(['username' => $phone])->one();
					if ($abc_user != '') {
						$abc_user->status_souscription = 2; // ETAT BLOQUER
						if ($abc_user->save()) {
							$message = 'La carte de Mr : ' . $user->nom . ' ' . $user->prenoms . ' (' . $phone . ')';
							$message .= " a été bloqué avec succès.";
							return json_encode([
								'status' => '000',
								'message' => $message,
							]);
						} else {
							$message = 'Une erreur est survenue, veuillez réessayer plus tard.';
							return json_encode([
								'status' => '001',
								'message' => $message,
							]);
						}
					} else {
						$message = 'Client introuvable';
						return json_encode([
							'status' => '001',
							'message' => $message,
						]);
					}
				} else {
					$message = 'Veuillez renseigner le numéro de téléphone du client';
					return json_encode([
						'status' => '002',
						'message' => $message,
					]);
				}
			} else {
				$message = 'Token incorrect';
				return json_encode([
					'status' => '002',
					'message' => $message,
				]);
			}
		} else {
			$message = 'Aucune donnée reçu en POST';
			return json_encode([
				'status' => '002',
				'message' => $message,
			]);
		}
	}

	public function actionUnblock_carte()
	{
		if (Yii::$app->request->post()) {
			$token = '1ElXH7V02KFlqqev7t9Lo7KfTeOr2HpnCreditAB';
			if ($_POST['access_token'] == $token) {
				if (isset($_POST['customer']) && $_POST['customer'] != '') {
					$tete_phone = substr($_POST['customer'], 0, 3);
					if ($tete_phone == '228') {
						$phone = $_POST['customer'];
					} else {
						$phone = '228' . $_POST['customer'];
					}
					$abc_user = AbcSouscription::find()->where(['customer' => $phone, 'status_souscription' => 2])->one();
					$user = User::find()->where(['username' => $phone])->one();
					if ($abc_user != '') {
						$abc_user->status_souscription = 1; // ETAT BLOQUER
						if ($abc_user->save()) {
							$message = 'La carte de Mr : ' . $user->nom . ' ' . $user->prenoms . ' (' . $phone . ')';
							$message .= " a été débloqué avec succès.";
							return json_encode([
								'status' => '000',
								'message' => $message,
							]);
						} else {
							$message = 'Une erreur est survenue, veuillez réessayer plus tard.';
							return json_encode([
								'status' => '001',
								'message' => $message,
							]);
						}
					} else {
						$message = 'Client introuvable';
						return json_encode([
							'status' => '001',
							'message' => $message,
						]);
					}
				} else {
					$message = 'Veuillez renseigner le numéro de téléphone du client';
					return json_encode([
						'status' => '002',
						'message' => $message,
					]);
				}
			} else {
				$message = 'Token incorrect';
				return json_encode([
					'status' => '002',
					'message' => $message,
				]);
			}
		} else {
			$message = 'Aucune donnée reçu en POST';
			return json_encode([
				'status' => '002',
				'message' => $message,
			]);
		}
	}

	public function actionAdd_incident()
	{
		if (Yii::$app->request->post()) {
			//return json_encode($_FILES);
			$token = '1ElXH7V02KFlqqev7t9Lo7KfTeOr2HpnCreditAB';
			if ($_POST['access_token'] == $token) {
				$model = new AbcIncident();
				if ($_POST['objet'] == "" || $_POST['contenu'] == "") {
					$message = Yii::t('app', 'Veuillez renseigner tous les champs');
					return json_encode([
						'status' => '001',
						'message' => $message,
					]);
				} else {
					$model->key_incident = Yii::$app->security->generateRandomString(32);
					$model->objet = $_POST['objet'];
					$tab_images = array();
					if (isset($_FILES)) {
						$files = $_FILES;
						$taille = sizeof($files);

						$i = 0;

						for ($i; $i < $taille; $i++) {
							$name = 'files_' . $i;
							if (isset($files[$name])) {
								$url = Yii::$app->basePath;
								$recup = explode(DIRECTORY_SEPARATOR . "api", $url);
								$target_path = $recup[0] . DIRECTORY_SEPARATOR . "backend" . DIRECTORY_SEPARATOR . "web" . DIRECTORY_SEPARATOR . "images" . DIRECTORY_SEPARATOR . "ab_credit" . DIRECTORY_SEPARATOR;
								$image_name = "INC_" . time() . $i . ".jpg";
								if (move_uploaded_file($files[$name]['tmp_name'], $target_path . $image_name)) {

									$tab_images[] = $image_name;
								}
							}
						}
					}
					$model->contenu = $_POST['contenu'];
					$model->images = json_encode($tab_images);
					$model->status = 0;
					$model->create_by = $_POST['user_id'];
					if ($model->save()) {
						$message = 'Incident envoyé avec succès';
						return json_encode([
							'status' => '000',
							'message' => $message,
						]);
					}
				}
			} else {
				$message = 'Token incorrect';
				return json_encode([
					'status' => '002',
					'message' => $message,
				]);
			}
		}
	}

	public function actionAdd_souscription()
	{
		if (Yii::$app->request->post()) {
			$token = '1ElXH7V02KFlqqev7t9Lo7KfTeOr2HpnCreditAB';
			if ($_POST['access_token'] == $token) {
				// print_r($_POST);
				// die;
				$ref = 'SPT' . substr(uniqid(rand(), true), 0, 4);
				$exist = AbcSouscription::find()->where(['ref_souscription' => $ref])->one();
				if ($exist != '') {
					$ref = 'SPT' . substr(uniqid(rand(), true), 0, 4);
				} else {
					if (
						!(isset($_POST['type_souscription'])) || $_POST['customer'] == ""
						|| $_POST['numero_compte'] == "" || !(isset($_POST['banque']))
						|| $_POST['no_carte'] == "" || $_POST['user_id'] == ""
						|| !(isset($_POST['abc_assurance_perte'])) || !(isset($_POST['abc_assurance_deces']))
					) {
						$message = Yii::t('app', 'Veuillez renseigner les champs obligatoires');
						return json_encode([
							'status' => '001',
							'message' => $message,
						]);
					} else {
						// if(!isset($_POST['abc_assurance_deces']))
						$tete_phone = substr($_POST['customer'], 0, 3);
						if ($tete_phone == '228') {
							$phone = $_POST['customer'];
						} else {
							$phone = '228' . $_POST['customer'];
						}
						$user = User::find()->where(['username' => $phone])->one();

						if ($user == null) {
							$new_user = new User();
							$new_user->idP = 1;
							$new_user->username = $phone;
							$new_user->nom = '-';
							$new_user->prenoms = '-';
							$new_user->id_user_profil  = 3;
							$new_user->auth_key  = Yii::$app->security->generateRandomString(32);
							$new_user->password_hash  =  Yii::$app->security->generatePasswordHash($phone);
							$new_user->role  = 10;
							$new_user->status  = 10;
							$new_user->created_at  = time();
							$new_user->updated_at  = time();
							$new_user->save();
							$user = $new_user;
						}

						if (isset($_POST['etiquette_id'])) {

							$find_etiquette = Etiquette::find()->where(['code_barre_etiquette' => $_POST['etiquette_id'], 'etat' => 0])->one();

							if ($find_etiquette != null) {

								$no_carte = str_replace('-', '', $_POST['no_carte']);

								$find_carte = Carte::find()->where(['code_barre_carte' => $no_carte, 'etat' => 0])->one();

								if ($find_carte != null) {

									$solde =  AbcTypeSouscription::find()->where(['id' => $_POST['type_souscription']])->one();

									if ($solde != null) {
										$model = new AbcSouscription;

										$model->key_souscription = Yii::$app->security->generateRandomString(32);
										$model->type_souscription = $_POST['type_souscription'];
										$model->ref_souscription = $ref;
										$model->customer = $phone;

										$model->id_user = $user->id;
										$model->solde_disponible = $solde->solde;
										$model->code_banque = $_POST['code_banque'];
										$model->code_guichet = $_POST['code_guichet'];
										$model->banque = $_POST['banque'];
										$model->cle_rib = $_POST['cle_rib'];
										$model->code_bic = $_POST['code_bic'];
										$model->iban = $_POST['iban'];
										$model->adresse_banque = $_POST['adresse_banque'];
										$model->numero_compte = $_POST['numero_compte'];
										$model->no_carte = $no_carte;
										$model->status_souscription = 0;
										$model->create_by = $_POST['user_id'];
										$model->date_create = date('Y-m-d H:i:s');
										$model->etiquette_id = $find_etiquette->id;
										$model->assurance_perte = $_POST['abc_assurance_perte'];
										$model->assurance_deces = $_POST['abc_assurance_deces'];

										if ($model->banque == $find_carte->banqueId) {
											if ($model->type_souscription == $find_carte->type_souscriptionId) {
												if ($model->save()) {
													$histo = new AbcHistorique();
													$histo->key_historique = Yii::$app->security->generateRandomString(32);
													$histo->mois_operation = date('Y-m');
													$histo->montant = $solde->solde;
													$histo->montant_operation = 0;
													$histo->montant_restant = $solde->solde;
													$histo->date_create = date('Y-m-d H:i:s');
													$histo->id_souscription = $model->id_souscription;
													if ($histo->save()) {

														$find_etiquette->etat = 1;
														$find_etiquette->description = date('Y-m-d H:i:s');
														$find_etiquette->save();

														$find_carte->etat = 1;
														//$find_carte->description = date('Y-m-d H:i:s');
														$find_carte->save();

														$message = 'Souscription ajoutée avec succès';
														return json_encode([
															'status' => '000',
															'message' => $message,
														]);
													} else {
														$message = "";
														foreach ($histo->getErrors() as $val) {
															$message .= $val[0] . '---';
														}
														//$message = Yii::t('app', 'Veuillez renseigner tous les champs');
														return json_encode([
															'status' => '001',
															'message' => $message,
														]);
													}
												} else {
													$message = "";
													foreach ($model->getErrors() as $val) {
														$message .= $val[0] . '<br/>';
													}
													return json_encode([
														'status' => '001',
														'message' => $message,
													]);
												}
											} else {
												return json_encode([
													'status' => '001',
													'message' => 'Le type de souscription selectionné n\'est pas conforme à celle de la carte',
												]);
											}
										} else {
											return json_encode([
												'status' => '001',
												'message' => 'La banque selectionnée n\'est pas conforme à celle de la carte',
											]);
										}
									} else {
										return json_encode([
											'status' => '001',
											'message' => 'Type de souscription non trouvé',
										]);
									}
								} else {
									return json_encode([
										'status' => '001',
										'message' => 'Le numéro de carte n\'est pas valide',
									]);
								}
							} else {
								return json_encode([
									'status' => '001',
									'message' => 'Etiquette non valide',
								]);
							}
						} else {
							return json_encode([
								'status' => '001',
								'message' => 'Veuillez scanner la carte Svp',
							]);
						}
					}
				}
			} else {
				return json_encode([
					'status' => '002',
					'message' => 'Token incorrect',
				]);
			}
		}
	}

	public function actionAdd_souscription_marchand()
	{
		if (Yii::$app->request->post()) {
			$token = '1ElXH7V02KFlqqev7t9Lo7KfTeOr2HpnCreditAB';
			if ($_POST['access_token'] == $token) {
				$banque = $_POST['code_banque'];
				$no_compte = $_POST['numero_compte'];
				$telephone = $_POST['telephone'];
				$tete_phone = substr($telephone, 0, 3);
				if ($tete_phone == '228') {
					$telephone = $_POST['telephone'];
				} else {
					$telephone = '228' . $_POST['telephone'];
				}
				$find_user = User::find()->where(['username' => $telephone])->one();
				if ($find_user != null) {
					$find_marchand = ActeurUser::find()->where(['id_user' =>  $find_user->id])->one();
					if ($find_marchand != null) {
						$model = new AbcSouscriptionMarchand();
						$model->key_ss_marchand = Yii::$app->security->generateRandomString(32);
						$model->numero_compte = $no_compte;
						$model->id_acteur_user = $find_marchand->id_acteur_user;
						$model->code_reference = $find_marchand->code_reference;
						$model->status_souscription = 0;
						$model->banque = $banque;
						$model->date_create =  date('Y-m-d H:i:s');
						if ($model->save()) {
							$message = 'Souscription ajoutée avec succès';
							return json_encode([
								'status' => '000',
								'message' => $message,
							]);
						} else {
							$message = "";
							foreach ($model->getErrors() as $val) {
								$message .= $val[0] . "\n";
							}
							return json_encode([
								'status' => '001',
								'message' => $message,
							]);
						}
					} else {
						return json_encode([
							'status' => '001',
							'message' => 'Vous n\'êtes pas marchand sur ABusiness',
						]);
					}
				} else {
					return json_encode([
						'status' => '001',
						'message' => 'Vous n\'êtes pas inscrit sur ABusiness',
					]);
				}
			} else {
				$message = 'Token incorrect';
				return json_encode([
					'status' => '001',
					'message' => $message,
				]);
			}
		}
	}

	public function process($form_type)
	{
		set_time_limit(0);
		date_default_timezone_set('UTC');

		$formulaires_ids = [
			'100' => 'aawgNYi63TmoYTAnsq83bL',
		];
		$designation = '';
		$montant = 0;

		if (array_key_exists($form_type, $formulaires_ids)) {
			$find_informations = Information::find()->where(['extract' => 0, 'type' => $form_type])->all();
			foreach ($find_informations as $db_information) {
				$information = json_decode($db_information->content);
				$data = (array)$information;

				// print_r($this->set($data, 'group_hi4jz21'));die;
				$zeze = $this->set($data, 'group_hi4jz21');
				foreach ($zeze as $val) {
					$value = (array)$val;
					$designation .= $value['group_hi4jz21/DESIGNATION'] . '<br/>';
					$montant += $value['group_hi4jz21/MONTANT'];
				};
				// print_r($montant);
				// die;
				// for ($i = 0; $i < sizeof($this->set($data, 'group_hi4jz21')); $i++) {
				// 	print_r(sizeof($this->set($data, 'group_hi4jz21/MONTANT')));
				// }
				// die;
				$ref = 'ABC' . substr(uniqid(rand(), true), 0, 5);
				$exist = AbcVente::find()->where(['ref_vente' => $ref])->one();
				if ($exist != '') {
					$ref = 'ABC' . substr(uniqid(rand(), true), 0, 5);
				} else {

					$tete_phone = substr($this->set($data, 'NUM_RO_DE_T_L_PHONE_DU_CLIENT'), 0, 3);
					if ($tete_phone == '228') {
						$phone = $this->set($data, 'NUM_RO_DE_T_L_PHONE_DU_CLIENT');
					} else {
						$phone = '228' . $this->set($data, 'NUM_RO_DE_T_L_PHONE_DU_CLIENT');
					}
					$info_marchand = ActeurUser::find()->where(['code_reference' => $this->set($data, '_submitted_by')])->one();
					$info_souscrip = AbcSouscription::find()->where(['customer ' => $phone])->one();
					$info_user = User::find()->where(['id' => $info_souscrip->id_user])->one();

					$new_vente = new AbcVente();
					$new_vente->information_id = $db_information->idInformation;

					$new_vente->key_vente_abc = Yii::$app->security->generateRandomString(32);
					$new_vente->ref_vente = $ref;
					$new_vente->libelle_vente_abc = $designation;
					$new_vente->montant_vente_abc = (string)$montant;
					$new_vente->date_create = date('Y-m-d H:i:s');
					$new_vente->id_acteur_user = $info_marchand->id_acteur_user;
					$new_vente->idclient = $info_souscrip->id_souscription;
					$new_vente->status_vente_abc = 1;
					$new_vente->provenance = "DATAWISE";
					$new_vente->submission_time = new DateTime($this->set($data, '_submission_time'));
					if ($this->set($data, 'INITIALISATION_DE_PAIEMENT') == 'WHATSAPP') {
						$new_vente->type_envoiepaiement = 1;
					} else if ($this->set($data, 'INITIALISATION_DE_PAIEMENT') == 'TELEGRAM') {
						$new_vente->type_envoiepaiement = 2;
					} else if ($this->set($data, 'INITIALISATION_DE_PAIEMENT') == 'SCAN') {
						$new_vente->type_envoiepaiement = 4;
					} else {
						$new_vente->type_envoiepaiement = 3;
					}

					if ($new_vente->save()) {
						$db_information->idCorrespondant = $new_vente->id_vente_abc;
						$db_information->extract = 1;
						if (!$db_information->save()) {
							print_r($db_information->getErrors());
							exit;
						}

						// $info_souscrip->id_user->username;
						// $info_user->username;
						// SEND PAY INFO TO THE CUSTOMER
						// ABSELLER
						// CRD
						// ABC4510 reference vente
						// CRD
						// CF63391 reference marchand
						// ETC
						// 1 id marchand
						// ETC
						// SPT1021 reference souscription
						// CRD
						// ABSELLER

						$code_paiement = "ABSELLERCRD" . $ref . "CRD" . $info_marchand->code_reference . "ETC" . $info_marchand->id_acteur_user . "ETC" . $info_souscrip->ref_souscription . "CRDABSELLER";
						$full_name = $info_user->nom . ' ' . $info_user->prenoms;
						$phone_number = $info_user->username;
						$content = "Cher Client " . $full_name;
						$content .= ".Nous avons bien recu votre commande.";
						$content .= "\nLe Marchand :" . $info_marchand->denomination . ", " . $info_marchand->address_acteur . " a initié une commande depuis votre carte ABCrédit.";
						$content .= "\nDescription :" . $new_vente->libelle_vente_abc;
						$content .= "\nMontant : " . $new_vente->montant_vente_abc . "FCFA";
						$content .= "\n\nPour approuver la commande et autoriser la livraison, cliquez sur le lien et envoyer le contenu :\n";

						// $phone_number = $info_user->username;
						$canal_key = $info_user->canal_key;


						if ($new_vente->type_envoiepaiement == "1") {
							$paiement_url = $this->lien_whatsapp . $code_paiement;
							$content . $paiement_url;
							Utils::send_information($phone_number, $content, "WHATSAPP");
						} else if (in_array($new_vente->type_envoiepaiement, array("2", "3"))) {
							$paiement_url = $this->lien_telegram . $code_paiement;
							$content . $paiement_url;
							if ($canal_key != "") {
								Utils::send_information($canal_key, $content, "TELEGRAM");
							} else {
								Common::hit_sms($content, $phone_number, "ABCrédit");
							}
						} else if ($new_vente->type_envoiepaiement == "4") {
							$code_qr = $this->set($data, 'SCANNER');
							if ($code_qr != "") {
								$carte_abc = AbcCarte::find()->where(['code_qr' => $code_qr, 'status' => 1])->one(); // VERIFIER SI LA CARTE EST VALIDE
								if ($carte_abc != "") {
									if ($info_souscrip->no_carte == $carte_abc->code_reference) {
										$info_histo = AbcHistorique::find()->where(['id_souscription' => $info_souscrip->id_souscription])->one();
										if ($info_histo->montant_restant >= $new_vente->montant_vente_abc) {
											// METTRE A JOUR L'HISTORIQUE DES TRANSACTIONS
											$info_histo->montant_operation += $new_vente->montant_vente_abc;
											$info_histo->montant_restant -= $new_vente->montant_vente_abc;
											if ($info_histo->save()) {
												// ENVOYER UNE NOTIFICATION AU MARCHAND ET AU CLIENT
												$info_marchand->id_user->username; // PHONE NUMBER MARCHAND
												$content_c = "Paiement validé et effectué avec succès auprès du marchand " . $info_marchand->denomination . ", " . $info_marchand->address_acteur;
												$content_m = "Paiement validé et effectué avec succès depuis le compte du client " . $full_name;
												Common::hit_sms($content_c, $phone_number, "ABCrédit"); // CLIENT
												Common::hit_sms($content_m, $info_marchand->id_user->username, "ABCrédit"); // MARCHAND
											}
										} else {
											$content_c = "Votre solde est insuffisant pour éffectuer cette opération.";
											$content_m = "Solde insuffisant sur le compte du client " . $full_name;
											Common::hit_sms($content_c, $phone_number, "ABCrédit"); // CLIENT
											Common::hit_sms($content_m, $info_marchand->id_user->username, "ABCrédit"); // MARCHAND
										}
									}
								} else {
									$content_c = "Votre carte a été désactivé. Veuillez contacter le service client pour plus d'information au (228)92 92 92 85";
									$content_m = "La carte de Mr " . $full_name . " a été désactivé.";
									Common::hit_sms($content_c, $phone_number, "ABCrédit"); // CLIENT
									Common::hit_sms($content_m, $info_marchand->id_user->username, "ABCrédit"); // MARCHAND
								}
							}
						}
					} else {
						print_r($new_vente->getErrors());
						exit;
					}
				}
			}
		} else {
			return [
				'status' => "006",
				'message' => "Le formulaire n'existe pas",
			];
		}
		exit();
	}


	public function actionAll_sell()
	{
		$token = "aawgNYi63TmoYTAnsq83bL";
		if (Yii::$app->request->post()) {
			if (isset($_POST['token']) && $_POST['token'] == $token) {
				return AbcVente::find()->select(['libelle_vente_abc', 'montant_vente_abc', 'date_create', 'datawise_id_marchand', 'client_phone', 'client_phone'])->where(['status_vente_abc' => 1])->all();
			} else {
				return [
					'status' => "006",
					'message' => "Paramètre token requis incorrect",
				];
			}
		}
		exit;
	}

	public static function move_file($source_url, $destination_url)
	{
		set_time_limit(0);
		$info = getimagesize($source_url);
		$quality = 40;
		$image = "";

		//$destination_url = Yii::getAlias('@backend');

		try {
			ini_set('gd.jpeg_ignore_warning', 1);
			if ($info['mime'] == 'image/jpeg') {
				$image = imagecreatefromjpeg($source_url);
				imagejpeg($image, $destination_url, $quality);
			} elseif ($info['mime'] == 'image/gif') {
				$image = imagecreatefromgif($source_url);
				imagegif($image, $destination_url);
			} elseif ($info['mime'] == 'image/png') {
				header('Content-type: image/png');
				$image = imagecreatefrompng($source_url);
				//$infosss = getimagesize($image);
				// var_dump($image);exit;
				imagepng($image, $destination_url, $quality);
			} else {
				return false;
			}
			imagedestroy($source_url);
			return true;

			// if ($image != "") {
			// 	imagejpeg($image, $destination_url, $quality);
			// 	unlink($source_url);
			// 	return true;
			// } else {
			// 	return false;
			// }
		} catch (Exception $e) {
			return false;
		}
	}

	public function actionManage_souscription()
	{
		// token
		// action
		// client_info

		$token = "enXFvQs7GCjCpTPapPQYG5";
		$action = "";
		$actions = array(1, 2, 3);
		if (Yii::$app->request->post()) {
			if (isset($_POST['token']) && $_POST['token'] == $token) {
				if (isset($_POST['action'])) $action = $_POST['action'];
				if (in_array($action, $actions)) {
					if ($action == 1) {
						// CREATE
						$new_model = new AbcSouscription();
						$new_model->key_souscription = Yii::$app->security->generateRandomString(32);
						$new_model->date_create = date('Y-m-d H:i:s');
						$new_model->customer = $_POST['client_info'];
						$new_model->status_souscription = 1;
						if ($new_model->save()) {
							return [
								'status' => "000",
								'message' => "Souscription enregistrée avec succès",
							];
						}
					} else {
						if (isset($_POST['sousciption_id']) && $_POST['sousciption_id'] != "") {
							$model = AbcSouscription::find()->where(['id_souscription ' => $_POST['sousciption_id']])->one();
							if (sizeof($model) > 0) {
								if ($action == 2) {
									// UPDATE
									if (isset($_POST['client_info']) && $_POST['client_info'] != "") {
										$model->customer = $_POST['client_info'];
									}
									$model->status_souscription = 1;
									if ($model->save()) {
										return [
											'status' => "000",
											'message' => "Souscription modifiée avec succès",
										];
									}
								} else if ($action == 3) {
									// DELETE
									$model->status_souscription = 0;
									if ($model->save()) {
										return [
											'status' => "000",
											'message' => "Souscription supprimée avec succès",
										];
									}
								}
							} else {
								return [
									'status' => "006",
									'message' => "Aucune souscription trouvée pour cet ID",
								];
							}
						} else {
							return [
								'status' => "006",
								'message' => "souscription_id manquant",
							];
						}
					}
				} else {
					return [
						'status' => "006",
						'message' => "Action non reconnue",
					];
				}
			} else {
				return [
					'status' => "006",
					'message' => "Token incorrect",
				];
			}
		}
	}
}
