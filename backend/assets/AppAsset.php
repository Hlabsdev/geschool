<?php

/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        //        'css/site.css',
        'theme/css/app.min.css',
        'theme/bundles/prism/prism.css',
        'theme/css/style.css',
        'theme/css/components.css',
        'theme/css/custom.css',
        'theme/bundles/datatables/datatables.min.css',
        'theme/bundles/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css',
    ];




    public $js = [
        'theme/js/app.min.js',
        'theme/bundles/apexcharts/apexcharts.min.js',
        'theme/bundles/prism/prism.js',
        'theme/js/page/index.js',
        'theme/js/scripts.js',
        'theme/js/custom.js',
        'theme/js/main.js',
        'theme/js/jquery.inputmask.bundle.min.js',
        //  'theme/js/form-inputmask.min.js',
        'theme/js/form-inputmask-personnalize.js',
        'theme/bundles/datatables/datatables.min.js',
        'theme/bundles/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js',
    ];


    public $depends = [
        //  'yii\web\YiiAsset',
        //'yii\bootstrap\BootstrapAsset',
    ];
}
