<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "historique_application".
 *
 * @property integer $idhistorique
 * @property integer $idUser
 * @property string $dateConnexion
 * @property string $dateDeconnexion
 * @property integer $etatHistorique
 * @property string $typehistorique
 * @property string $valeurhistorique
 */
class HistoriqueApplication extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'historique_application';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idUser', 'dateConnexion', 'dateDeconnexion', 'etatHistorique', 'typehistorique'], 'required'],
            [['idUser', 'etatHistorique'], 'integer'],
            [['dateConnexion', 'dateDeconnexion'], 'safe'],
            [['valeurhistorique'], 'string'],
            [['typehistorique'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idhistorique' => 'Idhistorique',
            'idUser' => 'Id User',
            'dateConnexion' => 'Date Connexion',
            'dateDeconnexion' => 'Date Deconnexion',
            'etatHistorique' => 'Etat Historique',
            'typehistorique' => 'Typehistorique',
            'valeurhistorique' => 'Valeurhistorique',
        ];
    }
}
