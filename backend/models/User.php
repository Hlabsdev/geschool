<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property integer $idP
 * @property string $id_center
 * @property integer $default_center
 * @property string $nom
 * @property string $prenoms
 * @property string $photo
 * @property string $username
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property integer $role
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property UserAccess[] $userAccesses
 */
class User extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
	public $password; 
	public $confirm_password; 
	public $canal_code; 
        
        //Codes statut
        const STATUS_SMS = "5mCs3WsPnSiLKmgpFOWYlVJVij3bwPwi";
        const STATUS_EMAIL = "fbvtEZktL_0j29WpTsnpx9dPcSm95ceY";

    
    
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idP', 'id_center', 'username', 'auth_key', 'password_hash', 'created_at', 'updated_at'], 'required'],
            [['canal_code', 'username'], 'required','on'=>'forget-password'],
            [['idP', 'default_center', 'role', 'status', 'created_at', 'updated_at'], 'integer'],
            [['id_center'], 'string'],
            [['nom', 'prenoms', 'photo', 'username', 'password_hash', 'password_reset_token', 'email'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idP' => 'Id P',
            'id_center' => 'Id Center',
            'default_center' => 'Default Center',
            'nom' => 'Nom',
            'prenoms' => 'Prenoms',
            'photo' => 'Photo',
            'username' => 'Username',
            'auth_key' => 'Auth Key',
            'password_hash' => 'Password Hash',
            'password_reset_token' => 'Password Reset Token',
            'email' => 'Email',
            'role' => 'Role',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserAccesses()
    {
        return $this->hasMany(UserAccess::className(), ['id_user' => 'id']);
    }
}
