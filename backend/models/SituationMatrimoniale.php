<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "situation_matrimoniale".
 *
 * @property integer $id
 * @property string $libelle
 * @property integer $status
 * @property integer $date_create
 * @property integer $date_update
 * @property integer $create_by
 * @property integer $update_by
 */
class SituationMatrimoniale extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'situation_matrimoniale';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['libelle', 'status'], 'required'],
            [['status', 'date_create', 'date_update', 'create_by', 'update_by'], 'integer'],
            [['libelle'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'libelle' => 'Libelle',
            'status' => 'Status',
            'date_create' => 'Date Create',
            'date_update' => 'Date Update',
            'create_by' => 'Create By',
            'update_by' => 'Update By',
        ];
    }
}
