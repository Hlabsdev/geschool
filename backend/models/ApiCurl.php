<?php

namespace backend\models;


use Yii;

class ApiCurl
{
    // const base_url = "https://testbed.datawise.site";

    // const base_url = "http://192.168.20.11/geschool";
    const base_url = "http://localhost:444/geschoolh";

    const acces_token = "SzrQt6TazKv2ayDHqs754qvvAvD";
    const url = ApiCurl::base_url . "/api/web/school_v1/";


    public static function ApiGet(string $url, array $data = [])
    {
        set_time_limit(0);
    }


    /**
     * @param $curl
     * @param array $data
     * @return mixed
     * @throws \Exception
     */
    public static function ApiPost($curl, array $data = [])
    {
        set_time_limit(0);
        $link = ApiCurl::url . $curl;
        $ch = curl_init($link);
        $data['token'] =  ApiCurl::acces_token;
        $data['id_center'] = Yii::$app->session->get('default_center');
        $data['user_auth_key'] = Yii::$app->user->identity->getAuthKey();
        $data['registration_id'] = "";
        $curlConfig = [
            CURLOPT_URL            => $link,
            CURLOPT_POST           => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTPHEADER => ['Content-Type: multipart/form-data'],
            CURLOPT_CONNECTTIMEOUT => 5,
            CURLOPT_POSTFIELDS     => $data,
        ];

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt_array($ch, $curlConfig);
        $response = curl_exec($ch);

        if ($errno = curl_errno($ch)) {
            $error_message = curl_strerror($errno);
            $message = "cURL error ({$errno}):\n {$error_message}";
            $tab["status"] = "005";
            $tab["message"] = $message;
            $response = json_encode($tab);
        }

        return $response;
    }
}
