<?php

namespace backend\modules\users\controllers;

use Yii;
use backend\modules\users\models\Activite;
use yii\data\ArrayDataProvider;
use backend\controllers\DefaultController;
use backend\controllers\Utils;
use backend\modules\users\models\Categorieactivite;
use yii\filters\AccessControl;

/**
 * ActiviteController implements the CRUD actions for Activite model.
 */
class ActiviteController extends DefaultController
{
    public $page_title = "";
    public $breadcrumb = "";
    public $select_menu = "ACTIVITE";

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['all_activite', 'operation', 'create_activite', 'save_activite'],
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    //Cette fonction permet de lister tous les activites
    public function actionAll_activite()
    {
        //On verifie les droits de l'user
        Utils::Rule("manage_activite", "READ");
        $id_center = Yii::$app->session->get('default_center');
        $annee_scolaire_id = Yii::$app->session->get('annee_scolaire');
        // $response_data = Activite::find()->where(['status' => [1, 2]])->all();
        $response_data = array();


        //On pr�pare les donn�es
        $url_api = Utils::getApiUrl() . "activites/activites";
        $ch = curl_init();
        $curlConfig = array(
            CURLOPT_URL => $url_api,
            CURLOPT_POST => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_CONNECTTIMEOUT => 5,
            CURLOPT_POSTFIELDS => array(
                'token' => Utils::getApiKey(),
                'user_auth_key' => Yii::$app->user->identity->getAuthKey(),
                'id_center' => $id_center,
                'annee_scolaire_id' => $annee_scolaire_id,
            ),
        );
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt_array($ch, $curlConfig);
        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($result);

        // print_r($response);die;

        if (isset($response->status) && ($response->status == '000')) {
            if (sizeof($response->data) > 0) {
                $response_data = $response->data;
            }
        } else {
            $message = Yii::t('app', 'update_error'); // Une erreur s'est produite
            Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
        }

        $donnee['all_activite'] = new ArrayDataProvider([
            'allModels' => $response_data,
            'pagination' => [
                'pageSize' => -1,
            ],
        ]);


        //Initialisation des titres de page
        $title = Yii::t('app', 'all_activite_menu');
        $board = array("add_activite" => $title);
        $this->page_title = $title;
        $this->breadcrumb = Utils::retour($board);

        return $this->render('index', array('donnee' => $donnee));
    }


    //Cette fonction permet de charger le formulaire d'ajout ou de modification diplome
    public function actionCreate_activite()
    {
        $return_info = "";
        $this->layout = false;
        $manage_activite = Utils::have_access("manage_activite");
        if ($manage_activite == "0_0_0_0") {
            return  $return_info;
        }
        $test = explode('_', $manage_activite);

        if ($test[0] == 0 && $test[2] == 0) {
            return  $return_info;
        }
        if (Yii::$app->request->isPost) {

            $info = Yii::$app->request->post();
            $id_center = Yii::$app->session->get('default_center');
            $key = $info['key'];
            $tab_request['status'] = ['1'];
            // $all_categorie_activite = Categorieactivite::find()->where($tab_request)->all();
            $all_categorie_activite = array();
            $all_personnel = array();

            // DEBUT LISTE DES CATEGORIES
            $url_api = Utils::getApiUrl() . "activites/categorieactivites";
            $ch = curl_init();
            $curlConfig = array(
                CURLOPT_URL => $url_api,
                CURLOPT_POST => true,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_CONNECTTIMEOUT => 5,
                CURLOPT_POSTFIELDS => array(
                    'token' => Utils::getApiKey(),
                    'user_auth_key' => Yii::$app->user->identity->getAuthKey(),
                    'id_center' => $id_center,
                ),
            );
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt_array($ch, $curlConfig);
            $result = curl_exec($ch);
            curl_close($ch);
            $response = json_decode($result);

            if (isset($response->status) && ($response->status == '000')) {
                if (sizeof($response->data) > 0) {
                    $all_categorie_activite = $response->data;
                }
            }
            // FIN LISTE DES CATEGORIES


            // DEBUT LISTE DES PERSONNELS
            $url_api = Utils::getApiUrl() . "personnels/personnels";
            $ch = curl_init();
            $curlConfig = array(
                CURLOPT_URL => $url_api,
                CURLOPT_POST => true,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_CONNECTTIMEOUT => 5,
                CURLOPT_POSTFIELDS => array(
                    'token' => Utils::getApiKey(),
                    'user_auth_key' => Yii::$app->user->identity->getAuthKey(),
                    'id_center' => $id_center,
                ),
            );
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt_array($ch, $curlConfig);
            $result = curl_exec($ch);
            curl_close($ch);
            $response = json_decode($result);

            if (isset($response->status) && ($response->status == '000')) {
                if (sizeof($response->data) > 0) {
                    $all_personnel = $response->data;
                }
            }
            // FIN LISTE DES PERSONNELS

            if ($key == '0') {
                $model = new Activite();
            } else {

                $url_api = Utils::getApiUrl() . "activites/activite";
                $ch = curl_init();
                $curlConfig = array(
                    CURLOPT_URL => $url_api,
                    CURLOPT_POST => true,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_CONNECTTIMEOUT => 5,
                    CURLOPT_POSTFIELDS => array(
                        'token' => Utils::getApiKey(),
                        'user_auth_key' => Yii::$app->user->identity->getAuthKey(),
                        'activite_key' => $key,
                        'id_center' => $id_center,
                    ),
                );
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
                curl_setopt_array($ch, $curlConfig);
                $result = curl_exec($ch);
                curl_close($ch);
                $response = json_decode($result);
                //Fin recherche

                if ($response->status == '000') {
                    //On convertie l'objet en model
                    $find_activite = new Activite();
                    $find_activite->load((array)$response->data, '');
                    $model = $find_activite;
                } else {
                    $model = new Activite();
                }
            }

            $this->layout = false;
            $return_info = $this->render('create', array("model" => $model, "all_categorie_activite" => $all_categorie_activite, "all_personnel" => $all_personnel));
        }
        return $return_info;
    }


    //Cette fonction permet de sauvegarder une nouvelle activite 
    public function actionSave_activite()
    {
        $return_info = "";
        $this->layout = false;
        $manage_activite = Utils::have_access("manage_activite");
        if ($manage_activite == "0_0_0_0") {
            return  $return_info;
        }
        $test = explode('_', $manage_activite);

        if ($test[0] == 0 && $test[2] == 0) {
            return  $return_info;
        }
        if (Yii::$app->request->isPost) {
            // $id_center = Yii::$app->session->get('default_center');

            $info = Yii::$app->request->post();

            $key = urldecode($info['key']);
            $categorie_activite  = urldecode($info['idcategorieactivite']);
            $designation_activite = urldecode($info['designationactivite']);
            $date_debut_activite = urldecode($info['datedebutactivite']);
            $date_fin_activite = urldecode($info['datefinactivite']);
            $user_suivi = urldecode($info['idusersuivi']);
            $user_exec = urldecode($info['iduserexec']);
            $centre_annee = urldecode($info['idcentreannee']);
            if ($centre_annee == '111') {
                $centre_annee = Yii::$app->session->get('default_center');
            }


            $fullInfo = array();
            $fullInfo['token'] = Utils::getApiKey();
            $fullInfo['id_center'] = $centre_annee;
            $fullInfo['user_auth_key'] = Yii::$app->user->identity->getAuthKey();
            $fullInfo['idcategorie'] = $categorie_activite;
            $fullInfo['iduserexec'] = $user_exec;
            $fullInfo['idusersuivi'] = $user_suivi;
            $fullInfo['designation'] = $designation_activite;
            $fullInfo['datedebut'] = $date_debut_activite;
            $fullInfo['datefin'] = $date_fin_activite;
            $fullInfo['operation'] = 1;

            if ($key != '0') {
                $fullInfo['activite_key'] = $key;
                $fullInfo['operation'] = 2;
            }


            $url_api = Utils::getApiUrl() . "activites/addactivite";
            $ch = curl_init();
            $curlConfig = array(
                CURLOPT_URL => $url_api,
                CURLOPT_POST => true,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_CONNECTTIMEOUT => 5,
                CURLOPT_POSTFIELDS => $fullInfo,
            );
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt_array($ch, $curlConfig);
            $result = curl_exec($ch);
            curl_close($ch);
            //Fin execution
            $response = json_decode($result);

            // print_r($fullInfo);
            // print_r($response);
            // die;

            if (isset($response->status)) {
                if ($response->status == '000') {
                    $donnee['all_activite'] = new ArrayDataProvider([
                        'allModels' => $response->data,
                        'pagination' => [
                            'pageSize' => -1,
                        ],
                    ]);

                    $return_info = $this->render('liste', array('donnee' => $donnee));
                } else {
                    // $return_info = "1" . "###" . $response->message;
                    $message = $response->message;
                    $verify_date = substr($message, -10);
                    $jour = substr($verify_date, 0, 2);
                    $mois = substr($verify_date, 4, 2);
                    $annee = substr($verify_date, 6, 4);
                    // print_r('terre');
                    // print_r($mois);
                    // die;
                    if (checkdate($mois, $jour, $annee)) {
                        $message = str_replace($verify_date, '', $message);
                    }

                    // if (dateIsValid(verify_date)) {
                    //     $error_msg = error.replace(verify_date, "");
                    // } else {
                    //     $error_msg = error;
                    // }
                    $return_alert = '<div class="alert alert-danger alert-dismissible show fade" style="margin-bottom: 30px">
                    <div class="alert-body">
                      <button class="close" data-dismiss="alert">
                        <span>×</span>
                      </button>
                      ' . Yii::t('app', $response->message) . '
                    </div>
                  </div>';
                    $return_info = '1###' . $return_alert;
                    // $return_info = '1###<div class="alert alert-danger alert-dismissible show fade" style="margin-bottom: 30px"> <div class="alert-body">' . Yii::t('app', $message) . ' </div></div>';
                }
            } else {
                $return_info = "0";
            }
        }
        return $return_info;
    }

    //Cette fonction permet de desactiver/activer & supprimer un activite
    public function actionOperation()
    {
        //On v�rifie les droits de l'user
        Utils::Rule("manage_activite", "DELETE");
        $id_center = Yii::$app->session->get('default_center');

        if (isset($_POST['operation']) != "" and isset($_POST['id']) != "") {

            $etat = $_POST['operation'];
            $keyactivite = $_POST['id'];

            //On recherche la ressource � modifier
            $url_api = Utils::getApiUrl() . "activites/activite";
            $ch = curl_init();
            $curlConfig = array(
                CURLOPT_URL => $url_api,
                CURLOPT_POST => true,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_CONNECTTIMEOUT => 5,
                CURLOPT_POSTFIELDS => array(
                    'token' =>  Utils::getApiKey(),
                    'user_auth_key' => Yii::$app->user->identity->getAuthKey(),
                    'activite_key' => $keyactivite,
                    'id_center' => $id_center,
                ),
            );
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt_array($ch, $curlConfig);
            $result = curl_exec($ch);
            curl_close($ch);
            $response = json_decode($result);
            //Fin recherche

            if ($response->status == '000') {
                //Execution de l'api pour la modification
                $url_api = Utils::getApiUrl() . "activites/deleteactivite";
                $ch = curl_init();
                $curlConfig = array(
                    CURLOPT_URL => $url_api,
                    CURLOPT_POST => true,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_CONNECTTIMEOUT => 5,
                    CURLOPT_POSTFIELDS => array(
                        'token' => Utils::getApiKey(),
                        'activite_key' => $keyactivite,
                        'operation' => $etat,
                        'user_auth_key' => Yii::$app->user->identity->getAuthKey(),
                        'id_center' => $id_center,
                    ),
                );
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
                curl_setopt_array($ch, $curlConfig);
                $result = curl_exec($ch);
                curl_close($ch);
                //Fin execution

                $response = json_decode($result);

                if ($response->status == '000') {

                    //$message=$response->message;
                    if ($etat == 1) {
                        $message = Yii::t('app', 'active_activite_success');
                    } else if ($etat == 2) {
                        $message = Yii::t('app', 'desactive_activite_success');
                    } else if ($etat == 3) {
                        $message = Yii::t('app', 'delete_activite_success');
                    }
                    Yii::$app->getSession()->setFlash('success', Yii::t('app', $message));
                } else {
                    $message = Yii::t('app', 'update_error');
                    Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
                }
            } else {
                $message = Yii::t('app', 'update_error');
                Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
            }
        } else {
            $message = Yii::t('app', 'update_error');
            Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
        }
    }
}
