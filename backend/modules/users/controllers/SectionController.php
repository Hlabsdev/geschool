<?php

namespace backend\modules\users\controllers;

use Yii;
use backend\modules\users\models\Section;
use yii\data\ArrayDataProvider;
use backend\controllers\DefaultController;
use backend\controllers\Utils;
use backend\models\ApiCurl;
use backend\modules\users\models\Resection;
use yii\filters\AccessControl;

/**
 * SectionController implements the CRUD actions for section model.
 */
class SectionController extends DefaultController
{
    public $page_title = "";
    public $breadcrumb = "";
    // public $select_menu = "SECTION";
    public $select_menu = "PARAMSYSTEME";

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['all_section', 'operation', 'create_section', 'save_section'],
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    //Cette fonction permet de lister toutes les sections
    public function actionAll_section()
    {
        //On verifie les droits de l'user
        Utils::Rule("manage_section", "READ");
        $data = array();
        $response_data = array();

        $apicurl = new ApiCurl();
        //On pr�pare les donn�es
        $response = $apicurl->ApiPost('sections/sections', $data);
        $decode = json_decode($response);
        $status = '';
        if (isset($decode->status)) $status = $decode->status;

        // print_r($decode);
        // die;


        if ($status == '000') {
            $response_data = $decode->data;
        } else if ($status == '001') {
            // $message = Yii::t('app', $decode->message);
            // Yii::$app->getSession()->setFlash('info', Yii::t('app', $message));
            $response_data = array();
        } else {
            $message = Yii::t('app', 'update_error'); // Une erreur s'est produite
            Yii::$app->getSession()->setFlash('danger', Yii::t('app', $message));
        }

        $donnee['all_section'] = new ArrayDataProvider([
            'allModels' => $response_data,
            'pagination' => [
                'pageSize' => -1,
            ],
        ]);

        //Initialisation des titres de page
        $title = Yii::t('app', 'all_section_menu');
        $board = array("add_section" => $title);
        $this->page_title = $title;
        $this->breadcrumb = Utils::retour($board);

        return $this->render('index', array('donnee' => $donnee));
    }

    //Cette fonction permet de charger le formulaire d'ajout ou de modification d'une section
    public function actionCreate_section()
    {
        $return_info = "";
        $this->layout = false;
        $manage_section = Utils::have_access("manage_section");
        if ($manage_section == "0_0_0_0") {
            return  $return_info;
        }
        $test = explode('_', $manage_section);

        if ($test[0] == 0 && $test[2] == 0) {
            return  $return_info;
        }
        if (Yii::$app->request->isPost) {
            $info = Yii::$app->request->post();
            $key = $info['key'];
            $data = array();
            $all_typeformation = array();
            $apicurl = new ApiCurl();

            $response = $apicurl->ApiPost('typeformations/typeformations', $data);
            $decode = json_decode($response);
            $status = $decode->status;
            if ($status == '000') {
                $all_typeformation = $decode->data;
            }

            if ($key == '0') {
                $model = new Section();
            } else {
                $data['section_key'] = $key;

                $response = $apicurl->ApiPost('sections/section', $data);
                $decode = json_decode($response);
                $status = $decode->status;
                //Fin recherche

                if ($status == '000') {
                    //On convertie l'objet en model
                    $find_typedecoupage = new Section();
                    $find_typedecoupage->load((array)$decode->data, '');
                    $model = $find_typedecoupage;
                    $model->idtypeformation = $decode->data->idtypeformation;
                } else {
                    $model = new Section();
                }
            }

            $this->layout = false;
            $return_info = $this->render('create', array("model" => $model, "all_typeformation" => $all_typeformation));
        }
        return $return_info;
    }

    //Cette fonction permet de sauvegarder une section
    public function actionSave_section()
    {
        $return_info = "";
        $this->layout = false;
        $manage_section = Utils::have_access("manage_section");
        if ($manage_section == "0_0_0_0") {
            return  $return_info;
        }
        $test = explode('_', $manage_section);

        if ($test[0] == 0 && $test[2] == 0) {
            return  $return_info;
        }
        if (Yii::$app->request->isPost) {
            $info = Yii::$app->request->post();

            $key = urldecode($info['key']);
            $designation = urldecode($info['designation']);
            $id_type_formation = urldecode($info['id_type_formation']);
            $fullInfo = array();


            $fullInfo['designation'] = $designation;
            $fullInfo['id_type_formation'] = $id_type_formation;
            $fullInfo['operation'] = 1;

            if ($key != '0') {
                $fullInfo['section_key'] = $key;
                $fullInfo['operation'] = 2;
            }


            $apicurl = new ApiCurl();
            $response = $apicurl->ApiPost('sections/addsection', $fullInfo);
            $decode = json_decode($response);
            $status = $decode->status;

            // print_r($decode);
            // die;
            if (isset($status)) {
                if ($status == '000') { // TOUT S'EST BIEN PASSÉ
                    $donnee['all_section'] = new ArrayDataProvider([
                        'allModels' => $decode->data,
                        'pagination' => [
                            'pageSize' => -1,
                        ],
                    ]);

                    $return_info = $this->render('liste', array('donnee' => $donnee));
                } else if ($status == '001' || $status == '003') { // ERREUR UTILISATEUR (PARAMETRE OU CHAMPS MANQUANT DEPUIS LE FORMULAIRE)
                    $return_alert = '<div class="alert alert-danger alert-dismissible show fade" style="margin-bottom: 30px">
                    <div class="alert-body">
                      <button class="close" data-dismiss="alert">
                        <span>×</span>
                      </button>
                      ' . Yii::t('app', $decode->message) . '
                    </div>
                  </div>';
                    $return_info = '1###' . $return_alert;
                } else if ($status == '002') { // ERREUR DEVELOPPEUR (PARAMETRE OU CHAMPS MANQUANT)
                    $return_info = "0"; // UNE ERREUR S'EST PRODUITE ...
                } else {
                    $return_info = "0";
                }
            } else {
                $return_info = "0"; // UNE ERREUR S'EST PRODUITE ...
            }
        }
        return $return_info;
    }

    //Cette fonction permet de desactiver/activer & supprimer une section
    public function actionOperation()
    {
        //On v�rifie les droits de l'user
        Utils::Rule("manage_section", "DELETE");

        if (isset($_POST['operation']) != "" and isset($_POST['id']) != "") {

            $data = array();
            $etat = $_POST['operation'];
            $key_section = $_POST['id'];
            $data['section_key'] = $key_section;


            $apicurl = new ApiCurl();
            //On recherche la ressource � modifier
            $response = $apicurl->ApiPost('sections/section', $data);
            $decode = json_decode($response);
            $status = $decode->status;
            //Fin recherche

            if ($status == '000') {
                $data['operation'] = $etat;
                //Execution de l'api pour la modification
                $response = $apicurl->ApiPost('sections/deletesection', $data);
                $decode = json_decode($response);
                $status = $decode->status;

                if ($status == '000') {

                    $message = $decode->message;
                    Yii::$app->getSession()->setFlash('success', Yii::t('app', $message));
                } else {
                    $message = Yii::t('app', 'update_error');
                    Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
                }
            } else if ($status == '001' || $status == '003') {
                $message = Yii::t('app', $decode->message);
                Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
            } else {
                $message = Yii::t('app', 'update_error');
                Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
            }
        } else {
            $message = Yii::t('app', 'update_error');
            Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
        }
    }
}
