<?php

namespace backend\modules\users\controllers;

use Yii;
use backend\modules\users\models\Typedecoupage;
use yii\data\ArrayDataProvider;
use backend\controllers\DefaultController;
use backend\controllers\Utils;
use yii\filters\AccessControl;

/**
 * TypedecoupageController implements the CRUD actions for typedecoupage model.
 */
class TypedecoupageController extends DefaultController
{
    public $page_title = "";
    public $breadcrumb = "";
    public $select_menu = "PARAMSYSTEME";

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['all_typedecoupage', 'operation', 'create_typedecoupage', 'save_typedecoupage'],
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    //Cette fonction permet de lister tous les typedecoupages
    public function actionAll_typedecoupage()
    {
        //On verifie les droits de l'user
        Utils::Rule("manage_typedecoupage", "READ");
        $id_center = Yii::$app->session->get('default_center');
        $response_data = array();

        //On pr�pare les donn�es
        $url_api = Utils::getApiUrl() . "typedecoupages/typedecoupages";
        $ch = curl_init();
        $curlConfig = array(
            CURLOPT_URL => $url_api,
            CURLOPT_POST => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_CONNECTTIMEOUT => 5,
            CURLOPT_POSTFIELDS => array(
                'token' => Utils::getApiKey(),
                'user_auth_key' => Yii::$app->user->identity->getAuthKey(),
                'id_center' => $id_center,
            ),
        );
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt_array($ch, $curlConfig);
        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($result);


        if (isset($response->status) && ($response->status == '000')) {
            if (sizeof($response->data) > 0) {
                $response_data = $response->data;
            }
        } else {
            $message = Yii::t('app', 'update_error'); // Une erreur s'est produite
            Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
        }

        $donnee['all_typedecoupage'] = new ArrayDataProvider([
            'allModels' => $response_data,
            'pagination' => [
                'pageSize' => -1,
            ],
        ]);

        //Initialisation des titres de page
        $title = Yii::t('app', 'all_typedecoupage_menu');
        $board = array("add_typedecoupage" => $title);
        $this->page_title = $title;
        $this->breadcrumb = Utils::retour($board);

        return $this->render('index', array('donnee' => $donnee));
    }

    //Cette fonction permet de charger le formulaire d'ajout ou de modification typedecoupage
    public function actionCreate_typedecoupage()
    {
        $return_info = "";
        $this->layout = false;
        $manage_typedecoupage = Utils::have_access("manage_typedecoupage");
        if ($manage_typedecoupage == "0_0_0_0") {
            return  $return_info;
        }
        $test = explode('_', $manage_typedecoupage);

        if ($test[0] == 0 && $test[2] == 0) {
            return  $return_info;
        }
        if (Yii::$app->request->isPost) {
            $id_center = Yii::$app->session->get('default_center');

            $info = Yii::$app->request->post();
            $key = $info['key'];

            if ($key == '0') {
                $model = new Typedecoupage();
            } else {
                $url_api = Utils::getApiUrl() . "typedecoupages/typedecoupage";
                $ch = curl_init();
                $curlConfig = array(
                    CURLOPT_URL => $url_api,
                    CURLOPT_POST => true,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_CONNECTTIMEOUT => 5,
                    CURLOPT_POSTFIELDS => array(
                        'token' => Utils::getApiKey(),
                        'user_auth_key' => Yii::$app->user->identity->getAuthKey(),
                        'type_decoupage_key' => $key,
                        'id_center' => $id_center,
                    ),
                );
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
                curl_setopt_array($ch, $curlConfig);
                $result = curl_exec($ch);
                curl_close($ch);
                $response = json_decode($result);
                //Fin recherche

                if ($response->status == '000') {
                    //On convertie l'objet en model
                    $find_typedecoupage = new Typedecoupage();
                    $find_typedecoupage->load((array)$response->data, '');
                    $model = $find_typedecoupage;
                } else {
                    $model = new Typedecoupage();
                }
            }

            $this->layout = false;
            $return_info = $this->render('create', array("model" => $model));
        }
        return $return_info;
    }

    //Cette fonction permet de sauvegarder un type de personnel
    public function actionSave_typedecoupage()
    {
        $return_info = "";
        $this->layout = false;
        $manage_typedecoupage = Utils::have_access("manage_typedecoupage");
        if ($manage_typedecoupage == "0_0_0_0") {
            return  $return_info;
        }
        $test = explode('_', $manage_typedecoupage);

        if ($test[0] == 0 && $test[2] == 0) {
            return  $return_info;
        }
        if (Yii::$app->request->isPost) {
            $id_center = Yii::$app->session->get('default_center');

            $info = Yii::$app->request->post();

            $key = urldecode($info['key']);
            $libelle = urldecode($info['libelle']);
            $fullInfo = array();


            $fullInfo['token'] = Utils::getApiKey();
            $fullInfo['libelle'] = $libelle;
            $fullInfo['user_auth_key'] = Yii::$app->user->identity->getAuthKey();
            $fullInfo['id_center'] = $id_center;
            $fullInfo['operation'] = 1;

            if ($key != '0') {
                $fullInfo['type_decoupage_key'] = $key;
                $fullInfo['operation'] = 2;
            }

            $url_api = Utils::getApiUrl() . "typedecoupages/addtypedecoupage";
            $ch = curl_init();
            $curlConfig = array(
                CURLOPT_URL => $url_api,
                CURLOPT_POST => true,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_CONNECTTIMEOUT => 5,
                CURLOPT_POSTFIELDS => $fullInfo,
            );
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt_array($ch, $curlConfig);
            $result = curl_exec($ch);
            curl_close($ch);
            //Fin execution
            $response = json_decode($result);

            if (isset($response->status)) {
                if ($response->status == '000') {
                    $donnee['all_typedecoupage'] = new ArrayDataProvider([
                        'allModels' => $response->data,
                        'pagination' => [
                            'pageSize' => -1,
                        ],
                    ]);

                    $return_info = $this->render('liste', array('donnee' => $donnee));
                } elseif ($response->status == '001') {
                    // Un element manque sur le formulaire soumis
                    $return_info = "1";
                } elseif ($response->status == '003') {
                    // L'element existe déjà
                    $return_info = "3";
                } else {
                    $return_info = "0";
                }
            } else {
                $return_info = "0";
            }
        }
        return $return_info;
    }

    //Cette fonction permet de desactiver/activer & supprimer un typedecoupage
    public function actionOperation()
    {
        //On v�rifie les droits de l'user
        Utils::Rule("manage_typedecoupage", "DELETE");
        $id_center = Yii::$app->session->get('default_center');

        if (isset($_POST['operation']) != "" and isset($_POST['id']) != "") {

            $etat = $_POST['operation'];
            $keytypedecoupage = $_POST['id'];

            //On recherche la ressource � modifier
            $url_api = Utils::getApiUrl() . "typedecoupages/typedecoupage";
            $ch = curl_init();
            $curlConfig = array(
                CURLOPT_URL => $url_api,
                CURLOPT_POST => true,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_CONNECTTIMEOUT => 5,
                CURLOPT_POSTFIELDS => array(
                    'token' =>  Utils::getApiKey(),
                    'user_auth_key' => Yii::$app->user->identity->getAuthKey(),
                    'type_decoupage_key' => $keytypedecoupage,
                    'id_center' => $id_center,
                ),
            );
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt_array($ch, $curlConfig);
            $result = curl_exec($ch);
            curl_close($ch);
            $response = json_decode($result);
            //Fin recherche

            if ($response->status == '000') {
                //Execution de l'api pour la modification
                $url_api = Utils::getApiUrl() . "typedecoupages/deletetypedecoupage";
                $ch = curl_init();
                $curlConfig = array(
                    CURLOPT_URL => $url_api,
                    CURLOPT_POST => true,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_CONNECTTIMEOUT => 5,
                    CURLOPT_POSTFIELDS => array(
                        'token' => Utils::getApiKey(),
                        'type_decoupage_key' => $keytypedecoupage,
                        'operation' => $etat,
                        'user_auth_key' => Yii::$app->user->identity->getAuthKey(),
                        'id_center' => $id_center,
                    ),
                );
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
                curl_setopt_array($ch, $curlConfig);
                $result = curl_exec($ch);
                curl_close($ch);
                //Fin execution

                $response = json_decode($result);

                if ($response->status == '000') {

                    //$message=$response->message;
                    if ($etat == 1) {
                        $message = Yii::t('app', 'active_typedecoupage_success');
                    } else if ($etat == 2) {
                        $message = Yii::t('app', 'desactive_typedecoupage_success');
                    } else if ($etat == 3) {
                        $message = Yii::t('app', 'delete_typedecoupage_success');
                    }
                    Yii::$app->getSession()->setFlash('success', Yii::t('app', $message));
                } else {
                    $message = Yii::t('app', 'update_error');
                    Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
                }
            } else {
                $message = Yii::t('app', 'update_error');
                Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
            }
        } else {
            $message = Yii::t('app', 'update_error');
            Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
        }
    }
}
