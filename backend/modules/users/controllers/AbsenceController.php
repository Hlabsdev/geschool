<?php

namespace backend\modules\users\controllers;

use api\modules\geschool\v1\models\Personnel;
use backend\modules\users\models\Typeabsence;
use Yii;
use backend\modules\users\models\Absence;
use yii\data\ArrayDataProvider;
use backend\controllers\DefaultController;
use backend\controllers\Utils;
use backend\models\ApiCurl;
use backend\modules\users\models\User;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;

/**
 * AbsenceController implements the CRUD actions for absence model.
 */
class AbsenceController extends DefaultController
{
    public $page_title = "";
    public $breadcrumb = "";
    // public $select_menu = "ABSENCEPERSONEL";
    public $select_menu = "ADMIN";

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['add_absence', 'all_absence', 'operation', 'create_absence', 'center_module', 'checking_center', 'save_absence'],
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    //Cette fonction permet de lister toutes les absences d'un centre
    public function actionAll_absence()
    {
        //On verifie les droits de l'user
        Utils::Rule("manage_absence", "READ");
        $response_data = array();

        $apicurl = new ApiCurl();
        $response = $apicurl->ApiPost('absences/absences', []);
        $decode = json_decode($response);
        $status = $decode->status;

        if (isset($status) && ($status == '000')) {
            if (sizeof($decode->data) > 0) {
                $response_data = $decode->data;
            }
            // print_r('toto');die;
        } elseif (isset($status) && ($status == '001')) {
            //Utilisateur n'as pas droit � la ressource
            return Yii::$app->getResponse()->redirect("account");
        } else {
            $message = Yii::t('app', 'update_error'); // Une erreur s'est produite
            Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
        }

        $donnee['all_absence'] = new ArrayDataProvider([
            'allModels' => $response_data,
            'pagination' => [
                'pageSize' => -1,
            ],
        ]);

        //Initialisation des titres de page
        $title = Yii::t('app', 'all_absence_personnel_menu');
        $board = array("add_absence_personnel" => $title);
        $this->page_title = $title;
        $this->breadcrumb = Utils::retour($board);

        return $this->render('index', array('donnee' => $donnee));
    }


    //Cette fonction permet de charger le formulaire  de modification d'une absence
    public function actionCreate_absence()
    {
        $return_info = "";
        $data = array();
        $this->layout = false;
        $manage_absence = Utils::have_access("manage_absence");
        if ($manage_absence == "0_0_0_0") {
            return  $return_info;
        }
        $test = explode('_', $manage_absence);

        if ($test[2] == 0) {
            return  $return_info;
        }


        if (Yii::$app->request->isPost) {

            /* Début Liste des types d'absence */
            $apicurl = new ApiCurl();
            $response = $apicurl->ApiPost('typeabsences/typeabsences', $data);
            $decode = json_decode($response);
            $status = $decode->status;
            // $message = $decode->message;
            if ($status == '000') {
                $all_typeabsence = $decode->data;
            }
            /* Fin Liste des types d'absence */


            /* Début Liste du personnel */
            $response = $apicurl->ApiPost('personnels/personnels', $data);
            $decode = json_decode($response);
            $status = $decode->status;
            // $message = $decode->message;
            if ($status == '000') {
                $all_personnel = $decode->data;
            }
            /* Fin Liste du personnel */

            $tab_personnel_result = array();

            if (sizeof($all_personnel) > 0) {
                $tab_result = array();
                foreach ($all_personnel as $line) {
                    // $user = User::find()->where(['id' => $line->iduser])->one();
                    $personnel = Personnel::find()->where(['status' => [1, 2], 'iduser' => $line->id])->one();

                    $tab_result['iduser'] = $personnel->keypersonnel;
                    $tab_result['nom'] = $line->nom;
                    $tab_result['prenoms'] = $line->prenoms;
                    $tab_result['info_personnel'] = $line->nom . ' ' . $line->prenoms;
                    $tab_personnel_result[] = $tab_result;
                }
            }

            $info = Yii::$app->request->post();
            $key = $info['key'];

            if ($key == '0') {
                $model = new Absence;
            } else {

                $data['abscence_key'] = $key;

                $response = $apicurl->ApiPost('absences/absence', $data);
                $decode = json_decode($response);
                $status = $decode->status;
                //Fin recherche


                if ($status == '000') {
                    //On convertie l'objet en model
                    $find_absence = new Absence();
                    $find_absence->load((array)$decode->data, '');
                    $model = $find_absence;
                } else {
                    $model = new Absence;
                }
            }

            $this->layout = false;
            return $this->render('create', array('model' => $model, 'all_typeabsence' => $all_typeabsence, 'all_personnel' => $tab_personnel_result));
        }

        return $return_info;
    }


    //Cette fonction permet de sauvegarder une nouvelle absence 
    public function actionSave_absence()
    {
        $return_info = "";
        $this->layout = false;
        $manage_absence = Utils::have_access("manage_absence");
        if ($manage_absence == "0_0_0_0") {
            return  $return_info;
        }
        $test = explode('_', $manage_absence);

        if ($test[0] == 0 && $test[2] == 0) {
            return  $return_info;
        }
        if (Yii::$app->request->isPost) {
            $id_center = Yii::$app->session->get('default_center');

            $info = Yii::$app->request->post();

            // print_r($info['type_absence']);
            // die;


            $key = urldecode($info['key']);
            $motif = urldecode($info['motif']);
            $date_demande = urldecode($info['date_demande']);
            $date_debut = urldecode($info['date_debut']);
            $date_fin = urldecode($info['date_fin']);
            $personnel = urldecode($info['personnel']);
            $type_absence = urldecode($info['type_absence']);
            $heure_debut = urldecode($info['heure_debut']);
            $heure_fin = urldecode($info['heure_fin']);
            $coordonnees = urldecode($info['coordonnees']);
            $lieu_destinantion = urldecode($info['lieu_destinantion']);
            $heure_a_rattraper = urldecode($info['heure_a_rattraper']);


            $fullInfo = array();
            $fullInfo['token'] = Utils::getApiKey();
            $fullInfo['motif'] = $motif;
            $fullInfo['date_demande'] = $date_demande;
            $fullInfo['date_debut'] = $date_debut;
            $fullInfo['date_fin'] = $date_fin;
            $fullInfo['personnel'] = $personnel;
            $fullInfo['type_absence'] = $type_absence;
            $fullInfo['heure_debut'] = $heure_debut;
            $fullInfo['heure_fin'] = $heure_fin;
            $fullInfo['coordonnees'] = $coordonnees;
            $fullInfo['lieu_destinantion'] = $lieu_destinantion;
            $fullInfo['heure_a_rattraper'] = $heure_a_rattraper;
            $fullInfo['operation'] = 1;
            $fullInfo['user_auth_key'] = Yii::$app->user->identity->getAuthKey();
            $fullInfo['id_center'] = $id_center;

            if ($key != '0') {
                $fullInfo['abscence_key'] = $key;
                $fullInfo['operation'] = 2;
            }

            $apicurl = new ApiCurl();
            $response = $apicurl->ApiPost('absences/addabsence', $fullInfo);
            $decode = json_decode($response);
            $status = $decode->status;

            if (isset($status)) {
                if ($status == '000') { // TOUT S'EST BIEN PASSÉ
                    $donnee['all_absence'] = new ArrayDataProvider([
                        'allModels' => $decode->data,
                        'pagination' => [
                            'pageSize' => -1,
                        ],
                    ]);

                    $return_info = $this->render('liste', array('donnee' => $donnee));
                } else if ($status == '001') { // ERREUR UTILISATEUR (PARAMETRE OU CHAMPS MANQUANT DEPUIS LE FORMULAIRE)
                    $return_alert = '<div class="alert alert-danger alert-dismissible show fade" style="margin-bottom: 30px">
                    <div class="alert-body">
                      <button class="close" data-dismiss="alert">
                        <span>×</span>
                      </button>
                      ' . Yii::t('app', $decode->message) . '
                    </div>
                  </div>';
                    $return_info = '1###' . $return_alert;
                    // $return_info = '1###<div class="alert alert-danger alert-dismissible show fade" style="margin-bottom: 30px"> <div class="alert-body">' . Yii::t('app', $decode->message) . ' </div></div>';
                } else if ($status == '002') { // ERREUR DEVELOPPEUR (PARAMETRE OU CHAMPS MANQUANT)
                    $return_info = "0"; // UNE ERREUR S'EST PRODUITE ...
                }
            } else {
                $return_info = "0"; // UNE ERREUR S'EST PRODUITE ...
            }
        }
        return $return_info;
    }


    //Cette fonction permet de  supprimer une absence
    public function actionOperation()
    {
        //On v�rifie les droits de l'user
        Utils::Rule("manage_absence", "DELETE");
        if (isset($_POST['operation']) != "" and isset($_POST['id']) != "") {

            $etat = $_POST['operation'];
            $keyabsence = $_POST['id'];

            $fullInfo = array(
                'abscence_key' => $keyabsence,
            );

            //On recherche la ressource � modifier

            $apicurl = new ApiCurl();
            $response = $apicurl->ApiPost('absences/absence', $fullInfo);
            $decode = json_decode($response);
            $status = $decode->status;
            //Fin recherche

            if ($status == '000') {
                //Execution de l'api pour la modification
                $fullInfo['operation'] = $etat;
                $response = $apicurl->ApiPost('absences/deleteabsence', $fullInfo);
                $decode = json_decode($response);
                $status = $decode->status;
                //Fin execution

                if ($status == '000') {
                    if ($etat == 3) {
                        $message = Yii::t('app', 'delete_absence_success');
                    }
                    Yii::$app->getSession()->setFlash('success', Yii::t('app', $message));
                } else {
                    $message = Yii::t('app', 'update_error');
                    Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
                }
            } else {
                $message = Yii::t('app', 'update_error1');
                Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
            }
        } else {
            $message = Yii::t('app', 'update_error');
            Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
        }
    }


    /**
     * Lists all absence models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Absence::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single absence model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new absence model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Absence();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing absence model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing absence model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the absence model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Absence the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Absence::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
