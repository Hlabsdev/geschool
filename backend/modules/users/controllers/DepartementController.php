<?php

namespace backend\modules\users\controllers;

use Yii;
use backend\modules\users\models\Departement;
use yii\data\ArrayDataProvider;
use backend\controllers\DefaultController;
use backend\controllers\Utils;
use yii\filters\AccessControl;

/**
 * DepartementController implements the CRUD actions for departement model.
 */
class DepartementController extends DefaultController
{
    public $page_title = "";
    public $breadcrumb = "";
    // public $select_menu = "DEPARTEMENT";
    public $select_menu = "PARAMSYSTEME";

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['all_departement', 'operation', 'create_departement', 'save_departement'],
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    //Cette fonction permet de lister tous les departements
    public function actionAll_departement()
    {
        //On verifie les droits de l'user
        Utils::Rule("manage_departement", "READ");
        $id_center = Yii::$app->session->get('default_center');
        $response_data = array();

        //On pr�pare les donn�es
        $url_api = Utils::getApiUrl() . "departements/departements";
        $ch = curl_init();
        $curlConfig = array(
            CURLOPT_URL => $url_api,
            CURLOPT_POST => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_CONNECTTIMEOUT => 5,
            CURLOPT_POSTFIELDS => array(
                'token' => Utils::getApiKey(),
                'user_auth_key' => Yii::$app->user->identity->getAuthKey(),
                'id_center' => $id_center,
            ),
        );
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt_array($ch, $curlConfig);
        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($result);


        if (isset($response->status) && ($response->status == '000')) {
            if (sizeof($response->data) > 0) {
                $response_data = $response->data;
            }
        } else {
            $message = Yii::t('app', 'update_error'); // Une erreur s'est produite
            Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
        }

        $donnee['all_departement'] = new ArrayDataProvider([
            'allModels' => $response_data,
            'pagination' => [
                'pageSize' => -1,
            ],
        ]);

        //Initialisation des titres de page
        $title = Yii::t('app', 'all_departement_menu');
        $board = array("add_departement" => $title);
        $this->page_title = $title;
        $this->breadcrumb = Utils::retour($board);

        return $this->render('index', array('donnee' => $donnee));
    }

    //Cette fonction permet de charger le formulaire d'ajout ou de modification d'une departement
    public function actionCreate_departement()
    {
        $return_info = "";
        $this->layout = false;
        $manage_departement = Utils::have_access("manage_departement");
        if ($manage_departement == "0_0_0_0") {
            return  $return_info;
        }
        $test = explode('_', $manage_departement);

        if ($test[0] == 0 && $test[2] == 0) {
            return  $return_info;
        }
        if (Yii::$app->request->isPost) {
            $id_center = Yii::$app->session->get('default_center');

            $url_api = Utils::getApiUrl() . "sections/sections";
            $ch = curl_init();
            $curlConfig = array(
                CURLOPT_URL => $url_api,
                CURLOPT_POST => true,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_CONNECTTIMEOUT => 5,
                CURLOPT_POSTFIELDS => array(
                    'token' => Utils::getApiKey(),
                    'user_auth_key' => Yii::$app->user->identity->getAuthKey(),
                    'id_center' => $id_center,
                ),
            );
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt_array($ch, $curlConfig);
            $result = curl_exec($ch);
            curl_close($ch);
            $response = json_decode($result);
            $all_section = $response->data;

            $info = Yii::$app->request->post();
            $key = $info['key'];

            if ($key == '0') {
                $model = new Departement();
            } else {
                $url_api = Utils::getApiUrl() . "departements/departement";
                $ch = curl_init();
                $curlConfig = array(
                    CURLOPT_URL => $url_api,
                    CURLOPT_POST => true,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_CONNECTTIMEOUT => 5,
                    CURLOPT_POSTFIELDS => array(
                        'token' => Utils::getApiKey(),
                        'user_auth_key' => Yii::$app->user->identity->getAuthKey(),
                        'departement_key' => $key,
                        'id_center' => $id_center,
                    ),
                );
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
                curl_setopt_array($ch, $curlConfig);
                $result = curl_exec($ch);
                curl_close($ch);
                $response = json_decode($result);
                //Fin recherche

                if ($response->status == '000') {
                    //On convertie l'objet en model
                    $find_typedecoupage = new Departement();
                    $find_typedecoupage->load((array)$response->data, '');
                    $model = $find_typedecoupage;
                } else {
                    $model = new Departement();
                }
            }

            $this->layout = false;
            $return_info = $this->render('create', array("model" => $model, 'all_section' => $all_section));
        }
        return $return_info;
    }

    //Cette fonction permet de sauvegarder un departement
    public function actionSave_departement()
    {
        $return_info = "";
        $this->layout = false;
        $manage_departement = Utils::have_access("manage_departement");
        if ($manage_departement == "0_0_0_0") {
            return  $return_info;
        }
        $test = explode('_', $manage_departement);

        if ($test[0] == 0 && $test[2] == 0) {
            return  $return_info;
        }
        if (Yii::$app->request->isPost) {
            $id_center = Yii::$app->session->get('default_center');

            $info = Yii::$app->request->post();

            $key = urldecode($info['key']);
            $libelle = urldecode($info['libelle']);
            $idsection = urldecode($info['idsection']);
            $fullInfo = array();


            $fullInfo['token'] = Utils::getApiKey();
            $fullInfo['idsection'] = $idsection;
            $fullInfo['libelle'] = $libelle;
            $fullInfo['user_auth_key'] = Yii::$app->user->identity->getAuthKey();
            $fullInfo['id_center'] = $id_center;
            $fullInfo['operation'] = 1;

            if ($key != '0') {
                $fullInfo['departement_key'] = $key;
                $fullInfo['operation'] = 2;
            }

            $url_api = Utils::getApiUrl() . "departements/adddepartement";
            $ch = curl_init();
            $curlConfig = array(
                CURLOPT_URL => $url_api,
                CURLOPT_POST => true,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_CONNECTTIMEOUT => 5,
                CURLOPT_POSTFIELDS => $fullInfo,
            );
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt_array($ch, $curlConfig);
            $result = curl_exec($ch);
            curl_close($ch);
            //Fin execution
            $response = json_decode($result);

            if (isset($response->status)) {
                if ($response->status == '000') {
                    $donnee['all_departement'] = new ArrayDataProvider([
                        'allModels' => $response->data,
                        'pagination' => [
                            'pageSize' => -1,
                        ],
                    ]);

                    $return_info = $this->render('liste', array('donnee' => $donnee));
                } else {
                    $return_info = '1###<div class="alert alert-danger alert-dismissible show fade" style="margin-bottom: 30px"> <div class="alert-body">' . Yii::t('app', $response->message) . ' </div></div>';
                }
            } else {
                $return_info = "0";
            }
        }
        return $return_info;
    }

    //Cette fonction permet de desactiver/activer & supprimer une departement
    public function actionOperation()
    {
        //On v�rifie les droits de l'user
        Utils::Rule("manage_departement", "DELETE");
        $id_center = Yii::$app->session->get('default_center');

        if (isset($_POST['operation']) != "" and isset($_POST['id']) != "") {

            $etat = $_POST['operation'];
            $key_departement = $_POST['id'];

            //On recherche la ressource � modifier
            $url_api = Utils::getApiUrl() . "departements/departement";
            $ch = curl_init();
            $curlConfig = array(
                CURLOPT_URL => $url_api,
                CURLOPT_POST => true,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_CONNECTTIMEOUT => 5,
                CURLOPT_POSTFIELDS => array(
                    'token' =>  Utils::getApiKey(),
                    'user_auth_key' => Yii::$app->user->identity->getAuthKey(),
                    'departement_key' => $key_departement,
                    'id_center' => $id_center,
                ),
            );
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt_array($ch, $curlConfig);
            $result = curl_exec($ch);
            curl_close($ch);
            $response = json_decode($result);
            //Fin recherche

            if ($response->status == '000') {
                //Execution de l'api pour la modification
                $url_api = Utils::getApiUrl() . "departements/deletedepartement";
                $ch = curl_init();
                $curlConfig = array(
                    CURLOPT_URL => $url_api,
                    CURLOPT_POST => true,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_CONNECTTIMEOUT => 5,
                    CURLOPT_POSTFIELDS => array(
                        'token' => Utils::getApiKey(),
                        'departement_key' => $key_departement,
                        'operation' => $etat,
                        'user_auth_key' => Yii::$app->user->identity->getAuthKey(),
                        'id_center' => $id_center,
                    ),
                );
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
                curl_setopt_array($ch, $curlConfig);
                $result = curl_exec($ch);
                curl_close($ch);
                //Fin execution

                $response = json_decode($result);

                if ($response->status == '000') {

                    //$message=$response->message;
                    if ($etat == 1) {
                        $message = Yii::t('app', 'active_departement_success');
                    } else if ($etat == 2) {
                        $message = Yii::t('app', 'desactive_departement_success');
                    } else if ($etat == 3) {
                        $message = Yii::t('app', 'delete_departement_success');
                    }
                    Yii::$app->getSession()->setFlash('success', Yii::t('app', $message));
                } else {
                    $message = Yii::t('app', 'update_error');
                    Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
                }
            } else {
                $message = Yii::t('app', 'update_error');
                Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
            }
        } else {
            $message = Yii::t('app', 'update_error');
            Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
        }
    }
}
