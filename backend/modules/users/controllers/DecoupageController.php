<?php

namespace backend\modules\users\controllers;

use Yii;
use backend\modules\users\models\Decoupage;
use yii\data\ArrayDataProvider;
use backend\controllers\DefaultController;
use backend\controllers\Utils;
use backend\modules\users\models\Typedecoupage;
use yii\filters\AccessControl;

/**
 * DecoupageController implements the CRUD actions for decoupage model.
 */
class DecoupageController extends DefaultController
{
    public $page_title = "";
    public $breadcrumb = "";
    public $select_menu = "PARAMSYSTEME";

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['all_decoupage', 'operation', 'create_decoupage', 'save_decoupage'],
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    //Cette fonction permet de lister tous les decoupages
    public function actionAll_decoupage()
    {
        //On verifie les droits de l'user
        Utils::Rule("manage_decoupage", "READ");
        $id_center = Yii::$app->session->get('default_center');
        // $response_data = Decoupage::find()->where(['status' => [1, 2]])->all();


        //On pr�pare les donn�es
        $url_api = Utils::getApiUrl() . "decoupages/decoupages";
        $ch = curl_init();
        $curlConfig = array(
            CURLOPT_URL => $url_api,
            CURLOPT_POST => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_CONNECTTIMEOUT => 5,
            CURLOPT_POSTFIELDS => array(
                'token' => Utils::getApiKey(),
                'user_auth_key' => Yii::$app->user->identity->getAuthKey(),
                'id_center' => $id_center,
            ),
        );
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt_array($ch, $curlConfig);
        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($result);


        $response_data = array();

        // print_r($response);die;

        if (isset($response->status) && ($response->status == '000')) {
            if (sizeof($response->data) > 0) {
                $response_data = $response->data;
            }
        } else {
            $message = Yii::t('app', 'update_error'); // Une erreur s'est produite
            Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
        }

        $donnee['all_decoupage'] = new ArrayDataProvider([
            'allModels' => $response_data,
            'pagination' => [
                'pageSize' => -1,
            ],
        ]);


        //Initialisation des titres de page
        $title = Yii::t('app', 'all_decoupage_menu');
        $board = array("add_decoupage" => $title);
        $this->page_title = $title;
        $this->breadcrumb = Utils::retour($board);

        return $this->render('index', array('donnee' => $donnee));
    }

    //Cette fonction permet de charger le formulaire d'ajout ou de modification decoupage
    public function actionCreate_decoupage()
    {
        $return_info = "";
        $this->layout = false;
        $manage_decoupage = Utils::have_access("manage_decoupage");
        if ($manage_decoupage == "0_0_0_0") {
            return  $return_info;
        }
        $test = explode('_', $manage_decoupage);

        if ($test[0] == 0 && $test[2] == 0) {
            return  $return_info;
        }
        if (Yii::$app->request->isPost) {
            $id_center = Yii::$app->session->get('default_center');


            $info = Yii::$app->request->post();

            $key = $info['key'];


            if ($key == '0') {

                $model = new Decoupage();
            } else {

                $url_api = Utils::getApiUrl() . "decoupages/decoupage";
                $ch = curl_init();
                $curlConfig = array(
                    CURLOPT_URL => $url_api,
                    CURLOPT_POST => true,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_CONNECTTIMEOUT => 5,
                    CURLOPT_POSTFIELDS => array(
                        'token' => Utils::getApiKey(),
                        'user_auth_key' => Yii::$app->user->identity->getAuthKey(),
                        'decoupage_key' => $key,
                        'id_center' => $id_center,
                    ),
                );
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
                curl_setopt_array($ch, $curlConfig);
                $result = curl_exec($ch);
                curl_close($ch);
                $response = json_decode($result);
                //Fin recherche


                if ($response->status == '000') {
                    //On convertie l'objet en model
                    $find_decoupage = new Decoupage();
                    $find_decoupage->load((array)$response->data, '');
                    $model = $find_decoupage;
                } else {
                    $model = new Decoupage();
                }
            }

            $all_typedecoupage = Typedecoupage::find()->where(['status' => [1]])->all();

            $this->layout = false;
            $return_info = $this->render('create', array("model" => $model, 'all_typedecoupage' => $all_typedecoupage));
        }
        return $return_info;
    }

    //Cette fonction permet de sauvegarder un type de personnel
    public function actionSave_decoupage()
    {
        $return_info = "";
        $this->layout = false;
        $manage_decoupage = Utils::have_access("manage_decoupage");
        if ($manage_decoupage == "0_0_0_0") {
            return  $return_info;
        }
        $test = explode('_', $manage_decoupage);

        if ($test[0] == 0 && $test[2] == 0) {
            return  $return_info;
        }
        if (Yii::$app->request->isPost) {
            $id_center = Yii::$app->session->get('default_center');

            $info = Yii::$app->request->post();

            $key = urldecode($info['key']);
            $type_decoupage = urldecode($info['type_decoupage']);
            $evenement_decoupage = urldecode($info['evenement_decoupage']);
            $datedebut_decoupage = urldecode($info['datedebut_decoupage']);
            $datefin_decoupage = urldecode($info['datefin_decoupage']);

            $fullInfo = array();

            $fullInfo['token'] = Utils::getApiKey();
            $fullInfo['type_decoupage'] = $type_decoupage;
            $fullInfo['evenement_decoupage'] = $evenement_decoupage;
            $fullInfo['datedebut_decoupage'] = $datedebut_decoupage;
            $fullInfo['datefin_decoupage'] = $datefin_decoupage;
            $fullInfo['operation'] = 1;
            $fullInfo['user_auth_key'] = Yii::$app->user->identity->getAuthKey();
            $fullInfo['id_center'] = $id_center;

            if ($key != '0') {
                $fullInfo['decoupage_key'] = $key;
                $fullInfo['operation'] = 2;
            }

            $url_api = Utils::getApiUrl() . "decoupages/adddecoupage";
            $ch = curl_init();
            $curlConfig = array(
                CURLOPT_URL => $url_api,
                CURLOPT_POST => true,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_CONNECTTIMEOUT => 5,
                CURLOPT_POSTFIELDS => $fullInfo,
            );
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt_array($ch, $curlConfig);
            $result = curl_exec($ch);
            curl_close($ch);
            //Fin execution
            $response = json_decode($result);


            if (isset($response->status)) {
                if ($response->status == '000') {
                    $donnee['all_decoupage'] = new ArrayDataProvider([
                        'allModels' => $response->data,
                        'pagination' => [
                            'pageSize' => -1,
                        ],
                    ]);

                    $return_info = $this->render('liste', array('donnee' => $donnee));
                } elseif ($response->status == '001') {
                    // Un element manque sur le formulaire soumis
                    $return_info = "1";
                } elseif ($response->status == '003') {
                    // L'element existe déjà
                    $return_info = "3";
                } else {
                    $return_info = "0";
                }
            } else {
                $return_info = "0";
            }

        }
        return $return_info;
    }

    //Cette fonction permet de desactiver/activer & supprimer un decoupage
    public function actionOperation()
    {
        //On v�rifie les droits de l'user
        Utils::Rule("manage_decoupage", "DELETE");
        $id_center = Yii::$app->session->get('default_center');

        if (isset($_POST['operation']) != "" and isset($_POST['id']) != "") {

            $etat = $_POST['operation'];
            $keydecoupage = $_POST['id'];

            //On recherche la ressource � modifier
            $url_api = Utils::getApiUrl() . "decoupages/decoupage";
            $ch = curl_init();
            $curlConfig = array(
                CURLOPT_URL => $url_api,
                CURLOPT_POST => true,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_CONNECTTIMEOUT => 5,
                CURLOPT_POSTFIELDS => array(
                    'token' =>  Utils::getApiKey(),
                    'user_auth_key' => Yii::$app->user->identity->getAuthKey(),
                    'decoupage_key' => $keydecoupage,
                    'id_center' => $id_center,
                ),
            );
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt_array($ch, $curlConfig);
            $result = curl_exec($ch);
            curl_close($ch);
            $response = json_decode($result);
            //Fin recherche

            if ($response->status == '000') {
                //Execution de l'api pour la modification
                $url_api = Utils::getApiUrl() . "decoupages/deletedecoupage";
                $ch = curl_init();
                $curlConfig = array(
                    CURLOPT_URL => $url_api,
                    CURLOPT_POST => true,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_CONNECTTIMEOUT => 5,
                    CURLOPT_POSTFIELDS => array(
                        'token' => Utils::getApiKey(),
                        'decoupage_key' => $keydecoupage,
                        'operation' => $etat,
                        'user_auth_key' => Yii::$app->user->identity->getAuthKey(),
                        'id_center' => $id_center,
                    ),
                );
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
                curl_setopt_array($ch, $curlConfig);
                $result = curl_exec($ch);
                curl_close($ch);
                //Fin execution

                $response = json_decode($result);

                if ($response->status == '000') {

                    //$message=$response->message;
                    if ($etat == 1) {
                        $message = Yii::t('app', 'active_decoupage_success');
                    } else if ($etat == 2) {
                        $message = Yii::t('app', 'desactive_decoupage_success');
                    } else if ($etat == 3) {
                        $message = Yii::t('app', 'delete_decoupage_success');
                    }
                    Yii::$app->getSession()->setFlash('success', Yii::t('app', $message));
                } else {
                    $message = Yii::t('app', 'update_error');
                    Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
                }
            } else {
                $message = Yii::t('app', 'update_error');
                Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
            }
        } else {
            $message = Yii::t('app', 'update_error');
            Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
        }
    }
}
