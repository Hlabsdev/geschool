<?php

namespace backend\modules\users\controllers;

use Yii;
use backend\modules\users\models\Anneescolaire;
use yii\data\ArrayDataProvider;
use backend\controllers\DefaultController;
use backend\controllers\Utils;
use yii\filters\AccessControl;

/**
 * AnneescolaireController implements the CRUD actions for anneescolaire model.
 */
class AnneescolaireController extends DefaultController
{
    public $page_title = "";
    public $breadcrumb = "";
    public $select_menu = "PARAMSYSTEME";

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['all_anneescolaire', 'operation', 'create_anneescolaire', 'save_anneescolaire'],
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    //Cette fonction permet de lister tous les anneescolaires
    public function actionAll_anneescolaire()
    {
        //On verifie les droits de l'user
        Utils::Rule("manage_anneescolaire", "READ");
        $id_center = Yii::$app->session->get('default_center');
        $response_data = Anneescolaire::find()->where(['status' => [1, 2]])->all();

        //On pr�pare les donn�es
        $url_api = Utils::getApiUrl() . "anneescolaires/anneescolaires";
        $ch = curl_init();
        $curlConfig = array(
            CURLOPT_URL => $url_api,
            CURLOPT_POST => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_CONNECTTIMEOUT => 5,
            CURLOPT_POSTFIELDS => array(
                'token' => Utils::getApiKey(),
                'user_auth_key' => Yii::$app->user->identity->getAuthKey(),
                'id_center' => $id_center,
            ),
        );
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt_array($ch, $curlConfig);
        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($result);

        if (isset($response->data)) {
            $response_data = $response->data;
        }

        $donnee['all_anneescolaire'] = new ArrayDataProvider([
            'allModels' => $response_data,
            'pagination' => [
                'pageSize' => -1,
            ],
        ]);

        //Initialisation des titres de page
        $title = Yii::t('app', 'all_anneescolaire_menu');
        $board = array("add_anneescolaire" => $title);
        $this->page_title = $title;
        $this->breadcrumb = Utils::retour($board);

        return $this->render('index', array('donnee' => $donnee));
    }

    //Cette fonction permet de charger le formulaire d'ajout ou de modification anneescolaire
    public function actionCreate_anneescolaire()
    {
        $return_info = "";
        $this->layout = false;
        $manage_anneescolaire = Utils::have_access("manage_anneescolaire");
        if ($manage_anneescolaire == "0_0_0_0") {
            return  $return_info;
        }
        $test = explode('_', $manage_anneescolaire);

        if ($test[0] == 0 && $test[2] == 0) {
            return  $return_info;
        }
        if (Yii::$app->request->isPost) {
            $id_center = Yii::$app->session->get('default_center');

            $info = Yii::$app->request->post();
            $key = $info['key'];

            if ($key == '0') {
                $model = new Anneescolaire();
            } else {

                $url_api = Utils::getApiUrl() . "anneescolaires/anneescolaire";
                $ch = curl_init();
                $curlConfig = array(
                    CURLOPT_URL => $url_api,
                    CURLOPT_POST => true,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_CONNECTTIMEOUT => 5,
                    CURLOPT_POSTFIELDS => array(
                        'token' => Utils::getApiKey(),
                        'user_auth_key' => Yii::$app->user->identity->getAuthKey(),
                        'annee_scolaire_key' => $key,
                        'id_center' => $id_center,
                    ),
                );
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
                curl_setopt_array($ch, $curlConfig);
                $result = curl_exec($ch);
                curl_close($ch);
                $response = json_decode($result);
                //Fin recherche

                if ($response->status == '000') {
                    //On convertie l'objet en model
                    $find_anneescolaire = new Anneescolaire();
                    $find_anneescolaire->load((array)$response->data, '');
                    $model = $find_anneescolaire;
                } else {
                    $model = new Anneescolaire();
                }
            }

            $this->layout = false;
            $return_info = $this->render('create', array("model" => $model));
        }
        return $return_info;
    }

    //Cette fonction permet de sauvegarder un type de personnel
    public function actionSave_anneescolaire()
    {
        $return_info = "";
        $this->layout = false;
        $manage_anneescolaire = Utils::have_access("manage_anneescolaire");
        if ($manage_anneescolaire == "0_0_0_0") {
            return  $return_info;
        }
        $test = explode('_', $manage_anneescolaire);

        if ($test[0] == 0 && $test[2] == 0) {
            return  $return_info;
        }
        if (Yii::$app->request->isPost) {
            $id_center = Yii::$app->session->get('default_center');

            $info = Yii::$app->request->post();

            $key = urldecode($info['key']);
            $anneedebut = urldecode($info['annee_debut']);
            $anneefin = urldecode($info['annee_fin']);
            $fullInfo = array();


            $fullInfo['token'] = Utils::getApiKey();
            $fullInfo['anneedebut'] = $anneedebut;
            $fullInfo['anneefin'] = $anneefin;
            $fullInfo['user_auth_key'] = Yii::$app->user->identity->getAuthKey();
            $fullInfo['id_center'] = $id_center;
            $fullInfo['operation'] = 1;

            if ($key != '0') {
                $fullInfo['annee_scolaire_key'] = $key;
                $fullInfo['operation'] = 2;
            }

            $url_api = Utils::getApiUrl() . "anneescolaires/addanneescolaire";
            $ch = curl_init();
            $curlConfig = array(
                CURLOPT_URL => $url_api,
                CURLOPT_POST => true,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_CONNECTTIMEOUT => 5,
                CURLOPT_POSTFIELDS => $fullInfo,
            );
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt_array($ch, $curlConfig);
            $result = curl_exec($ch);
            curl_close($ch);
            //Fin execution
            $response = json_decode($result);

            if (isset($response->status)) {
                if ($response->status == '000') {
                    $donnee['all_anneescolaire'] = new ArrayDataProvider([
                        'allModels' => $response->data,
                        'pagination' => [
                            'pageSize' => -1,
                        ],
                    ]);

                    $return_info = $this->render('liste', array('donnee' => $donnee));
                } elseif (isset($response->status) && ($response->status == '001')) {
                    $return_info = "1";
                } else {
                    // print_r($response);
                    $return_info = "0";
                }
            } else {
                $return_info = "0";
            }
        }
        return $return_info;
    }

    //Cette fonction permet de desactiver/activer & supprimer un anneescolaire
    public function actionOperation()
    {
        //On v�rifie les droits de l'user
        Utils::Rule("manage_anneescolaire", "DELETE");
        $id_center = Yii::$app->session->get('default_center');

        if (isset($_POST['operation']) != "" and isset($_POST['id']) != "") {

            $etat = $_POST['operation'];
            $keyanneescolaire = $_POST['id'];

            //On recherche la ressource � modifier
            $url_api = Utils::getApiUrl() . "anneescolaires/anneescolaire";
            $ch = curl_init();
            $curlConfig = array(
                CURLOPT_URL => $url_api,
                CURLOPT_POST => true,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_CONNECTTIMEOUT => 5,
                CURLOPT_POSTFIELDS => array(
                    'token' =>  Utils::getApiKey(),
                    'user_auth_key' => Yii::$app->user->identity->getAuthKey(),
                    'annee_scolaire_key' => $keyanneescolaire,
                    'id_center' => $id_center,
                ),
            );
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt_array($ch, $curlConfig);
            $result = curl_exec($ch);
            curl_close($ch);
            $response = json_decode($result);
            //Fin recherche

            if ($response->status == '000') {
                //Execution de l'api pour la modification
                $url_api = Utils::getApiUrl() . "anneescolaires/clotureranneescolaire";
                $fullInfo = array();

                $fullInfo['token'] = Utils::getApiKey();
                $fullInfo['annee_scolaire_key'] = $keyanneescolaire;
                $fullInfo['user_auth_key'] = Yii::$app->user->identity->getAuthKey();
                $fullInfo['id_center'] = $id_center;

                if ($etat != '111') {
                    $fullInfo['operation'] = $etat;
                    $url_api = Utils::getApiUrl() . "anneescolaires/deleteanneescolaire";
                }

                $ch = curl_init();
                $curlConfig = array(
                    CURLOPT_URL => $url_api,
                    CURLOPT_POST => true,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_CONNECTTIMEOUT => 5,
                    CURLOPT_POSTFIELDS => $fullInfo,
                );
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
                curl_setopt_array($ch, $curlConfig);
                $result = curl_exec($ch);
                curl_close($ch);
                //Fin execution

                $response = json_decode($result);

                if ($response->status == '000') {
                    //$message=$response->message;
                    if ($etat == 1) {
                        $message = Yii::t('app', 'active_anneescolaire_success');
                    } else if ($etat == 2) {
                        $message = Yii::t('app', 'desactive_anneescolaire_success');
                    } else if ($etat == 3) {
                        $message = Yii::t('app', 'delete_anneescolaire_success');
                    } else if ($etat == '111') {
                        $message = Yii::t('app', 'cloture_anneescolaire_success');
                    }
                    Yii::$app->getSession()->setFlash('success', Yii::t('app', $message));
                } else {
                    $message = Yii::t('app', 'update_error');
                    Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
                }
            } else {
                $message = Yii::t('app', 'update_error');
                Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
            }
        } else {
            $message = Yii::t('app', 'update_error');
            Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
        }
    }
}
