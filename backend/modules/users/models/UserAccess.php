<?php

namespace backend\modules\users\models;

use Yii;

/**
 * This is the model class for table "user_access".
 *
 * @property integer $id_user_access
 * @property integer $id_user
 * @property integer $id_user_profil
 * @property integer $etat
 * @property string $date_create
 * @property integer $created_by
 * @property string $date_update
 * @property integer $updated_by
 *
 * @property User $idUser
 * @property UserProfil $idUserProfil
 */
class UserAccess extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_access';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_user', 'id_user_profil', 'etat', 'created_by'], 'required'],
            [['id_user', 'id_user_profil', 'etat', 'created_by', 'updated_by'], 'integer'],
            [['date_create', 'date_update'], 'safe'],
            [['id_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_user' => 'id']],
            [['id_user_profil'], 'exist', 'skipOnError' => true, 'targetClass' => UserProfil::className(), 'targetAttribute' => ['id_user_profil' => 'id_user_profil']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_user_access' => 'Id User Access',
            'id_user' => 'Id User',
            'id_user_profil' => 'Id User Profil',
            'etat' => 'Etat',
            'date_create' => 'Date Create',
            'created_by' => 'Created By',
            'date_update' => 'Date Update',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdUser()
    {
        return $this->hasOne(User::className(), ['id' => 'id_user']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdUserProfil()
    {
        return $this->hasOne(UserProfil::className(), ['id_user_profil' => 'id_user_profil']);
    }
}
