<?php

namespace backend\modules\users\models;

use Yii;

/**
 * This is the model class for table "diplome".
 *
 * @property integer $id
 * @property string $keydiplome
 * @property string $libellediplome
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $create_by
 * @property integer $updated_by
 *
 * @property Personneldiplome[] $personneldiplomes
 */
class Diplome extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'diplome';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['keydiplome', 'libellediplome', 'created_at', 'create_by'], 'required'],
            [['status', 'created_at', 'updated_at', 'create_by', 'updated_by'], 'integer'],
            [['keydiplome'], 'string', 'max' => 32],
            [['libellediplome'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'keydiplome' => 'Keydiplome',
            'libellediplome' => 'Libellediplome',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'create_by' => 'Create By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPersonneldiplomes()
    {
        return $this->hasMany(Personneldiplome::className(), ['iddiplome' => 'id']);
    }
}
