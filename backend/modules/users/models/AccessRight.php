<?php

namespace backend\modules\users\models;

use Yii;

/**
 * This is the model class for table "access_right".
 *
 * @property integer $id_right
 * @property string $lib_right
 */
class AccessRight extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'access_right';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['lib_right'], 'required'],
            [['lib_right'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_right' => 'Id Right',
            'lib_right' => 'Lib Right',
        ];
    }
}
