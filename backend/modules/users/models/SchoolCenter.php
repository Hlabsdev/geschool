<?php

namespace backend\modules\users\models;

use Yii;

/**
 * This is the model class for table "school_center".
 *
 * @property integer $id_center
 * @property string $key_center
 * @property string $denomination_center
 * @property string $localite_center
 * @property integer $sms_disponible
 * @property integer $etat
 * @property string $date_create
 * @property integer $created_by
 * @property string $date_update
 * @property integer $updated_by
 */
class SchoolCenter extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'school_center';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['key_center', 'denomination_center', 'localite_center', 'etat', 'created_by'], 'required'],
            [['sms_disponible', 'etat', 'created_by', 'updated_by'], 'integer'],
            [['date_create', 'date_update'], 'safe'],
            [['key_center'], 'string', 'max' => 50],
            [['denomination_center', 'localite_center'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_center' => 'Id Center',
            'key_center' => 'Key Center',
            'denomination_center' => 'Denomination Center',
            'localite_center' => 'Localite Center',
            'sms_disponible' => 'Sms Disponible',
            'etat' => 'Etat',
            'date_create' => 'Date Create',
            'created_by' => 'Created By',
            'date_update' => 'Date Update',
            'updated_by' => 'Updated By',
        ];
    }
}
