<?php

namespace backend\modules\users\models;

use Yii;

/**
 * This is the model class for table "decoupage".
 *
 * @property integer $id
 * @property integer $idtypedecoupage
 * @property integer $idcentreannee
 * @property string $keydecoupage
 * @property string $evenementdecoupage
 * @property string $datedebutdecoupage
 * @property string $datefindecoupage
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $create_by
 * @property integer $updated_by
 *
 * @property Centreannee $idcentreannee0
 * @property Typedecoupage $idtypedecoupage0
 * @property Enseignement[] $enseignements
 */
class Decoupage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'decoupage';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idtypedecoupage', 'idcentreannee', 'keydecoupage', 'evenementdecoupage', 'datedebutdecoupage', 'datefindecoupage', 'created_at', 'create_by'], 'required'],
            [['idtypedecoupage', 'idcentreannee', 'status', 'created_at', 'updated_at', 'create_by', 'updated_by'], 'integer'],
            [['datedebutdecoupage', 'datefindecoupage'], 'safe'],
            [['keydecoupage'], 'string', 'max' => 32],
            [['evenementdecoupage'], 'string', 'max' => 254],
            [['idcentreannee'], 'exist', 'skipOnError' => true, 'targetClass' => Centreannee::className(), 'targetAttribute' => ['idcentreannee' => 'id']],
            [['idtypedecoupage'], 'exist', 'skipOnError' => true, 'targetClass' => Typedecoupage::className(), 'targetAttribute' => ['idtypedecoupage' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idtypedecoupage' => 'Idtypedecoupage',
            'idcentreannee' => 'Idcentreannee',
            'keydecoupage' => 'Keydecoupage',
            'evenementdecoupage' => 'Evenementdecoupage',
            'datedebutdecoupage' => 'Datedebutdecoupage',
            'datefindecoupage' => 'Datefindecoupage',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'create_by' => 'Create By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdcentreannee0()
    {
        return $this->hasOne(Centreannee::className(), ['id' => 'idcentreannee']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdtypedecoupage0()
    {
        return $this->hasOne(Typedecoupage::className(), ['id' => 'idtypedecoupage']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEnseignements()
    {
        return $this->hasMany(Enseignement::className(), ['iddecoupage' => 'id']);
    }
}
