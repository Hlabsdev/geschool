<?php

use yii\grid\GridView;
use backend\controllers\Utils;

$manage_jourferie = Utils::have_access("manage_jourferie");
$droits = explode('_', $manage_jourferie);


echo GridView::widget([
    'dataProvider' => $donnee['all_jourferie'],
    'layout' => '{items}',
    'showOnEmpty' => false,
    'emptyText' => Utils::emptyContent(),
    'tableOptions' => [
        'class' => 'table table-striped table-bordered table-hover',
        'id' => 'example1',
    ],
    'columns' => [

        [
            'label' => Yii::t('app', 'evenement'),
            'value' =>  function ($data) {
                return $data->evenement;
            },
        ],

        [
            'label' => Yii::t('app', 'datejourferie'),
            'value' =>  function ($data) {
                return Utils::reconversion_date($data->datejourferie);
            },
        ],

        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{update}',
            'headerOptions' => ['width' => '40'],
            'visible' => $droits[2] == 1 ? true : false,
            'buttons' => [
                'update' => function ($url, $data) {
                    return '<a title="' . Yii::t('app', 'updatebutton') . '" href="#" data-toggle="modal" data-target=".Form-modal-lg"  onclick="add_member(\'' . $data->keyjourferie . '\')"><i class="fa fa-edit" style="color:green"></i></a>';
                },
            ],

        ],
        /*[
            'class' => 'yii\grid\ActionColumn',
            'template' => '{active}',
            'headerOptions' => ['width' => '40'],
            'visible' => $droits[3] == 1 ? true : false,
            'buttons' => [
                'active' => function ($url, $data) {

                    if ($data->status == 1) {
                        return '<a title="' . Yii::t('app', 'desactive') . '" href="#" data-toggle="modal" data-target=".bs-example-modal-lg"  onclick="desactive_member(\'' . $data->keyjourferie . '\')"><i class="fa fa-check" style="color:red"></i></a>';
                    } else {
                        return '<a title="' . Yii::t('app', 'active') . '" href="#" data-toggle="modal" data-target=".bs-example-modal-lg"  onclick="active_member(\'' . $data->keyjourferie . '\')"><i class="fa fa-check" style="color:blue"></i></a>';
                    }
                },
            ],

        ],*/
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{delete}',
            'headerOptions' => ['width' => '40'],
            'visible' => $droits[3] == 1 ? true : false,
            'buttons' => [
                'delete' => function ($url, $data) {
                    $firstDate  = new DateTime(date('Y-m-d'));
                    $secondDate = new DateTime($data->datejourferie);
                    $intvl = $firstDate->diff($secondDate, false);
                    if (($intvl->invert == 0) && ($intvl->days > 0)) {
                        $return = '<a title="' . Yii::t('app', 'delete') . '" href="#" data-toggle="modal" data-target=".bs-example-modal-lg"  onclick="delete_member(\'' . $data->keyjourferie . '\')"><i class="fa fa-window-close" style="color:red"></i></a>';
                    } else {
                        $return = '<a title="' . Yii::t('app', 'delete') . '"><i class="fa fa-window-close" style="color:black"></i></a>';
                    }
                    return $return;
                },
            ],

        ]
    ],

]);
