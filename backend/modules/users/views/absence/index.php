<?php


use yii\helpers\Html;
use backend\controllers\Utils;

$manage_absence = Utils::have_access("manage_absence");
$droits = explode('_', $manage_absence);
?>


<div class="card card-body card-block">
    <?= $this->render("/_modal") ?>
    <?= $this->render("_form") ?>

    <div id="successInfo" style="display: none" class="alert alert-success alert-dismissible show fade" style="margin-bottom: 30px">
        <div class="alert-body"> <button class="close" data-dismiss="alert"> <span>×</span> </button>
            <div id="bodyInfo"> <?php echo Yii::t('app', 'success_operation') ?></div>
        </div>
    </div>

    <?php
    if ($droits[0] == 1) {
    ?>
        <div class="buttons">
            <a onclick="add_member('0')" href="#" data-toggle="modal" data-target=".Form-modal-lg" class="btn btn-icon icon-left btn-primary"><i class="far fa-save"></i> <?= Yii::t('app', 'add_absence_personnel_menu') ?></a>
        </div>
    <?php }    ?>

    <div id="contentList">
        <?= $this->render("liste", array('donnee' => $donnee)) ?>
    </div>
</div>




<script>
    function add_member(key_information) {
        // alert(key_information);
        var url_submit = "<?php echo Yii::$app->request->baseUrl ?>/create_absence";
        var url_image = '<center><?= Html::img(Yii::getAlias("@web/theme/img/ajax-loader.gif"), ["width" => "200px", "height" => "200px",]) ?></center>';

        $('#successInfo').hide();
        $('#FormLargeModalContent').hide();
        $('#Formcontent_footer').hide();
        $('#Formcontent_image').show();
        $('#Formcontent_image').html(url_image);
        $('#key_form').val(key_information);
        if (key_information == "0") {
            $('#FormLargeModalLabel').html('<?php echo Yii::t('app', 'add_absence_personnel_titre') ?>');
        } else {
            $('#FormLargeModalLabel').html('<?php echo Yii::t('app', 'update_absence_menu') ?>');
        }

        $.ajax({
            url: url_submit,
            type: "POST",
            data: {
                key: encodeURIComponent(key_information),
            },
            success: function(data) {

                if (data == '') {
                    var error = '<div class="alert alert-danger alert-dismissible show fade" style="margin-bottom: 30px">' +
                        '<div class="alert-body"> <button class="close" data-dismiss="alert"> <span>×</span> </button> <?php echo Yii::t('app', 'noaccess') ?> </div> </div>';
                    $('#Formcontent_image').html(error);
                } else {
                    $('#Formcontent_image').hide();
                    $('#Formcontent_footer').show();
                    $('#FormLargeModalContent').show();
                    $('#FormLargeModalContent').html(data);
                }


            }
        });
    }

    function save_member() {

        var url_submit = "<?php echo Yii::$app->request->baseUrl ?>/save_absence";
        var url_image = '<center><?= Html::img(Yii::getAlias("@web/theme/img/ajax-loader.gif"), ["width" => "200px", "height" => "200px",]) ?></center>';

        var key_form = $("#key_form").val();
        var type_absence = $("#absence-idtypeabsence").val();
        var personnel = $("#absence-iduser").val();

        var motifabsence = $("#absence-motifabsence").val();
        var datedemandeabsence = $("#absence-datedemandeabsence").val();
        var datedebutabsence = $("#absence-datedebutabsence").val();
        var heuredebutabsence = $("#absence-heuredebutabsence").val();
        var datefinabsence = $("#absence-datefinabsence").val();
        var heurefinabsence = $("#absence-heurefinabsence").val();
        var lieudestinantionabsence = $("#absence-lieudestinantionabsence").val();
        var coordonneesabsence = $("#absence-coordonneesabsence").val();
        var heurearattraper = $("#absence-heurearattraper").val();

        if (type_absence != '' || personnel != '' || motifabsence != '' ||
            datedemandeabsence != '' || datedebutabsence != '' || heuredebutabsence != '' ||
            datefinabsence != '' || heurefinabsence != '' || lieudestinantionabsence != '' ||
            coordonneesabsence != '') {
            $('#FormLargeModalContent').hide();
            $('#Formcontent_footer').hide();
            $('#Formcontent_image').show();
            $('#Formcontent_image').html(url_image);

            $.ajax({
                url: url_submit,
                type: "POST",
                data: {
                    key: encodeURIComponent(key_form),
                    type_absence: encodeURIComponent(type_absence),
                    personnel: encodeURIComponent(personnel),
                    motif: encodeURIComponent(motifabsence),
                    date_demande: encodeURIComponent(datedemandeabsence),
                    date_debut: encodeURIComponent(datedebutabsence),
                    heure_debut: encodeURIComponent(heuredebutabsence),
                    date_fin: encodeURIComponent(datefinabsence),
                    heure_fin: encodeURIComponent(heurefinabsence),
                    lieu_destinantion: encodeURIComponent(lieudestinantionabsence),
                    coordonnees: encodeURIComponent(coordonneesabsence),
                    heure_a_rattraper: encodeURIComponent(heurearattraper),
                },
                success: function(data) {

                    var head_back = data.charAt(0);
                    if (head_back.trim() == "0") {
                        var error = '<div class="alert alert-danger alert-dismissible show fade" style="margin-bottom: 30px">' + ' <div class="alert-body">' +
                            '<button class="close" data-dismiss="alert"> <span>×</span> </button> <?php echo Yii::t('app', 'update_error') ?> </div> </div>';

                        $('#Formcontent_image').html(error);
                        $('#Formcontent_footer').show();
                        $('#FormLargeModalContent').show();
                    } else if (head_back.trim() == "1") {

                        error_msg = data.split('###')[1];

                        $('#Formcontent_image').html(error_msg);
                        $('#Formcontent_footer').show();
                        $('#FormLargeModalContent').show();
                    } else {

                        if (key_form == "0") {
                            $('#bodyInfo').html('<?php echo Yii::t('app', 'succes_creation_absence') ?>');
                        } else {
                            <?php Yii::t('app', 'succes_update_absence') ?>
                            $('#bodyInfo').html('<?php echo Yii::t('app', 'succes_update_absence') ?>');
                        }

                        $('#Formcontent_close')[0].click();
                        $('#successInfo').show();
                        $('#contentList').html(data);
                    }

                }
            });
        } else {
            var error = '<div class="alert alert-danger alert-dismissible show fade" style="margin-bottom: 30px">' + ' <div class="alert-body">' +
                '<button class="close" data-dismiss="alert"> <span>×</span> </button> <?php echo Yii::t('app', 'input_empty') ?> </div> </div>';
            $('#Formcontent_image').show();
            $('#Formcontent_image').html(error);
        }

    }

    function delete_member(id) {
        $('#myLargeModalLabel').html('<?php echo Yii::t('app', 'confirm_delete') ?>');
        $('#myLargeModalContent').html('<?php echo Yii::t('app', 'info_delete_absence') ?>');
        document.getElementById('type_operation').value = '3';
        document.getElementById('id_member').value = id;
        document.getElementById('submit_mod').style.display = "block";

    }

    function do_operation() {

        var url = "<?php echo Yii::$app->request->baseUrl ?>/absence_operation";

        var mypostrequest = null;
        if (window.XMLHttpRequest) { // Firefox et autres
            mypostrequest = new XMLHttpRequest();
        } else if (window.ActiveXObject) { // Internet Explorer
            try {
                mypostrequest = new ActiveXObject("Msxml2.XMLHTTP");
            } catch (e) {
                mypostrequest = new ActiveXObject("Microsoft.XMLHTTP");
            }
        }

        mypostrequest.onreadystatechange = function() {
            if (mypostrequest.readyState == 4) {
                if (mypostrequest.status == 200) {
                    document.location.href = location.href;
                }
            }
        }

        var operation = encodeURIComponent(document.getElementById('type_operation').value);
        var id = encodeURIComponent(document.getElementById('id_member').value);

        var parameters = "operation=" + operation + "&id=" + id;

        mypostrequest.open("POST", url, true);
        mypostrequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        mypostrequest.send(parameters);
    }
</script>