<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\controllers\Utils;
use backend\modules\users\models\User;
use backend\modules\users\models\SchoolCenter;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;

$required_sign = Utils::required();
/* @var $this yii\web\View */
/* @var $model backend\modules\users\models\Typeabsence */

?>


<div class="card-body card-block">

    <?php

    echo GridView::widget([
        'dataProvider' => $all_affectation,
        'layout' => '{items}',
        'showOnEmpty' => false,
        'emptyText' => Utils::emptyContent(),
        'tableOptions' => [
            'class' => 'table table-striped table-bordered table-hover',
            'id' => 'affectation_tab',
        ],
        'columns' => [

            [
                'label' => Yii::t('app', 'full_name'),
                'value' =>  function ($data) {
                    $user = User::findOne(['id' => $data->iduser]);
                    return $user->nom . " " . $user->prenoms;
                },
            ],
            [
                'format' => 'html',
                'label' => Yii::t('app', 'affectation'),
                'value' => function ($data) {
                    $center = SchoolCenter::findOne(['id_center' => $data->idcenter]);
                    return $center->denomination_center . "<br>" . Yii::t('app', 'date_debut') . $data->datedebutaffectation . "<br>" . Yii::t('app', 'date_fin') . $data->datefinaffectation;
                },
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{uptade}',
                'headerOptions' => ['width' => '40'],
                // 'visible' => $test[2] == 1 ? true : false,
                'buttons' => [
                    'uptade' => function ($url, $data) {
                        return '<a title="' . Yii::t('app', 'update') . '" href="#" data-toggle="modal" data-target=".Form-modal-lg"  onclick="add_affectation(\'' . $data->keyplanaffectation . '\')"><i class="fa fa-edit" style="color:green"></i></a>';
                    },
                ],

            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{affectation}',
                'headerOptions' => ['width' => '40'],
                // 'visible' => $test[2] == 1 ? true : false,
                'buttons' => [
                    'affectation' => function ($url, $data) {
                        return '<a title="' . Yii::t('app', 'delete') . '" href="#" data-toggle="modal" data-target=".bs-example-modal-lg"  onclick="delete_affectation(\'' . $data->keyplanaffectation . '\')"><i class="fa fa-window-close" style="color:red"></i></a>';
                    },
                ],

            ],
        ],

    ]);

    ?>

</div>