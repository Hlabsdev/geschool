<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\modules\users\models\UserProfil;
use backend\modules\users\models\SchoolCenter;

use backend\controllers\Utils;

$required_sign = Utils::required();


$id_center = Yii::$app->user->identity->id_center;
$test_center = explode(",", $id_center);

if ($id_center == 1) {
	$allcenter = SchoolCenter::find()->where(['etat' => 1])->all();
} else {
	$allcenter = SchoolCenter::find()->where(['etat' => 1, 'id_center' => $test_center])->all();
}


?>


<script type="application/javascript">
	function isNumber(number) {
		var number = number.substr(number.length - 1); // => "1"
		return !isNaN(parseFloat(number)) && isFinite(number);
	}

	function checkProfil(id) {

		//renitialisation du prodil de la personne avec visibilite de la partie profil a selectionner				
		$('#profil_' + id).hide();
		var status = document.getElementById(id).checked;
		if (status == true) {
			$('#profil_' + id).show();
		}
	}

	var loadFile = function(event) {
		var output = document.getElementById('output');
		output.src = URL.createObjectURL(event.target.files[0]);
		output.onload = function() {
			URL.revokeObjectURL(output.src) // free memory
		}
	};
</script>


<style>
	.img_container {
		max-width: 960px;
		margin: 20px auto;
		padding: 20px;
	}

	#output {
		margin: 50px;
	}
</style>

<div class="card">
	<?php $form = ActiveForm::begin([
		'id' => 'form-signup',
		'fieldConfig' => [
			'options' => [
				'tag' => false,
			]
		]
	]); ?>
	<div class="card-body card-block">

		<div class="row form-group">
			<div class="col col-md-3"><label for="text-input" class=" form-control-label"><?= Yii::t('app', 'nom') ?> <?= $required_sign ?></label></div>
			<div class="input-group col-sm-5">
				<div class="input-group-prepend">
					<div class="input-group-text"><i class="fas fa-user"></i></div>
				</div>
				<?= $form->field($model, 'nom')->textInput(["onkeyup" => "if(isNumber(this.value)){this.value= this.value.slice(0,-1);}", 'required' => 'required', 'placeholder' => Yii::t('app', 'eg') . ' : Doe'])->error(false)->label(false); ?>
			</div>


			<div class="col col-md-1"><label for="text-input" class=" form-control-label"><?= Yii::t('app', 'indicegrade') ?> <?= $required_sign ?></label></div>
			<div class="input-group col-sm-3">
				<div class="input-group-prepend">
					<div class="input-group-text"><i class="fas fa-star"></i></div>
				</div>
				<?= $form->field($model_personnel, 'indicegrade')->dropdownList(
					ArrayHelper::map(
						$all_grade,
						'id',
						'libelle'
					),
					['prompt' => Yii::t('app', 'select_indicegrade'), 'required' => true],
					['class' => 'form-control']
				)->error(false)->label(false); ?>
			</div>

		</div>

		<div class="row form-group">
			<div class="col col-md-3"><label for="text-input" class=" form-control-label"><?= Yii::t('app', 'prenoms') ?> <?= $required_sign ?></label></div>
			<div class="input-group col-sm-5">
				<div class="input-group-prepend">
					<div class="input-group-text"><i class="fas fa-user"></i></div>
				</div>
				<?= $form->field($model, 'prenoms')->textInput(["onkeyup" => "if(isNumber(this.value)){this.value= this.value.slice(0,-1);}", 'required' => 'required', 'placeholder' => Yii::t('app', 'eg') . ' : John'])->error(false)->label(false); ?>
			</div>

			<div class="col col-md-1"><label for="text-input" class=" form-control-label"><?= Yii::t('app', 'situationmatrimoniale') ?> <?= $required_sign ?></label></div>
			<div class="input-group col-sm-3">
				<div class="input-group-prepend">
					<div class="input-group-text"><i class="fas fa-user"></i></div>
				</div>
				<?= $form->field($model_personnel, 'situationmatrimoniale')->dropdownList(
					ArrayHelper::map(
						$all_situation_matrimoniale,
						'id',
						'libelle'
					),
					['prompt' => Yii::t('app', 'select_situationm'), 'required' => true],
					['class' => 'form-control']
				)->error(false)->label(false); ?>
			</div>

		</div>

		<div class="row form-group">
			<div class="col col-md-3"><label for="text-input" class=" form-control-label"><?= Yii::t('app', 'phone_number') ?> <?= $required_sign ?></label></div>
			<div class="input-group col-sm-5">
				<div class="input-group-prepend">
					<div class="input-group-text"><i class="fas fa-phone"></i></div>
				</div>
				<?= $form->field($model, 'telephoneuser')->textInput(['required' => 'required', 'class' => 'form-control phone-inputmask'])->error(false)->label(false); ?>
				<?php $form->field($model, 'telephoneuser')->textInput(['required' => 'required', 'class' => 'form-control phone-inputmask', 'pattern' => '^((2289[0-4][0-9]{6})|(22870[0-9]{6})|(2289[6-9][0-9]{6})|(22879[0-9]{6}))$'])->error(false)->label(false); ?>
				<?php $form->field($model, 'telephoneuser')->textInput(['required' => 'required', 'class' => 'form-control', 'placeholder' => Yii::t('app', 'eg') . ' : 228XXXXXXXX', 'pattern' => '^((2289[0-4][0-9]{6})|(22870[0-9]{6})|(2289[6-9][0-9]{6})|(22879[0-9]{6}))$'])->error(false)->label(false); ?>
			</div>

			<div class="col col-md-1"><label for="text-input" class=" form-control-label"><?= Yii::t('app', 'dateembauche') ?> <?= $required_sign ?></label></div>
			<div class="input-group col-sm-3">
				<div class="input-group-prepend">
					<div class="input-group-text"><i class="fas fa-calendar"></i></div>
				</div>
				<?= $form->field($model_personnel, 'dateembauche')->textInput(['type' => 'date', 'required' => 'required'])->error(false)->label(false); ?>
			</div>
		</div>

		<div class="row form-group">
			<div class="col col-md-3"><label for="text-input" class=" form-control-label"><?= Yii::t('app', 'email') ?> <?= $required_sign ?></label></div>
			<div class="input-group col-sm-5">
				<div class="input-group-prepend">
					<div class="input-group-text"><i class="fas fa-envelope"></i></div>
				</div>
				<?= $form->field($model, 'email')->textInput(['type' => 'email', 'required' => 'required', 'placeholder' => Yii::t('app', 'eg') . ' : email@xyz.com'])->error(false)->Label(false); ?>

			</div>

			<div class="col col-md-1"><label for="text-input" class=" form-control-label"><?= Yii::t('app', 'typepersonnel') ?> <?= $required_sign ?></label></div>
			<div class="input-group col-sm-3">
				<div class="input-group-prepend">
					<div class="input-group-text"><i class="fas fa-user"></i></div>
				</div>

				<?= $form->field($model_personnel, 'typepersonnel')->dropdownList(
					ArrayHelper::map(
						$alltype_personnel,
						'id',
						'libelle'
					),
					['prompt' => Yii::t('app', 'select_typepersonnel'), 'required' => true],
					['class' => 'form-control']
				)->error(false)->label(false); ?>
			</div>
		</div>

		<div class="row form-group">
			<div class="col col-md-3"><label for="text-input" class=" form-control-label"><?= Yii::t('app', 'user_name') ?> <?= $required_sign ?></label></div>
			<div class="input-group col-sm-5">
				<div class="input-group-prepend">
					<div class="input-group-text"><i class="fas fa-user"></i></div>
				</div>
				<?= $form->field($model, 'username')->textInput(['type' => 'text', 'required' => 'required', 'placeholder' => Yii::t('app', 'eg') . ' : jhon21'])->error(false)->Label(false); ?>

			</div>

			<div class="col col-md-1"><label for="text-input" class=" form-control-label"><?= Yii::t('app', 'sectionconcerne') ?> <?= $required_sign ?></label></div>
			<div class="input-group col-sm-3">
				<div class="input-group-prepend">
					<div class="input-group-text"><i class="fas fa-user"></i></div>
				</div>

				<?= $form->field($model_personnel, 'sectionconcerne')->dropdownList(
					ArrayHelper::map(
						$allsection,
						'id',
						'designation'
					),
					['prompt' => Yii::t('app', 'select_section'), 'required' => true],
					['class' => 'form-control']
				)->error(false)->label(false); ?>
			</div>
		</div>

		<?php if ($type == "CREATE") { ?>

			<div class="row form-group">
				<div class="col col-md-3"><label for="text-input" class=" form-control-label"><?= Yii::t('app', 'user_password') ?> <?= $required_sign ?></label></div>
				<div class="input-group col-sm-5">
					<div class="input-group-prepend">
						<div class="input-group-text"><i class="fas fa-lock"></i></div>
					</div>
					<?= $form->field($model, 'password')->textInput(['required' => 'required', 'type' => 'password'])->error(false)->Label(false); ?>
				</div>
			</div>

			<div class="row form-group">
				<div class="col col-md-3"><label for="text-input" class=" form-control-label"><?= Yii::t('app', 'cuser_password') ?> <?= $required_sign ?></label></div>
				<div class="input-group col-sm-5">
					<div class="input-group-prepend">
						<div class="input-group-text"><i class="fas fa-lock"></i></div>
					</div>
					<?= $form->field($model, 'password2')->textInput(['required' => 'required', 'type' => 'password'])->error(false)->Label(false); ?>
				</div>
			</div>

		<?php }
		if (sizeof($allcenter) > 0) {
			$all_existProfil = explode(",", $userProfil);
		?>
			<div class="row form-group">

				<div class="col-sm-8" id="module_page" style="margin-top: 30px;padding-right: 0px">
					<table class="table table-bordered table-striped">
						<tr>
							<th></th>
							<th style="width:50%"><?php echo Yii::t('app', 'denomination_center') ?></th>
							<th style="width:45%"><?php echo Yii::t('app', 'user_profil') ?></th>
						</tr>

						<?php foreach ($allcenter as $infoCenter) {

							$show_option = '<option value="" >' . Yii::t('app', 'select_profil') . '</option>';
							$is_visible = false;

							//recuperation du profil
							$center_profil = UserProfil::find()->where(['etat_user_profil' => 1, 'id_center' => $infoCenter->id_center])->all();
							if (sizeof($center_profil) > 0) {
								foreach ($center_profil as $infoprofil) {
									$selected = "selected='false'";
									if (in_array($infoprofil->id_user_profil, $all_existProfil)) {
										$selected = "selected='true'";
										$is_visible = true;
									}
									$show_option .= '<option value="' . $infoprofil->id_user_profil . '" ' . $selected . '>' . $infoprofil->nameProfil . '</option>';
								}
							}

						?>
							<tr>
								<td><input type="checkbox" id="<?= $infoCenter->id_center ?>" name="<?= 'geschoolcenter_' . $infoCenter->id_center ?>" value="1" onchange="checkProfil(this.id)" <?php if ($is_visible) {
																																																		echo 'checked="true"';
																																																	} ?> /></td>
								<td><?= Yii::t('app', $infoCenter->denomination_center); ?></td>
								<td>
									<div id="<?= 'profil_' . $infoCenter->id_center ?>" style="<?php if (!$is_visible) {
																									echo 'display:none';
																								} ?>">
										<select class="form-control" name="<?= 'profilcenter_' . $infoCenter->id_center ?>">
											<?= $show_option ?>
										</select>
									</div>
								</td>
							</tr>
						<?php } ?>


					</table>

				</div>

				<?php
				$profile_src = 'default_user_picture.png';
				$this_user = $model;
				if ($this_user->photo != null && $this_user->photo != "") {
					$profile_src = $this_user->photo;
				}

				?>

				<div class="col-sm-4">
					<div class="img_container">
						<h3><?= Yii::t('app', 'pick_picture') ?></h3>
						<?= $form->field($model, 'photo')->fileInput(['id' => 'imageUpload', 'accept' => "image/*", 'onchange' => "loadFile(event)"])->error(false)->Label(false); ?>
						<img id="output" width="250" height="250" src="<?= Yii::$app->homeUrl ?>uploads/<?= $profile_src ?>" />
					</div>
				</div>

			</div>
		<?php } ?>
	</div>
	<div class="card-footer">
		<?= Utils::resetbtn(); ?>
		<?= Html::submitButton('<i class="fa fa-dot-circle-o"></i> ' . Yii::t('app', 'submitbutton'), ['class' => 'btn btn-sm btn-success', 'name' => 'updatebutton']) ?>
	</div>
	<?php ActiveForm::end(); ?>
</div>