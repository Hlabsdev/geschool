<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;
use backend\controllers\Utils;

?>


<script type="text/javascript">
	function add_affectation(key_information) {

		var url_submit = "<?php echo Yii::$app->request->baseUrl ?>/create_planaffectation";
		var url_image = '<center><?= Html::img(Yii::getAlias("@web/theme/img/ajax-loader.gif"), ["width" => "200px", "height" => "200px",]) ?></center>';

		$('#successInfo').hide();
		$('#FormLargeModalContent').hide();
		$('#Formcontent_footer').hide();
		$('#Formcontent_image').show();
		$('#Formcontent_image').html(url_image);
		$('#key_form').val(key_information);
		// $('#user_id').val($('#user_id1').val());
		if (key_information == "0") {
			$('#FormLargeModalLabel').html('<?php echo Yii::t('app', 'add_affectation') ?>');
		} else {
			$('#FormLargeModalLabel').html('<?php echo Yii::t('app', 'update_affectation') ?>');
		}

		$.ajax({
			url: url_submit,
			type: "POST",
			data: {
				key: encodeURIComponent(key_information),
			},
			success: function(data) {
				if (data == '') {
					var error = '<div class="alert alert-danger alert-dismissible show fade" style="margin-bottom: 30px">' +
						'<div class="alert-body"> <button class="close" data-dismiss="alert"> <span>×</span> </button> <?php echo Yii::t('app', 'noaccess') ?> </div> </div>';
					$('#Formcontent_image').html(error);
				} else {
					$('#Formcontent_image').hide();
					$('#Formcontent_footer').show();
					$('#FormLargeModalContent').show();
					$('#FormLargeModalContent').html(data);
				}
			},
			error: function(err) {
				console.log(err);
			}
		});
	}

	function delete_affectation(id) {
		console.log($(this).closest('td').parent()[0]);
		// alert($(this).closest('td').parent()[0].sectionRowIndex);
		$('#myLargeModalLabel').html('<?php echo Yii::t('app', 'confirm_delete') ?>');
		$('#myLargeModalContent').html('<?php echo Yii::t('app', 'info_delete_affectation') ?>');
		document.getElementById('type_operation').value = '111';
		document.getElementById('id_member').value = id;
		// document.getElementById('id_row').value = $(this).closest('td').parent()[0].sectionRowIndex;
		document.getElementById('submit_mod').style.display = "block";
	}

	function delete_member(id) {
		$('#myLargeModalLabel').html('<?php echo Yii::t('app', 'confirm_delete') ?>');
		$('#myLargeModalContent').html('<?php echo Yii::t('app', 'info_delete_user') ?>');
		document.getElementById('type_operation').value = '30';
		document.getElementById('id_member').value = id;
		document.getElementById('submit_mod').style.display = "block";
	}

	function active_member(id) {
		$('#myLargeModalLabel').html('<?php echo Yii::t('app', 'confirm_active') ?>');
		$('#myLargeModalContent').html('<?php echo Yii::t('app', 'info_active_user') ?>');
		document.getElementById('type_operation').value = '10';
		document.getElementById('id_member').value = id;
		document.getElementById('submit_mod').style.display = "block";
	}

	function desactive_member(id) {
		$('#myLargeModalLabel').html('<?php echo Yii::t('app', 'confirm_desactive') ?>');
		$('#myLargeModalContent').html('<?php echo Yii::t('app', 'info_desactive_user') ?>');
		document.getElementById('type_operation').value = '20';
		document.getElementById('id_member').value = id;
		document.getElementById('submit_mod').style.display = "block";
	}

	function do_operation() {

		var url = "<?php echo Yii::$app->request->baseUrl ?>/user_operation";

		var operation = encodeURIComponent(document.getElementById('type_operation').value);
		var id = encodeURIComponent(document.getElementById('id_member').value);
		var id_row = encodeURIComponent(document.getElementById('id_row').value);

		if (operation == '111') {
			url = "<?php echo Yii::$app->request->baseUrl ?>/delete_affectation";
			$.ajax({
				type: "POST",
				url: url,
				data: {
					id: id,
					operation: operation,
				},
				success: function(data) {
					var head_back = data.charAt(0);
					if (head_back.trim() == "0") {
						msg = '<div class="alert alert-danger alert-dismissible show fade" style="margin-bottom: 30px">' + ' <div class="alert-body">' +
							'<button class="close" data-dismiss="alert"> <span>×</span> </button> <?php echo Yii::t('app', 'update_error') ?> </div> </div>';

						$('#Formcontent_imageList').show();
						$('#Formcontent_imageList').html(msg);
						$('#FormLargeModalContentList').show();

					} else if (head_back.trim() == "1") {
						msg = data.split('###')[1];

						$('#button_close').click();
						$('#Formcontent_imageList').show();
						$('#Formcontent_imageList').html(msg);

						if (data.split('###')[2] != 'undefined' && data.split('###')[2] != '') {
							msg = data.split('###')[2];

							$('#FormLargeModalContentList').show();
							$('#FormLargeModalContentList').html(msg);
						}

					} else {
						console.log(data);
					}
				}
			});
		} else {
			var mypostrequest = null;
			if (window.XMLHttpRequest) { // Firefox et autres
				mypostrequest = new XMLHttpRequest();
			} else if (window.ActiveXObject) { // Internet Explorer
				try {
					mypostrequest = new ActiveXObject("Msxml2.XMLHTTP");
				} catch (e) {
					mypostrequest = new ActiveXObject("Microsoft.XMLHTTP");
				}
			}

			mypostrequest.onreadystatechange = function() {
				if (mypostrequest.readyState == 4) {
					if (mypostrequest.status == 200) {

						document.location.href = location.href;
					}
				}
			}


			var parameters = "operation=" + operation + "&id=" + id;

			mypostrequest.open("POST", url, true);
			mypostrequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			mypostrequest.send(parameters);
		}


	}

	function save_affectation() {

		var datedebuteffective = '';
		var datedfineffective = '';

		var key_form = $("#key_form").val();
		var user = $('#user_id1').val();
		var center = $('#planaffectation-idcenter').val();
		var datedebutaffectation = $('#planaffectation-datedebutaffectation').val();
		var datefinaffectation = $('#planaffectation-datefinaffectation').val();
		if ($('#planaffectation-datedebuteffective').val() != 'undefined' && $('#planaffectation-datedebuteffective').val() != '') {
			datedebuteffective = $('#planaffectation-datedebuteffective').val();
		}

		if ($('#planaffectation-datedfineffective').val() != 'undefined' && $('#planaffectation-datedfineffective').val() != '') {
			datedfineffective = $('#planaffectation-datedfineffective').val();
		}
		var url_submit = "<?php echo Yii::$app->request->baseUrl ?>/save_planaffectation";
		var url_image = '<center><?= Html::img(Yii::getAlias("@web/theme/img/ajax-loader.gif"), ["width" => "200px", "height" => "200px",]) ?></center>';

		var msg = '';

		if (user == '' || center == '' || datedebutaffectation == '' || datefinaffectation == '') {
			msg = '<div class="alert alert-danger alert-dismissible show fade" style="margin-bottom: 30px">' + ' <div class="alert-body">' +
				'<button class="close" data-dismiss="alert"> <span>×</span> </button> <?php echo Yii::t('app', 'input_empty') ?> </div> </div>';
			$('#Formcontent_image').show();
			$('#Formcontent_image').html(msg);
		} else {

			var date_debut = new Date(datedebutaffectation);
			var date_fin = new Date(datefinaffectation);
			if (date_debut > date_fin) {
				msg = '<div class="alert alert-danger alert-dismissible show fade" style="margin-bottom: 30px">' + ' <div class="alert-body">' +
					'<button class="close" data-dismiss="alert"> <span>×</span> </button> <?php echo Yii::t('app', 'affectation_date_debut_err') ?> </div> </div>';
				$('#Formcontent_image').show();
				$('#Formcontent_image').html(msg);
			} else {
				$.ajax({
					url: url_submit,
					type: "POST",
					data: {
						key: encodeURIComponent(key_form),
						user: encodeURIComponent(user),
						center: encodeURIComponent(center),
						date_debut: encodeURIComponent(datedebutaffectation),
						date_fin: encodeURIComponent(datefinaffectation),
						date_debut_effective: encodeURIComponent(datedebuteffective),
						date_fin_effective: encodeURIComponent(datedfineffective),
					},
					success: function(data) {

						if (data == '') {
							msg = '<div class="alert alert-danger alert-dismissible show fade" style="margin-bottom: 30px">' +
								'<div class="alert-body"> <button class="close" data-dismiss="alert"> <span>×</span> </button> <?php echo Yii::t('app', 'noaccess') ?> </div> </div>';
							$('#Formcontent_imageList').html(msg);
						} else {
							var head_back = data.charAt(0);
							// var str = data;
							if (head_back.trim() == "0") {
								msg = '<div class="alert alert-danger alert-dismissible show fade" style="margin-bottom: 30px">' + ' <div class="alert-body">' +
									'<button class="close" data-dismiss="alert"> <span>×</span> </button> <?php echo Yii::t('app', 'update_error') ?> </div> </div>';
								$('#Formcontent_image').show();
								$('#Formcontent_image').html(msg);
								$('#Formcontent_footer').show();
								$('#FormLargeModalContent').show();
							} else if (head_back.trim() == "1") {

								msg = data.split('###')[1];

								if (data.split('###')[2] != 'undefined' && data.split('###')[2] != '') {
									$('#button_close1').click();
									msg_alert = data.split('###')[1];
									msg = data.split('###')[2];

									$('#Formcontent_imageList').show();
									$('#Formcontent_imageList').html(msg_alert);

									$('#FormLargeModalContentList').show();
									$('#FormLargeModalContentList').html(msg);
								} else {
									$('#Formcontent_image').show();
									$('#Formcontent_image').html(msg);
									$('#Formcontent_footer').show();
									$('#FormLargeModalContent').show();
								}


								// $('#planaffectation-iduser').val('');
								// $('#planaffectation-idcenter').val('');
								// $('#planaffectation-datedebutaffectation').val('');
								// $('#planaffectation-datefinaffectation').val('');
								// $('#planaffectation-datedebuteffective').val('');
								// $('#planaffectation-datedfineffective').val('');


								// $("#affectation_tab").load(location.href + "#affectation_tab");
							} else {
								console.log('err');
							}

						}
					},
					error: function(err) {
						console.log(err);
					}
				});
			}
		}
	}

	function get_user_list_affectation(user_id) {
		var url_submit = "<?php echo Yii::$app->request->baseUrl ?>/user_planaffectations";
		var url_image = '<center><?= Html::img(Yii::getAlias("@web/theme/img/ajax-loader.gif"), ["width" => "200px", "height" => "200px",]) ?></center>';

		$('#successInfo').hide();
		$('#FormLargeModalContentList').hide();
		$('#Formcontent_imageList').show();
		$('#Formcontent_imageList').html(url_image);
		$('#user_id1').val(user_id);
		$('#FormLargeModalLabelList').html('<?php echo Yii::t('app', 'plan_affectation') ?>');

		$.ajax({
			url: url_submit,
			type: "POST",
			data: {
				key: encodeURIComponent(user_id),
			},
			success: function(data) {

				if (data == '') {
					var error = '<div class="alert alert-danger alert-dismissible show fade" style="margin-bottom: 30px">' +
						'<div class="alert-body"> <button class="close" data-dismiss="alert"> <span>×</span> </button> <?php echo Yii::t('app', 'noaccess') ?> </div> </div>';
					$('#Formcontent_imageList').html(error);
				} else {
					$('#Formcontent_imageList').hide();
					$('#FormLargeModalContentList').show();
					$('#FormLargeModalContentList').html(data);
				}
			},
			error: function(err) {
				console.log(err);
			}
		});
	}
</script>

<div class="card card-body card-block">

	<?= $this->render("_modal") ?>
	<?= $this->render("_modal_affectation") ?>
	<?= $this->render("_modal_list_affectation") ?>


	<?php
	$manage_admin = Utils::have_access("manage_admin");
	$test = explode('_', $manage_admin);

	echo GridView::widget([
		'dataProvider' => $donnee['all_user'],
		'layout' => '{items}',
		'showOnEmpty' => false,
		'emptyText' => Utils::emptyContent(),
		'tableOptions' => [
			'class' => 'table table-striped table-bordered table-hover',
			'id' => 'example1',
		],
		'columns' => [

			[
				'label' => Yii::t('app', 'full_name'),
				'value' =>  function ($data) {
					return $data->nom . " " . $data->prenoms;
				},
			],
			[
				'format' => 'html',
				'label' => Yii::t('app', 'contacts'),
				'value' => function ($data) {
					return $data->telephoneuser . "<br>" . $data->email;
				},
			],

			[
				'format' => 'html',
				'label' => Yii::t('app', 'user_profil'),
				'value' =>  function ($data) {

					$auProfil = $data->userAccesses;
					$infoProfil = "";
					if (sizeof($auProfil) > 0) {
						foreach ($auProfil as $info) {
							if ($infoProfil != "") $infoProfil .= "<br/>";
							$infoProfil .= "<b>- " . $info->idUserProfil->idCenter->denomination_center . "</b>: " . $info->idUserProfil->nameProfil;
						}
					}
					return $infoProfil;
				},
			],
			[
				'class' => 'yii\grid\ActionColumn',
				'template' => '{update}',
				'headerOptions' => ['width' => '40'],
				'visible' => $test[2] == 1 ? true : false,
				'buttons' => [
					'update' => function ($url, $data) {
						if (Yii::$app->user->identity->id != $data->id) {
							$url = Yii::$app->homeUrl . 'update_user?key=' . $data->auth_key;
							return Html::a(
								'<span title="' . Yii::t('app', 'updatebutton') . '" class="fa fa-edit"></span>',
								$url
							);
						} else {
							return '<span  class="fa fa-edit" style="color:black"></span>';
						}
					},
				],

			],
			[
				'class' => 'yii\grid\ActionColumn',
				'template' => '{active}',
				'headerOptions' => ['width' => '40'],
				'visible' => $test[3] == 1 ? true : false,
				'buttons' => [
					'active' => function ($url, $data) {
						if (Yii::$app->user->identity->id != $data->id) {
							if ($data->status == 10) {
								return '<a title="' . Yii::t('app', 'desactive') . '" href="#" data-toggle="modal" data-target=".bs-example-modal-lg"  onclick="desactive_member(\'' . $data->auth_key . '\')"><i class="fa fa-check" style="color:red"></i></a>';
							} else {
								return '<a title="' . Yii::t('app', 'active') . '" href="#" data-toggle="modal" data-target=".bs-example-modal-lg"  onclick="active_member(\'' . $data->auth_key . '\')"><i class="fa fa-check" style="color:blue"></i></a>';
							}
						} else {
							return '<span  class="fa fa-check" style="color:black"></span>';
						}
					},
				],

			],
			[
				'class' => 'yii\grid\ActionColumn',
				'template' => '{delete}',
				'headerOptions' => ['width' => '40'],
				'visible' => $test[3] == 1 ? true : false,
				'buttons' => [
					'delete' => function ($url, $data) use ($test) {
						if (Yii::$app->user->identity->id != $data->id) {
							return '<a title="' . Yii::t('app', 'delete') . '" href="#" data-toggle="modal" data-target=".bs-example-modal-lg"  onclick="delete_member(\'' . $data->auth_key . '\')"><i class="fa fa-window-close" style="color:red"></i></a>';
						} else {
							return '<span  class="fa fa-window-close" style="color:black"></span>';
						}
					},
				],

			],
			[
				'class' => 'yii\grid\ActionColumn',
				'template' => '{update}',
				'headerOptions' => ['width' => '40'],
				'visible' => $test[2] == 1 ? true : false,
				'buttons' => [
					'update' => function ($url, $data) use ($test) {

						if (Yii::$app->user->identity->id != $data->id) {
							$url = Yii::$app->homeUrl . 'reset_user?key=' . $data->auth_key;
							return Html::a(
								'<span title="' . Yii::t('app', 'reset_password') . '" class="fas fa-lock"></span>',
								$url
							);
						} else {
							return '<span  class="fas fa-lock" style="color:black"></span>';
						}
					},
				],

			],
			[
				'class' => 'yii\grid\ActionColumn',
				'template' => '{affectation}',
				'headerOptions' => ['width' => '40'],
				'visible' => $test[2] == 1 ? true : false,
				'buttons' => [
					'affectation' => function ($url, $data) use ($test) {
						if (Yii::$app->user->identity->id != $data->id) {
							// $url = Yii::$app->homeUrl . 'reset_user?key=' . $data->auth_key;
							return '<a title="' . Yii::t('app', 'affecter') . '" href="#" data-toggle="modal" data-target=".Form_list-modal-lg"  onclick="get_user_list_affectation(\'' . $data->id . '\')"><i class="fa fa-user-plus" style="color:green"></i></a>';
							// return Html::a(
							// 	'<span title="' . Yii::t('app', 'reset_password') . '" class="fa fa-user-plus"></span>',
							// 	$url
							// );
						} else {
							return '<span  class="fa fa-user-plus" style="color:black"></span>';
						}
					},
				],

			],
		],

	]);
	?>

</div>