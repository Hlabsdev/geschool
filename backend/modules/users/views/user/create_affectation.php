<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\controllers\Utils;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;

$required_sign = Utils::required();
/* @var $this yii\web\View */
/* @var $model backend\modules\users\models\Typeabsence */


?>

<?php $form = ActiveForm::begin([
    'id' => 'form-create-affectation',
    'fieldConfig' => [
        'options' => [
            'tag' => false,
        ]
    ]
]); ?>
<div class="card-body card-block">

    <div class="row form-group">
        <div class="col col-md-3"><label for="text-input" class=" form-control-label"><?= Yii::t('app', 'center_affectation') ?> <?= $required_sign ?></label></div>
        <div class="input-group col-md-9">
            <?= $form->field($model, 'idcenter')->dropdownList(
                ArrayHelper::map(
                    $all_center,
                    'key_center',
                    'denomination_center'
                ),
                ['prompt' => Yii::t('app', 'select_center'), 'onchange' => '', 'required' => true],
                ['class' => 'form-control']
            )->error(false)->label(false); ?>
        </div>
    </div>

    <div class="row form-group">
        <div class="col col-md-3"><label for="text-input" class=" form-control-label"><?= Yii::t('app', 'date_debut_affectation') ?> <?= $required_sign ?></label></div>
        <div class="input-group col-md-9">
            <?= $form->field($model, 'datedebutaffectation', ['options' => ['class' => 'form-control']])->textInput(['type' => 'date', 'required' => true])->label(false) ?>
        </div>
    </div>

    <?php if ($type == 'UPDATE') { ?>
        <div class="row form-group">
            <div class="col col-md-3"><label for="text-input" class=" form-control-label"><?= Yii::t('app', 'date_debut_effective') ?></label></div>
            <div class="input-group col-md-9">
                <?= $form->field($model, 'datedebuteffective', ['options' => ['class' => 'form-control']])->textInput(['type' => 'date', 'required' => true])->label(false) ?>
            </div>
        </div>
    <?php } ?>

    <div class="row form-group">
        <div class="col col-md-3"><label for="text-input" class=" form-control-label"><?= Yii::t('app', 'date_fin_affectation') ?> <?= $required_sign ?></label></div>
        <div class="input-group col-md-9">
            <?= $form->field($model, 'datefinaffectation', ['options' => ['class' => 'form-control']])->textInput(['type' => 'date', 'required' => true])->label(false) ?>
        </div>
    </div>

    <?php if ($type == 'UPDATE') { ?>
        <div class="row form-group">
            <div class="col col-md-3"><label for="text-input" class=" form-control-label"><?= Yii::t('app', 'date_fin_effective') ?></label></div>
            <div class="input-group col-md-9">
                <?= $form->field($model, 'datedfineffective', ['options' => ['class' => 'form-control']])->textInput(['type' => 'date', 'required' => true])->label(false) ?>
            </div>
        </div>
    <?php } ?>

</div>
<?php ActiveForm::end(); ?>