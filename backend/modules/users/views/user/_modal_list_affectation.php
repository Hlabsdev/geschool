<div class="modal fade Form_list-modal-lg" style="z-index: 9999;" tabindex="-1" role="dialog" aria-labelledby="FormLargeModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">

			<!-- header modal -->
			<div class="modal-header">
				<h4 class="modal-title" id="FormLargeModalLabelList">Large modal</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			</div>
			<!-- body modal -->

			<div class="modal-body">

				<button style="float: right;" type="button" data-toggle="modal" data-target=".Form-modal-lg" class="btn btn-sm btn-success" onclick="add_affectation('0')"><?= Yii::t('app', 'add') ?></button>
			</div>

			<div class="container" id="Formcontent_imageList">
				...
			</div>

			<div class="modal-body" id="FormLargeModalContentList">
				...
			</div>


			<input type="hidden" value="0" name="key_form" id="key_form" />
			<input type="hidden" value="0" name="user_id1" id="user_id1" />
		</div>
	</div>
</div>