<?php

use yii\grid\GridView;
use backend\controllers\Utils;

$manage_personnel = Utils::have_access("manage_personnel");
$droits = explode('_', $manage_personnel);


echo GridView::widget([
    'dataProvider' => $donnee['all_personnel'],
    'layout' => '{items}',
    'showOnEmpty' => false,
    'emptyText' => Utils::emptyContent(),
    'tableOptions' => [
        'class' => 'table table-striped table-bordered table-hover',
        'id' => 'example1',
    ],
    'columns' => [

        [
            'label' => Yii::t('app', 'matricule'),
            'value' =>  function ($data) {
                return $data->matricule;
            },
        ],

        [
            'label' => Yii::t('app', 'user'),
            'value' =>  function ($data) {
                return $data->nom . ' ' . $data->prenoms;
            },
        ],

        [
            'label' => Yii::t('app', 'typepersonnel'),
            'value' =>  function ($data) {
                return $data->typepersonnellibelle;
            },
        ],

        [
            'label' => Yii::t('app', 'dateembauche'),
            'value' =>  function ($data) {
                return Utils::reconversion_date($data->dateembauche);
            },
        ],

        [
            'label' => Yii::t('app', 'sectionconcerne'),
            'value' =>  function ($data) {
                return $data->sectionconcerne;
            },
        ],

        [
            'label' => Yii::t('app', 'indicegrade'),
            'value' =>  function ($data) {
                return $data->indicegrade;
            },
        ],


        [
            'label' => Yii::t('app', 'situationmatrimoniale'),
            'value' =>  function ($data) {
                return $data->situationmatrimoniale;
            },
        ],


        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{update}',
            'headerOptions' => ['width' => '40'],
            'visible' => $droits[2] == 1 ? true : false,
            'buttons' => [
                'update' => function ($url, $data) {
                    return '<a title="' . Yii::t('app', 'updatebutton') . '" href="#" data-toggle="modal" data-target=".Form-modal-lg"  onclick="add_member(\'' . $data->keypersonnel . '\')"><i class="fa fa-edit" style="color:green"></i></a>';
                },
            ],

        ],

        /*[
            'class' => 'yii\grid\ActionColumn',
            'template' => '{active}',
            'headerOptions' => ['width' => '40'],
            'visible' => $droits[3]==1 ? true : false ,
            'buttons' => [
                'active' => function ($url,$data) {

                    if($data->status==1){
                        return '<a title="'.Yii::t('app', 'desactive').'" href="#" data-toggle="modal" data-target=".bs-example-modal-lg"  onclick="desactive_member(\''.$data->keydiplome.'\')"><i class="fa fa-check" style="color:red"></i></a>';
                    }else{
                        return '<a title="'.Yii::t('app', 'active').'" href="#" data-toggle="modal" data-target=".bs-example-modal-lg"  onclick="active_member(\''.$data->keydiplome.'\')"><i class="fa fa-check" style="color:blue"></i></a>';
                    }
                },
            ],

        ],*/

        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{delete}',
            'headerOptions' => ['width' => '40'],
            'visible' => $droits[3] == 1 ? true : false,
            'buttons' => [
                'delete' => function ($url, $data) {
                    return '<a title="' . Yii::t('app', 'delete') . '" href="#" data-toggle="modal" data-target=".bs-example-modal-lg"  onclick="delete_member(\'' . $data->keypersonnel . '\')"><i class="fa fa-window-close" style="color:red"></i></a>';
                },
            ],
        ]
    ],

]);
