<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\controllers\Utils;
use yii\helpers\ArrayHelper;

$required_sign = Utils::required();
/* @var $this yii\web\View */
/* @var $model backend\modules\users\models\Typepersonnel */

// print_r($model);
// die;

?>

<?php $form = ActiveForm::begin([
    'id' => 'form-signup',
    'fieldConfig' => [
        'options' => [
            'tag' => false,
        ]
    ]
]); ?>
<div class="card-body card-block">

    <div class="row form-group">
        <div class="col col-md-3"><label for="text-input" class=" form-control-label"><?= Yii::t('app', 'typeformation') ?> <?= $required_sign ?></label></div>
        <div class="input-group col-md-9">
            <?= $form->field($model, 'idtypeformation')->dropdownList(
                ArrayHelper::map(
                    $all_typeformation,
                    'id',
                    'libelletypeformation'
                ),
                ['prompt' => Yii::t('app', 'select_typeformation'), 'onchange' => '', 'required' => true],
                ['class' => 'form-control']
            )->error(false)->label(false); ?>
        </div>
    </div>


    <div class="row form-group">
        <div class="col col-md-3"><label for="text-input" class=" form-control-label"><?= Yii::t('app', 'designation') ?> <?= $required_sign ?></label></div>
        <div class="input-group col-md-9">
            <?= $form->field($model, 'designation')->textInput(['required' => 'required'])->error(false)->label(false); ?>
        </div>
    </div>


</div>
<?php ActiveForm::end(); ?>