<?php

use yii\widgets\ActiveForm;
use backend\controllers\Utils;

$required_sign = Utils::required();

?>
<?php $form = ActiveForm::begin([
    'id' => 'form-signup',
    'fieldConfig' => [
        'options' => [
            'tag' => false,
        ]
    ]
]); ?>

<div class="card-body card-block">

    <div class="row form-group">
        <div class="col col-md-3"><label for="text-input" class=" form-control-label"><?= Yii::t('app', 'motif') ?></label></div>
        <div class="input-group col-sm-5">
            <?= $form->field($model, 'motif')->textarea(['required' => true, 'placeHolder' => ""])->error(false)->label(false); ?>
        </div>
    </div>

    <div class="row form-group">
        <div class="col col-md-3"><label for="text-input" class=" form-control-label"><?= Yii::t('app', 'datedepart') ?> <?= $required_sign ?></label></div>
        <div class="input-group col-sm-5">
            <?= $form->field($model, 'datedepart', ['options' => ['class' => 'form-control']])->textInput(['type' => 'datetime-local', 'required' => true])->label(false) ?>
        </div>
    </div>

    <div class="row form-group">
        <div class="col col-md-3"><label for="text-input" class=" form-control-label"><?= Yii::t('app', 'dateretourprob') ?> <?= $required_sign ?></label></div>
        <div class="input-group col-sm-5">
            <?= $form->field($model, 'dateretourprob', ['options' => ['class' => 'form-control']])->textInput(['type' => 'date', 'required' => true])->label(false) ?>
        </div>
    </div>

    <div class="row form-group">
        <div class="col col-md-3"><label for="text-input" class=" form-control-label"><?= Yii::t('app', 'lieumission') ?> <?= $required_sign ?></label></div>
        <div class="input-group col-sm-5">
            <?= $form->field($model, 'lieumission')->textinput(['required' => 'required'])->error(false)->label(false); ?>
        </div>
    </div>

    <div class="row form-group">
        <div class="col col-md-3"><label for="text-input" class=" form-control-label"><?= Yii::t('app', 'itineraireretenu') ?> </label></div>
        <div class="input-group col-sm-5">
            <?= $form->field($model, 'itineraireretenu')->textinput()->error(false)->label(false); ?>
        </div>
    </div>

</div>

<?php ActiveForm::end(); ?>