<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\controllers\Utils;
use backend\modules\users\models\SchoolCenter;

$required_sign = Utils::required();
/* @var $this yii\web\View */
/* @var $model backend\modules\users\models\Typepersonnel */


$id_center = str_replace('-', '', Yii::$app->user->identity->id_center);
$test_center = explode(",", $id_center);

$allcenter = array();

if ($id_center == 1) {
    $allcenter = SchoolCenter::find()->where(['etat' => [1, 4]])->orderBy(['denomination_center' => SORT_ASC])->all();
} else if (sizeof($test_center) > 1) {
    $allcenter = SchoolCenter::find()->where(['etat' => 1, 'id_center' => $test_center])->orderBy(['denomination_center' => SORT_ASC])->all();
}

?>

<?php $form = ActiveForm::begin([
    'id' => 'form-signup',
    'fieldConfig' => [
        'options' => [
            'tag' => false,
        ]
    ]
]); ?>
<div class="card-body card-block">



    <?php

    if ($type == 'CREATE') {

        if (sizeof($allcenter) > 1) { ?>

            <div class="row form-group">
                <div class="col col-md-3"><label for="text-input" class=" form-control-label"><?= Yii::t('app', 'center') ?> <?= $required_sign ?> </label></div>
                <div class="input-group col-md-8">
                    <select class="" name="jourferie-centreconcerne" id="jourferie-centreconcerne" multiple>
                        <option value="" data-placeholder="true"><?= Yii::t('app', 'select_center') ?> </option>
                        <?php
                        $default_center = Yii::$app->session->get('default_center');
                        for ($i = 0; $i < sizeof($allcenter); $i++) {
                            if ($allcenter[$i]->etat == '1') { ?>
                                <option value="<?= $allcenter[$i]->id_center ?>" <?php if ($allcenter[$i]->id_center == $default_center) echo 'selected' ?>> <?= $allcenter[$i]->denomination_center ?></option>;
                        <?php }
                        }
                        ?>
                    </select>
                </div>
            </div>
    <?php
        }
    }
    ?>


    <div class="row form-group">
        <div class="col col-md-3"><label for="text-input" class=" form-control-label"><?= Yii::t('app', 'evenement') ?> <?= $required_sign ?></label></div>
        <div class="input-group col-md-8">
            <?= $form->field($model, 'evenement')->textInput(['required' => 'required'])->error(false)->label(false); ?>
        </div>
    </div>

    <div class="row form-group">
        <div class="col col-md-3"><label for="text-input" class=" form-control-label"><?= Yii::t('app', 'datejourferie') ?> <?= $required_sign ?></label></div>
        <div class="input-group col-md-8">
            <?= $form->field($model, 'datejourferie', [
                'options' => [
                    'class' => 'form-control',
                ],
            ])->textInput(['type' => 'date'])->label(false) ?>
        </div>
    </div>


</div>
<?php ActiveForm::end(); ?>

<script>
    new SlimSelect({
        select: '#jourferie-centreconcerne',
        allowDeselectOption: true,
        searchText: '<?= Yii::t('app', 'search_nothing') ?>',
        searchPlaceholder: '<?= Yii::t('app', 'search') ?>',
        searchFocus: true, // Whether or not to focus on the search input field
        // searchHighlight: true
        hideSelectedOption: true,
        // selectByGroup: true
    });
</script>