<?php

use yii\widgets\ActiveForm;
use backend\controllers\Utils;
use yii\helpers\ArrayHelper;

$required_sign = Utils::required();
/* @var $this yii\web\View */
/* @var $model backend\modules\parametres\models\Decoupage */

?>

<?php $form = ActiveForm::begin([
    'id' => 'form-signup',
    'fieldConfig' => [
        'options' => [
            'tag' => false,
        ]
    ]
]); ?>
<div class="card-body card-block">

    <!--  Type Decoupage selection debut  -->
    <div class="row form-group">
        <div class="col col-md-3"><label for="text-input" class=" form-control-label"><?= Yii::t('app', 'typedecoupage') ?> <?= $required_sign ?></label></div>
        <div class="input-group col-md-9">
            <?= $form->field($model, 'idtypedecoupage')->dropdownList(
                ArrayHelper::map($all_typedecoupage,'keytypedecoupage','libelle'),
                ['prompt' => Yii::t('app', 'select_typedecoupage'), 'onchange' => '', 'required' => true],
                ['class' => 'form-control']
            )->error(false)->label(false); ?>
        </div>
    </div>
    <!--  Type Decoupage selection fin  -->
    
    <!--  Date de debut start  -->
    <div class="row form-group">
        <div class="col col-md-3"><label for="text-input" class=" form-control-label"><?= Yii::t('app', 'datedebutdecoupage') ?> <?= $required_sign ?></label></div>
        <div class="input-group col-md-9">
            <?= $form->field($model, 'datedebutdecoupage', [
                'options' => [
                    'class' => 'form-control',
                ],
            ])->textInput(['type' => 'date'])->label(false) ?>
        </div>
    </div>
    <!--  Date de debut fin  -->

    <!--  Date de fin start  -->
    <div class="row form-group">
        <div class="col col-md-3"><label for="text-input" class=" form-control-label"><?= Yii::t('app', 'datefindecoupage') ?> <?= $required_sign ?></label></div>
        <div class="input-group col-md-9">
            <?= $form->field($model, 'datefindecoupage', [
                'options' => [
                    'class' => 'form-control',
                ],
            ])->textInput(['type' => 'date'])->label(false) ?>
        </div>
    </div>
    <!--  Date de fin end  -->
    
    <input type="text" name="fauxinput" class="fauxinput" value="">
</div>
<?php ActiveForm::end(); ?>