<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\controllers\Utils;
use yii\helpers\ArrayHelper;
use backend\modules\users\models\SchoolCenter;

$required_sign = Utils::required();
/* @var $this yii\web\View */
/* @var $model backend\modules\users\models\Typepersonnel */

// $all_typedecours = [
//     'Principal' => 'Principal',
//     'Secondaire' => 'Secondaire',
// ];

?>

<?php $form = ActiveForm::begin([
    'id' => 'form-signup',
    'fieldConfig' => [
        'options' => [
            'tag' => false,
        ]
    ]
]); ?>
<div class="card-body card-block">

    <div class="row form-group">
        <div class="col col-md-3"><label for="text-input" class=" form-control-label"><?= Yii::t('app', 'typecours') ?> <?= $required_sign ?></label></div>
        <div class="input-group col-md-8">
            <?= $form->field($model, 'idtypecours')->dropdownList(
                ArrayHelper::map(
                    $all_typedecours,
                    'id',
                    'libelletypecours'
                ),
                ['prompt' => Yii::t('app', 'select_typecours'), 'onchange' => '', 'required' => true],
                ['class' => 'form-control']
            )->error(false)->label(false); ?>
        </div>
    </div>

    <div class="row form-group">
        <div class="col col-md-3"><label for="text-input" class=" form-control-label"><?= Yii::t('app', 'designation') ?> <?= $required_sign ?></label></div>
        <div class="input-group col-md-8">
            <?= $form->field($model, 'designation')->textarea(['required' => 'required'])->error(false)->label(false); ?>
        </div>
    </div>

</div>
<?php ActiveForm::end(); ?>