<?php

use yii\widgets\ActiveForm;
use backend\controllers\Utils;

$required_sign = Utils::required();

?>

<?php $form = ActiveForm::begin([
    'id' => 'form-signup',
    'fieldConfig' => [
        'options' => [
            'tag' => false,
        ]
    ]
]); ?>
<div class="card-body card-block">


    <div class="row form-group">
        <div class="col col-md-3"><label for="text-input" class=" form-control-label"><?= Yii::t('app', 'libellediplome') ?> <?= $required_sign ?></label></div>
        <div class="input-group col-md-8">
            <?= $form->field($model, 'libellediplome')->textInput(['required' => 'required'])->error(false)->label(false); ?>
        </div>
    </div>

    <input type="text" name="fauxinput" class="fauxinput" value="">

</div>
<?php ActiveForm::end(); ?>