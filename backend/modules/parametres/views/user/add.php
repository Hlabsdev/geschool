<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\modules\parametres\models\UserProfil;
use backend\modules\parametres\models\SchoolCenter;

use backend\controllers\Utils;

$required_sign = Utils::required();


$id_center = Yii::$app->user->identity->id_center;
$test_center = explode(",", $id_center);

if ($id_center == 1) {
	$allcenter = SchoolCenter::find()->where(['etat' => 1])->all();
} else {
	$allcenter = SchoolCenter::find()->where(['etat' => 1, 'id_center' => $test_center])->all();
}


?>



<script type="application/javascript">
	function checkProfil(id) {

		//renitialisation du prodil de la personne avec visibilite de la partie profil a selectionner				
		$('#profil_' + id).hide();
		var status = document.getElementById(id).checked;
		if (status == true) {
			$('#profil_' + id).show();
		}
	}
</script>

<div class="card">
	<?php $form = ActiveForm::begin([
		'id' => 'form-signup',
		'fieldConfig' => [
			'options' => [
				'tag' => false,
			]
		]
	]); ?>
	<div class="card-body card-block">




		<div class="row form-group">
			<div class="col col-md-3"><label for="text-input" class=" form-control-label"><?= Yii::t('app', 'nom') ?> <?= $required_sign ?></label></div>
			<div class="input-group col-sm-5">
				<div class="input-group-prepend">
					<div class="input-group-text"><i class="fas fa-user"></i></div>
				</div>
				<?= $form->field($model, 'nom')->textInput(['required' => 'required'])->error(false)->label(false); ?>
			</div>
		</div>

		<div class="row form-group">
			<div class="col col-md-3"><label for="text-input" class=" form-control-label"><?= Yii::t('app', 'prenoms') ?> <?= $required_sign ?></label></div>
			<div class="input-group col-sm-5">
				<div class="input-group-prepend">
					<div class="input-group-text"><i class="fas fa-user"></i></div>
				</div>
				<?= $form->field($model, 'prenoms')->textInput(['required' => 'required'])->error(false)->label(false); ?>
			</div>
		</div>

		<div class="row form-group">
			<div class="col col-md-3"><label for="text-input" class=" form-control-label"><?= Yii::t('app', 'phone_number') ?> <?= $required_sign ?></label></div>
			<div class="input-group col-sm-5">
				<div class="input-group-prepend">
					<div class="input-group-text"><i class="fas fa-phone"></i></div>
				</div>
				<?= $form->field($model, 'username')->textInput(['required' => 'required', 'class' => 'form-control phone-inputmask'])->error(false)->label(false); ?>
			</div>
		</div>

		<div class="row form-group">
			<div class="col col-md-3"><label for="text-input" class=" form-control-label"><?= Yii::t('app', 'email') ?> <?= $required_sign ?></label></div>
			<div class="input-group col-sm-5">
				<div class="input-group-prepend">
					<div class="input-group-text"><i class="fas fa-envelope"></i></div>
				</div>
				<?= $form->field($model, 'email')->textInput(['type' => 'email', 'required' => 'required'])->error(false)->Label(false); ?>
			</div>
		</div>

		<?php if ($type == "CREATE") { ?>

			<div class="row form-group">
				<div class="col col-md-3"><label for="text-input" class=" form-control-label"><?= Yii::t('app', 'user_password') ?> <?= $required_sign ?></label></div>
				<div class="input-group col-sm-5">
					<div class="input-group-prepend">
						<div class="input-group-text"><i class="fas fa-lock"></i></div>
					</div>
					<?= $form->field($model, 'password')->textInput(['required' => 'required', 'type' => 'password'])->error(false)->Label(false); ?>

				</div>
			</div>

			<div class="row form-group">
				<div class="col col-md-3"><label for="text-input" class=" form-control-label"><?= Yii::t('app', 'cuser_password') ?> <?= $required_sign ?></label></div>
				<div class="input-group col-sm-5">
					<div class="input-group-prepend">
						<div class="input-group-text"><i class="fas fa-lock"></i></div>
					</div>
					<?= $form->field($model, 'password2')->textInput(['required' => 'required', 'type' => 'password'])->error(false)->Label(false); ?>

				</div>
			</div>

		<?php }
		if (sizeof($allcenter) > 0) {
			$all_existProfil = explode(",", $userProfil);
		?>
			<div class="row form-group">

				<div class="col-sm-8" id="module_page" style="margin-top: 30px;padding-right: 0px">


					<table class="table table-bordered table-striped">
						<tr>
							<th></th>
							<th style="width:50%"><?php echo Yii::t('app', 'denomination_center') ?></th>
							<th style="width:45%"><?php echo Yii::t('app', 'user_profil') ?></th>
						</tr>


						<?php foreach ($allcenter as $infoCenter) {


							$show_option = '<option value="" >' . Yii::t('app', 'select_profil') . '</option>';
							$is_visible = false;

							//recuperation du profil
							$center_profil = UserProfil::find()->where(['etat_user_profil' => 1, 'id_center' => $infoCenter->id_center])->all();
							if (sizeof($center_profil) > 0) {
								foreach ($center_profil as $infoprofil) {
									$selected = "";
									if (in_array($infoprofil->id_user_profil, $all_existProfil)) {
										$selected = "selected='true'";
										$is_visible = true;
									}
									$show_option .= '<option value="' . $infoprofil->id_user_profil . '" ' . $selected . '>' . $infoprofil->nameProfil . '</option>';
								}
							}

						?>
							<tr>
								<td><input type="checkbox" id="<?= $infoCenter->id_center ?>" name="<?= 'geschoolcenter_' . $infoCenter->id_center ?>" value="1" onchange="checkProfil(this.id)" <?php if ($is_visible) {
																																																echo 'checked="true"';
																																															} ?> /></td>
								<td><?= Yii::t('app', $infoCenter->denomination_center); ?></td>
								<td>
									<div id="<?= 'profil_' . $infoCenter->id_center ?>" style="<?php if (!$is_visible) {
																								echo 'display:none';
																							} ?>">
										<select class="form-control" name="<?= 'profilcenter_' . $infoCenter->id_center ?>">
											<?= $show_option ?>
										</select>
									</div>
								</td>
							</tr>
						<?php } ?>


					</table>

				</div>

			</div>
		<?php } ?>
	</div>
	<div class="card-footer">
		<?= Utils::resetbtn(); ?>
		<?= Html::submitButton('<i class="fa fa-dot-circle-o"></i> ' . Yii::t('app', 'submitbutton'), ['class' => 'btn btn-sm btn-success', 'name' => 'updatebutton']) ?>
	</div>
    <input type="text" name="fauxinput" class="fauxinput" value="">
	<?php ActiveForm::end(); ?>
</div>