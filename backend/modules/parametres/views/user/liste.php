<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;
use backend\controllers\Utils;
?>


<script type="text/javascript">
	function delete_member(id) {
		$('#myLargeModalLabel').html('<?php echo Yii::t('app', 'confirm_delete') ?>');
		$('#myLargeModalContent').html('<?php echo Yii::t('app', 'info_delete_user') ?>');
		document.getElementById('type_operation').value = '30';
		document.getElementById('id_member').value = id;
		document.getElementById('submit_mod').style.display = "block";
	}

	function active_member(id) {
		$('#myLargeModalLabel').html('<?php echo Yii::t('app', 'confirm_active') ?>');
		$('#myLargeModalContent').html('<?php echo Yii::t('app', 'info_active_user') ?>');
		document.getElementById('type_operation').value = '10';
		document.getElementById('id_member').value = id;
		document.getElementById('submit_mod').style.display = "block";
	}

	function desactive_member(id) {
		$('#myLargeModalLabel').html('<?php echo Yii::t('app', 'confirm_desactive') ?>');
		$('#myLargeModalContent').html('<?php echo Yii::t('app', 'info_desactive_user') ?>');
		document.getElementById('type_operation').value = '20';
		document.getElementById('id_member').value = id;
		document.getElementById('submit_mod').style.display = "block";
	}

	function do_operation() {

		var url = "<?php echo Yii::$app->request->baseUrl ?>/user_operation";

		var mypostrequest = null;
		if (window.XMLHttpRequest) { // Firefox et autres
			mypostrequest = new XMLHttpRequest();
		} else if (window.ActiveXObject) { // Internet Explorer
			try {
				mypostrequest = new ActiveXObject("Msxml2.XMLHTTP");
			} catch (e) {
				mypostrequest = new ActiveXObject("Microsoft.XMLHTTP");
			}
		}

		mypostrequest.onreadystatechange = function() {
			if (mypostrequest.readyState == 4) {
				if (mypostrequest.status == 200) {

					document.location.href = location.href;
				}
			}
		}

		var operation = encodeURIComponent(document.getElementById('type_operation').value);
		var id = encodeURIComponent(document.getElementById('id_member').value);

		var parameters = "operation=" + operation + "&id=" + id;

		mypostrequest.open("POST", url, true);
		mypostrequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		mypostrequest.send(parameters);
	}
</script>

<div class="card card-body card-block">
	<?= $this->render("_modal") ?>
	<?php
	$manage_admin = Utils::have_access("manage_admin");
	$test = explode('_', $manage_admin);

	echo GridView::widget([
		'dataProvider' => $donnee['all_user'],
		'layout' => '{items}',
		'showOnEmpty' => false,
		'emptyText' => Utils::emptyContent(),
		'tableOptions' => [
			'class' => 'table table-striped table-bordered table-hover',
			'id' => 'example1',
		],
		'columns' => [

			[
				'label' => Yii::t('app', 'full_name'),
				'value' =>  function ($data) {
					return $data->nom . " " . $data->prenoms;
				},
			],
			[
				'format' => 'html',
				'label' => Yii::t('app', 'phone_number'),
				'value' => function ($data) {
					return $data->username . "<br>" . $data->email;
				},
			],

			[
				'format' => 'html',
				'label' => Yii::t('app', 'user_profil'),
				'value' =>  function ($data) {

					$auProfil = $data->userAccesses;
					$infoProfil = "";
					if (sizeof($auProfil) > 0) {
						foreach ($auProfil as $info) {
							if ($infoProfil != "") $infoProfil .= "<br/>";
							$infoProfil .= "<b>- " . $info->idUserProfil->idCenter->denomination_center . "</b>: " . $info->idUserProfil->nameProfil;
						}
					}
					return $infoProfil;
				},
			],
			[
				'class' => 'yii\grid\ActionColumn',
				'template' => '{update}',
				'headerOptions' => ['width' => '40'],
				'visible' => $test[2] == 1 ? true : false,
				'buttons' => [
					'update' => function ($url, $data) {
						if (Yii::$app->user->identity->id != $data->id) {
							$url = Yii::$app->homeUrl . 'update_user?key=' . $data->auth_key;
							return Html::a(
								'<span title="' . Yii::t('app', 'updatebutton') . '" class="fa fa-edit"></span>',
								$url
							);
						} else {
							return '<span  class="fa fa-edit" style="color:black"></span>';
						}
					},
				],

			],
			[
				'class' => 'yii\grid\ActionColumn',
				'template' => '{active}',
				'headerOptions' => ['width' => '40'],
				'visible' => $test[3] == 1 ? true : false,
				'buttons' => [
					'active' => function ($url, $data) {
						if (Yii::$app->user->identity->id != $data->id) {
							if ($data->status == 10) {
								return '<a title="' . Yii::t('app', 'desactive') . '" href="#" data-toggle="modal" data-target=".bs-example-modal-lg"  onclick="desactive_member(\'' . $data->auth_key . '\')"><i class="fa fa-check" style="color:red"></i></a>';
							} else {
								return '<a title="' . Yii::t('app', 'active') . '" href="#" data-toggle="modal" data-target=".bs-example-modal-lg"  onclick="active_member(\'' . $data->auth_key . '\')"><i class="fa fa-check" style="color:blue"></i></a>';
							}
						} else {
							return '<span  class="fa fa-check" style="color:black"></span>';
						}
					},
				],

			],
			[
				'class' => 'yii\grid\ActionColumn',
				'template' => '{delete}',
				'headerOptions' => ['width' => '40'],
				'visible' => $test[3] == 1 ? true : false,
				'buttons' => [
					'delete' => function ($url, $data) use ($test) {
						if (Yii::$app->user->identity->id != $data->id) {
							return '<a title="' . Yii::t('app', 'delete') . '" href="#" data-toggle="modal" data-target=".bs-example-modal-lg"  onclick="delete_member(\'' . $data->auth_key . '\')"><i class="fa fa-window-close" style="color:red"></i></a>';
						} else {
							return '<span  class="fa fa-window-close" style="color:black"></span>';
						}
					},
				],

			],
			[
				'class' => 'yii\grid\ActionColumn',
				'template' => '{update}',
				'headerOptions' => ['width' => '40'],
				'visible' => $test[2] == 1 ? true : false,
				'buttons' => [
					'update' => function ($url, $data) use ($test) {

						if (Yii::$app->user->identity->id != $data->id) {
							$url = Yii::$app->homeUrl . 'reset_user?key=' . $data->auth_key;
							return Html::a(
								'<span title="' . Yii::t('app', 'reset_password') . '" class="fas fa-lock"></span>',
								$url
							);
						} else {
							return '<span  class="fas fa-lock" style="color:black"></span>';
						}
					},
				],

			],
		],

	]);
	?>

</div>