<?php

use yii\widgets\ActiveForm;
use backend\controllers\Utils;
use yii\helpers\ArrayHelper;

$required_sign = Utils::required();

foreach ($all_personnel as $personnel) {
    $personnel->nom = $personnel->nom . ' ' . $personnel->prenoms;
}

?>


<?php $form = ActiveForm::begin([
    'id' => 'form-signup',
    'fieldConfig' => [
        'options' => [
            'tag' => false,
        ]
    ]
]); ?>

<div class="card-body card-block">

    <div class="row form-group">
        <div class="col col-md-4"><label for="text-input" class=" form-control-label"><?= Yii::t('app', 'user') ?> <?= $required_sign ?> </label></div>
        <div class="input-group col-sm-8">
            <?= $form->field($model, 'iduser')->dropdownList(
                ArrayHelper::map($all_personnel, 'id', 'nom'),
                ['prompt' => Yii::t('app', 'select_personnel'), 'required' => true],
                ['class' => 'form-control']
            )->error(false)->label(false);
            ?>
        </div>
    </div>

    <div class="row form-group">
        <div class="col col-md-4"><label for="text-input" class=" form-control-label"><?= Yii::t('app', 'frais_mission') ?> <?= $required_sign ?></label></div>
        <div class="input-group col-sm-8">
            <?= $form->field($model, 'frais')->textinput(['required' => 'required'])->error(false)->label(false); ?>
        </div>
    </div>

    <div class="row form-group">
        <div class="col col-md-4"><label for="text-input" class=" form-control-label"><?= Yii::t('app', 'contact_abs') ?> <?= $required_sign ?> </label></div>
        <div class="input-group col-sm-8">
            <?= $form->field($model, 'contactabsence')->textinput()->error(false)->label(false); ?>
        </div>
    </div>


    <?php if ($type == 'UPDATE') { ?>
        <div class="row form-group">
            <div class="col col-md-4"><label for="text-input" class=" form-control-label"><?= Yii::t('app', 'dateretour_eff') ?> </label></div>
            <div class="input-group col-sm-8">
                <?= $form->field($model, 'dateretoureffmission', ['options' => ['class' => 'form-control']])->textInput(['type' => 'datetime-local'])->label(false) ?>
            </div>
        </div>
    <?php } ?>


</div>

<?php ActiveForm::end(); ?>