<?php

use yii\widgets\ActiveForm;
use backend\controllers\Utils;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

$required_sign = Utils::required();

?>

<script>
    function delete_detail_mission(data, element) {
        var correct_data = '###' + data + '-';
        var rowIndex = element.closest('tr').rowIndex;
        var all_selected_pers = $("#all_selected_pers").val();
        var res = all_selected_pers.replace(correct_data, "");
        $("#all_selected_pers").val(res);
        document.getElementById("tab_temp_preview").deleteRow(rowIndex);
    }

    function add_detail_mission() {
        if ($('#temp_idpersonnel').val() != 'undefined' && $('#temp_idpersonnel').val() != '') {
            var idpersonnel = $('#temp_idpersonnel').val();
            var namepersonnel = $("#mission-idpersonnel option:selected").text();
            var old_selected_pers = $("#all_selected_pers").val();
            if ($('#frais_mission').val() != '' && $('#contact_abs_mission').val() != '') {
                var frais_mission = $('#frais_mission').val();
                var contact_abs_mission = $('#contact_abs_mission').val();

                var new_data = idpersonnel + ';' + frais_mission + ';' + contact_abs_mission;

                $("#all_selected_pers").val(old_selected_pers + '###' + new_data + '-');
                // var new_line = '<tr> <td> <b> ' + namepersonnel + ' </b> </td> </tr>';
                var newRow = document.createElement("tr");
                var newCell = document.createElement("td");
                var newCell1 = document.createElement("td");
                var newCell2 = document.createElement("td");
                var newCell3 = document.createElement("td");
                newCell.innerHTML = namepersonnel;
                newCell1.innerHTML = frais_mission;
                newCell2.innerHTML = contact_abs_mission;
                newCell3.innerHTML = '<i class="fa fa-window-close" style="color:red" onclick="delete_detail_mission(\'' + new_data + '\', this)"></i>';
                newRow.append(newCell);
                newRow.append(newCell1);
                newRow.append(newCell2);
                newRow.append(newCell3);
                document.getElementById("rows").appendChild(newRow);

                $('#modalDetailMission').modal('hide');
                $('#frais_mission').val('');
                $('#contact_abs_mission').val('');

            } else {
                msg = '<div class="alert alert-danger alert-dismissible show fade" style="margin-bottom: 30px">' + ' <div class="alert-body">' +
                    '<button class="close" data-dismiss="alert"> <span>×</span> </button> <?php echo Yii::t('app', 'input_empty') ?> </div> </div>';
                $('#alert_place_modal').show();
                $('#alert_place_modal').html(msg);
            }

        } else {
            msg = '<div class="alert alert-danger alert-dismissible show fade" style="margin-bottom: 30px">' + ' <div class="alert-body">' +
                '<button class="close" data-dismiss="alert"> <span>×</span> </button> <?php echo Yii::t('app', 'select_personnel_notfound') ?> </div> </div>';
            $('#alert_place_modal').show();
            $('#alert_place_modal').html(msg);
        }
    }

    function add_personnel() {
        var msg = '';
        if ($('#mission-idpersonnel').val() != 'undefined' && $('#mission-idpersonnel').val() != '') {
            var idpersonnel = $('#mission-idpersonnel').val();
            var namepersonnel = $("#mission-idpersonnel option:selected").text();
            var old_selected_pers = $("#all_selected_pers").val();
            var search_position = old_selected_pers.search('###' + idpersonnel + ';');
            if (search_position >= 0) {
                msg = '<div class="alert alert-danger alert-dismissible show fade" style="margin-bottom: 30px">' + ' <div class="alert-body">' +
                    '<button class="close" data-dismiss="alert"> <span>×</span> </button> <?php echo Yii::t('app', 'select_personnel_double_added') ?> </div> </div>';
                $('#alert_place').show();
                $('#alert_place').html(msg);
            } else {
                $("#temp_idpersonnel").val(idpersonnel); // Ajout de l'id du personnel dans un champs tampon
                $('#modalDetailMission').modal('show');

            }
        } else {
            msg = '<div class="alert alert-danger alert-dismissible show fade" style="margin-bottom: 30px">' + ' <div class="alert-body">' +
                '<button class="close" data-dismiss="alert"> <span>×</span> </button> <?php echo Yii::t('app', 'select_personnel_empty') ?> </div> </div>';
            $('#alert_place').show();
            $('#alert_place').html(msg);
        }
    }
</script>

<div class="card">

    <?php $form = ActiveForm::begin([
        'id' => 'form-signup',
        'fieldConfig' => [
            'options' => [
                'tag' => false,
            ]
        ]
    ]); ?>

    <div class="card-body card-block">

        <div class="container" id="alert_place">

        </div>

        <input type="hidden" name="temp_idpersonnel" id="temp_idpersonnel">

        <textarea style="display: none;" name="all_selected_pers" id="all_selected_pers" cols="30" rows="10"></textarea>

        <div class="row">
            <div class="col-md-8" style="margin: auto;">

                <div class="row form-group">
                    <div class="col col-md-3"><label for="text-input" class=" form-control-label"><?= Yii::t('app', 'user') ?></label></div>
                    <div class="input-group col-sm-5">
                        <select class="form-control" name="mission[idpersonnel]" id="mission-idpersonnel">
                            <option value="" selected> <?= Yii::t('app', 'select_personnel') ?></option>
                            <?php
                            for ($i = 0; $i < sizeof($all_personnel); $i++) {
                                echo '<option value="' . $all_personnel[$i]->keypersonnel . '">' . $all_personnel[$i]->nom . ' ' . $all_personnel[$i]->prenoms . '</option>';
                            }
                            ?>
                        </select>
                        <?php

                        // $activities = ArrayHelper::map($all_personnel, 'nom', 'prenoms');
                        // array_walk($activities, function (&$value, $key) {
                        //     $value = $key . ' ' . $value;
                        // });

                        // echo $form->field($model, 'idpersonnel')->dropdownList(
                        //     ArrayHelper::map($all_personnel, 'id', 'nom', 'prenoms'),
                        //     ['prompt' => Yii::t('app', 'select_personnel'), 'required' => true],
                        //     ['class' => 'form-control']
                        // )->error(false)->label(false); 
                        ?>
                    </div>

                    <a style="margin: auto;" href="#" class="btn btn-primary" onclick="add_personnel()"><?= Yii::t('app', 'add') ?> <i class="fas fa-angle-double-right"></i></a>

                </div>

                <div class="row form-group">
                    <div class="col col-md-3"><label for="text-input" class=" form-control-label"><?= Yii::t('app', 'motif') ?></label></div>
                    <div class="input-group col-sm-5">
                        <?= $form->field($model, 'motif')->textarea(['required' => true, 'placeHolder' => ""])->error(false)->label(false); ?>
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col col-md-3"><label for="text-input" class=" form-control-label"><?= Yii::t('app', 'datedepart') ?> <?= $required_sign ?></label></div>
                    <div class="input-group col-sm-5">
                        <?= $form->field($model, 'datedepart', ['options' => ['class' => 'form-control']])->textInput(['type' => 'datetime-local', 'required' => true])->label(false) ?>
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col col-md-3"><label for="text-input" class=" form-control-label"><?= Yii::t('app', 'dateretourprob') ?> <?= $required_sign ?></label></div>
                    <div class="input-group col-sm-5">
                        <?= $form->field($model, 'dateretourprob', ['options' => ['class' => 'form-control']])->textInput(['type' => 'date', 'required' => true])->label(false) ?>
                    </div>

                </div>

                <div class="row form-group">
                    <div class="col col-md-3"><label for="text-input" class=" form-control-label"><?= Yii::t('app', 'lieumission') ?> <?= $required_sign ?></label></div>
                    <div class="input-group col-sm-5">
                        <?= $form->field($model, 'lieumission')->textinput(['required' => 'required'])->error(false)->label(false); ?>
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col col-md-3"><label for="text-input" class=" form-control-label"><?= Yii::t('app', 'itineraireretenu') ?> </label></div>
                    <div class="input-group col-sm-5">
                        <?= $form->field($model, 'itineraireretenu')->textinput()->error(false)->label(false); ?>
                    </div>
                </div>

            </div>

            <div class="col-md-4">
                <div class="row" style="margin-right: auto;">
                    <table style="width:100%" class="table table-striped table-bordered table-hover" id="tab_temp_preview">
                        <thead>
                            <tr>
                                <th><?= Yii::t('app', 'users') ?></th>
                                <th><?= Yii::t('app', 'frais_mission') ?></th>
                                <th><?= Yii::t('app', 'contact_abs') ?></th>
                                <th><?= Yii::t('app', 'action') ?></th>
                            </tr>
                        </thead>
                        <tbody id="rows">
                        </tbody>
                    </table>
                </div>
            </div>

        </div>

    </div>

    <div class="card-footer">
        <!-- <div style="float: right; margin-bottom: 25px;"> -->
        <?= Utils::resetbtn(); ?>
        <?= Html::submitButton('<i class="fa fa-dot-circle-o"></i> ' . Yii::t('app', 'submitbutton'), ['class' => 'btn btn-sm btn-success', 'name' => 'updatebutton']) ?>
        <!-- </div> -->
    </div>

    <?php ActiveForm::end(); ?>


    <!-- Modal -->
    <div class="modal fade" id="modalDetailMission" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title"> <?= Yii::t('app', 'detail_mission') ?></h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="container" id="alert_place_modal"> </div>
                <div class="modal-body">
                    <form role="form">
                        <div class="form-group">
                            <label for="frais_mission"> <?= Yii::t('app', 'frais_mission') ?></label>
                            <input type="text" class="form-control" id="frais_mission" onkeyup="if(isNaN(this.value)){this.value=''}">
                        </div>
                        <div class="form-group">
                            <label for="contact_abs_mission"> <?= Yii::t('app', 'contact_abs') ?></label>
                            <input type="text" class="form-control" id="contact_abs_mission">
                        </div>
                        <a href="#" style="float: right;" onclick="add_detail_mission()" class="btn btn-default btn-success"> <?= Yii::t('app', 'add') ?></a>
                    </form>
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>

</div>