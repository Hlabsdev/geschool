<?php

use yii\grid\GridView;
use backend\controllers\Utils;

$manage_mission = Utils::have_access("manage_mission");
$droits = explode('_', $manage_mission);


echo GridView::widget([
    'dataProvider' => $donnee['all_mission'],
    'layout' => '{items}',
    'showOnEmpty' => false,
    'emptyText' => Utils::emptyContent(),
    'tableOptions' => [
        'class' => 'table table-striped table-bordered table-hover',
        'id' => 'example1',
    ],
    'columns' => [

        [
            'label' => Yii::t('app', 'motif'),
            'value' =>  function ($data) {
                return $data->motif;
            },
        ],
        [
            'label' => Yii::t('app', 'lieumission'),
            'value' =>  function ($data) {
                return $data->lieumission;
            },
        ],
        [
            'label' => Yii::t('app', 'itineraireretenu'),
            'value' =>  function ($data) {
                return $data->itineraireretenu;
            },
        ],
        [
            'label' => Yii::t('app', 'datedepart'),
            'value' =>  function ($data) {
                return $data->datedepart;
            },
        ],
        [
            'label' => Yii::t('app', 'dateretourprob'),
            'value' =>  function ($data) {
                return $data->dateretourprob;
            },
        ],


        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{detail}',
            'headerOptions' => ['width' => '40'],
            'visible' => $droits[2] == 1 ? true : false,
            'buttons' => [
                'detail' => function ($url, $data) {
                    $url = Yii::$app->homeUrl . 'detail_mission?key=' . $data->keymission;
                    return '<a title="' . Yii::t('app', 'detail') . '" href="' . $url . '"><i class="fa fa-eye" style="color:green"></i></a>';
                },
            ],

        ],

        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{update}',
            'headerOptions' => ['width' => '40'],
            'visible' => $droits[2] == 1 ? true : false,
            'buttons' => [
                'update' => function ($url, $data) {
                    return '<a title="' . Yii::t('app', 'updatebutton') . '" href="#" data-toggle="modal" data-target=".Form-modal-lg"  onclick="update_mission(\'' . $data->keymission . '\')"><i class="fa fa-edit" style="color:green"></i></a>';
                },
            ],

        ],
        /*[
            'class' => 'yii\grid\ActionColumn',
            'template' => '{active}',
            'headerOptions' => ['width' => '40'],
            'visible' => $droits[3] == 1 ? true : false,
            'buttons' => [
                'active' => function ($url, $data) {

                    if ($data->status == 1) {
                        return '<a title="' . Yii::t('app', 'desactive') . '" href="#" data-toggle="modal" data-target=".bs-example-modal-lg"  onclick="desactive_member(\'' . $data->keymission . '\')"><i class="fa fa-check" style="color:red"></i></a>';
                    } else {
                        return '<a title="' . Yii::t('app', 'active') . '" href="#" data-toggle="modal" data-target=".bs-example-modal-lg"  onclick="active_member(\'' . $data->keymission . '\')"><i class="fa fa-check" style="color:blue"></i></a>';
                    }
                },
            ],

        ],*/
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{delete}',
            'headerOptions' => ['width' => '40'],
            'visible' => $droits[3] == 1 ? true : false,
            'buttons' => [
                'delete' => function ($url, $data) {
                    return '<a title="' . Yii::t('app', 'delete') . '" href="#" data-toggle="modal" data-target=".bs-example-modal-lg"  onclick="delete_member(\'' . $data->keymission . '\')"><i class="fa fa-window-close" style="color:red"></i></a>';
                },
            ],

        ]
    ],

]);
