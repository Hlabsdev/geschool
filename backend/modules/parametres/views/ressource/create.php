<?php

use api\modules\geschool\v1\models\Etatusage;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\controllers\Utils;

$required_sign = Utils::required();


?>
<?php $form = ActiveForm::begin([
    'id' => 'form-signup',
    'fieldConfig' => [
        'options' => [
            'tag' => false,
        ]
    ]
]); ?>

<div class="card-body card-block">

    <!--  designation ressource deb  -->
    <div class="row form-group">
        <div class="col col-md-3"><label for="text-input" class=" form-control-label"><?= Yii::t('app', 'designation') ?> <?= $required_sign ?></label></div>
        <div class="input-group col-md-8">
            <?= $form->field($model, 'designation')->textinput(['required' => 'required'])->error(false)->label(false); ?>
        </div>
    </div>
    <!--  designation ressource fin  -->

    <!--  coderessource deb  -->
    <div class="row form-group">
        <div class="col col-md-3"><label for="text-input" class=" form-control-label"><?= Yii::t('app', 'code') ?> <?= $required_sign ?></label></div>
        <div class="input-group col-md-8">
            <?= $form->field($model, 'coderessource')->textInput(['required' => 'required'])->error(false)->label(false); ?>
        </div>
    </div>
    <!--  coderessource fin  -->

    <!--  EtatUsage selection deb  -->
    <div class="row form-group">
        <div class="col col-md-3"><label for="text-input" class=" form-control-label"><?= Yii::t('app', 'etat') ?><?= $required_sign ?></label></div>

        <div class="input-group col-md-8">
            <?= $form->field($model, 'idetatusage')->dropdownList(
                ArrayHelper::map($all_etat, 'keyetatusage', 'libelle'),
                ['prompt' => Yii::t('app', 'select_etatusage'), 'onchange' => '', 'required' => true],
                ['class' => 'form-control']
            )->error(false)->label(false); ?>
        </div>

    </div>
    <!--  EtatUsage selection fin  -->

    <input type="text" name="fauxinput" class="fauxinput" value="">
</div>

<?php ActiveForm::end(); ?>