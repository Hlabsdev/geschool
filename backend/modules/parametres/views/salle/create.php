<?php

use backend\modules\parametres\models\SchoolCenter;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\controllers\Utils;

$required_sign = Utils::required();

?>
<?php $form = ActiveForm::begin([
    'id' => 'form-signup',
    'fieldConfig' => [
        'options' => [
            'tag' => false,
        ]
    ]
]); ?>

<div class="card-body card-block">

    <!--  libelle salle deb  -->
    <div class="row form-group">
        <div class="col col-md-3"><label for="text-input" class=" form-control-label"><?= Yii::t('app', 'libelle') ?> <?= $required_sign ?></label></div>
        <div class="input-group col-md-8">
            <?= $form->field($model, 'libelle')->textinput(['required' => 'required'])->error(false)->label(false); ?>
        </div>
    </div>
    <!--  libelle salle fin  -->

    <!--  capacite deb  -->
    <div class="row form-group">
        <div class="col col-md-3"><label for="text-input" class=" form-control-label"><?= Yii::t('app', 'Capacité') ?> <?= $required_sign ?></label></div>
        <div class="input-group col-md-8">
            <?= $form->field($model, 'nbreplacemaxi')->textInput(['required' => 'required', 'type' => 'number'])->error(false)->label(false); ?>
        </div>
    </div>
    <!--  capacite fin  -->
    <input type="text" name="fauxinput" class="fauxinput" value="">

</div>

<?php ActiveForm::end(); ?>