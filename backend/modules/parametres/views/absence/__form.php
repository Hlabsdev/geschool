<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\users\models\Absence */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="absence-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'idtypeabsence')->textInput() ?>

    <?= $form->field($model, 'iduser')->textInput() ?>

    <?= $form->field($model, 'keyabsence')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'datedemandeabsence')->textInput() ?>

    <?= $form->field($model, 'motifabsence')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'datedebutabsence')->textInput() ?>

    <?= $form->field($model, 'heuredebutabsence')->textInput() ?>

    <?= $form->field($model, 'datefinabsence')->textInput() ?>

    <?= $form->field($model, 'heurefinabsence')->textInput() ?>

    <?= $form->field($model, 'coordonneesabsence')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'lieudestinantionabsence')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'heurearattraper')->textInput() ?>

    <?= $form->field($model, 'status')->textInput() ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>

    <?= $form->field($model, 'create_by')->textInput() ?>

    <?= $form->field($model, 'updated_by')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
