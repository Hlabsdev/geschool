<?php

namespace backend\modules\parametres\models;

use Yii;

/**
 * This is the model class for table "user_profil".
 *
 * @property integer $id_user_profil
 * @property integer $id_center
 * @property string $nameProfil
 * @property string $descriptionProfil
 * @property string $key_profil
 * @property integer $etat_user_profil
 * @property integer $id_userCreate
 * @property integer $id_userUpdate
 * @property string $date_create_profil
 * @property string $date_update_profil
 *
 * @property UserAccess[] $userAccesses
 * @property SchoolCenter $idCenter
 */
class UserProfil extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_profil';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_center', 'nameProfil', 'key_profil', 'etat_user_profil', 'id_userCreate', 'id_userUpdate', 'date_create_profil', 'date_update_profil'], 'required'],
            [['id_center', 'etat_user_profil', 'id_userCreate', 'id_userUpdate'], 'integer'],
            [['descriptionProfil'], 'string'],
            [['date_create_profil', 'date_update_profil'], 'safe'],
            [['nameProfil', 'key_profil'], 'string', 'max' => 255],
            [['id_center'], 'exist', 'skipOnError' => true, 'targetClass' => SchoolCenter::className(), 'targetAttribute' => ['id_center' => 'id_center']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_user_profil' => 'Id User Profil',
            'id_center' => 'Id Center',
            'nameProfil' => 'Name Profil',
            'descriptionProfil' => 'Description Profil',
            'key_profil' => 'Key Profil',
            'etat_user_profil' => 'Etat User Profil',
            'id_userCreate' => 'Id User Create',
            'id_userUpdate' => 'Id User Update',
            'date_create_profil' => 'Date Create Profil',
            'date_update_profil' => 'Date Update Profil',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserAccesses()
    {
        return $this->hasMany(UserAccess::className(), ['id_user_profil' => 'id_user_profil']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdCenter()
    {
        return $this->hasOne(SchoolCenter::className(), ['id_center' => 'id_center']);
    }
}
