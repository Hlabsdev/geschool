<?php

namespace backend\modules\parametres\models;

use Yii;

/**
 * This is the model class for table "Personnel".
 *
 * @property integer $iduser
 * @property string $keypersonnel
 * @property string $indicegrade
 * @property string $situationmatrimoniale
 * @property string $dateembauche
 * @property string $matricule
 * @property string $sectionconcerne
 * @property integer $typepersonnel
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $create_by
 * @property integer $updated_by
 */
class Personnel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Personnel';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['iduser', 'keypersonnel', 'dateembauche', 'matricule', 'typepersonnel', 'created_at', 'create_by'], 'required'],
            [['iduser', 'typepersonnel', 'status', 'created_at', 'updated_at', 'create_by', 'updated_by'], 'integer'],
            [['dateembauche'], 'safe'],
            [['sectionconcerne'], 'string'],
            [['keypersonnel'], 'string', 'max' => 32],
            [['indicegrade', 'situationmatrimoniale', 'matricule'], 'string', 'max' => 25],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'iduser' => 'Iduser',
            'keypersonnel' => 'Keypersonnel',
            'indicegrade' => 'Indicegrade',
            'situationmatrimoniale' => 'Situationmatrimoniale',
            'dateembauche' => 'Dateembauche',
            'matricule' => 'Matricule',
            'sectionconcerne' => 'Sectionconcerne',
            'typepersonnel' => 'Typepersonnel',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'create_by' => 'Create By',
            'updated_by' => 'Updated By',
        ];
    }
}
