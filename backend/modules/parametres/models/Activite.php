<?php

namespace backend\modules\parametres\models;

use Yii;

/**
 * This is the model class for table "activite".
 *
 * @property integer $id
 * @property string $keyactivite
 * @property integer $idcategorieactivite
 * @property integer $idcentreannee
 * @property integer $iduserexec
 * @property integer $idusersuivi
 * @property string $designationactivite
 * @property string $datedebutactivite
 * @property string $datefinactivite
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $create_by
 * @property integer $updated_by
 *
 * @property Categorieactivite $idcategorieactivite0
 * @property Centreannee $idcentreannee0
 */
class Activite extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'activite';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['keyactivite', 'idcategorieactivite', 'idcentreannee', 'iduserexec', 'idusersuivi', 'designationactivite', 'datedebutactivite', 'datefinactivite', 'created_at', 'create_by'], 'required'],
            [['idcategorieactivite', 'idcentreannee', 'iduserexec', 'idusersuivi', 'status', 'created_at', 'updated_at', 'create_by', 'updated_by'], 'integer'],
            [['datedebutactivite', 'datefinactivite'], 'safe'],
            [['keyactivite'], 'string', 'max' => 32],
            [['designationactivite'], 'string', 'max' => 254],
            [['idcategorieactivite'], 'exist', 'skipOnError' => true, 'targetClass' => Categorieactivite::className(), 'targetAttribute' => ['idcategorieactivite' => 'id']],
            [['idcentreannee'], 'exist', 'skipOnError' => true, 'targetClass' => Centreannee::className(), 'targetAttribute' => ['idcentreannee' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'keyactivite' => 'Keyactivite',
            'idcategorieactivite' => 'Idcategorieactivite',
            'idcentreannee' => 'Idcentreannee',
            'iduserexec' => 'Iduserexec',
            'idusersuivi' => 'Idusersuivi',
            'designationactivite' => 'Designationactivite',
            'datedebutactivite' => 'Datedebutactivite',
            'datefinactivite' => 'Datefinactivite',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'create_by' => 'Create By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdcategorieactivite0()
    {
        return $this->hasOne(Categorieactivite::className(), ['id' => 'idcategorieactivite']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdcentreannee0()
    {
        return $this->hasOne(Centreannee::className(), ['id' => 'idcentreannee']);
    }
}
