<?php

namespace backend\modules\parametres\models;

use Yii;

/**
 * This is the model class for table "decoupage".
 *
 * @property integer $id
 * @property integer $idtypedecoupage
 * @property integer $idannee
 * @property integer $idcentre
 * @property string $keydecoupage
 * @property string $datedebutdecoupage
 * @property string $datefindecoupage
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $create_by
 * @property integer $updated_by
 */
class Decoupage extends \yii\db\ActiveRecord
{
    public $keytypedecoupage;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'decoupage';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idtypedecoupage', 'idannee', 'idcentre', 'keydecoupage', 'datedebutdecoupage', 'datefindecoupage', 'created_at', 'create_by'], 'required'],
            [['idtypedecoupage', 'idannee', 'idcentre', 'status', 'created_at', 'updated_at', 'create_by', 'updated_by'], 'integer'],
            [['datedebutdecoupage', 'datefindecoupage'], 'safe'],
            [['keydecoupage'], 'string', 'max' => 32],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idtypedecoupage' => 'Idtypedecoupage',
            'idannee' => 'Idannee',
            'idcentre' => 'Idcentre',
            'keydecoupage' => 'Keydecoupage',
            'datedebutdecoupage' => 'Datedebutdecoupage',
            'datefindecoupage' => 'Datefindecoupage',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'create_by' => 'Create By',
            'updated_by' => 'Updated By',
        ];
    }
}
