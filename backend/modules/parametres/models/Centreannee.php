<?php

namespace backend\modules\parametres\models;

use Yii;

/**
 * This is the model class for table "centreannee".
 *
 * @property integer $id
 * @property integer $idannee
 * @property integer $idcentre
 * @property string $keycentreannee
 * @property string $datedebutcentreannee
 * @property string $datefincentreannee
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $create_by
 * @property integer $updated_by
 *
 * @property Activite[] $activites
 * @property Anneescolaire $idannee0
 * @property SchoolCenter $idcentre0
 * @property Decoupage[] $decoupages
 */
class Centreannee extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'centreannee';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idannee', 'idcentre', 'keycentreannee', 'datedebutcentreannee', 'datefincentreannee', 'created_at', 'create_by'], 'required'],
            [['idannee', 'idcentre', 'status', 'created_at', 'updated_at', 'create_by', 'updated_by'], 'integer'],
            [['datedebutcentreannee', 'datefincentreannee'], 'safe'],
            [['keycentreannee'], 'string', 'max' => 32],
            [['idannee'], 'exist', 'skipOnError' => true, 'targetClass' => Anneescolaire::className(), 'targetAttribute' => ['idannee' => 'id']],
            [['idcentre'], 'exist', 'skipOnError' => true, 'targetClass' => SchoolCenter::className(), 'targetAttribute' => ['idcentre' => 'id_center']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idannee' => 'Idannee',
            'idcentre' => 'Idcentre',
            'keycentreannee' => 'Keycentreannee',
            'datedebutcentreannee' => 'Datedebutcentreannee',
            'datefincentreannee' => 'Datefincentreannee',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'create_by' => 'Create By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActivites()
    {
        return $this->hasMany(Activite::className(), ['idcentreannee' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdannee0()
    {
        return $this->hasOne(Anneescolaire::className(), ['id' => 'idannee']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdcentre0()
    {
        return $this->hasOne(SchoolCenter::className(), ['id_center' => 'idcentre']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDecoupages()
    {
        return $this->hasMany(Decoupage::className(), ['idcentreannee' => 'id']);
    }
}
