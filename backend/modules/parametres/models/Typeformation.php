<?php

namespace backend\modules\parametres\models;

use Yii;

/**
 * This is the model class for table "typeformation".
 *
 * @property integer $id
 * @property string $libelletypeformation
 * @property string $keytypeformation
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $create_by
 * @property integer $updated_by
 */
class Typeformation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'typeformation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at', 'create_by', 'libelletypeformation' , 'keytypeformation'], 'required'],
            [['status', 'created_at', 'updated_at', 'create_by', 'updated_by'], 'integer'],
            [['libelletypeformation'], 'string', 'max' => 254],
            [['keytypeformation'], 'string', 'max' => 32],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'libelletypeformation' => 'Libelletypeformation',
            'keytypeformation' => 'Keytypeformation',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'create_by' => 'Create By',
            'updated_by' => 'Updated By',
        ];
    }
}
