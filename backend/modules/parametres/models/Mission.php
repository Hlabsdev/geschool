<?php

namespace backend\modules\parametres\models;

use Yii;

/**
 * This is the model class for table "mission".
 *
 * @property integer $id
 * @property string $keymission
 * @property string $motif
 * @property string $datedepart
 * @property string $lieumission
 * @property string $itineraireretenu
 * @property string $dateretourprob
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $create_by
 * @property integer $updated_by
 *
 * @property Detailmission[] $detailmissions
 */
class Mission extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mission';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['keymission', 'datedepart', 'lieumission', 'dateretourprob', 'created_at', 'create_by'], 'required'],
            [['motif', 'itineraireretenu'], 'string'],
            [['datedepart', 'dateretourprob'], 'safe'],
            [['status', 'created_at', 'updated_at', 'create_by', 'updated_by'], 'integer'],
            [['keymission'], 'string', 'max' => 32],
            [['lieumission'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'keymission' => 'Keymission',
            'motif' => 'Motif',
            'datedepart' => 'Datedepart',
            'lieumission' => 'Lieumission',
            'itineraireretenu' => 'Itineraireretenu',
            'dateretourprob' => 'Dateretourprob',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'create_by' => 'Create By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDetailmissions()
    {
        return $this->hasMany(Detailmission::className(), ['idmission' => 'id']);
    }
}
