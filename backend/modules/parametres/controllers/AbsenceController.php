<?php

namespace backend\modules\parametres\controllers;

use api\modules\geschool\v1\models\Personnel;
use backend\modules\parametres\models\Typeabsence;
use Yii;
use backend\modules\parametres\models\Absence;
use yii\data\ArrayDataProvider;
use backend\controllers\DefaultController;
use backend\controllers\Utils;
use backend\modules\parametres\models\User;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;

/**
 * AbsenceController implements the CRUD actions for absence model.
 */
class AbsenceController extends DefaultController
{
    public $page_title = "";
    public $breadcrumb = "";
    public $select_menu = "ABSENCEPERSONEL";

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['add_absence', 'all_absence', 'operation', 'create_absence', 'center_module', 'checking_center', 'save_absence'],
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    //Cette fonction permet de lister toutes les absences d'un centre

    public function actionAll_absence()
    {
        //On verifie les droits de l'user
        Utils::Rule("manage_absence", "READ");
        $id_center = Yii::$app->session->get('default_center');
        $response_data = array();


        //On pr�pare les donn�es
        $url_api = Utils::getApiUrl() . "absences/absences";
        $ch = curl_init();
        $curlConfig = array(
            CURLOPT_URL => $url_api,
            CURLOPT_POST => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_CONNECTTIMEOUT => 5,
            CURLOPT_POSTFIELDS => array(
                'token' => Utils::getApiKey(),
                'user_auth_key' => Yii::$app->user->identity->getAuthKey(),
                'id_center' => $id_center,
            ),
        );

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt_array($ch, $curlConfig);
        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($result);

        // print_r($response);
        // die;

        if (isset($response->status) && ($response->status == '000')) {
            if (sizeof($response->data) > 0) {
                $response_data = $response->data;
            }
        } elseif ($response->status == '001') {
            //Utilisateur n'as pas droit � la ressource
            return Yii::$app->getResponse()->redirect("account");
        } else {
            $message = Yii::t('app', 'update_error'); // Une erreur s'est produite
            Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
        }

        $donnee['all_absence'] = new ArrayDataProvider([
            'allModels' => $response_data,
            'pagination' => [
                'pageSize' => -1,
            ],
        ]);


        //Initialisation des titres de page
        $title = Yii::t('app', 'all_absence_personnel_menu');
        $board = array("add_absence_personnel" => $title);
        $this->page_title = $title;
        $this->breadcrumb = Utils::retour($board);

        return $this->render('index', array('donnee' => $donnee));
    }

    //Cette fonction permet d'ajouter un type d'absence
    public function actionAdd_absence()
    {
        //On v�rifie les droits de l'user
        Utils::Rule("manage_absence", "CREATE");
        $model = new Absence();

        //Selection des types d'absences
        $url_api = Utils::getApiUrl() . "typeabsences/typeabsences";
        $ch = curl_init();
        $curlConfig = array(
            CURLOPT_URL => $url_api,
            CURLOPT_POST => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_CONNECTTIMEOUT => 5,
            CURLOPT_POSTFIELDS => array(
                'token' => Utils::getApiKey(),
                'user_auth_key' => Yii::$app->user->identity->getAuthKey(),
            ),
        );
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt_array($ch, $curlConfig);
        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($result);



        $tab_request['status'] = ['1'];
        $all_typeabsence = Typeabsence::find()->where($tab_request)->all();
        // $tab_request2['status'] = ['10'];
        // $all_user = User::find()->where($tab_request2)->all();
        $all_personnel = Personnel::find()->where($tab_request)->all();
        $tab_personnel_result = array();
        $tab_result = array();
        foreach ($all_personnel as $line) {
            $user = User::find()->where(['id' => $line->iduser])->one();

            $tab_result['iduser'] = $line->iduser;
            $tab_result['nom'] = $user->nom;
            $tab_result['prenoms'] = $user->prenoms;
            $tab_result['info_personnel'] = $user->nom . ' ' . $user->prenoms;
            $tab_personnel_result[] = $tab_result;
        }



        //Initialisation des titres de page
        $title = Yii::t('app', 'add_absence_personnel_titre');
        $board = array("add_absence_personnel" => $title);
        $this->page_title = $title;
        $this->breadcrumb = Utils::retour($board);

        if (Yii::$app->request->post()) {
            if ($model->load(Yii::$app->request->post())) {

                //Execution de l'api pour l'ajout
                $url_api = Utils::getApiUrl() . "typeabsences/addtypeabsence";
                $ch = curl_init();
                $curlConfig = array(
                    CURLOPT_URL => $url_api,
                    CURLOPT_POST => true,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_CONNECTTIMEOUT => 5,
                    CURLOPT_POSTFIELDS => array(
                        'token' => 'SzrQt6TazKv2ayDHqs754qvvAvD',
                        'designation' => $model->designationtypeabsence,
                        'operation' => 1,
                        //'user' => Yii::$app->user->identity->id,
                        'user_auth_key' => Yii::$app->user->identity->getAuthKey(),
                    ),
                );
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
                curl_setopt_array($ch, $curlConfig);
                $result = curl_exec($ch);
                curl_close($ch);
                $response = json_decode($result);
                if (isset($response->status) && ($response->status == '000')) {
                    //if($model->save()){
                    $message = Yii::t('app', 'succes_creation_typeabsence');
                    Yii::$app->getSession()->setFlash('success', Yii::t('app', $message));
                    $model = new Typeabsence();
                } else {
                    $message = Yii::t('app', 'update_error');
                    Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
                }
            } else {
                $message = Yii::t('app', 'input_empty');
                Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
            }
        }

        return $this->render('create', array('model' => $model, 'all_typeabsence' => $all_typeabsence, 'all_personnel' => $tab_personnel_result));
    }


    //Cette fonction permet de charger le formulaire  de modification d'une absence
    public function actionCreate_absence()
    {
        $return_info = "";
        $this->layout = false;
        $manage_absence = Utils::have_access("manage_absence");
        if ($manage_absence == "0_0_0_0") {
            return  $return_info;
        }
        $test = explode('_', $manage_absence);

        if ($test[2] == 0) {
            return  $return_info;
        }


        if (Yii::$app->request->isPost) {

            $tab_request['status'] = ['1'];
            $all_typeabsence = Typeabsence::find()->where($tab_request)->all();
            // $tab_request2['status'] = ['10'];
            // $all_user = User::find()->where($tab_request2)->all();

            $all_personnel = Personnel::find()->where($tab_request)->all();
            $tab_personnel_result = array();
            $tab_result = array();
            foreach ($all_personnel as $line) {
                $user = User::find()->where(['id' => $line->iduser])->one();

                $tab_result['iduser'] = $line->iduser;
                $tab_result['nom'] = $user->nom;
                $tab_result['prenoms'] = $user->prenoms;
                $tab_result['info_personnel'] = $user->nom . ' ' . $user->prenoms;
                $tab_personnel_result[] = $tab_result;
            }

            $id_center = Yii::$app->session->get('default_center');


            $info = Yii::$app->request->post();

            $key = $info['key'];


            if ($key == '0') {
                $model = new Absence();
            } else {

                $url_api = Utils::getApiUrl() . "absences/absence";
                $ch = curl_init();
                $curlConfig = array(
                    CURLOPT_URL => $url_api,
                    CURLOPT_POST => true,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_CONNECTTIMEOUT => 5,
                    CURLOPT_POSTFIELDS => array(
                        'token' => Utils::getApiKey(),
                        'user_auth_key' => Yii::$app->user->identity->getAuthKey(),
                        'abscence_key' => $key,
                        'id_center' => $id_center,
                    ),
                );
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
                curl_setopt_array($ch, $curlConfig);
                $result = curl_exec($ch);
                curl_close($ch);
                $response = json_decode($result);
                //Fin recherche


                if ($response->status == '000') {
                    //On convertie l'objet en model
                    $find_absence = new Absence();
                    $find_absence->load((array)$response->data, '');
                    $model = $find_absence;
                } else {
                    $model = new Absence();
                }
            }

            $this->layout = false;
            return $this->render('create', array('model' => $model, 'all_typeabsence' => $all_typeabsence, 'all_personnel' => $tab_personnel_result));
        }

        return $return_info;
    }


    //Cette fonction permet de sauvegarder une nouvelle absence 
    public function actionSave_absence()
    {
        $return_info = "";
        $this->layout = false;
        $manage_absence = Utils::have_access("manage_absence");
        if ($manage_absence == "0_0_0_0") {
            return  $return_info;
        }
        $test = explode('_', $manage_absence);

        if ($test[0] == 0 && $test[2] == 0) {
            return  $return_info;
        }
        if (Yii::$app->request->isPost) {
            $id_center = Yii::$app->session->get('default_center');

            $info = Yii::$app->request->post();


            $key = urldecode($info['key']);
            $motif = urldecode($info['motif']);
            $date_demande = urldecode($info['date_demande']);
            $date_debut = urldecode($info['date_debut']);
            $date_fin = urldecode($info['date_fin']);
            $personnel = urldecode($info['personnel']);
            $type_absence = urldecode($info['type_absence']);
            $heure_debut = urldecode($info['heure_debut']);
            $heure_fin = urldecode($info['heure_fin']);
            $coordonnees = urldecode($info['coordonnees']);
            $lieu_destinantion = urldecode($info['lieu_destinantion']);
            $heure_a_rattraper = urldecode($info['heure_a_rattraper']);


            $fullInfo = array();
            $fullInfo['token'] = Utils::getApiKey();
            $fullInfo['motif'] = $motif;
            $fullInfo['date_demande'] = $date_demande;
            $fullInfo['date_debut'] = $date_debut;
            $fullInfo['date_fin'] = $date_fin;
            $fullInfo['personnel'] = $personnel;
            $fullInfo['type_absence'] = $type_absence;
            $fullInfo['heure_debut'] = $heure_debut;
            $fullInfo['heure_fin'] = $heure_fin;
            $fullInfo['coordonnees'] = $coordonnees;
            $fullInfo['lieu_destinantion'] = $lieu_destinantion;
            $fullInfo['heure_a_rattraper'] = $heure_a_rattraper;
            $fullInfo['operation'] = 1;
            $fullInfo['user_auth_key'] = Yii::$app->user->identity->getAuthKey();
            $fullInfo['id_center'] = $id_center;

            if ($key != '0') {
                $fullInfo['abscence_key'] = $key;
                $fullInfo['operation'] = 2;
            }


            $url_api = Utils::getApiUrl() . "absences/addabsence";
            $ch = curl_init();
            $curlConfig = array(
                CURLOPT_URL => $url_api,
                CURLOPT_POST => true,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_CONNECTTIMEOUT => 5,
                CURLOPT_POSTFIELDS => $fullInfo,
            );
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt_array($ch, $curlConfig);
            $result = curl_exec($ch);
            curl_close($ch);
            //Fin execution
            $response = json_decode($result);

            print_r($response);
            die;

            if (isset($response->status)) {
                if ($response->status == '000') {
                    $donnee['all_absence'] = new ArrayDataProvider([
                        'allModels' => $response->data,
                        'pagination' => [
                            'pageSize' => -1,
                        ],
                    ]);

                    $return_info = $this->render('liste', array('donnee' => $donnee));
                } elseif ($response->status == '001') {
                    // Un element manque sur le formulaire soumis
                    $return_info = "1";
                } elseif ($response->status == '003') {
                    // L'element existe déjà
                    $return_info = "3";
                } else {
                    $return_info = "0";
                }
            } else {
                $return_info = "0";
            }
        }
        return $return_info;
    }


    //Cette fonction permet de  supprimer une absence
    public function actionOperation()
    {
        //On v�rifie les droits de l'user
        Utils::Rule("manage_absence", "DELETE");
        if (isset($_POST['operation']) != "" and isset($_POST['id']) != "") {

            $etat = $_POST['operation'];
            $keyabsence = $_POST['id'];

            //On recherche la ressource � modifier
            $url_api = Utils::getApiUrl() . "absences/absence";
            $ch = curl_init();
            $curlConfig = array(
                CURLOPT_URL => $url_api,
                CURLOPT_POST => true,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_CONNECTTIMEOUT => 5,
                CURLOPT_POSTFIELDS => array(
                    'token' =>  Utils::getApiKey(),
                    'abscence_key' => $keyabsence,
                    'user_auth_key' => Yii::$app->user->identity->getAuthKey(),

                ),
            );
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt_array($ch, $curlConfig);
            $result = curl_exec($ch);
            curl_close($ch);
            $response = json_decode($result);
            //Fin recherche

            if ($response->status == '000') {
                //Execution de l'api pour la modification
                $url_api = Utils::getApiUrl() . "absences/deleteabsence";
                $ch = curl_init();
                $curlConfig = array(
                    CURLOPT_URL => $url_api,
                    CURLOPT_POST => true,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_CONNECTTIMEOUT => 5,
                    CURLOPT_POSTFIELDS => array(
                        'token' => Utils::getApiKey(),
                        'abscence_key' => $keyabsence,
                        'operation' => $etat,
                        'user_auth_key' => Yii::$app->user->identity->getAuthKey(),
                    ),
                );
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
                curl_setopt_array($ch, $curlConfig);
                $result = curl_exec($ch);
                curl_close($ch);
                //Fin execution

                $response = json_decode($result);

                if ($response->status == '000') {

                    //$message=$response->message;
                    /*if($etat==1){
                        $message=Yii::t('app','active_diplome_success');
                    }else if($etat==2){
                        $message=Yii::t('app','desactive_diplome_success');
                    }else */
                    if ($etat == 3) {
                        $message = Yii::t('app', 'delete_absence_success');
                    }
                    Yii::$app->getSession()->setFlash('success', Yii::t('app', $message));
                } else {
                    $message = Yii::t('app', 'update_error');
                    Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
                }
            } else {
                $message = Yii::t('app', 'update_error');
                Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
            }
        } else {
            $message = Yii::t('app', 'update_error');
            Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
        }
    }


    /**
     * Lists all absence models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Absence::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single absence model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new absence model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Absence();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing absence model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing absence model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the absence model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Absence the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Absence::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
