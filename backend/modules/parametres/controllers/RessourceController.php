<?php

namespace backend\modules\parametres\controllers;

use backend\controllers\DefaultController;
use backend\controllers\Utils;
use backend\models\ApiCurl;
use Yii;
use backend\modules\parametres\models\Ressource;
use yii\data\ArrayDataProvider;
use yii\filters\AccessControl;

/**
 * RessourceController implements the CRUD actions for Ressource model.
 */
class RessourceController extends DefaultController
{
    public $page_title = "";
    public $breadcrumb = "";
    public $select_menu = "PARAMSYSTEME";

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['add_ressource', 'all_ressource', 'operation', 'create_ressource', 'save_ressource'],
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }


    //Cette fonction permet de lister toutes les ressources d'un centre
    public function actionAll_ressource()
    {
        //On verifie les droits de l'user
        Utils::Rule("manage_ressource", "READ");
        $response_data = array();

        $apicurl = new ApiCurl();
        $response = $apicurl->ApiPost('ressources/ressources', []);
        $decode = json_decode($response);
        $status = '';

        if (isset($decode->status)) $status = $decode->status;

        if ($status == '000') {
            $response_data = $decode->data;
        } elseif ($status == '001') {
            $response_data;
        } else {
            $message = Yii::t('app', 'update_error'); // Une erreur s'est produite
            Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
        }


        $donnee['all_ressource'] = new ArrayDataProvider([
            'allModels' => $response_data,
            'pagination' => [
                'pageSize' => -1,
            ],
        ]);

        //Initialisation des titres de page
        $title = Yii::t('app', 'all_ressource_menu');
        $board = array("add_ressource" => $title);
        $this->page_title = $title;
        $this->breadcrumb = Utils::retour($board);

        return $this->render('index', array('donnee' => $donnee));
    }


    //Cette fonction permet de charger le formulaire  de modification d'une ressource
    public function actionCreate_ressource()
    {
        $return_info = "";
        $data = array();
        $all_etat = array();
        $this->layout = false;
        $manage_ressource = Utils::have_access("manage_ressource");
        if ($manage_ressource == "0_0_0_0") {
            return  $return_info;
        }
        $test = explode('_', $manage_ressource);

        if ($test[2] == 0) {
            return  $return_info;
        }
        if (Yii::$app->request->isPost) {

            /* Début Liste des etas par Api */
            $apicurl = new ApiCurl();
            $response = $apicurl->ApiPost('ressources/etatusages', $data);
            $decode = json_decode($response);
            $status = '';
            if (isset($decode->status)) $status = $decode->status;

            if ($status == '000') {
                $all_etat = $decode->data;
            }
            /* Fin Liste des etas par Api */

            $info = Yii::$app->request->post();
            $key = $info['key'];

            if ($key == '0') {
                $model = new Ressource;
            } else {
                /* Debut recherche de la ressource */
                $data['ressource_key'] = $key;

                $apicurl = new ApiCurl();
                $response = $apicurl->ApiPost('ressources/ressource', $data);
                $decode = json_decode($response);
                $status = '';
                if (isset($decode->status)) $status = $decode->status;
                /* Fin recherche de la ressource */

                if ($status == '000') {
                    //Si Specialte trouve, On convertie l'objet de type Api en model
                    $find_ressource = new Ressource;
                    $find_ressource->load((array)$decode->data, '');
                    $find_ressource->idetatusage = $decode->data->keyetatusage;
                    $model = $find_ressource;
                } else {
                    $model = new Ressource;
                }
            }



            $this->layout = false;
            return $this->render('create', array('model' => $model, 'all_etat' => $all_etat));
        }
        return $return_info;
    }


    //Cette fonction permet de sauvegarder une nouvelle ressource
    public function actionSave_ressource()
    {
        $return_info = "";
        $this->layout = false;
        $manage_ressource = Utils::have_access("manage_ressource");
        if ($manage_ressource == "0_0_0_0") {
            return  $return_info;
        }
        $test = explode('_', $manage_ressource);

        if ($test[0] == 0 && $test[2] == 0) {
            return  $return_info;
        }
        if (Yii::$app->request->isPost) {
            $info = Yii::$app->request->post();

            $key = urldecode($info['key']);
            $designation = urldecode($info['designation']);
            $code = urldecode($info['code']);
            $etat = urldecode($info['etat']);

            /* Début Liste des etas par Api */
            $data = array();
            $apicurl = new ApiCurl();
            $response = $apicurl->ApiPost('ressources/etatusages', $data);
            $decode = json_decode($response);
            $status = '';
            if (isset($decode->status)) $status = $decode->status;
            /* Fin Liste des etas par Api */

            $fullInfo = array();
            $fullInfo['designation'] = $designation;
            $fullInfo['code'] = $code;
            $fullInfo['etat'] = $etat;
            $fullInfo['operation'] = 1;

            if ($key != '0') {
                $fullInfo['ressource_key'] = $key;
                $fullInfo['operation'] = 2;
            }

            $apicurl = new ApiCurl();
            $response = $apicurl->ApiPost('ressources/addressource', $fullInfo);
            $decode = json_decode($response);
            $status = '';
            if (isset($decode->status)) $status = $decode->status;


            if ($status == '000') { // TOUT S'EST BIEN PASSÉ
                $donnee['all_ressource'] = new ArrayDataProvider([
                    'allModels' => $decode->data,
                    'pagination' => [
                        'pageSize' => -1,
                    ],
                ]);

                $return_info = $this->render('liste', array('donnee' => $donnee));
            } else if ($status == '001' || $status == '003') { // ERREUR UTILISATEUR (PARAMETRE OU CHAMPS MANQUANT DEPUIS LE FORMULAIRE)
                $return_alert = '<div class="alert alert-danger alert-dismissible show fade" style="margin-bottom: 30px">
                        <div class="alert-body">
                        <button class="close" data-dismiss="alert">
                            <span>×</span>
                        </button>
                        ' . Yii::t('app', $decode->message) . '
                        </div>
                    </div>';
                $return_info = '1###' . $return_alert;
            } else { // ERREUR DEVELOPPEUR (PARAMETRE OU CHAMPS MANQUANT)
                $return_info = "0";
            }
        }
        return $return_info;
    }


    //Cette fonction permet de  supprimer une ressource
    public function actionOperation()
    {
        //On vérifie les droits de l'user
        Utils::Rule("manage_ressource", "DELETE");
        if (isset($_POST['operation']) != "" and isset($_POST['id']) != "") {

            $operation = $_POST['operation'];
            $keyressource = $_POST['id'];

            $fullInfo = array(
                'ressource_key' => $keyressource,
                'operation' => $operation,
            );

            $apicurl = new ApiCurl();
            //Execution de l'api pour la modification
            $response = $apicurl->ApiPost("ressources/deleteressource", $fullInfo);
            $decode = json_decode($response);
            //Fin execution
            $status = '';

            if (isset($decode->status)) {
                $status = $decode->status;
                $message = $decode->message;
            }

            if ($status == '000') { // TOUT S'EST BIEN PASSÉ
                Yii::$app->getSession()->setFlash('success', Yii::t('app', $message));
            } else if ($status == '001') { // UNE ERREUR UTILISATEUR EST SURVENUE
                Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
            } else { // UNE ERREUR NE DEPENDANT PAS DE L'UTILISATEUR EST SURVENUE
                $message = Yii::t('app', 'update_error');
                Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
            }
        } else {
            $message = Yii::t('app', 'update_error');
            Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
        }
    }
}
