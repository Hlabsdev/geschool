<?php

namespace backend\modules\parametres\controllers;

use Yii;
use backend\modules\parametres\models\Decoupage;
use yii\data\ArrayDataProvider;
use backend\controllers\DefaultController;
use backend\controllers\Utils;
use yii\filters\AccessControl;
use backend\models\ApiCurl;

/**
 * DecoupageController implements the CRUD actions for decoupage model.
 */
class DecoupageController extends DefaultController
{
    public $page_title = "";
    public $breadcrumb = "";
    public $select_menu = "PARAMSYSTEME";

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['all_decoupage', 'operation', 'create_decoupage', 'save_decoupage'],
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    //Cette fonction permet de lister tous les decoupages
    public function actionAll_decoupage()
    {
        //On verifie les droits de l'user
        Utils::Rule("manage_decoupage", "READ");
        $response_data = array();

        $apicurl = new ApiCurl();
        $response = $apicurl->ApiPost('decoupages/decoupages', []);
        $decode = json_decode($response);
        $status = '';

        if (isset($decode->status)) $status = $decode->status;

        if ($status == '000') {
            $response_data = $decode->data;
        } elseif ($status == '001') {
            $response_data;
        } else {
            $message = Yii::t('app', 'update_error'); // Une erreur s'est produite
            Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
        }

        $donnee['all_decoupage'] = new ArrayDataProvider([
            'allModels' => $response_data,
            'pagination' => [
                'pageSize' => -1,
            ],
        ]);


        //Initialisation des titres de page
        $title = Yii::t('app', 'all_decoupage_menu');
        $board = array("add_decoupage" => $title);
        $this->page_title = $title;
        $this->breadcrumb = Utils::retour($board);

        return $this->render('index', array('donnee' => $donnee));
    }

    //Cette fonction permet de charger le formulaire d'ajout ou de modification decoupage
    public function actionCreate_decoupage()
    {
        $return_info = "";
        $data = array();
        $all_annee = array();
        $all_typedecoupage = array();
        $this->layout = false;
        $manage_decoupage = Utils::have_access("manage_decoupage");
        if ($manage_decoupage == "0_0_0_0") {
            return  $return_info;
        }
        $test = explode('_', $manage_decoupage);

        if ($test[2] == 0) {
            return  $return_info;
        }
        if (Yii::$app->request->isPost) {

            /* Début Liste des annees par Api */
            $apicurl = new ApiCurl();
            $response = $apicurl->ApiPost('anneescolaires/anneescolaires', $data);
            $decode = json_decode($response);
            $status = '';
            if (isset($decode->status)) $status = $decode->status;

            if ($status == '000') {
                $all_annee = $decode->data;
            }
            /* Fin Liste des annees par Api */

            /* Début Liste des typedecoupage par Api */
            $apicurl = new ApiCurl();
            $response = $apicurl->ApiPost('typedecoupages/typedecoupages', $data);
            $decode = json_decode($response);
            $status = '';
            if (isset($decode->status)) $status = $decode->status;

            if ($status == '000') {
                $all_typedecoupage = $decode->data;
            }
            /* Fin Liste des typedecoupage par Api */

            $info = Yii::$app->request->post();
            $key = $info['key'];

            if ($key == '0') {
                $model = new Decoupage();
            } else {
                /* Debut recherche de decoupage */
                $data['decoupage_key'] = $key;

                $apicurl = new ApiCurl();
                $response = $apicurl->ApiPost('decoupages/decoupage', $data);
                $decode = json_decode($response);
                $status = '';
                if (isset($decode->status)) $status = $decode->status;
                /* Fin recherche de decoupage */

                if ($status == '000') {
                    //On convertie l'objet en model
                    $find_decoupage = new Decoupage();
                    $find_decoupage->load((array)$decode->data, '');
                    $find_decoupage->idtypedecoupage = $decode->data->keytypedecoupage;
                    $model = $find_decoupage;
                } else {
                    $model = new Decoupage();
                }
            }



            $this->layout = false;
            return $this->render('create', array("model" => $model, 'all_typedecoupage' => $all_typedecoupage, 'all_annee' => $all_annee));
        }
        return $return_info;
    }

    //Cette fonction permet de sauvegarder un type de personnel
    public function actionSave_decoupage()
    {
        $return_info = "";
        $this->layout = false;
        $manage_decoupage = Utils::have_access("manage_decoupage");
        if ($manage_decoupage == "0_0_0_0") {
            return  $return_info;
        }
        $test = explode('_', $manage_decoupage);

        if ($test[0] == 0 && $test[2] == 0) {
            return  $return_info;
        }
        if (Yii::$app->request->isPost) {
            $info = Yii::$app->request->post();

            $key = urldecode($info['key']);
            $key_type = urldecode($info['key_type']);
            $datedebut = urldecode($info['datedebut']);
            $datefin = urldecode($info['datefin']);

            $fullInfo = array();

            $fullInfo['key_type'] = $key_type;
            $fullInfo['datedebut'] = $datedebut;
            $fullInfo['datefin'] = $datefin;
            $fullInfo['operation'] = 1;
            if ($key != '0') {
                $fullInfo['decoupage_key'] = $key;
                $fullInfo['operation'] = 2;
            }

            
            $apicurl = new ApiCurl();
            $response = $apicurl->ApiPost('decoupages/adddecoupage', $fullInfo);
            $decode = json_decode($response);
            $status = '';
            if (isset($decode->status)) $status = $decode->status;

    
            if ($status == '000') {
                $donnee['all_decoupage'] = new ArrayDataProvider([
                    'allModels' => $decode->data,
                    'pagination' => [
                        'pageSize' => -1,
                    ],
                ]);

                $return_info = $this->render('liste', array('donnee' => $donnee));
            } else if ($status == '001' || $status == '003') { // ERREUR UTILISATEUR (PARAMETRE OU CHAMPS MANQUANT DEPUIS LE FORMULAIRE)
                $return_alert = '
                <div class="alert alert-danger alert-dismissible show fade" style="margin-bottom: 30px">
                    <div class="alert-body">
                        <button class="close" data-dismiss="alert">
                            <span>×</span>
                        </button>
                        ' . Yii::t('app', $decode->message) . '
                    </div>
                </div>';
                $return_info = '1###' . $return_alert;
            } else { // ERREUR DEVELOPPEUR (PARAMETRE OU CHAMPS MANQUANT)
                $return_info = "0";
            }
        }
        return $return_info;
    }

    //Cette fonction permet de desactiver/activer & supprimer un decoupage
    public function actionOperation()
    {
        //On v�rifie les droits de l'user
        Utils::Rule("manage_decoupage", "DELETE");
        if (isset($_POST['operation']) != "" and isset($_POST['id']) != "") {

            $operation = $_POST['operation'];
            $keydecoupage = $_POST['id'];

            $fullInfo = array(
                'decoupage_key' => $keydecoupage,
                // 'operation' => $operation,
            );

            $apicurl = new ApiCurl();
            //Execution de l'api pour la modification
            $response = $apicurl->ApiPost("decoupages/deletedecoupage", $fullInfo);
            $decode = json_decode($response);
            //Fin execution
            $status = '';

            if (isset($decode->status)) {
                $status = $decode->status;
                $message = $decode->message;
            }

            if ($status == '000') { // TOUT S'EST BIEN PASSÉ
                Yii::$app->getSession()->setFlash('success', Yii::t('app', $message));
            } else if ($status == '001') { // UNE ERREUR UTILISATEUR EST SURVENUE
                Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
            } else { // UNE ERREUR NE DEPENDANT PAS DE L'UTILISATEUR EST SURVENUE
                $message = Yii::t('app', 'update_error');
                Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
            }
        } else {
            $message = Yii::t('app', 'update_error');
            Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
        }
    }
}
