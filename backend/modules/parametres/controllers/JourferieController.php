<?php

namespace backend\modules\parametres\controllers;

use Yii;
use backend\modules\parametres\models\Jourferie;
use yii\data\ArrayDataProvider;
use backend\controllers\DefaultController;
use backend\controllers\Utils;
use yii\filters\AccessControl;

/**
 * JourferieController implements the CRUD actions for jourferie model.
 */
class JourferieController extends DefaultController
{
    public $page_title = "";
    public $breadcrumb = "";
    public $select_menu = "PARAMSYSTEME";

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['all_jourferie', 'operation', 'create_jourferie', 'save_jourferie'],
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    //Cette fonction permet de lister tous les jourferies
    public function actionAll_jourferie()
    {
        //On verifie les droits de l'user
        Utils::Rule("manage_jourferie", "READ");
        $id_center = Yii::$app->session->get('default_center');
        $annee_scolaire_id = Yii::$app->session->get('annee_scolaire');
        // $response_data = Jourferie::find()->where(['status' => [1, 2]])->all();
        $response_data = array();

        //On pr�pare les donn�es
        $url_api = Utils::getApiUrl() . "jourferies/jourferies";
        $ch = curl_init();
        $curlConfig = array(
            CURLOPT_URL => $url_api,
            CURLOPT_POST => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_CONNECTTIMEOUT => 5,
            CURLOPT_POSTFIELDS => array(
                'token' => Utils::getApiKey(),
                'user_auth_key' => Yii::$app->user->identity->getAuthKey(),
                'annee_scolaire_id' => $annee_scolaire_id,
                'id_center' => $id_center,
            ),
        );
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt_array($ch, $curlConfig);
        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($result);

        if (isset($response->status) && ($response->status == '000')) {
            if (sizeof($response->data) > 0) {
                $response_data = $response->data;
            }
        } else {
            $message = Yii::t('app', 'update_error'); // Une erreur s'est produite
            Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
        }

        $donnee['all_jourferie'] = new ArrayDataProvider([
            'allModels' => $response_data,
            'pagination' => [
                'pageSize' => -1,
            ],
        ]);

        //Initialisation des titres de page
        $title = Yii::t('app', 'all_jourferie_menu');
        $board = array("add_jourferie" => $title);
        $this->page_title = $title;
        $this->breadcrumb = Utils::retour($board);

        return $this->render('index', array('donnee' => $donnee));
    }

    //Cette fonction permet de charger le formulaire d'ajout ou de modification jourferie
    public function actionCreate_jourferie()
    {
        $return_info = "";
        $this->layout = false;
        $manage_jourferie = Utils::have_access("manage_jourferie");
        if ($manage_jourferie == "0_0_0_0") {
            return  $return_info;
        }
        $test = explode('_', $manage_jourferie);

        if ($test[0] == 0 && $test[2] == 0) {
            return  $return_info;
        }
        if (Yii::$app->request->isPost) {
            $id_center = Yii::$app->session->get('default_center');
            $annee_scolaire_id = Yii::$app->session->get('annee_scolaire');


            $info = Yii::$app->request->post();

            $key = $info['key'];

            if ($key == '0') {

                $model = new Jourferie();
            } else {

                $url_api = Utils::getApiUrl() . "jourferies/jourferie";
                $ch = curl_init();
                $curlConfig = array(
                    CURLOPT_URL => $url_api,
                    CURLOPT_POST => true,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_CONNECTTIMEOUT => 5,
                    CURLOPT_POSTFIELDS => array(
                        'token' => Utils::getApiKey(),
                        'user_auth_key' => Yii::$app->user->identity->getAuthKey(),
                        'annee_scolaire_id' => $annee_scolaire_id,
                        'jour_ferie_key' => $key,
                        'id_center' => $id_center,
                    ),
                );
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
                curl_setopt_array($ch, $curlConfig);
                $result = curl_exec($ch);
                curl_close($ch);
                $response = json_decode($result);
                //Fin recherche


                if ($response->status == '000') {
                    //On convertie l'objet en model
                    $find_jourferie = new Jourferie();
                    $find_jourferie->load((array)$response->data, '');
                    $model = $find_jourferie;
                } else {
                    $model = new Jourferie();
                }
            }

            $this->layout = false;
            $return_info = $this->render('create', array("model" => $model));
        }
        return $return_info;
    }

    //Cette fonction permet de sauvegarder un type de personnel
    public function actionSave_jourferie()
    {
        $return_info = "";
        $this->layout = false;
        $manage_jourferie = Utils::have_access("manage_jourferie");
        if ($manage_jourferie == "0_0_0_0") {
            return  $return_info;
        }
        $test = explode('_', $manage_jourferie);

        if ($test[0] == 0 && $test[2] == 0) {
            return  $return_info;
        }
        if (Yii::$app->request->isPost) {
            // $id_center = Yii::$app->session->get('default_center');
            $annee_scolaire_id = Yii::$app->session->get('annee_scolaire');

            $info = Yii::$app->request->post();

            $key = urldecode($info['key']);
            $evenement = urldecode($info['evenement']);
            $datejourferie = urldecode($info['datejourferie']);
            $centreconcerne = urldecode($info['centreconcerne']);
            if ($centreconcerne == '111') {
                $centreconcerne = Yii::$app->session->get('default_center');
            }
            $fullInfo = array();

            $fullInfo['token'] = Utils::getApiKey();
            $fullInfo['evenement'] = $evenement;
            $fullInfo['datejourferie'] = $datejourferie;
            $fullInfo['user_auth_key'] = Yii::$app->user->identity->getAuthKey();
            $fullInfo['id_center'] = $centreconcerne;
            $fullInfo['annee_scolaire_id'] = $annee_scolaire_id;
            $fullInfo['operation'] = 1;

            if ($key != '0') {
                $fullInfo['jour_ferie_key'] = $key;
                $fullInfo['operation'] = 2;
            }

            $url_api = Utils::getApiUrl() . "jourferies/addjourferie";
            $ch = curl_init();
            $curlConfig = array(
                CURLOPT_URL => $url_api,
                CURLOPT_POST => true,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_CONNECTTIMEOUT => 5,
                CURLOPT_POSTFIELDS => $fullInfo,
            );
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt_array($ch, $curlConfig);
            $result = curl_exec($ch);
            curl_close($ch);
            //Fin execution
            $response = json_decode($result);

            if (isset($response->status)) {
                if ($response->status == '000') {
                    $donnee['all_jourferie'] = new ArrayDataProvider([
                        'allModels' => $response->data,
                        'pagination' => [
                            'pageSize' => -1,
                        ],
                    ]);

                    $return_info = $this->render('liste', array('donnee' => $donnee));
                } elseif ($response->status == '001') {
                    // Un element manque sur le formulaire soumis
                    $return_info = "1";
                } elseif ($response->status == '003') {
                    // L'element existe déjà
                    $return_info = "3";
                } else {
                    $return_info = "0";
                }
            } else {
                $return_info = "0";
            }

        }
        return $return_info;
    }

    //Cette fonction permet de desactiver/activer & supprimer un jourferie
    public function actionOperation()
    {
        //On v�rifie les droits de l'user
        Utils::Rule("manage_jourferie", "DELETE");
        $id_center = Yii::$app->session->get('default_center');
        $annee_scolaire_id = Yii::$app->session->get('annee_scolaire');

        if (isset($_POST['operation']) != "" and isset($_POST['id']) != "") {

            $etat = $_POST['operation'];
            $keyjourferie = $_POST['id'];

            //On recherche la ressource � modifier
            $url_api = Utils::getApiUrl() . "jourferies/jourferie";
            $ch = curl_init();
            $curlConfig = array(
                CURLOPT_URL => $url_api,
                CURLOPT_POST => true,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_CONNECTTIMEOUT => 5,
                CURLOPT_POSTFIELDS => array(
                    'token' =>  Utils::getApiKey(),
                    'user_auth_key' => Yii::$app->user->identity->getAuthKey(),
                    'jour_ferie_key' => $keyjourferie,
                    'annee_scolaire_id' => $annee_scolaire_id,
                    'id_center' => $id_center,
                ),
            );
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt_array($ch, $curlConfig);
            $result = curl_exec($ch);
            curl_close($ch);
            $response = json_decode($result);
            //Fin recherche



            if ($response->status == '000') {
                //Execution de l'api pour la modification
                $url_api = Utils::getApiUrl() . "jourferies/deletejourferie";
                $ch = curl_init();
                $curlConfig = array(
                    CURLOPT_URL => $url_api,
                    CURLOPT_POST => true,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_CONNECTTIMEOUT => 5,
                    CURLOPT_POSTFIELDS => array(
                        'token' => Utils::getApiKey(),
                        'jour_ferie_key' => $keyjourferie,
                        'operation' => $etat,
                        'user_auth_key' => Yii::$app->user->identity->getAuthKey(),
                        'annee_scolaire_id' => $annee_scolaire_id,
                        'id_center' => $id_center,
                    ),
                );
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
                curl_setopt_array($ch, $curlConfig);
                $result = curl_exec($ch);
                curl_close($ch);
                //Fin execution

                $response = json_decode($result);

                if ($response->status == '000') {

                    //$message=$response->message;
                    if ($etat == 1) {
                        $message = Yii::t('app', 'active_jourferie_success');
                    } else if ($etat == 2) {
                        $message = Yii::t('app', 'desactive_jourferie_success');
                    } else if ($etat == 3) {
                        $message = Yii::t('app', 'delete_jourferie_success');
                    }
                    Yii::$app->getSession()->setFlash('success', Yii::t('app', $message));
                } else {
                    $message = Yii::t('app', 'update_error');
                    Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
                }
            } else {
                $message = Yii::t('app', 'update_error');
                Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
            }
        } else {
            $message = Yii::t('app', 'update_error');
            Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
        }
    }
}
