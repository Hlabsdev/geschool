<?php

namespace backend\modules\parametres\controllers;

use backend\controllers\DefaultController;
use backend\models\ApiCurl;
use Yii;
use backend\modules\parametres\models\Typeformation;
use yii\filters\AccessControl;

use yii\data\ArrayDataProvider;
use backend\controllers\Utils;

/**
 * TypeformationController implements the CRUD actions for Typeformation model.
 */
class TypeformationController extends DefaultController
{
    public $page_title = "";
    public $breadcrumb = "";
    public $select_menu = "PARAMSYSTEME";

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['all_typeformation', 'operation', 'create_typeformation', 'save_typeformation'],
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }



    //Cette fonction permet de lister toutes les typeformations d'un centre
    public function actionAll_typeformation()
    {
        //On verifie les droits de l'user
        Utils::Rule("manage_typeformation", "READ");
        $response_data = array();

        $apicurl = new ApiCurl();
        $response = $apicurl->ApiPost('typeformations/typeformations', []);
        $decode = json_decode($response);
        $status = $decode->status;

        if (isset($status) && ($status == '000')) {
            if (sizeof($decode->data) > 0) {
                $response_data = $decode->data;
            }
            // print_r('toto');die;
        }

        /* elseif (isset($status) && ($status == '001')) {
             //Liste vide
             $message = $decode->message; // Une erreur s'est produite
             // $message = Yii::t('app', 'empty_liste_typeformation'); // La liste est vide
             Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
         } */


        elseif (isset($status) && ($status == '002')) {
            $message = $decode->message; // Une erreur s'est produite
            Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
        }


        /*else {
            $message = Yii::t('app', 'update_error'); // Une erreur s'est produite
            Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
        }*/



        $donnee['all_typeformation'] = new ArrayDataProvider([
            'allModels' => $response_data,
            'pagination' => [
                'pageSize' => -1,
            ],
        ]);

        //Initialisation des titres de page
        $title = Yii::t('app', 'all_typeformation_menu');
        $board = array("add_typeformation" => $title);
        $this->page_title = $title;
        $this->breadcrumb = Utils::retour($board);

        return $this->render('index', array('donnee' => $donnee));
    }


    //Cette fonction permet de charger le formulaire  de modification d'une typeformation
    public function actionCreate_typeformation()
    {
        $return_info = "";
        $data = array();
        $this->layout = false;
        $manage_typeformation = Utils::have_access("manage_typeformation");
        if ($manage_typeformation == "0_0_0_0") {
            return  $return_info;
        }
        $test = explode('_', $manage_typeformation);

        if ($test[2] == 0) {
            return  $return_info;
        }


        if (Yii::$app->request->isPost) {

            /* Début Liste des types d'typeformation */
            $apicurl = new ApiCurl();

            $info = Yii::$app->request->post();
            $key = $info['key'];

            if ($key == '0') {
                $model = new Typeformation;
            } else {

                $data['keytypeformation'] = $key;
                $response = $apicurl->ApiPost('typeformations/typeformation', $data);
                $decode = json_decode($response);
                $status = $decode->status;
                //Fin recherche


                if ($status == '000') {
                    //On convertie l'objet en model
                    $find_typeformation = new Typeformation();
                    $find_typeformation->load((array)$decode->data, '');
                    $model = $find_typeformation;
                } else {
                    $model = new Typeformation;
                }
            }

            $this->layout = false;
            return $this->render('create', array('model' => $model));
        }

        return $return_info;
    }


    //Cette fonction permet de sauvegarder une nouvelle typeformation
    public function actionSave_typeformation()
    {
        $return_info = "";
        $this->layout = false;
        $manage_typeformation = Utils::have_access("manage_typeformation");
        if ($manage_typeformation == "0_0_0_0") {
            return  $return_info;
        }
        $test = explode('_', $manage_typeformation);

        if ($test[0] == 0 && $test[2] == 0) {
            return  $return_info;
        }
        if (Yii::$app->request->isPost) {
            $id_center = Yii::$app->session->get('default_center');

            $info = Yii::$app->request->post();


            $key = urldecode($info['key']);
            $libelletypeformation = urldecode($info['libelletypeformation']);


            $fullInfo = array();
            $fullInfo['token'] = Utils::getApiKey();
            $fullInfo['libelletypeformation'] = $libelletypeformation;
            $fullInfo['operation'] = 1;
            $fullInfo['user_auth_key'] = Yii::$app->user->identity->getAuthKey();
            $fullInfo['id_center'] = $id_center;

            if ($key != '0') {
                $fullInfo['keytypeformation'] = $key;
                $fullInfo['operation'] = 2;
            }

            $apicurl = new ApiCurl();
            $response = $apicurl->ApiPost('typeformations/addtypeformation', $fullInfo);
            $decode = json_decode($response);
            $status = $decode->status;

            if (isset($status)) {
                if ($status == '000') { // TOUT S'EST BIEN PASSÉ
                    $donnee['all_typeformation'] = new ArrayDataProvider([
                        'allModels' => $decode->data,
                        'pagination' => [
                            'pageSize' => -1,
                        ],
                    ]);

                    $return_info = $this->render('liste', array('donnee' => $donnee));
                } else if ($status == '001' or $status == '003') { // ERREUR UTILISATEUR (PARAMETRE OU CHAMPS MANQUANT DEPUIS LE FORMULAIRE)
                    $return_alert = '<div class="alert alert-danger alert-dismissible show fade" style="margin-bottom: 30px">
                    <div class="alert-body">
                      <button class="close" data-dismiss="alert">
                        <span>×</span>
                      </button>
                      ' . Yii::t('app', $decode->message) . '
                    </div>
                  </div>';
                    $return_info = '1###' . $return_alert;
                    // $return_info = '1###<div class="alert alert-danger alert-dismissible show fade" style="margin-bottom: 30px"> <div class="alert-body">' . Yii::t('app', $decode->message) . ' </div></div>';
                } else if ($status == '002') { // ERREUR DEVELOPPEUR (PARAMETRE OU CHAMPS MANQUANT)
                    $return_info = "0"; // UNE ERREUR S'EST PRODUITE ...
                }
            } else {
                $return_info = "0"; // UNE ERREUR S'EST PRODUITE ...
            }
        }
        return $return_info;
    }


    //Cette fonction permet de  supprimer une typeformation
    public function actionOperation()
    {
        //On v�rifie les droits de l'user
        Utils::Rule("manage_typeformation", "DELETE");
        if (isset($_POST['operation']) != "" and isset($_POST['id']) != "") {

            $etat = $_POST['operation'];
            $keytypeformation = $_POST['id'];

            $fullInfo = array(
                'keytypeformation' => $keytypeformation,
            );

            //On recherche la ressource � modifier

            $apicurl = new ApiCurl();
            $response = $apicurl->ApiPost('typeformations/typeformation', $fullInfo);
            $decode = json_decode($response);
            $status = $decode->status;
            //Fin recherche

            if ($status == '000') {
                //Execution de l'api pour la modification
                $fullInfo['operation'] = $etat;
                $response = $apicurl->ApiPost('typeformations/deletetypeformation', $fullInfo);
                $decode = json_decode($response);
                $status = $decode->status;
                //Fin execution

                if ($status == '000') {
                    if ($etat == 1) {
                        $message = Yii::t('app', 'active_typeformation_success');
                    }
                    else if ($etat == 2) {
                        $message = Yii::t('app', 'desactive_typeformation_success');
                    }
                    else if ($etat == 3) {
                        $message = Yii::t('app', 'delete_typeformation_success');
                    }
                    Yii::$app->getSession()->setFlash('success', Yii::t('app', $message));
                } else {
                    $message = Yii::t('app', 'update_error');
                    Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
                }
            } else {
                $message = Yii::t('app', 'update_error1');
                Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
            }
        } else {
            $message = Yii::t('app', 'update_error');
            Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
        }
    }

}
