<?php
namespace backend\modules\parametres\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use backend\modules\parametres\models\User;
use backend\modules\parametres\models\UserProfil;
use backend\modules\parametres\models\UserAccess;
use backend\controllers\Utils;
use backend\controllers\DefaultController;

use yii\data\ActiveDataProvider;

/**
 * Site controller
 */
class UserController extends DefaultController
{
	
	public $page_title="";
	public $breadcrumb="";
	public $select_menu ="ADMIN";
   
	public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [                  
                    [
                        'allow' => true,
                        'actions' => ['add_user','all_user','operation','update_user','reset_user'],
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }
	
		
	public function actionAdd_user()
	{         
		Utils::Rule("manage_admin","CREATE");
		
		
        $model=new User;
		$userProfil="";
		
		if (Yii::$app->request->post()){
			
			
			$all_center="";
			if ($model->load(Yii::$app->request->post())){
				$post=Yii::$app->request->post();
				
				$model->password=Yii::$app->request->post()['User']['password'];
				$model->password2=Yii::$app->request->post()['User']['password2'];
					
				$newProfil="";
				
				foreach($post as $key=>$value){
					
					 if(!in_array($key,array("_csrf","User","updatebutton"))){
						 $test_key=explode("geschoolcenter_",$key);
						 if(sizeof($test_key)>1){
							 
							 $response=$test_key[1];
							 if(isset($post['profilcenter_'.$response]) && trim($post['profilcenter_'.$response])!="" ){
								 if($all_center!="")$all_center.=",";
								 if($newProfil!="")$newProfil.=",";
								 $all_center.="-".$response."-";
								 $newProfil.=$post['profilcenter_'.$response];	
							}							 
						 }
						 
					 }
				}
				$userProfil=$newProfil;
				
				if($all_center!=""){
						
					
					
					if(trim($model->email)!="" && trim($model->username)!="" && trim($model->nom)!="" && trim($model->prenoms)!="" && trim($model->password)!="" && trim($model->password2)!=""){
						
						$testPhone=explode("_",$model->username);
						if(sizeof($testPhone)==1){
							$model->username= preg_replace("/[^0-9]/","",$model->username);
										
							if (strcmp(trim($model->password), trim($model->password2)) == 0) { 
								$test_email= User::find()->where(['email'=>$model->email])->one();
								if($test_email==null){
									$test_phone= User::find()->where(['username'=>$model->username])->one();
									if($test_phone==null){							
										$model->role=10;
										$model->status=10;
										$model->id_center=$all_center;
										$model->created_at=time();
										$model->updated_at=time();
										$model->idP=Yii::$app->user->identity->id;
										$model->auth_key=Yii::$app->security->generateRandomString(32);
										$model->password_hash = Yii::$app->security->generatePasswordHash($model->password);
										
										if($model->save()){		

											//enregistrement des profil de ce utilisateur dans la table converne
											$allProfil=explode(",",$userProfil);
											if(sizeof($allProfil)>0){
												foreach($allProfil as $idProfil){
													$modelAccess=new UserAccess;
													$modelAccess->id_user=$model->id;
													$modelAccess->id_user_profil=$idProfil;
													$modelAccess->etat=1;
													$modelAccess->date_create =date("Y-m-d H:i:s");
													$modelAccess->created_by=Yii::$app->user->identity->id;
													$modelAccess->save();
												}
											}
											
											
											$message=Yii::t('app', 'succes_creation_user');
											Yii::$app->getSession()->setFlash('success',Yii::t('app', $message));
											$model=new User;
										}else{
											$message=Yii::t('app', 'update_error');
											Yii::$app->getSession()->setFlash('error',Yii::t('app', $message));
										}									
									}else{
										$message=Yii::t('app', 'phone_used');
										Yii::$app->getSession()->setFlash('error',Yii::t('app', $message));	
									}
								}else{
									$message=Yii::t('app', 'email_used');
									Yii::$app->getSession()->setFlash('error',Yii::t('app', $message));	
								}	
							}else{
								$message=Yii::t('app', 'passe_identique');
								Yii::$app->getSession()->setFlash('error',Yii::t('app', $message));	
							}
						
						}else {
							$message=Yii::t('app', 'phone_format_error');
							Yii::$app->getSession()->setFlash('error',Yii::t('app', $message));  
						}						
					}else {
						$message=Yii::t('app', 'input_empty');
						Yii::$app->getSession()->setFlash('error',Yii::t('app', $message));  
					}
				
			
				}else{
					$message=Yii::t('app', 'empty_center');
					Yii::$app->getSession()->setFlash('error',Yii::t('app', $message));
				}
				
			}else {
					$message=Yii::t('app', 'input_empty');
					Yii::$app->getSession()->setFlash('error',Yii::t('app', $message));  
			}
			
		}
		
		$title=Yii::t('app', 'add_user_menu');		
		$board=array("add_user"=>$title);
		
		$this->page_title=$title;
		$this->breadcrumb=Utils::retour($board);
		
		return $this->render('add',array('model'=>$model,'userProfil'=>$userProfil,'type'=>'CREATE'));                
	}

	public function actionAll_user()
	{         
		    Utils::Rule("manage_admin","READ");
		   
		    $idCentre="-".Yii::$app->session->get('default_center')."-";
			$donnee['all_user'] = new ActiveDataProvider([
				'query' => User::find()->where(['status'=>['10','20']])->andWhere(['!=', 'idP', 0])->andWhere(['like', 'id_center',$idCentre]),
				'pagination' => [
					'pageSize' => -1,
				],
			]);
		   
			$title=Yii::t('app', 'all_admin');		
			$board=array("add_user"=>$title);
			
			$this->page_title=$title;
			$this->breadcrumb=Utils::retour($board);
		
		   return $this->render('liste',array('donnee'=>$donnee));     

	}
	
	public function actionOperation()
	{	 
		Utils::Rule("manage_admin","DELETE");
		if(isset($_POST['operation'])!="" and isset($_POST['id'])!=""){
		
				$etat=$_POST['operation'];
				$auth_key=$_POST['id'];
				
				$find_user = User::find()->where(['auth_key'=>$auth_key,'status'=>['10','20']])->one();	
				if($find_user!==null){
						$find_user->status=$etat;
						$find_user->updated_at=time();
						if($find_user->save()){
								$message="";
								if($etat==10){
									$message=Yii::t('app','active_user_success');
								}else if($etat==20){
									$message=Yii::t('app','desactive_user_success');
								}else if($etat==30){
									$message=Yii::t('app','delete_user_success');
								}
								Yii::$app->getSession()->setFlash('success',Yii::t('app', $message)); 
						}else{							
								$message=Yii::t('app','update_error3').json_decode($find_user->getErrors())  ;
								Yii::$app->getSession()->setFlash('error',Yii::t('app', $message));
						}
				}else{							
						$message=Yii::t('app','update_error2')  ;
						Yii::$app->getSession()->setFlash('error',Yii::t('app', $message));
				}							
		}else{
			$message=Yii::t('app','update_error1')  ;
			Yii::$app->getSession()->setFlash('error',Yii::t('app', $message));
		}
			
	}

	public function actionUpdate_user()
	{   
		Utils::Rule("manage_admin","UPDATE");
		if(isset($_GET["key"])){  
		   $auth_key=$_GET["key"];		   
			   
			$users = User::find()->where(['auth_key'=>$auth_key,'status'=>['10','20']])->one();	
			if( $users !== null and $users->id!=Yii::$app->user->identity->id){
				
					$model=$users;
					$infoAccess=UserAccess::find()->select(['GROUP_CONCAT(id_user_profil) key_user_access'])->where(['id_user'=>$users->id,'etat'=>1])->one();	
					$userProfil=$infoAccess->key_user_access;
					
					if (Yii::$app->request->post()){
						if ($model->load(Yii::$app->request->post())){
							
							
							$all_center="";
							$post=Yii::$app->request->post();								
							$newProfil="";							
							foreach($post as $key=>$value){
								
								 if(!in_array($key,array("_csrf","User","updatebutton"))){
									 $test_key=explode("geschoolcenter_",$key);
									 if(sizeof($test_key)>1){
										 
										 $response=$test_key[1];
										 if(isset($post['profilcenter_'.$response]) && trim($post['profilcenter_'.$response])!="" ){
											 if($all_center!="")$all_center.=",";
											 if($newProfil!="")$newProfil.=",";
											$all_center.="-".$response."-";
											 $newProfil.=$post['profilcenter_'.$response];	
										}							 
									 }
									 
								 }
							}
							$userProfil=$newProfil;
							
							if($all_center!=""){
									
								
								if(trim($model->email)!="" && trim($model->username)!="" && trim($model->nom)!="" && trim($model->prenoms)!="" ){
									
									//verifier si le phone est correcte
									$testPhone=explode("_",$model->username);
									if(sizeof($testPhone)==1){
										$model->username= preg_replace("/[^0-9]/","",$model->username);
										
										$test_email= User::find()->where(['email'=>$model->email])->andWhere(['!=', 'id', $users->id])->one();;
										if($test_email==null){
											$test_phone= User::find()->where(['username'=>$model->username])->andWhere(['!=', 'id', $users->id])->one();;
											if($test_phone==null){								
												
												$model->updated_at=time();
												$model->id_center=$all_center;
												if($model->save()){		

													//mise a jour de son profil		
													UserAccess::updateAll(array('etat'=>3), 'id_user = '.$users->id.' and etat=1');	
													
													//enregistrement des profil de ce utilisateur dans la table converne
													$allProfil=explode(",",$userProfil);
													if(sizeof($allProfil)>0){
														foreach($allProfil as $idProfil){
															$modelAccess=new UserAccess;
															$modelAccess->id_user=$model->id;
															$modelAccess->id_user_profil=$idProfil;
															$modelAccess->etat=1;
															$modelAccess->date_create =date("Y-m-d H:i:s");
															$modelAccess->created_by=Yii::$app->user->identity->id;
															$modelAccess->save();
														}
													}
													
													$message=Yii::t('app', 'update_success');
													Yii::$app->getSession()->setFlash('success',Yii::t('app', $message));
													return $this->redirect(array('/all_user'));
												}else{
													$message=Yii::t('app', 'update_error');
													Yii::$app->getSession()->setFlash('error',Yii::t('app', $message));
												}									
											}else{
												$message=Yii::t('app', 'phone_used');
												Yii::$app->getSession()->setFlash('error',Yii::t('app', $message));	
											}
										}else{
											$message=Yii::t('app', 'email_used');
											Yii::$app->getSession()->setFlash('error',Yii::t('app', $message));	
										}	
										
									}else {
										$message=Yii::t('app', 'phone_format_error');
										Yii::$app->getSession()->setFlash('error',Yii::t('app', $message));  
									}
								}else {
									$message=Yii::t('app', 'input_empty');
									Yii::$app->getSession()->setFlash('error',Yii::t('app', $message));  
								}
								
							}else{
								$message=Yii::t('app', 'empty_center');
								Yii::$app->getSession()->setFlash('error',Yii::t('app', $message));
							}
						
						}else {
								$message=Yii::t('app', 'input_empty');
								Yii::$app->getSession()->setFlash('error',Yii::t('app', $message));  
						}
						
					}
						
					$title=Yii::t('app','update_information').": ".$users->nom." ".$users->prenoms ;		
					$board=array("all_user"=>Yii::t('app', 'all_admin'),"update_user"=>$title);
					
					$this->page_title=$title;
					$this->breadcrumb=Utils::retour($board);

					return $this->render('add',array('model'=>$model,'userProfil'=>$userProfil,'type'=>'UPDATE'));
			}else{
			   return $this->redirect(array('/all_user'));
			}
 
		}else{
			return $this->redirect(array('/all_user')); 
		}
	}
	
	public function actionReset_user()
	{       
		Utils::Rule("manage_admin","UPDATE");
		if(isset($_GET["key"])){  
		   $auth_key=$_GET["key"];		   
			$model=new User;   
			$find_user = User::find()->where(['auth_key'=>$auth_key,'status'=>['10','20']])->one();
			if( $find_user !== null and $find_user->id!=Yii::$app->user->identity->id){
				
					
					if (Yii::$app->request->post()){
						
						$post=Yii::$app->request->post();				
						$model->password=Yii::$app->request->post()['User']['password'];
						$model->password2=Yii::$app->request->post()['User']['password2'];
						
						if(trim($model->password)!="" && trim($model->password2)!=""){
							if (strcmp(trim($model->password), trim($model->password2)) == 0) { 
													
									
									$find_user->updated_at=time();
									$find_user->password_hash = Yii::$app->security->generatePasswordHash($model->password);
									
									if($find_user->save()){									
										$message=Yii::t('app', 'update_success');
										Yii::$app->getSession()->setFlash('success',Yii::t('app', $message));
										return $this->redirect(array('/all_user'));
									}else{
										$message=Yii::t('app', 'update_error');
										Yii::$app->getSession()->setFlash('error',Yii::t('app', $message));
									}									
								
							
							
							}else{
								$message=Yii::t('app', 'passe_identique');
								Yii::$app->getSession()->setFlash('error',Yii::t('app', $message));	
							}
						}else {
							$message=Yii::t('app', 'input_empty');
							Yii::$app->getSession()->setFlash('error',Yii::t('app', $message));  
						}
					
						
					}
						
					$title=Yii::t('app','reset_password').": ".$find_user->nom." ".$find_user->prenoms ;		
					$board=array("all_user"=>Yii::t('app', 'all_admin'),"update_user"=>$title);
					
					$this->page_title=$title;
					$this->breadcrumb=Utils::retour($board);

					return $this->render('reset',array('model'=>$model));  
			}else{
			   return $this->redirect(array('/all_user'));
			}
 
		}else{
			return $this->redirect(array('/all_user')); 
		}
	}
	
}
