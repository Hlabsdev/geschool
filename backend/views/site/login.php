<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $model \common\models\LoginForm */
use backend\widgets\Alert;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>

<section class="section">
  <div class="container mt-5">
    <div class="row">
      <div class="col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3 col-lg-6 offset-lg-3 col-xl-4 offset-xl-4">
        <div class="card card-primary">
          <div class="card-header">
            <img style="margin: auto !important" alt="image" src="<?php echo Yii::$app->request->baseUrl; ?>/images/logo.svg" width="80" class="header-logo" />
          </div>
          <div class="card-body">
            <?= Alert::widget() ?>
            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
            <div class="form-group">
              <label><?= Yii::t('app', 'user_id') ?> *</label>
              <?= $form->field($model, 'username')->textInput(['autofocus' => true, 'required' => true])->error(false)->Label(false) ?>
            </div>
            <div class="form-group">
              <label><?= Yii::t('app', 'password') ?> *</label>
              <?= $form->field($model, 'password')->passwordInput()->passwordInput(['required' => true])->error(false)->Label(false); ?>
            </div>

            <div class="form-group">
              <a href="<?= Yii::$app->homeUrl ?>forget_password" class="text-small"> <i class="menu-icon fas fa-angle-right"></i> <?= Yii::t('app', 'forget_password') ?> </a>
            </div>

            <div class="form-group">
              <?= Html::submitButton(Yii::t('app', 'login'), ['class' => 'btn btn-primary btn-lg btn-block', 'name' => 'login-button']) ?>
            </div>
            <?php ActiveForm::end(); ?>
          </div>
        </div>

      </div>
    </div>
  </div>
</section>