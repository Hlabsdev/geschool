<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $model \common\models\LoginForm */
use backend\widgets\Alert;
$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>

    <section class="section">
      <div class="container mt-5">
        <div class="row">
          <div class="col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3 col-lg-6 offset-lg-3 col-xl-4 offset-xl-4">
            <div class="card card-primary">
              <div class="card-header" >
                   <img style="margin: auto !important" alt="image" src="<?php echo Yii::$app->request->baseUrl; ?>/images/logo.svg" width="80" class="header-logo" />
              </div>
              <div class="card-body">
			   <?= Alert::widget() ?>
                <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
                  <div class="form-group">
					 <label><?=Yii::t('app', 'verification_code')?> *</label>
                      <?= $form->field($model, 'username')->textInput(['autofocus' => true,'required'=>true,'maxlenght'=>2])->error(false)->Label(false) ?>                    
                  </div>
                  <div class="form-group">
					<label><?=Yii::t('app', 'new_password')?> *</label>
                    <?= $form->field($model, 'password')->passwordInput()->passwordInput(['required'=>true])->error(false)->Label(false); ?>
                  </div>
				   <div class="form-group">
					<label><?=Yii::t('app', 'repeat_password')?> *</label>
                    <?= $form->field($model, 'confirm_password')->passwordInput()->passwordInput(['required'=>true])->error(false)->Label(false); ?>
                  </div>
				  
                   <div class="form-group">
					 <a href="<?=Yii::$app->homeUrl."resend_code?key1=".$key1."&key2=".$key2."&key3=".$key3 ?>" class="text-small">  <i class="menu-icon fas fa-angle-right"></i> <?=Yii::t('app', 'resend_code')?>  </a>
                  </div>
                  
                  <div class="form-group">
                    <?= Html::submitButton(Yii::t('app', 'reset'), ['class' => 'btn btn-primary btn-lg btn-block', 'name' => 'login-button']) ?>
                  </div>
                 <?php ActiveForm::end(); ?>
              </div>
            </div>
           
          </div>
        </div>
      </div>
    </section>
  