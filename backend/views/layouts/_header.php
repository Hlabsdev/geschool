<?php

use yii\bootstrap\Html;
use yii\helpers\Url;
use backend\modules\users\models\SchoolCenter;

$nom = Yii::$app->user->identity->nom . " " . Yii::$app->user->identity->prenoms;
$id_center = str_replace('-', '', Yii::$app->user->identity->id_center);

$test_center = explode(",", $id_center);

$default_center = Yii::$app->session->get('default_center');

?>
<nav class="navbar navbar-expand-lg main-navbar sticky">
	<div class="form-inline mr-auto">
		<ul class="navbar-nav mr-3">
			<li><a href="#" data-toggle="sidebar" class="nav-link nav-link-lg
									collapse-btn"> <i data-feather="align-justify"></i></a></li>
			<li><a href="#" class="nav-link nav-link-lg fullscreen-btn">
					<i data-feather="maximize"></i>
				</a>
			</li>
			<li>

			</li>
		</ul>

		<?php if ($id_center == 1 || sizeof($test_center) > 1) {
			//recuperer la liste des centres
			if ($id_center == 1) {
				$allcenter = SchoolCenter::find()->where(['etat' => 1])->all();
			} else {
				$allcenter = SchoolCenter::find()->where(['etat' => 1, 'id_center' => $test_center])->all();
			}
		?>
			<form class="form-inline mr-auto">
				<div class="search-element">
					<select class="form-control" onchange="selectCenter(this.value)">
						<?php
						if (sizeof($allcenter) > 0) {
							$checkselected = "";
							foreach ($allcenter as $infocenter) {
								$selected = "";
								if ($default_center == $infocenter->id_center) {
									$selected = "selected='true'";
									$checkselected = "ok";
								}
								echo '<option value="' . $infocenter->id_center . '" ' . $selected . '>' . $infocenter->denomination_center . '</option>';
							}

							if ($checkselected == "") {
								Yii::$app->session->set('default_center', $allcenter[0]->id_center);
							}
						}
						?>
					</select>
				</div>
			</form>
			<?php } else {

			if ($default_center != $test_center[0]) {
				Yii::$app->session->set('default_center', $test_center[0]);
			}
			$infocenter = SchoolCenter::find()->where(['etat' => 1, 'id_center' => $test_center[0]])->one();
			if ($infocenter != null) {
			?>
				<center style="font-size:25px;color:red"><?= $infocenter->denomination_center ?></center>
		<?php } else {
				Yii::$app->session->set('default_center', 0);
				echo '<center style="font-size:25px;color:red">' . Yii::t('app', 'no_centre') . '</center>';
			}
		} ?>

	</div>
	<ul class="navbar-nav navbar-right">
		<li class="dropdown dropdown-list-toggle"><a href="#" data-toggle="dropdown" class="nav-link nav-link-lg message-toggle"><i data-feather="mail"></i>
				<span class="badge headerBadge1">
					0 </span> </a>
			<div class="dropdown-menu dropdown-list dropdown-menu-right pullDown">
				<div class="dropdown-header">
					Messages
					<div class="float-right">
						<a href="#">Aucun message</a>
					</div>
				</div>
				<!-- <div class="dropdown-list-content dropdown-list-message">
					<a href="#" class="dropdown-item"> <span class="dropdown-item-avatar
											text-white"> <img alt="image" src="<?= Yii::$app->homeUrl ?>theme/img/users/user-1.png" class="rounded-circle">
						</span> <span class="dropdown-item-desc"> <span class="message-user">John
								Deo</span>
							<span class="time messege-text">Please check your mail !!</span>
							<span class="time">2 Min Ago</span>
						</span>
					</a> <a href="#" class="dropdown-item"> <span class="dropdown-item-avatar text-white">
							<img alt="image" src="<?= Yii::$app->homeUrl ?>theme/img/users/user-2.png" class="rounded-circle">
						</span> <span class="dropdown-item-desc"> <span class="message-user">Sarah
								Smith</span> <span class="time messege-text">Request for leave
								application</span>
							<span class="time">5 Min Ago</span>
						</span>
					</a> <a href="#" class="dropdown-item"> <span class="dropdown-item-avatar text-white">
							<img alt="image" src="<?= Yii::$app->homeUrl ?>theme/img/users/user-5.png" class="rounded-circle">
						</span> <span class="dropdown-item-desc"> <span class="message-user">Jacob
								Ryan</span> <span class="time messege-text">Your payment invoice is
								generated.</span> <span class="time">12 Min Ago</span>
						</span>
					</a> <a href="#" class="dropdown-item"> <span class="dropdown-item-avatar text-white">
							<img alt="image" src="<?= Yii::$app->homeUrl ?>theme/img/users/user-4.png" class="rounded-circle">
						</span> <span class="dropdown-item-desc"> <span class="message-user">Lina
								Smith</span> <span class="time messege-text">hii John, I have upload
								doc
								related to task.</span> <span class="time">30
								Min Ago</span>
						</span>
					</a> <a href="#" class="dropdown-item"> <span class="dropdown-item-avatar text-white">
							<img alt="image" src="<?= Yii::$app->homeUrl ?>theme/img/users/user-3.png" class="rounded-circle">
						</span> <span class="dropdown-item-desc"> <span class="message-user">Jalpa
								Joshi</span> <span class="time messege-text">Please do as specify.
								Let me
								know if you have any query.</span> <span class="time">1
								Days Ago</span>
						</span>
					</a> <a href="#" class="dropdown-item"> <span class="dropdown-item-avatar text-white">
							<img alt="image" src="<?= Yii::$app->homeUrl ?>theme/img/users/user-2.png" class="rounded-circle">
						</span> <span class="dropdown-item-desc"> <span class="message-user">Sarah
								Smith</span> <span class="time messege-text">Client Requirements</span>
							<span class="time">2 Days Ago</span>
						</span>
					</a>
				</div>
				<div class="dropdown-footer text-center">
					<a href="#">View All <i class="fas fa-chevron-right"></i></a>
				</div> -->
			</div>
		</li>
		<li class="dropdown dropdown-list-toggle"><a href="#" data-toggle="dropdown" class="nav-link notification-toggle nav-link-lg"><i data-feather="bell" class="bell"></i>
			</a>
			<div class="dropdown-menu dropdown-list dropdown-menu-right pullDown">
				<div class="dropdown-header">
					Notifications
					<div class="float-right">
						<a href="#">Aucune notification</a>
					</div>
				</div>
				<!-- <div class="dropdown-list-content dropdown-list-icons">
					<a href="#" class="dropdown-item dropdown-item-unread"> <span class="dropdown-item-icon bg-primary text-white"> <i class="fas
												fa-code"></i>
						</span> <span class="dropdown-item-desc"> Template update is
							available now! <span class="time">2 Min
								Ago</span>
						</span>
					</a> <a href="#" class="dropdown-item"> <span class="dropdown-item-icon bg-info text-white"> <i class="far
												fa-user"></i>
						</span> <span class="dropdown-item-desc"> <b>You</b> and <b>Dedik
								Sugiharto</b> are now friends <span class="time">10 Hours
								Ago</span>
						</span>
					</a> <a href="#" class="dropdown-item"> <span class="dropdown-item-icon bg-success text-white"> <i class="fas
												fa-check"></i>
						</span> <span class="dropdown-item-desc"> <b>Kusnaedi</b> has
							moved task <b>Fix bug header</b> to <b>Done</b> <span class="time">12
								Hours
								Ago</span>
						</span>
					</a> <a href="#" class="dropdown-item"> <span class="dropdown-item-icon bg-danger text-white"> <i class="fas fa-exclamation-triangle"></i>
						</span> <span class="dropdown-item-desc"> Low disk space. Let's
							clean it! <span class="time">17 Hours Ago</span>
						</span>
					</a> <a href="#" class="dropdown-item"> <span class="dropdown-item-icon bg-info text-white"> <i class="fas
												fa-bell"></i>
						</span> <span class="dropdown-item-desc"> Welcome to Otika
							template! <span class="time">Yesterday</span>
						</span>
					</a>
				</div>
				<div class="dropdown-footer text-center">
					<a href="#">View All <i class="fas fa-chevron-right"></i></a>
				</div> -->
			</div>
		</li>

		<?php
		$profile_src = 'default_user_picture.png';
		$this_user = Yii::$app->user->identity;
		if ($this_user->photo != null && $this_user->photo != "")
			$profile_src = $this_user->photo;
		?>
		<li class="dropdown"><a href="#" data-toggle="dropdown" class="nav-link dropdown-toggle nav-link-lg nav-link-user"> <img alt="image" src="<?= Yii::$app->homeUrl ?>uploads/<?= $profile_src ?>" class="user-img-radious-style"> <span class="d-sm-none d-lg-inline-block"></span></a>
			<div class="dropdown-menu dropdown-menu-right pullDown">
				<div class="dropdown-title"><?= Yii::t('app', 'hello') . " " . $nom ?></div>
				<a href="<?php echo Yii::$app->request->baseUrl; ?>/update_info" class="dropdown-item has-icon"> <i class="far fa-user"></i> <?= Yii::t('app', 'update_info') ?> </a>
				<a href="<?php echo Yii::$app->request->baseUrl; ?>/update_password" class="dropdown-item has-icon"> <i class="fas fa-lock"></i> <?= Yii::t('app', 'user_password') ?></a>
				<a href="<?php echo Yii::$app->request->baseUrl; ?>/center_params" class="dropdown-item has-icon"> <i class="fas fa-cog"></i> <?= Yii::t('app', 'center_params') ?></a>
				<div class="dropdown-divider"></div>
				<a href="<?php echo Yii::$app->request->baseUrl; ?>/logout" class="dropdown-item has-icon text-danger"> <i class="fas fa-sign-out-alt"></i> <?= Yii::t('app', 'logout') ?></a>

			</div>
		</li>
	</ul>
</nav>