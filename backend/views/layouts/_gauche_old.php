<?php

use backend\controllers\Utils;

$manage_profil = Utils::have_access("manage_profil");
$manage_admin = Utils::have_access("manage_admin");
$manage_center = Utils::have_access("manage_center");
$manage_module = Utils::have_access("manage_module");
$manage_typepersonnel = Utils::have_access("manage_typepersonnel");
$manage_diplome = Utils::have_access("manage_diplome");
$manage_typeabsence = Utils::have_access("manage_typeabsence");
$manage_absence = Utils::have_access("manage_absence");
$manage_activite = Utils::have_access("manage_activite");
$manage_decoupage = Utils::have_access("manage_decoupage");
$manage_typedecoupage = Utils::have_access("manage_typedecoupage");
$manage_anneescolaire = Utils::have_access("manage_anneescolaire");
$manage_jourferie = Utils::have_access("manage_jourferie");
$manage_cours = Utils::have_access("manage_cours");
$manage_mission = Utils::have_access("manage_mission");
$manage_section = Utils::have_access("manage_section");
$manage_departement = Utils::have_access("manage_departement");
?>


<div class="main-sidebar sidebar-style-2">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">

            <a href="<?php echo Yii::$app->request->baseUrl; ?>/account"> <img alt="image" src="<?php echo Yii::$app->request->baseUrl; ?>/images/logo.svg" class="header-logo" /> <span class="logo-name"><?php echo Yii::t('app', 'smprojet_title') ?></span>
            </a>
        </div>

        <ul class="sidebar-menu">
            <li class="menu-header"><?php echo Yii::t('app', 'menu') ?></li>
            <li class="dropdown <?php if ($this->context->select_menu == "BOARD") echo 'active' ?>">
                <a href="<?php echo Yii::$app->request->baseUrl; ?>/account" class="nav-link"><i data-feather="monitor"></i><span><?php echo Yii::t('app', 'dashboard') ?></span></a>
            </li>

            <li class="menu-header"><?php echo Yii::t('app', 'manage') ?></li>




            <?php
            if ($manage_center != "0_0_0_0") {
                $test = explode('_', $manage_center);
                ?>
                <li class="dropdown <?php if ($this->context->select_menu == "CENTER") echo 'active' ?>">
                    <a href="#" class="menu-toggle nav-link has-dropdown">
                        <i class="menu-icon fa fa-cog"></i><span><?= Yii::t('app', 'menu_center') ?></span></a>
                    <ul class="dropdown-menu">
                        <?php if ($test[0] == 1) { ?>
                            <li><a class="nav-link" href="<?= Yii::$app->request->baseUrl . "/add_center"; ?>"><?= Yii::t('app', 'add_center_menu') ?></a></li>
                        <?php } ?>
                        <?php if ($test[1] == 1 or $test[2] == 1 or $test[3] == 1) { ?>
                            <li><a class="nav-link" href="<?= Yii::$app->request->baseUrl . "/all_center"; ?>"><?= Yii::t('app', 'all_center_menu') ?></a></li>
                        <?php } ?>
                    </ul>
                </li>

                <?php
            }
            if ($manage_module != "0_0_0_0") {
                $test = explode('_', $manage_module);
                ?>
                <li class="dropdown <?php if ($this->context->select_menu == "MODULE") echo 'active' ?>">
                    <a href="<?php echo Yii::$app->request->baseUrl; ?>/center_module" class="nav-link"><i class="menu-icon fas fa-atom"></i><span><?php echo Yii::t('app', 'center_module') ?></span></a>
                </li>
                <?php
            }
            if ($manage_profil != "0_0_0_0") {
                $test = explode('_', $manage_profil);
                ?>
                <li class="dropdown <?php if ($this->context->select_menu == "PROFIL") echo 'active' ?>">
                    <a href="#" class="menu-toggle nav-link has-dropdown">
                        <i class="menu-icon fa fa-th"></i><span><?= Yii::t('app', 'menu_profil_admin') ?></span></a>
                    <ul class="dropdown-menu">
                        <?php if ($test[0] == 1) { ?>
                            <li><a class="nav-link" href="<?= Yii::$app->request->baseUrl . "/add_profil"; ?>"><?= Yii::t('app', 'add_profil_admin') ?></a></li>
                        <?php } ?>
                        <?php if ($test[1] == 1 or $test[2] == 1 or $test[3] == 1) { ?>
                            <li><a class="nav-link" href="<?= Yii::$app->request->baseUrl . "/all_profil"; ?>"><?= Yii::t('app', 'all_profil_admin') ?></a></li>
                        <?php } ?>
                    </ul>
                </li>

                <?php
            }
            if ($manage_admin != "0_0_0_0") {
                $test = explode('_', $manage_admin);
                ?>
                <li class="dropdown <?php if ($this->context->select_menu == "ADMIN") echo 'active' ?>">
                    <a href="#" class="menu-toggle nav-link has-dropdown">
                        <i class="menu-icon fa fa-users"></i><span><?= Yii::t('app', 'menu_admin') ?></span></a>
                    <ul class="dropdown-menu">
                        <?php if ($test[0] == 1) { ?>
                            <li><a class="nav-link" href="<?= Yii::$app->request->baseUrl . "/add_user"; ?>"><?= Yii::t('app', 'add_admin') ?></a></li>
                        <?php } ?>
                        <?php if ($test[1] == 1 or $test[2] == 1 or $test[3] == 1) { ?>
                            <li><a class="nav-link" href="<?= Yii::$app->request->baseUrl . "/all_user"; ?>"><?= Yii::t('app', 'all_admin') ?></a></li>
                        <?php } ?>
                    </ul>
                </li>

                <?php
            }
            if ($manage_absence != "0_0_0_0") {
                $test = explode('_', $manage_absence);
                ?>
                <li class="dropdown <?php if ($this->context->select_menu == "ABSENCEPERSONEL") echo 'active' ?>">
                    <a href="#" class="menu-toggle nav-link has-dropdown">
                        <i class="menu-icon fa fa-user-clock"></i><span><?= Yii::t('app', 'menu_absence_personnel') ?></span></a>
                    <ul class="dropdown-menu">
                        <?php if ($test[0] == 1) { ?>
                            <li><a class="nav-link" href="<?= Yii::$app->request->baseUrl . "/add_absencepersonnel"; ?>"><?= Yii::t('app', 'add_absence_personnel_menu') ?></a></li>
                        <?php } ?>
                        <?php if ($test[1] == 1 or $test[2] == 1 or $test[3] == 1) { ?>
                            <li><a class="nav-link" href="<?= Yii::$app->request->baseUrl . "/all_absencepersonnel"; ?>"><?= Yii::t('app', 'all_absence_personnel_menu') ?></a></li>
                        <?php } ?>
                    </ul>
                </li>

                <?php
            }
            if ($manage_typepersonnel != "0_0_0_0" or $manage_diplome != "0_0_0_0" or $manage_typeabsence != "0_0_0_0") {
                ?>
                <li class="dropdown <?php if ($this->context->select_menu == "PARAMSYSTEME") echo 'active' ?>">
                    <a href="#" class="menu-toggle nav-link has-dropdown">
                        <i class="menu-icon fa fa-cogs"></i><span><?= Yii::t('app', 'menu_param_sys') ?></span></a>
                    <?php
                    $test = explode('_', $manage_typepersonnel);
                    if ($test[0] == 1 or $test[1] == 1 or $test[2] == 1 or $test[3] == 1) {
                        ?>
                        <ul class="dropdown-menu">
                            <li><a class="nav-link" href="<?= Yii::$app->request->baseUrl . "/all_typepersonnel"; ?>"><?= Yii::t('app', 'all_typepersonnel_menu') ?></a></li>

                        </ul>
                    <?php } ?>
                    <?php
                    $test = explode('_', $manage_diplome);
                    if ($test[0] == 1 or $test[1] == 1 or $test[2] == 1 or $test[3] == 1) {
                        ?>
                        <ul class="dropdown-menu">
                            <li><a class="nav-link" href="<?= Yii::$app->request->baseUrl . "/all_diplome"; ?>"><?= Yii::t('app', 'all_diplome_menu') ?></a></li>
                        </ul>
                    <?php } ?>
                    <?php
                    $test = explode('_', $manage_typeabsence);
                    if ($test[0] == 1 or $test[1] == 1 or $test[2] == 1 or $test[3] == 1) {
                        ?>
                        <ul class="dropdown-menu">
                            <li><a class="nav-link" href="<?= Yii::$app->request->baseUrl . "/all_typeabsence"; ?>"><?= Yii::t('app', 'all_typeabsence_menu') ?></a></li>
                        </ul>
                    <?php } ?>
                    <?php
                    $test = explode('_', $manage_typedecoupage);
                    if ($test[0] == 1 or $test[1] == 1 or $test[2] == 1 or $test[3] == 1) {
                        ?>
                        <ul class="dropdown-menu">
                            <li><a class="nav-link" href="<?= Yii::$app->request->baseUrl . "/all_typedecoupage"; ?>"><?= Yii::t('app', 'all_typedecoupage_menu') ?></a></li>
                        </ul>
                    <?php } ?>

                    <?php
                    $test = explode('_', $manage_anneescolaire);
                    if ($test[0] == 1 or $test[1] == 1 or $test[2] == 1 or $test[3] == 1) {
                        ?>
                        <ul class="dropdown-menu">
                            <li><a class="nav-link" href="<?= Yii::$app->request->baseUrl . "/all_anneescolaire"; ?>"><?= Yii::t('app', 'all_anneescolaire_menu') ?></a></li>
                        </ul>
                    <?php } ?>
                    <?php
                    $test = explode('_', $manage_jourferie);
                    if ($test[0] == 1 or $test[1] == 1 or $test[2] == 1 or $test[3] == 1) {
                        ?>
                        <ul class="dropdown-menu">
                            <li><a class="nav-link" href="<?= Yii::$app->request->baseUrl . "/all_jourferie"; ?>"><?= Yii::t('app', 'all_jourferie_menu') ?></a></li>
                        </ul>
                    <?php } ?>
                    <?php
                    $test = explode('_', $manage_section);
                    if ($test[0] == 1 or $test[1] == 1 or $test[2] == 1 or $test[3] == 1) {
                        ?>
                        <ul class="dropdown-menu">
                            <li><a class="nav-link" href="<?= Yii::$app->request->baseUrl . "/all_section"; ?>"><?= Yii::t('app', 'all_section_menu') ?></a></li>
                        </ul>
                    <?php } ?>
                    <?php
                    $test = explode('_', $manage_departement);
                    if ($test[0] == 1 or $test[1] == 1 or $test[2] == 1 or $test[3] == 1) {
                        ?>
                        <ul class="dropdown-menu">
                            <li><a class="nav-link" href="<?= Yii::$app->request->baseUrl . "/all_departement"; ?>"><?= Yii::t('app', 'all_departement_menu') ?></a></li>
                        </ul>
                    <?php } ?>

                </li>

                <?php
            }
            if ($manage_activite != "0_0_0_0") {
                $test = explode('_', $manage_activite);
                ?>
                <li class="dropdown <?php if ($this->context->select_menu == "ACTIVITE") echo 'active' ?>">
                    <a href="#" class="menu-toggle nav-link has-dropdown">
                        <i class="menu-icon fa fa-th"></i><span><?= Yii::t('app', 'menu_activite') ?></span></a>
                    <ul class="dropdown-menu">
                        <?php if ($test[0] == 1) { ?>
                                                        <!-- <li><a class="nav-link" href="<?= Yii::$app->request->baseUrl . "/add_activite"; ?>"><?= Yii::t('app', 'add_activite_menu') ?></a></li> -->
                        <?php } ?>
                        <?php if ($test[1] == 1 or $test[2] == 1 or $test[3] == 1) { ?>
                            <li><a class="nav-link" href="<?= Yii::$app->request->baseUrl . "/all_activite"; ?>"><?= Yii::t('app', 'all_activite_menu') ?></a></li>
                        <?php } ?>
                    </ul>
                </li>

                <?php
            }
            /* if ($manage_section != "0_0_0_0") {
              $test = explode('_', $manage_section);
              ?>
              <li class="dropdown <?php if ($this->context->select_menu == "SECTION") echo 'active' ?>">
              <a href="#" class="menu-toggle nav-link has-dropdown">
              <i class="menu-icon fa fa-th"></i><span><?= Yii::t('app', 'menu_section') ?></span></a>
              <ul class="dropdown-menu">
              <?php if ($test[1] == 1 or $test[2] == 1 or $test[3] == 1) { ?>
              <li><a class="nav-link" href="<?= Yii::$app->request->baseUrl . "/all_section"; ?>"><?= Yii::t('app', 'all_section_menu') ?></a></li>
              <?php } ?>
              </ul>
              </li>

              <?php }
              if ($manage_departement != "0_0_0_0") {
              $test = explode('_', $manage_departement);
              ?>
              <li class="dropdown <?php if ($this->context->select_menu == "DEPARTEMENT") echo 'active' ?>">
              <a href="#" class="menu-toggle nav-link has-dropdown">
              <i class="menu-icon fa fa-th"></i><span><?= Yii::t('app', 'menu_departement') ?></span></a>
              <ul class="dropdown-menu">
              <?php if ($test[1] == 1 or $test[2] == 1 or $test[3] == 1) { ?>
              <li><a class="nav-link" href="<?= Yii::$app->request->baseUrl . "/all_departement"; ?>"><?= Yii::t('app', 'all_departement_menu') ?></a></li>
              <?php } ?>
              </ul>
              </li>

              <?php } */
            if ($manage_cours != "0_0_0_0") {
                $test = explode('_', $manage_cours);
                ?>
                <li class="dropdown <?php if ($this->context->select_menu == "COURS") echo 'active' ?>">
                    <a href="#" class="menu-toggle nav-link has-dropdown">
                        <i class="menu-icon fa fa-th"></i><span><?= Yii::t('app', 'menu_cours') ?></span></a>
                    <ul class="dropdown-menu">
                        <?php if ($test[0] == 1 or $test[1] == 1 or $test[2] == 1 or $test[3] == 1) { ?>
                            <li><a class="nav-link" href="<?= Yii::$app->request->baseUrl . "/all_cours"; ?>"><?= Yii::t('app', 'all_cours_menu') ?></a></li>
                        <?php } ?>
                    </ul>
                </li>

                <?php
            }

            if ($manage_mission != "0_0_0_0") {
                $test = explode('_', $manage_mission);
                ?>
                <li class="dropdown <?php if ($this->context->select_menu == "MISSION") echo 'active' ?>">
                    <a href="#" class="menu-toggle nav-link has-dropdown">
                        <i class="menu-icon fa fa-th"></i><span><?= Yii::t('app', 'menu_mission') ?></span></a>
                    <ul class="dropdown-menu">
                        <?php if ($test[0] == 1 or $test[1] == 1 or $test[2] == 1 or $test[3] == 1) { ?>
                            <li><a class="nav-link" href="<?= Yii::$app->request->baseUrl . "/all_mission"; ?>"><?= Yii::t('app', 'all_mission_menu') ?></a></li>
                            <?php } ?>
                    </ul>
                </li>

            <?php } ?>

        </ul>

    </aside>
</div>

<!--end::Aside-->