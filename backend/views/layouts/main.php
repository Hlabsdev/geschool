<?php

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use backend\widgets\Alert;
use backend\controllers\Utils;
/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">

<head>
	<meta charset="<?= Yii::$app->charset ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?= Html::csrfMetaTags() ?>
	<title><?= Yii::t('app', 'smprojet_title') ?></title>
	<link rel="apple-touch-icon" href="<?php echo Yii::$app->request->baseUrl; ?>/images/logo.svg">
	<link rel="shortcut icon" type="image/x-icon" href="<?php echo Yii::$app->request->baseUrl; ?>/images/logo.svg">
	<?php $this->head() ?>
</head>

<body class="light dark-sidebar theme-white">
	<?php $this->beginBody() ?>
	<!--<div class="loader"></div>-->
	<div id="app">
		<div class="main-wrapper main-wrapper-1">
			<div class="navbar-bg"></div>
			<?= $this->render('_header'); ?>

			<?= $this->render('_gauche'); ?>
			<!-- Main Content -->

			<div class="main-content">
				<?php
				echo $this->context->breadcrumb
				?>
				<?= Alert::widget() ?>
				<?= $content ?>
			</div>

			<?= $this->render('_footer'); ?>
		</div>
	</div>


	<?php $this->endBody() ?>


	<script type="text/javascript">
		function selectCenter(center) {
			if (center != '') {
				var xhr = null;
				if (window.XMLHttpRequest) // Firefox et autres
					xhr = new XMLHttpRequest();
				else if (window.ActiveXObject) { // Internet Explorer
					try {
						xhr = new ActiveXObject("Msxml2.XMLHTTP");
					} catch (e) {
						xhr = new ActiveXObject("Microsoft.XMLHTTP");
					}
				}
				xhr.onreadystatechange = function() {
					if (xhr.readyState == 4 && xhr.status == 200) {
						location.reload();
					}
				}
				xhr.open("GET", "<?php echo Yii::$app->request->baseUrl ?>/welcome/mycenter?center=" + center, true);
				xhr.send(null);
			}
		}

		$('#example1').DataTable({
			"paging": true,
			"pageLength": 10,
			"info": false,
			"lengthChange": false,
			"searching": true,
			"ordering": false,
			"language": {
				"zeroRecords": "<div class='alert alert-danger alert-dismissible show fade' style='margin-bottom: 30px'><div class='alert-body'><?= Yii::t('app', 'liste_empty') ?></div></div>",
				"lengthMenu": "  _MENU_ records",
				"search": "<?= Yii::t('app', 'search') ?>",
				"paginate": {
					"previous": "<?= Yii::t('app', 'previous') ?>",
					"next": "<?= Yii::t('app', 'next') ?>",
					"last": "<?= Yii::t('app', 'last') ?>",
					"first": "<?= Yii::t('app', 'first') ?>"
				}
			},
		});
	</script>

</body>

</html>
<?php $this->endPage() ?>