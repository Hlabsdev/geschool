<?php

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">

<head>
    <meta charset="<?= Yii::$app->charset ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Yii::t('app', 'smprojet_title') ?></title>
    <link rel="apple-touch-icon" href="<?php echo Yii::$app->request->baseUrl; ?>/images/logo.svg">
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo Yii::$app->request->baseUrl; ?>/images/logo.svg">
    <?php $this->head() ?>
</head>

<body>
    <?php $this->beginBody() ?>
    <!--<div class="loader"></div>-->
    <div id="app">


        <?= $content ?>

    </div>


    <?php $this->endBody() ?>
</body>

</html>
<?php $this->endPage() ?>