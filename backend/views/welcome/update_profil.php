<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use backend\controllers\Utils;

$required_sign = Utils::required();

?>


<script type="application/javascript">
	var loadFile = function(event) {
		var output = document.getElementById('output');
		output.src = URL.createObjectURL(event.target.files[0]);
		output.onload = function() {
			URL.revokeObjectURL(output.src) // free memory
		}
	};
</script>


<style>
	#output {
		margin: 10px;
	}
</style>

<div class="card">

	<?php $form = ActiveForm::begin([
		'id' => 'form-signup',
		'fieldConfig' => [
			'options' => [
				'tag' => false,
			]
		]
	]); ?>

	<div class="card-body card-block">


		<div class="row">
			<div class="col-sm-8">

				<div class="row form-group">
					<div class="col col-md-3"><label for="text-input" class=" form-control-label"><?= Yii::t('app', 'nom') ?> <?= $required_sign ?></label></div>
					<div class="input-group col-sm-5">
						<div class="input-group-prepend">
							<div class="input-group-text">
								<i class="fas fa-user"></i>
							</div>
						</div>
						<?= $form->field($model, 'nom')->textInput(['autofocus' => true, 'required' => true])->error(false)->label(false); ?>
					</div>

				</div>
				<div class="row form-group">
					<div class="col col-md-3"><label for="text-input" class=" form-control-label"><?= Yii::t('app', 'prenoms') ?> <?= $required_sign ?></label></div>
					<div class="input-group col-sm-5">
						<div class="input-group-prepend">
							<div class="input-group-text">
								<i class="fas fa-user"></i>
							</div>
						</div>
						<?= $form->field($model, 'prenoms')->textInput(['required' => true])->error(false)->label(false); ?>
					</div>
				</div>
				<div class="row form-group">
					<div class="col col-md-3"><label for="text-input" class=" form-control-label"><?= Yii::t('app', 'user_name') ?> <?= $required_sign ?></label></div>
					<div class="input-group col-sm-5">
						<div class="input-group-prepend">
							<div class="input-group-text">
								<i class="fas fa-user"></i>
							</div>
						</div>
						<?= $form->field($model, 'username')->textInput(['required' => true])->error(false)->label(false); ?>
					</div>
				</div>
				<div class="row form-group">
					<div class="col col-md-3"><label for="text-input" class=" form-control-label"><?= Yii::t('app', 'email') ?> <?= $required_sign ?></label></div>
					<div class="input-group col-sm-5">
						<div class="input-group-prepend">
							<div class="input-group-text">
								<i class="fas fa-envelope"></i>
							</div>
						</div>
						<?= $form->field($model, 'email')->textInput(['type' => 'email', 'required' => true])->error(false)->Label(false); ?>
					</div>
				</div>
				<div class="row form-group">
					<div class="col col-md-3"><label for="text-input" class=" form-control-label"><?= Yii::t('app', 'telephoneuser') ?> <?= $required_sign ?></label></div>
					<div class="input-group col-sm-5">
						<div class="input-group-prepend">
							<div class="input-group-text">
								<i class="fas fa-phone"></i>
							</div>
						</div>
						<?= $form->field($model, 'telephoneuser')->textInput(['type' => 'text', 'required' => true])->error(false)->Label(false); ?>
					</div>
				</div>

			</div>

			<?php
			$profile_src = '';
			$this_user = Yii::$app->user->identity;
			if ($this_user->photo != null && $this_user->photo != "")
				$profile_src = Yii::$app->homeUrl . 'uploads/' . $this_user->photo;
			?>

			<div class="col-sm-4">
				<div class="img_container">
					<h3><?= Yii::t('app', 'pick_picture') ?></h3>
					<?= $form->field($model, 'photo')->fileInput(['id' => 'imageUpload', 'accept' => "image/*", 'onchange' => "loadFile(event)"])->error(false)->Label(false); ?>
					<img id="output" width="250" height="250" src="<?= $profile_src ?>" />
				</div>
			</div>
		</div>



	</div>

	<div class="card-footer ">
		<?= Utils::resetbtn(); ?>
		<?= Html::submitButton('<i class="fa fa-dot-circle-o"></i> ' . Yii::t('app', 'updatebutton'), ['class' => 'btn btn-success btn-sm', 'name' => 'updatebutton']) ?>
	</div>

	<?php ActiveForm::end(); ?>
</div>