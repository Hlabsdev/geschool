<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\modules\members\models\CountryAirport;
use kartik\select2\Select2;
?>

	
	<div class="col-lg-12">	
		<?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>
		
		<div class="form-group col-lg-6" >
			<input type="text" value="<?=$model->date_send?>" class="form-control pull-right active" name="date_send" id="date_send" readonly placeholder="<?=Yii::t('app', 'collecte_date')?>">	
		</div>
		<div class="form-group col-lg-2" >
			<?= Html::submitButton(Yii::t('app', 'filterbutton'), ['class' => 'btn btn-sm btn-success', 'name' => 'updatebutton']) ?>
		</div>
		<?php ActiveForm::end(); ?>
	</div>	