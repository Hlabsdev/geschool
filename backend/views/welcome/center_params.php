<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\modules\users\models\SchoolCenter;
use backend\modules\users\models\AccessRight;
use backend\controllers\Utils;

$required_sign = Utils::required();

$id_center = str_replace('-', '', Yii::$app->user->identity->id_center);
$test_center = explode(",", $id_center);

if ($id_center == 1) {
	$allcenter = SchoolCenter::find()->where(['etat' => 1])->orderBy(['denomination_center' => SORT_ASC])->all();
} else {
	$allcenter = SchoolCenter::find()->where(['etat' => 1, 'id_center' => $test_center])->orderBy(['denomination_center' => SORT_ASC])->all();
}

?>


<div class="card">

	<?php $form = ActiveForm::begin([
		'id' => 'form-signup',
		'fieldConfig' => [
			'options' => [
				'tag' => false,
			]
		]
	]); ?>

	<div class="card-body card-block">

		<div class="row form-group">
			<div class="col col-md-3"><label for="text-input" class=" form-control-label"><?= Yii::t('app', 'denomination_center') ?> <?= $required_sign ?></label></div>
			<div class="input-group col-sm-5">
				<?= $form->field($model, 'default_center')->dropdownList(
					ArrayHelper::map(
						$allcenter,
						'id_center',
						'denomination_center'
					),
					['prompt' => Yii::t('app', 'select_center'), 'required' => true],
					['class' => 'form-control']
				)->error(false)->label(false); ?>
			</div>
		</div>

	</div>
	<div class="card-footer">
		<?= Utils::resetbtn(); ?>
		<?= Html::submitButton('<i class="fa fa-dot-circle-o"></i> ' . Yii::t('app', 'submitbutton'), ['class' => 'btn btn-sm btn-success', 'name' => 'updatebutton']) ?>
	</div>
	<?php ActiveForm::end(); ?>
</div>